package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.NewsItem;
import com.next.aappublicapp.adapters.NewsItemListAdapter;
import com.next.aappublicapp.listeners.OnPageNewsLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.server.services.LocalCachService;
import com.next.aappublicapp.util.UserSessionManager;

public class NewsListActivity extends TrackableListActivity implements OnPageNewsLoadSuccessListener, OnServiceFailListener{

	int currentPageNumber = 1;
	NewsItemListAdapter newsItemListAdapter;
	NewsItem moreNewsItem;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_list);
		AllHomeScreenItem allHomeScreenItem = LocalCachService.getInstance().getAllHomeScreenItem(this);
		List<NewsItem> allnewsItems; 
		if(allHomeScreenItem.getNewsItems() == null){
			allnewsItems = new ArrayList<NewsItem>();
		}else{
			allnewsItems = new ArrayList<NewsItem>(allHomeScreenItem.getNewsItems());
		}
		newsItemListAdapter = new NewsItemListAdapter(this, allnewsItems);
	
		moreNewsItem = new NewsItem();
		moreNewsItem.setId(-1L);
		
		newsItemListAdapter.add(moreNewsItem);
		setListAdapter(newsItemListAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_list, menu);
		return true;
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent openNewsItem = new Intent(this, NewsActivity.class);
		NewsItem oneNewsItem = newsItemListAdapter.getItem(position);
		Log.i("AAP", "ListView="+listView);
		Log.i("AAP", "view="+view);
		if(oneNewsItem.getId() == -1){
			//means clicked on More Item
			startLoading(view);
			reloadNextPage();
		}else{
			openNewsItem.putExtra(NewsActivity.INTENT_NEWS_ITEM, oneNewsItem);
			startActivity(openNewsItem);
		}
		//TrackerUtl.sendAapEvent("CandidateInfo");
	}

	@Override
	public void onServiceFail(String message) {
		
	}

	@Override
	public void onSuccesfullPageNewsLoad(List<NewsItem> acList) {
		currentPageNumber++;
		newsItemListAdapter.remove(moreNewsItem);
		for(NewsItem oneNewsItem : acList){
			newsItemListAdapter.add(oneNewsItem);
		}
		//newsItemListAdapter.addAll(acList);
		newsItemListAdapter.add(moreNewsItem);
		
	}
	
	private void reloadNextPage(){
		UserSessionManager userSessionManager = UserSessionManager.getInstance();
		long livingAcId = userSessionManager.getLivingAssemblyConstituencyId();
		long votingAcId = userSessionManager.getVotingAssemblyConstituencyId();
		long livingPcId = userSessionManager.getLivingParliamentConstituencyId();
		long votingPcId = userSessionManager.getVotingParliamentConstituencyId();
		
		AapServerServices.getInstance().loadNewsOfPageAsync(this, currentPageNumber + 1, livingAcId, votingAcId, livingPcId,votingPcId, this, this);
	}
	
	private void startLoading(View view){
		ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.load_progress_bar);
		if(progressBar != null){
			progressBar.setVisibility(View.VISIBLE);
		}
		TextView moreTextView = (TextView)view.findViewById(R.id.more_title);
		if(moreTextView != null){
			moreTextView.setVisibility(View.GONE);
		}
	}

}
