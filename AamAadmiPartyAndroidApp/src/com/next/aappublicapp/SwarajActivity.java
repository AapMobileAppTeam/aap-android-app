package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.next.aap.dto.SwarajChapter;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.SwarajNavigationDrawerHelper;
import com.next.aappublicapp.util.TwitterUtil;

public class SwarajActivity extends TrackableActionBarActivity {

	public static final String INTENT_SWARAJ_CHAPTER = "INTENT_SWARAJ_CHAPTER";
	public static final String INTENT_SWARAJ_CHAPTER_POSITION = "INTENT_SWARAJ_CHAPTER_POSITION";

	private DrawerLayout menuDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView menuDrawerList;
	private CharSequence mTitle;
	private CharSequence mDrawerTitle;

	TextView swarajChapterTitle;
	WebView swarajChapterContentWebView;
	private LinearLayout oneLinersLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_swaraj);
		setupDrawerMenu(savedInstanceState);
		int selectedPosition = getIntent().getExtras().getInt(INTENT_SWARAJ_CHAPTER_POSITION);
		selectItem(selectedPosition);
		final SwarajChapter swarajChapter = (SwarajChapter) getIntent()
				.getExtras().getSerializable(INTENT_SWARAJ_CHAPTER);
		swarajChapterTitle = (TextView) findViewById(R.id.swaraj_chapter_title);
		swarajChapterContentWebView = (WebView) findViewById(R.id.swaraj_chapter_content);
		oneLinersLinearLayout = (LinearLayout) findViewById(R.id.oneliners);

		swarajChapterTitle.setText(swarajChapter.getTitle());

		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		swarajChapterContentWebView.loadDataWithBaseURL("",
				swarajChapter.getContent(), mimeType, encoding, "");

		List<String> oneLiners = new ArrayList<String>(1);
		String title = swarajChapter.getTitle();
		oneLiners.add("#swaraj " + title);

		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (final String oneliner : oneLiners) {
			RelativeLayout onelinerLayout = (RelativeLayout) inflater.inflate(
					R.layout.item_oneliner, oneLinersLinearLayout, false);
			TextView text = (TextView) onelinerLayout.findViewById(R.id.text);
			text.setText(oneliner);
			Button tweetButton = (Button) onelinerLayout
					.findViewById(R.id.tweet_button);
			Button fbButton = (Button) onelinerLayout
					.findViewById(R.id.fb_button);
			tweetButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TwitterUtil.onTweetShare(SwarajActivity.this,
							swarajChapter.getWebUrl(), "", oneliner,
							swarajChapter.getContent());
				}
			});
			fbButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// https://lh3.googleusercontent.com/-x3seksAy3lE/UpqdgDvQGJI/AAAAAAAANs4/NPOMxdI0Zpo/s320/swaraj2.JPG
					FacebookUtil
							.onFacebookShareClick(
									SwarajActivity.this,
									swarajChapter.getWebUrl(),
									"https://lh5.googleusercontent.com/-AgjTHn4JgQs/UpqdgY_xmhI/AAAAAAAANs8/grg-CMFXgqU/s1024/swaraj.jpg",
									oneliner,
									swarajChapter.getContent());
				}
			});
			oneLinersLinearLayout.addView(onelinerLayout);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.jan_lokpal, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = menuDrawerLayout.isDrawerOpen(menuDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_settings:
			// create intent to perform web search for this planet
			Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
			intent.putExtra(SearchManager.QUERY, getSupportActionBar()
					.getTitle());
			// catch event that there's no activity to handle intent
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivity(intent);
			} else {
				Toast.makeText(this, "Can not do anything", Toast.LENGTH_LONG)
						.show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void setupDrawerMenu(Bundle savedInstanceState) {
		mTitle = getTitle();
		mDrawerTitle = getTitle();

		menuDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		menuDrawerList = (ListView) findViewById(R.id.left_drawer);

		SwarajNavigationDrawerHelper.getSingletonInstance().initializeDrawer(
				this, menuDrawerList, -1);
		// set a custom shadow that overlays the main content when the drawer
		// opens
		menuDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		menuDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}
		};
		menuDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// selectItem(0);
		}
	}
	private void selectItem(int position) {
		SwarajNavigationDrawerHelper.getSingletonInstance().setSelectedOption(position);
		 
	}

}
