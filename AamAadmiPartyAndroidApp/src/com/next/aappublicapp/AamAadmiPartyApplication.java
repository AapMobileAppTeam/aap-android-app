package com.next.aappublicapp;

import android.app.Application;

import com.next.aappublicapp.util.AppUsageUtil;
import com.next.aappublicapp.util.UserSessionManager;
import com.next.aappublicapp.util.VolleyUtil;

/**
 * 
 * @author ravi
 *
 */
public class AamAadmiPartyApplication extends Application {

	private static AamAadmiPartyApplication instance;


	public static AamAadmiPartyApplication get() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		//Initialize Volley
		VolleyUtil.getInstance().initilize(this);
		//Initialize User Session
		UserSessionManager.getInstance().initSession(this);
		//update app usage stats
		AppUsageUtil.appUsed(this);
	}

}
