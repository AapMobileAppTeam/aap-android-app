package com.next.aappublicapp;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.dto.StateDto;
import com.next.aap.messages.TaskStatus;
import com.next.aap.messages.UserRegisteredMessage;
import com.next.aappublicapp.adapters.AssemblyConstituencySpinnerAdapter;
import com.next.aappublicapp.adapters.DistrictSpinnerAdapter;
import com.next.aappublicapp.adapters.ParliamentConstituncySpinnerAdapter;
import com.next.aappublicapp.adapters.StateSpinnerAdapter;
import com.next.aappublicapp.fragment.DatePickerFragment;
import com.next.aappublicapp.listeners.OnAssemblyConstituencyLoadSuccessListener;
import com.next.aappublicapp.listeners.OnDistrictLoadSuccessListener;
import com.next.aappublicapp.listeners.OnParliamentConstituencyLoadSuccessListener;
import com.next.aappublicapp.listeners.OnStateLoadSuccessListener;
import com.next.aappublicapp.listeners.OnSucessUserRegistrationListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.util.GcmUtil;
import com.next.aappublicapp.util.UserSessionManager;
import com.next.aappublicapp.util.VolleyUtil;

public class UserProfileActivity extends FragmentActivity implements
		OnStateLoadSuccessListener, DatePickerDialog.OnDateSetListener,
		OnClickListener, OnSucessUserRegistrationListener {

	public static final String INTENT_EXTRA_FROM_START = "INTENT_EXTRA_FROM_START";

	static final int DATE_DIALOG_ID = 999;
	private EditText nameEditText;
	private EditText emailEditText;
	private EditText phoneEditText;
	private EditText dateOfBirthEditText;
	private Button registerButton;
	private RadioButton maleRadioButton;
	private RadioButton femaleRadioButton;
	private RadioButton otherRadioButton;
	private Spinner livingStatesSpinner;
	private Spinner livingDistrictsSpinner;
	private Spinner livingAssemblyConstituencySpinner;
	private Spinner livingParliamentConstituencySpinner;
	private Spinner voterStatesSpinner;
	private Spinner voterDistrictsSpinner;
	private Spinner voterAssemblyConstituencySpinner;
	private Spinner voterParliamentConstituencySpinner;
	private TextView skipRegistrationTextView;
	private NetworkImageView profilePic;
	private CheckBox sameAsCheckbox;

	private long livingStateId;
	private long livingDistrictId;
	private long livingParliamentConstituencyId;
	private long livingAssemblyConstituencyId;
	private long votingStateId;
	private long votingDistrictId;
	private long votingAssemblyConstituencyId;
	private long votingParliamentConstituencyId;

	private boolean goToHomeOnSaveOrSkip;

	private RegisterUserProfile registerUserProfile;

	private String gender;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_user_profile);

		goToHomeOnSaveOrSkip = getIntent().getBooleanExtra(
				INTENT_EXTRA_FROM_START, true);

		nameEditText = (EditText) findViewById(R.id.nameEdit);
		emailEditText = (EditText) findViewById(R.id.emailEdit);
		maleRadioButton = (RadioButton) findViewById(R.id.gender_male);
		femaleRadioButton = (RadioButton) findViewById(R.id.gender_female);
		otherRadioButton = (RadioButton) findViewById(R.id.gender_other);
		phoneEditText = (EditText) findViewById(R.id.phoneEdit);
		dateOfBirthEditText = (EditText) findViewById(R.id.dob);
		livingStatesSpinner = (Spinner) findViewById(R.id.living_state_spinner);
		livingDistrictsSpinner = (Spinner) findViewById(R.id.living_district_spinner);
		livingAssemblyConstituencySpinner = (Spinner) findViewById(R.id.living_ac_spinner);
		livingParliamentConstituencySpinner = (Spinner) findViewById(R.id.living_parliament_constituncy_spinner);
		voterStatesSpinner = (Spinner) findViewById(R.id.voting_state_spinner);
		voterDistrictsSpinner = (Spinner) findViewById(R.id.voting_district_spinner);
		voterAssemblyConstituencySpinner = (Spinner) findViewById(R.id.voting_ac_spinner);
		voterParliamentConstituencySpinner = (Spinner) findViewById(R.id.voter_parliament_constituncy_spinner);
		registerButton = (Button) findViewById(R.id.register_button);
		skipRegistrationTextView = (TextView) findViewById(R.id.skip_link);
		profilePic = (NetworkImageView) findViewById(R.id.profile_pic);
		sameAsCheckbox = (CheckBox) findViewById(R.id.same_as_check_box);

		livingStatesSpinner
				.setOnItemSelectedListener(new StateOnItemSelectedListener(
						livingDistrictsSpinner,
						livingAssemblyConstituencySpinner,
						livingParliamentConstituencySpinner));
		voterStatesSpinner
				.setOnItemSelectedListener(new StateOnItemSelectedListener(
						voterDistrictsSpinner,
						voterAssemblyConstituencySpinner,
						voterParliamentConstituencySpinner));

		livingDistrictsSpinner
				.setOnItemSelectedListener(new DistrictOnItemSelectedListener(
						livingAssemblyConstituencySpinner));

		voterDistrictsSpinner
				.setOnItemSelectedListener(new DistrictOnItemSelectedListener(
						voterAssemblyConstituencySpinner));

		dateOfBirthEditText.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					showDateDialog(v);
				}
				return false;
			}
		});

		skipRegistrationTextView.setOnClickListener(this);
		registerButton.setOnClickListener(this);
		hideKeyBoardForAllSpinners();

		loadData();

		AapServerServices.getInstance().loadStatesAsync(this, this, null);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_profile, menu);
		return true;
	}

	@Override
	public void onSuccesfullStateLoad(List<StateDto> stateList) {
		StateDto selecteState = new StateDto();
		selecteState.setId(-1L);
		selecteState.setName("--Select State---");
		stateList.add(0, selecteState);
		livingStatesSpinner.setAdapter(new StateSpinnerAdapter(
				UserProfileActivity.this, android.R.layout.simple_spinner_item,
				stateList));
		voterStatesSpinner.setAdapter(new StateSpinnerAdapter(
				UserProfileActivity.this, android.R.layout.simple_spinner_item,
				stateList));

		selectState(livingStateId, livingStatesSpinner);
		selectState(votingStateId, voterStatesSpinner);
	}

	private void loadData() {
		UserSessionManager userSessionManager = UserSessionManager
				.getInstance();
		Log.i("AAP", "Loading Data from Local");
		if (userSessionManager.isUserRegistered(this)) {
			Log.i("AAP", "User Registered Before so will load that");
			// If user already registered, retrieve last time save information
			nameEditText
					.setText(userSessionManager.getRegisteredUserFullName());
			emailEditText.setText(userSessionManager.getRegisteredUserEmail());
			phoneEditText.setText(userSessionManager.getRegisteredUserMobile());
			// dateOfBirthEditText.setText(userSessionManager.getRegisteredUserDateOfiorth());
			gender = userSessionManager.getRegisteredUserGender();
			Log.i("AAP", "gender=" + gender);
			if (getString(R.string.user_registration_gender_male_message)
					.equals(gender)) {
				maleRadioButton.setChecked(true);
			} else {
				maleRadioButton.setChecked(false);
			}
			if (getString(R.string.user_registration_gender_female_message)
					.equals(gender)) {
				femaleRadioButton.setChecked(true);
			} else {
				femaleRadioButton.setChecked(false);
			}
			if (getString(R.string.user_registration_gender_other_message)
					.equals(gender)) {
				otherRadioButton.setChecked(true);
			} else {
				otherRadioButton.setChecked(false);
			}
			dateOfBirthEditText.setText(userSessionManager.getDateOfBirth());
			livingStateId = userSessionManager.getLivingStateId();
			Log.i("AAP", "livingStateId=" + livingStateId);
			livingDistrictId = userSessionManager.getLivingDistrictId();
			Log.i("AAP", "livingDistrictId=" + livingDistrictId);
			livingParliamentConstituencyId = userSessionManager
					.getLivingParliamentConstituencyId();
			Log.i("AAP", "livingParliamentConstituencyId="
					+ livingParliamentConstituencyId);
			livingAssemblyConstituencyId = userSessionManager
					.getLivingAssemblyConstituencyId();
			livingDistrictId = userSessionManager.getLivingDistrictId();
			Log.i("AAP", "livingAssemblyConstituencyId="
					+ livingAssemblyConstituencyId);
			votingStateId = userSessionManager.getVotingStateId();
			Log.i("AAP", "votingStateId=" + votingStateId);
			votingDistrictId = userSessionManager.getVotingDistrictId();
			Log.i("AAP", "votingDistrictId=" + votingDistrictId);
			votingAssemblyConstituencyId = userSessionManager
					.getVotingAssemblyConstituencyId();
			Log.i("AAP", "votingAssemblyConstituencyId="
					+ votingAssemblyConstituencyId);
			votingParliamentConstituencyId = userSessionManager
					.getVotingParliamentConstituencyId();
			Log.i("AAP", "votingParliamentConstituencyId="
					+ votingParliamentConstituencyId);
			return;
		} else {
			Log.i("AAP",
					"User Registered Wasnt registerd before so will use Facebook/twitter/google data");
			// Get some information from Facebook/Twiter/google etc
			if (userSessionManager.isLoggedOntoFacebook()) {
				Log.i("AAP", "User Loged in with facebook");
				nameEditText.setText(userSessionManager
						.getFacebookUserFullName());
				emailEditText
						.setText(userSessionManager.getFacebookUserEmail());
				String gender = userSessionManager.getFacebookUserGender();
				if (gender != null) {
					if (getString(
							R.string.user_registration_gender_male_message)
							.equals(gender)) {
						maleRadioButton.setSelected(true);
					} else {
						maleRadioButton.setSelected(false);
					}
					if (getString(
							R.string.user_registration_gender_female_message)
							.equals(gender)) {
						femaleRadioButton.setSelected(true);
					} else {
						femaleRadioButton.setSelected(false);
					}
					if (getString(
							R.string.user_registration_gender_other_message)
							.equals(gender)) {
						otherRadioButton.setSelected(true);
					} else {
						otherRadioButton.setSelected(false);
					}
				}
				profilePic.setImageUrl(
						userSessionManager.getFacebookProfilePicLarge(),
						VolleyUtil.getInstance().getImageLoader());
				return;
			}
		}
	}

	public class StateOnItemSelectedListener implements OnItemSelectedListener {
		Spinner districtSpinner;
		Spinner assemblyContituencySpinner;
		Spinner parliamentConstituencySpinner;

		public StateOnItemSelectedListener(Spinner districtSpinner,
				Spinner assemblyContituencySpinner,
				Spinner parliamentConstituencySpinner) {
			this.districtSpinner = districtSpinner;
			this.assemblyContituencySpinner = assemblyContituencySpinner;
			this.parliamentConstituencySpinner = parliamentConstituencySpinner;
		}

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			Log.i("AAP", "State Selected " + pos);
			if (pos == 0) {
				districtSpinner.setEnabled(false);
				assemblyContituencySpinner.setEnabled(false);
				parliamentConstituencySpinner.setEnabled(false);
			} else {

				StateDto selectedState = (StateDto) parent
						.getItemAtPosition(pos);
				Log.i("AAP", "State Selected " + selectedState);
				if (selectedState.getDistrictDataAvailable() != null
						&& selectedState.getDistrictDataAvailable()) {
					if (parent.equals(livingStatesSpinner)) {
						loadDistrictOfStateAsync(districtSpinner,
								selectedState, livingDistrictId);
						loadParliamentConstituncyOfStateAsync(
								parliamentConstituencySpinner, selectedState,
								livingParliamentConstituencyId);
					} else {
						loadDistrictOfStateAsync(districtSpinner,
								selectedState, votingDistrictId);
						loadParliamentConstituncyOfStateAsync(
								parliamentConstituencySpinner, selectedState,
								votingParliamentConstituencyId);
					}

				} else {
					districtSpinner.setEnabled(false);
					assemblyContituencySpinner.setEnabled(false);
					parliamentConstituencySpinner.setEnabled(false);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			districtSpinner.setEnabled(false);
			assemblyContituencySpinner.setEnabled(false);
			parliamentConstituencySpinner.setEnabled(false);
		}

	}

	private void loadDistrictOfStateAsync(Spinner districtSpinner,
			StateDto stateDto, long selectedDistrictId) {
		ProgressDialog progressDialog = ProgressDialog.show(this,
				"Loading Districts", "Please wait while we get districts of "
						+ stateDto.getName());
		OnDistrictLoadSuccessListener onDistrictLoadSuccessListener = new DistrictLoadSuccessListenerImpl(
				districtSpinner, progressDialog, selectedDistrictId);
		AapServerServices.getInstance().loadDistrictAsync(this, stateDto.getId(),
				onDistrictLoadSuccessListener, null);
	}

	private void loadParliamentConstituncyOfStateAsync(Spinner pcSpinner,
			StateDto stateDto, long selectedPcId) {
		ProgressDialog progressDialog = ProgressDialog.show(this,
				"Loading Districts", "Please wait while we get districts of "
						+ stateDto.getName());
		OnParliamentConstituencyLoadSuccessListener onParliamentConstituencyLoadSuccessListener = new ParliamentConstituencyLoadSuccessListenerImpl(
				pcSpinner, progressDialog, selectedPcId);
		AapServerServices.getInstance().loadParliamentConstituencyOfStateAsync(this, 
				stateDto.getId(), onParliamentConstituencyLoadSuccessListener,
				null);
	}

	public class DistrictLoadSuccessListenerImpl implements
			OnDistrictLoadSuccessListener {
		Spinner districtSpinner;
		ProgressDialog progressDialog;
		long selectedDistrictId;

		public DistrictLoadSuccessListenerImpl(Spinner districtSpinner,
				ProgressDialog progressDialog, long selectedDistrictId) {
			this.districtSpinner = districtSpinner;
			this.progressDialog = progressDialog;
			this.selectedDistrictId = selectedDistrictId;
		}

		@Override
		public void onSuccesfullDistrictLoad(List<DistrictDto> districtList) {
			try {
				DistrictDto selecteDistrict = new DistrictDto();
				selecteDistrict.setId(-1L);
				selecteDistrict.setName("--Select District---");
				districtList.add(0, selecteDistrict);
				districtSpinner.setEnabled(true);
				districtSpinner.requestFocusFromTouch();
				districtSpinner.requestFocus();
				districtSpinner.setAdapter(new DistrictSpinnerAdapter(
						UserProfileActivity.this,
						android.R.layout.simple_spinner_item, districtList));

				selectDistrict(selectedDistrictId, districtSpinner);
			} finally {
				progressDialog.cancel();
			}

		}
	}

	public class DistrictOnItemSelectedListener implements
			OnItemSelectedListener {
		Spinner assemblyContituencySpinner;

		public DistrictOnItemSelectedListener(Spinner assemblyContituencySpinner) {
			this.assemblyContituencySpinner = assemblyContituencySpinner;
		}

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			Log.i("AAP", "District Selected " + pos);
			if (pos == 0) {
				assemblyContituencySpinner.setEnabled(false);
			} else {

				DistrictDto selectedDistrict = (DistrictDto) parent
						.getItemAtPosition(pos);
				Log.i("AAP", "District Selected " + selectedDistrict);
				if (selectedDistrict.getAcDataAvailable() != null
						&& selectedDistrict.getAcDataAvailable()) {
					if (parent.equals(livingDistrictsSpinner)) {
						loadAssemblyConstituencyOfDistrict(
								assemblyContituencySpinner, selectedDistrict,
								livingAssemblyConstituencyId);
					} else {
						loadAssemblyConstituencyOfDistrict(
								assemblyContituencySpinner, selectedDistrict,
								votingAssemblyConstituencyId);
					}

				} else {
					assemblyContituencySpinner.setEnabled(false);
				}

			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			assemblyContituencySpinner.setEnabled(false);
		}

	}

	public class ParliamentConstituencyLoadSuccessListenerImpl implements
			OnParliamentConstituencyLoadSuccessListener {
		Spinner pcSpinner;
		ProgressDialog progressDialog;
		long selectedPcId;

		public ParliamentConstituencyLoadSuccessListenerImpl(Spinner pcSpinner,
				ProgressDialog progressDialog, long selectedPcId) {
			this.pcSpinner = pcSpinner;
			this.progressDialog = progressDialog;
			this.selectedPcId = selectedPcId;
		}

		@Override
		public void onSuccesfullParliamentConstituncyLoad(
				List<ParliamentConstituencyDto> pcList) {
			try {
				ParliamentConstituencyDto selectePc = new ParliamentConstituencyDto();
				selectePc.setId(-1L);
				selectePc.setName("--Select Parliament Constituency---");
				pcList.add(0, selectePc);
				pcSpinner.setEnabled(true);
				pcSpinner.requestFocusFromTouch();
				pcSpinner.requestFocus();
				pcSpinner.setAdapter(new ParliamentConstituncySpinnerAdapter(
						UserProfileActivity.this,
						android.R.layout.simple_spinner_item, pcList));

				selectParliamentConstituency(selectedPcId, pcSpinner);
			} finally {
				progressDialog.cancel();
			}

		}

	}

	private void loadAssemblyConstituencyOfDistrict(
			Spinner assemblyConstituencySpinner, DistrictDto districtDto,
			long selectedAcId) {
		ProgressDialog progressDialog = ProgressDialog.show(this,
				"Loading Assembly Constituencies",
				"Please wait while we get assembly Constituencies of District "
						+ districtDto.getName());
		OnAssemblyConstituencyLoadSuccessListener onDistrictLoadSuccessListener = new AssemblyConstituencyLoadSuccessListenerImpl(
				assemblyConstituencySpinner, progressDialog, selectedAcId);
		AapServerServices.getInstance()
				.loadAssemblyConstituencyOfDistrictAsync(this, districtDto.getId(),
						onDistrictLoadSuccessListener, null);
	}

	public class AssemblyConstituencyLoadSuccessListenerImpl implements
			OnAssemblyConstituencyLoadSuccessListener {
		Spinner assemblyConstituencySpinner;
		ProgressDialog progressDialog;
		long selectedAcId;

		public AssemblyConstituencyLoadSuccessListenerImpl(
				Spinner assemblyConstituencySpinner,
				ProgressDialog progressDialog, long selectedAcId) {
			this.assemblyConstituencySpinner = assemblyConstituencySpinner;
			this.progressDialog = progressDialog;
			this.selectedAcId = selectedAcId;
		}

		@Override
		public void onSuccesfullAssemblyConstituencyLoad(
				List<AssemblyConstituencyDto> assemblyConstituencyList) {
			try {
				AssemblyConstituencyDto selecteAssemblyConstituency = new AssemblyConstituencyDto();
				selecteAssemblyConstituency.setId(-1L);
				selecteAssemblyConstituency
						.setName("--Select Assembly Constituency---");
				assemblyConstituencyList.add(0, selecteAssemblyConstituency);
				assemblyConstituencySpinner.setEnabled(true);
				assemblyConstituencySpinner.requestFocusFromTouch();
				assemblyConstituencySpinner
						.setAdapter(new AssemblyConstituencySpinnerAdapter(
								UserProfileActivity.this,
								android.R.layout.simple_spinner_item,
								assemblyConstituencyList));
				selectAssemblyContituency(selectedAcId,
						assemblyConstituencySpinner);
			} finally {
				progressDialog.cancel();
			}
		}
	}

	public void showDateDialog(View view) {
		Log.i("AAP", "Clicked on DOB");

		DatePickerFragment newFragment = new DatePickerFragment();
		newFragment.setDateString(dateOfBirthEditText.getText().toString());
		newFragment.setOnDateSetListener(this);
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Log.i("AAP", "monthOfYear=" + monthOfYear);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, year);
		cal.add(Calendar.MONTH, monthOfYear + 1);
		cal.add(Calendar.DATE, dayOfMonth);
		dateOfBirthEditText.setText(dayOfMonth
				+ "-"
				+ cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,
						Locale.getDefault()) + "-" + year);
	}

	private void hideKeyBoardForAllSpinners() {
		hideKeyBoardForSpinner(livingStatesSpinner);
		hideKeyBoardForSpinner(livingDistrictsSpinner);
		hideKeyBoardForSpinner(livingAssemblyConstituencySpinner);
		hideKeyBoardForSpinner(livingParliamentConstituencySpinner);
		hideKeyBoardForSpinner(voterStatesSpinner);
		hideKeyBoardForSpinner(voterDistrictsSpinner);
		hideKeyBoardForSpinner(voterAssemblyConstituencySpinner);
		hideKeyBoardForSpinner(voterAssemblyConstituencySpinner);
	}

	private void hideKeyBoardForSpinner(Spinner mSpinner) {
		mSpinner.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager imm = (InputMethodManager) getApplicationContext()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(nameEditText.getWindowToken(), 0);
				return false;
			}
		});
	}

	ProgressDialog progressDialog;

	private void showErrorMessage(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// FIRE ZE MISSILES!
					}
				});
		// Create the AlertDialog object and return it
		builder.create().show();
	}

	@Override
	public void onClick(View v) {
		if (v.equals(registerButton)) {
			registerUserProfile = readDataFromUi();
			if (registerUserProfile.getLivingStateId() == null
					|| registerUserProfile.getLivingStateId() <= 0) {
				showErrorMessage("Please select State where you live");
				return;
			}
			if (registerUserProfile.getLivingDistrictId() == null
					|| registerUserProfile.getLivingDistrictId() <= 0) {
				showErrorMessage("Please select District where you live");
				return;
			}
			if (registerUserProfile.getLivingPcId() == null
					|| registerUserProfile.getLivingPcId() <= 0) {
				showErrorMessage("Please select Parliament Constituency where you live");
				return;
			}
			/*
			 * if (registerUserProfile.getLivingAcId() == null ||
			 * registerUserProfile.getLivingAcId() <= 0) {
			 * showErrorMessage("Please select Assembly Constituency where you live"
			 * ); return; }
			 */
			if (registerUserProfile.getVotingStateId() == null
					|| registerUserProfile.getVotingStateId() <= 0) {
				showErrorMessage("Please select State where you registered as voter, if not registered then same as Where you living");
				return;
			}
			if (registerUserProfile.getVotingDistrictId() == null
					|| registerUserProfile.getVotingDistrictId() <= 0) {
				showErrorMessage("Please select District where you registered as voter, if not registered then same as Where you living");
				return;
			}
			if (registerUserProfile.getVotingPcId() == null
					|| registerUserProfile.getVotingPcId() <= 0) {
				showErrorMessage("Please select Parliament Constituency where you registered as voter, if not registered then same as Where you living");
				return;
			}
			/*
			 * if (registerUserProfile.getVotingAcId() == null ||
			 * registerUserProfile.getLivingAcId() <= 0) { showErrorMessage(
			 * "Please select Assembly Constituency where you registered as voter, if not registered then same as Where you living"
			 * ); return; }
			 */
			if (registerUserProfile.getEmail() == null
					|| registerUserProfile.getEmail().trim().equals("")) {
				showErrorMessage("Please enter email id");
				return;
			}
			if (registerUserProfile.getGender() == null
					|| registerUserProfile.getGender().trim().equals("")) {
				showErrorMessage("Please select gender");
				return;
			}
			progressDialog = ProgressDialog
					.show(this,
							"Saving Profile",
							"Saving user profile, it may take few seconds depending on your internet conenction");
			AapServerServices.getInstance().registerUserAsync(this, 
					registerUserProfile, this, null);
		}
		if (v.equals(skipRegistrationTextView)) {
			CharSequence text = "Skipping Registration";
			int duration = Toast.LENGTH_SHORT;

			Toast toast = Toast.makeText(this, text, duration);
			toast.show();
			goToNextActivity();
		}
	}

	private void goToNextActivity() {
		finish();
		if (goToHomeOnSaveOrSkip) {
			Intent userProfileIntent = new Intent(this,
					HomeScreenActivity.class);
			startActivity(userProfileIntent);
		}
	}

	private RegisterUserProfile readDataFromUi() {
		RegisterUserProfile registerUserProfile = new RegisterUserProfile();
		try {
			registerUserProfile.setBirthday(dateOfBirthEditText.getText()
					.toString());
			registerUserProfile.setDeviceRegId(GcmUtil
					.getGcmDeviceRegistrationId(this));
			registerUserProfile.setDeviceType(GcmUtil.getDeviceType());
			registerUserProfile.setEmail(emailEditText.getText().toString());
			registerUserProfile.setGender(gender);

			registerUserProfile
					.setLivingStateId(((StateDto) livingStatesSpinner
							.getSelectedItem()).getId());
			if (livingStatesSpinner.getSelectedItemPosition() > 0) {
				registerUserProfile
						.setLivingDistrictId(((DistrictDto) livingDistrictsSpinner
								.getSelectedItem()).getId());
				if (livingDistrictsSpinner.getSelectedItemPosition() > 0) {
					registerUserProfile
							.setLivingAcId(((AssemblyConstituencyDto) livingAssemblyConstituencySpinner
									.getSelectedItem()).getId());
				}
				registerUserProfile
						.setLivingPcId(((ParliamentConstituencyDto) livingParliamentConstituencySpinner
								.getSelectedItem()).getId());
			}

			if (sameAsCheckbox != null && sameAsCheckbox.isChecked()) {
				registerUserProfile.setVotingStateId(registerUserProfile
						.getLivingStateId());
				registerUserProfile.setVotingDistrictId(registerUserProfile
						.getLivingDistrictId());
				registerUserProfile.setVotingAcId(registerUserProfile
						.getLivingAcId());
				registerUserProfile.setVotingPcId(registerUserProfile
						.getLivingPcId());
			} else {
				registerUserProfile
						.setVotingStateId(((StateDto) voterStatesSpinner
								.getSelectedItem()).getId());
				if (voterStatesSpinner.getSelectedItemPosition() > 0) {
					registerUserProfile
							.setVotingDistrictId(((DistrictDto) voterDistrictsSpinner
									.getSelectedItem()).getId());
					if (voterDistrictsSpinner.getSelectedItemPosition() > 0) {
						registerUserProfile
								.setVotingAcId(((AssemblyConstituencyDto) voterAssemblyConstituencySpinner
										.getSelectedItem()).getId());
					}
					registerUserProfile
							.setVotingPcId(((ParliamentConstituencyDto) voterParliamentConstituencySpinner
									.getSelectedItem()).getId());
				}
			}

			registerUserProfile.setMobile(phoneEditText.getText().toString());
			registerUserProfile.setName(nameEditText.getText().toString());
			registerUserProfile.setUserId(UserSessionManager.getInstance()
					.getUserId());
		} catch (Exception ex) {
			Log.e("AAP", "Unable to read data from UI", ex);
		}

		return registerUserProfile;
	}

	@Override
	public void onSuccessfullUserRegistration(
			UserRegisteredMessage userRegisteredMessage) {
		if (userRegisteredMessage.getStatus().equals(
				TaskStatus.STATUS_COMPLETED)) {
			UserSessionManager.getInstance().setUserRegistered(this,
					userRegisteredMessage.getUserId(), registerUserProfile);
		}
		if (progressDialog != null) {
			progressDialog.cancel();
		}
		goToNextActivity();

	}

	private void selectState(long stateId, Spinner paramStateSpinner) {
		if (stateId > 0) {
			StateSpinnerAdapter stateSpinnerAdapter = (StateSpinnerAdapter) paramStateSpinner
					.getAdapter();
			if (stateSpinnerAdapter != null) {
				StateDto stateDto;
				for (int i = 0; i < stateSpinnerAdapter.getCount(); i++) {
					stateDto = stateSpinnerAdapter.getItem(i);
					if (stateDto.getId().equals(stateId)) {
						paramStateSpinner.setSelection(i);
						break;
					}
				}
			}
		}
	}

	private void selectDistrict(long districtId, Spinner paramDistrictSpinner) {
		if (districtId > 0) {
			DistrictSpinnerAdapter districtSpinnerAdapter = (DistrictSpinnerAdapter) paramDistrictSpinner
					.getAdapter();
			if (districtSpinnerAdapter != null) {
				DistrictDto districtDto;
				for (int i = 0; i < districtSpinnerAdapter.getCount(); i++) {
					districtDto = districtSpinnerAdapter.getItem(i);
					if (districtDto.getId().equals(districtId)) {
						paramDistrictSpinner.setSelection(i);
						break;
					}
				}
			}
		}
	}

	private void selectParliamentConstituency(long pcId, Spinner paramPcSpinner) {
		if (pcId > 0) {
			ParliamentConstituncySpinnerAdapter pcSpinnerAdapter = (ParliamentConstituncySpinnerAdapter) paramPcSpinner
					.getAdapter();
			if (pcSpinnerAdapter != null) {
				ParliamentConstituencyDto parliamentConstituencyDto;
				for (int i = 0; i < pcSpinnerAdapter.getCount(); i++) {
					parliamentConstituencyDto = pcSpinnerAdapter.getItem(i);
					if (parliamentConstituencyDto.getId().equals(pcId)) {
						paramPcSpinner.setSelection(i);
						break;
					}
				}
			}
		}
	}

	private void selectAssemblyContituency(long assemblyConstituencyId,
			Spinner paramAcSpinner) {
		if (assemblyConstituencyId > 0) {
			AssemblyConstituencySpinnerAdapter districtSpinnerAdapter = (AssemblyConstituencySpinnerAdapter) paramAcSpinner
					.getAdapter();
			if (districtSpinnerAdapter != null) {
				AssemblyConstituencyDto assemblyConstituencyDto;
				for (int i = 0; i < districtSpinnerAdapter.getCount(); i++) {
					assemblyConstituencyDto = districtSpinnerAdapter.getItem(i);
					if (assemblyConstituencyDto.getId().equals(
							assemblyConstituencyId)) {
						paramAcSpinner.setSelection(i);
						break;
					}
				}
			}
		}
	}
	
	public void onSameAsCheckboxButtonClicked(View view){
		boolean enabled = !sameAsCheckbox.isChecked();
		voterStatesSpinner.setEnabled(enabled);
		voterDistrictsSpinner.setEnabled(enabled);
		voterAssemblyConstituencySpinner.setEnabled(enabled);
		voterParliamentConstituencySpinner.setEnabled(enabled);
	}

	public void onGenderRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();
		Log.i("AAp", "checked=" + checked);
		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.gender_male:
			if (checked) {
				gender = getString(R.string.user_registration_gender_male_message);
			}
			break;
		case R.id.gender_female:
			if (checked) {
				gender = getString(R.string.user_registration_gender_female_message);
			}
			break;
		case R.id.gender_other:
			if (checked) {
				gender = getString(R.string.user_registration_gender_other_message);
			}
			break;
		}
		Log.i("AAp", "gender=" + gender);
	}
}
