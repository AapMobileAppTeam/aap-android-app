package com.next.aappublicapp.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

	private String dateString;
	private OnDateSetListener onDateSetListener;
	
	
	public DatePickerFragment(){
		super();
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		if (dateString != null && !dateString.trim().equals("")) {
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			try {
				c.setTime(dateFormat.parse(dateString));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		onDateSetListener.onDateSet(view, year, month, day);
	}
	public String getDateString() {
		return dateString;
	}
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
	public OnDateSetListener getOnDateSetListener() {
		return onDateSetListener;
	}
	public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
		this.onDateSetListener = onDateSetListener;
	}
}
