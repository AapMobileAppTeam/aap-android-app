package com.next.aappublicapp.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

public class GoogleAccountDialog extends DialogFragment {
	private static final String	TAG	= "GoogleAccountDialog";

	public interface GoogleAccountDialogListener {
		void onAccountSelect(Account account);
	}

	public GoogleAccountDialog() {
		// empty required
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// get the list of accounts
		AccountManager am = AccountManager.get(getActivity());

		final Account[] accounts = am.getAccountsByType("com.google");
		String[] names = new String[accounts.length];
		for (int i = 0; i < accounts.length; i++) {
			Log.d(TAG, "found account:" + accounts[i].name);
			names[i] = accounts[i].name;
		}

		// show the accounts
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose account to sign in with");
		builder.setItems(names, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				GoogleAccountDialogListener activity = (GoogleAccountDialogListener) getActivity();
				activity.onAccountSelect(accounts[which]);
				dismiss();
			}
		});

		return builder.create();
	}
}
