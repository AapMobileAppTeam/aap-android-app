package com.next.aappublicapp;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.RequestToken;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.next.aap.dto.RegisterTwitterUserProfile;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.util.GcmUtil;
import com.next.aappublicapp.util.LocalPersistence;
import com.next.aappublicapp.util.UserSessionManager;

public class TwitterLoginActivity extends TrackableActivity {
	static final String TWITTER_REQUEST_TOKEN_FILE = "requestToken.txt";
	Uri tokenUri = null;

	ProgressDialog pd = null;
	LoginTask loginTask = null;

	// twitter
	private Twitter mTwitter;
	private RequestToken twitterRequestToken;
	
	public static final String TWITTER_CONSUMER_KEY = "QwAtfFGQ4XyH2qSFX0UOg";
	public static final String TWITTER_CONSUMER_SECRET = "5fmM9fVoDTIgHqKb8OeZ9cZullLdbL0uSrcC3mrTyM";
	public static final String	TWITTER_CALLBACK_URL = "aap://twitter";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#9e000000")));

		mTwitter = new TwitterFactory().getInstance();
		mTwitter.setOAuthConsumer(TWITTER_CONSUMER_KEY,TWITTER_CONSUMER_SECRET);
	}

	@Override
	public void onStart() {
		super.onStart();
		tokenUri = getIntent().getData();
		if (tokenUri != null
				&& tokenUri.toString().startsWith(TWITTER_CALLBACK_URL)) {
			//mean we are back to our activity after login
			String oauthVerifier = tokenUri.getQueryParameter("oauth_verifier");
			twitterRequestToken = (RequestToken) LocalPersistence
					.readObjectFromFile(this, TWITTER_REQUEST_TOKEN_FILE);
			new TwitterConnectTask().execute(new String[] { oauthVerifier });
		} else {
			//we started twitter login
			new TwitterRequestTokenTask().execute(new String[] {});
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (pd != null)
			pd.cancel();
	}

	class TwitterRequestTokenTask extends AsyncTask<String, Void, Integer> {
		RequestToken requestToken;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = ProgressDialog.show(TwitterLoginActivity.this,
					"Starting Twitter login",
					"Please wait while we login to Twitter");
		}

		@Override
		protected Integer doInBackground(String... params) {
			try {
				requestToken = mTwitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);
				Log.d("AAP", "requestToken" + requestToken.getToken());
				return 0;
			} catch (TwitterException e) {
				Log.e("AAP", "Twitter connect failed!", e);
				return 1;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (pd != null)
				pd.cancel();
			if (result == 0) {
				twitterRequestToken = requestToken;
				LocalPersistence.witeObjectToFile(TwitterLoginActivity.this,
						twitterRequestToken, TWITTER_REQUEST_TOKEN_FILE);
				Intent i = new Intent(Intent.ACTION_VIEW,
						Uri.parse(twitterRequestToken.getAuthenticationURL()));
				i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
						| Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(i);
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!",
						Toast.LENGTH_LONG).show();
				finish();
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (pd != null)
				pd.cancel();
		}
	}

	class TwitterConnectTask extends AsyncTask<String, Void, Integer> {
		User twitterUser = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null)
				pd.cancel();
			pd = ProgressDialog.show(TwitterLoginActivity.this,
					"Fetching Twitter profile...",
					"Please wait while we fetch your Twitter profile");
		}

		@Override
		protected Integer doInBackground(String... params) {
			String oauthVerifier = params[0];
			twitter4j.auth.AccessToken at;
			try {
				at = mTwitter.getOAuthAccessToken(twitterRequestToken,
						oauthVerifier);
				mTwitter.setOAuthAccessToken(at);
				twitterUser = mTwitter.showUser(mTwitter.getId());
				return 0;
			} catch (TwitterException e) {
				Log.e("AAP", "twitter connect failed", e);
				return 1;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.cancel();
			if (result == 0) {
				try {
					if (loginTask != null)
						loginTask.cancel(true);
					loginTask = new LoginTask(twitterUser);
					loginTask.execute(new String[] {});
				} catch (Exception e) {
					Log.e("AAP", "error connecting twitter", e);
					Toast.makeText(TwitterLoginActivity.this,
							"Twitter connect failed", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!",
						Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}

	class LoginTask extends AsyncTask<String, Void, Integer> {
		private User twitterUser;
		public LoginTask(User twitterUser) {
			this.twitterUser = twitterUser;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null)
				pd.cancel();
			pd = ProgressDialog.show(TwitterLoginActivity.this,
					"Updating connection...",
					"Please wait while we update your connections");

		}

		@Override
		protected Integer doInBackground(String... params) {
			// ApiHandler apiHandler =
			// ApiHandler.getInstance(getApplicationContext());
			//
			// TelephonyManager tm = (TelephonyManager)
			// getSystemService(TELEPHONY_SERVICE);
			// ApiResponse response =
			// apiHandler.doLogin(sessionProvider.getUserId(), tokenTypes,
			// tokens,
			// tm.getDeviceId());
			// if (response.responseCode == ApiResponse.RESPONSE_OK) {
			// try {
			// userId = response.responseData.getString("user-id");
			// name = response.responseData.getString("name");
			// profilePicUrl = response.responseData.getString("image");
			// } catch (JSONException e) {
			// Log.e(TAG, "error fetching userid", e);
			// return 1;
			// }
			// }
			// return response.responseCode;
			UserSessionManager userSessionManager = UserSessionManager.getInstance();
			try {
				RegisterTwitterUserProfile registerTwitterUserProfile = new RegisterTwitterUserProfile();
				registerTwitterUserProfile.setAccessToken(mTwitter.getOAuthAccessToken().getToken());
				registerTwitterUserProfile.setAccessTokenSecret(mTwitter.getOAuthAccessToken().getTokenSecret());
				registerTwitterUserProfile.setTwitterUserId(twitterUser.getId()+"");
				registerTwitterUserProfile.setHandle(twitterUser.getScreenName());
				registerTwitterUserProfile.setUserId(userSessionManager.getUserId());
				registerTwitterUserProfile.setDeviceRegId(GcmUtil.getOrCreateDeviceRegistrationId(TwitterLoginActivity.this));
				registerTwitterUserProfile.setDeviceType(GcmUtil.getDeviceType());
				AapServerServices.getInstance().registerTwitterUserAsync(TwitterLoginActivity.this, registerTwitterUserProfile, userSessionManager, null);
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			userSessionManager.saveTwitterProfile(mTwitter, twitterUser);
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.cancel();
			if (result == 0) {
				/*
				// sessionProvider.setUserId(userId);
				sessionProvider.setUserName(name);
				// sessionProvider.setProfilePicUrl(profilePicUrl);

				sessionProvider.putParam(
						UserSessionProvider.PREFS_KEY_TWITTER_ID, twitId);
				sessionProvider.putParam(
						UserSessionProvider.PREFS_KEY_TWITTER_NAME, twitName);
				sessionProvider.putParam(
						UserSessionProvider.PREFS_KEY_TWITTER_TOKEN, twitToken);
				sessionProvider.putParam(
						UserSessionProvider.PREFS_KEY_TWITTER_SECRET,
						twitSecret);
						*/
				Toast.makeText(TwitterLoginActivity.this,
						"Connection successful!", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!",
						Toast.LENGTH_LONG).show();
			}
			finish();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (pd != null)
				pd.cancel();
			finish();
		}
	}
}