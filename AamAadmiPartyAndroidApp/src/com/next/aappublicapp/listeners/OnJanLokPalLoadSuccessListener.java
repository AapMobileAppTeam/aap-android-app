package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.JanLokPalChapter;

public interface OnJanLokPalLoadSuccessListener {

	void onSuccesfullJanLokPalLoad(List<JanLokPalChapter> janLokPalChapters);
}
