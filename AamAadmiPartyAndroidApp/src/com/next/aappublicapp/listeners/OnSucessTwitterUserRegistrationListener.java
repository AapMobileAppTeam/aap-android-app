package com.next.aappublicapp.listeners;

import com.next.aap.messages.TwitterUserRegisteredMessage;

public interface OnSucessTwitterUserRegistrationListener {

	void onSuccessfullTwitterRegistration(TwitterUserRegisteredMessage twitterUserRegisteredMessage);
}
