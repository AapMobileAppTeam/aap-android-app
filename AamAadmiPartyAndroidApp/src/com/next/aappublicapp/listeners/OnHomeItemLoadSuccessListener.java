package com.next.aappublicapp.listeners;

import com.next.aap.dto.AllHomeScreenItem;

public interface OnHomeItemLoadSuccessListener {

	void onSuccesfullHomeItemLoad(AllHomeScreenItem allHomeScreenItem);
}
