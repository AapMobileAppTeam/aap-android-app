package com.next.aappublicapp.listeners;

import com.next.aap.messages.GoogleUserRegisteredMessage;

public interface OnSucessGoogleUserRegistrationListener {

	void onSuccessfullGoogleRegistration(GoogleUserRegisteredMessage googleUserRegisteredMessage);
}
