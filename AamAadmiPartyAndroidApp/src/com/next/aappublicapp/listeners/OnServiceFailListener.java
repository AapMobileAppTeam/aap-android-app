package com.next.aappublicapp.listeners;

public interface OnServiceFailListener {

	void onServiceFail(String message);
}
