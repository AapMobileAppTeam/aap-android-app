package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.VideoItem;

public interface OnPageVideoLoadSuccessListener {

	void onSuccesfullPageVideoLoad(List<VideoItem> videoList);
}
