package com.next.aappublicapp.listeners;

import com.next.aap.dto.RegisterUserDeviceResponse;

public interface OnSuccesfullDeviceRegistrationListener {

	void onSuccesfullDeviceRegistration(RegisterUserDeviceResponse registerUserDeviceResponse);
}
