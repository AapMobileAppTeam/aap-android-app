package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.NewsItem;

public interface OnPageNewsLoadSuccessListener {

	void onSuccesfullPageNewsLoad(List<NewsItem> acList);
}
