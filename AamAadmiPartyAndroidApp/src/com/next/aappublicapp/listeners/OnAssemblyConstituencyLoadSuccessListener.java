package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.AssemblyConstituencyDto;

public interface OnAssemblyConstituencyLoadSuccessListener {

	void onSuccesfullAssemblyConstituencyLoad(List<AssemblyConstituencyDto> acList);
}
