package com.next.aappublicapp.listeners;

import com.next.aap.messages.FacebookUserRegisteredMessage;

public interface OnSucessFacebookUserRegistrationListener {

	void onSuccessfullFacebookRegistration(FacebookUserRegisteredMessage facebookUserRegisteredMessage);
}
