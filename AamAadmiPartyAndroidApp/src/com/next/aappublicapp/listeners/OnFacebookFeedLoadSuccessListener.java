package com.next.aappublicapp.listeners;

import com.next.aap.dto.FacebookPostList;

public interface OnFacebookFeedLoadSuccessListener {

	void onSuccesfullPageFacebookFeedLoad(FacebookPostList facebookPostList);
}
