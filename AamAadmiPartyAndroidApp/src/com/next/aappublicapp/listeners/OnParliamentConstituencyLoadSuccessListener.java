package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.ParliamentConstituencyDto;

public interface OnParliamentConstituencyLoadSuccessListener {

	void onSuccesfullParliamentConstituncyLoad(List<ParliamentConstituencyDto> pcList);
}
