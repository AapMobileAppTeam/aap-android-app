package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.DistrictDto;

public interface OnDistrictLoadSuccessListener {

	void onSuccesfullDistrictLoad(List<DistrictDto> stateList);
}
