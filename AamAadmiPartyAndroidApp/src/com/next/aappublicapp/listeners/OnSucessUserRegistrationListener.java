package com.next.aappublicapp.listeners;

import com.next.aap.messages.UserRegisteredMessage;

public interface OnSucessUserRegistrationListener {

	void onSuccessfullUserRegistration(UserRegisteredMessage userRegisteredMessage);
}
