package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.StateDto;

public interface OnStateLoadSuccessListener {

	void onSuccesfullStateLoad(List<StateDto> stateList);
}
