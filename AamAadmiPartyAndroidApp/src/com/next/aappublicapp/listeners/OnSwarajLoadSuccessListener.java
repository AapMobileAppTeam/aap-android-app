package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.SwarajChapter;

public interface OnSwarajLoadSuccessListener {

	void onSuccesfullSwarajLoad(List<SwarajChapter> janLokPalChapters);
}
