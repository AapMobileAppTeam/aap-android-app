package com.next.aappublicapp.listeners;

import java.util.List;

import com.next.aap.dto.BlogItem;

public interface OnPageBlogLoadSuccessListener {

	void onSuccesfullPageBlogLoad(List<BlogItem> blogList);
}
