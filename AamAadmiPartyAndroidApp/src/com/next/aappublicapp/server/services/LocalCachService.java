package com.next.aappublicapp.server.services;

import java.lang.reflect.Type;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.EventItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;
import com.next.aappublicapp.util.PreferenceNameUtil;

public class LocalCachService {

	private static LocalCachService instance = new LocalCachService();
	public static LocalCachService getInstance() {
		return instance;
	}
	
	public AllHomeScreenItem getAllHomeScreenItem(Context context){
		Gson gson = new Gson();
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.APP_CONTENT_PREF, Context.MODE_PRIVATE);
        //SharedPreferences.Editor editor = prefs.edit();
		
		Type newsListType = new TypeToken<List<NewsItem>>() {}.getType();
		List<NewsItem> newsItems = (List<NewsItem>)getContent(prefs, PreferenceNameUtil.HOME_NEWS_ITEMS, gson, newsListType);
		String cachedData = prefs.getString(PreferenceNameUtil.HOME_NEWS_ITEMS, null);
		Log.i("AAP","cachedDate = "+ cachedData);
		if(cachedData != null){
			try{
				
				newsItems = gson.fromJson(cachedData, newsListType);
				Log.i("AAP","newsItems = "+ newsItems);
				for(NewsItem oneNewsItem:newsItems){
					Log.i("AAP","oneNewsItem = "+ oneNewsItem);
				}
				
			}catch(Exception ex){
				newsItems = null;
			}
		}
		
		Type VideoListType = new TypeToken<List<VideoItem>>() {}.getType();
		List<VideoItem> videoItems = (List<VideoItem>)getContent(prefs, PreferenceNameUtil.HOME_VIDEO_ITEMS, gson, VideoListType);
		Type blogListType = new TypeToken<List<BlogItem>>() {}.getType();
		List<BlogItem> blogItems = (List<BlogItem>)getContent(prefs, PreferenceNameUtil.HOME_BLOG_ITEMS, gson, blogListType);
		Type eventListType = new TypeToken<List<EventItem>>() {}.getType();
		List<EventItem> eventItems = (List<EventItem>)getContent(prefs, PreferenceNameUtil.HOME_EVENT_ITEMS, gson, eventListType);
		
		AllHomeScreenItem allHomeScreenItem = new AllHomeScreenItem();
		allHomeScreenItem.setNewsItems(newsItems);
		allHomeScreenItem.setBlogItems(blogItems);
		allHomeScreenItem.setEventItems(eventItems);
		allHomeScreenItem.setVideoItems(videoItems);
		
		return allHomeScreenItem;

	}
	public void saveAllHomeScreenItem(Context context, AllHomeScreenItem allHomeScreenItem){
		Gson gson = new Gson();
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.APP_CONTENT_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        saveContent(editor, PreferenceNameUtil.HOME_BLOG_ITEMS, gson, allHomeScreenItem.getBlogItems());
        saveContent(editor, PreferenceNameUtil.HOME_EVENT_ITEMS, gson, allHomeScreenItem.getEventItems());
        saveContent(editor, PreferenceNameUtil.HOME_NEWS_ITEMS, gson, allHomeScreenItem.getNewsItems());
        saveContent(editor, PreferenceNameUtil.HOME_VIDEO_ITEMS, gson, allHomeScreenItem.getVideoItems());
        editor.commit();
	}
	private void saveContent(SharedPreferences.Editor editor,String preferanceName,Gson gson, Object data){
		String dataString = gson.toJson(data);
		editor.putString(preferanceName, dataString);
	}
	private Object getContent(SharedPreferences prefs,String preferanceName,Gson gson, Type listType){
		String cachedData = prefs.getString(preferanceName, null);
		if(cachedData == null){
			return null;
		}
		try{
			return gson.fromJson(cachedData, listType);
		}catch(Exception ex){
			
		}
		return null;
	}

}
