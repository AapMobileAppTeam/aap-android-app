package com.next.aappublicapp.server.services;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.FacebookPost;
import com.next.aap.dto.FacebookPostList;
import com.next.aap.dto.JanLokPalChapter;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.NewsItemList;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterGoogleUserProfile;
import com.next.aap.dto.RegisterTwitterUserProfile;
import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.dto.StateDto;
import com.next.aap.dto.SwarajChapter;
import com.next.aap.dto.VideoItem;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.GoogleUserRegisteredMessage;
import com.next.aap.messages.TwitterUserRegisteredMessage;
import com.next.aap.messages.UserRegisteredMessage;
import com.next.aappublicapp.listeners.OnAssemblyConstituencyLoadSuccessListener;
import com.next.aappublicapp.listeners.OnDistrictLoadSuccessListener;
import com.next.aappublicapp.listeners.OnFacebookFeedLoadSuccessListener;
import com.next.aappublicapp.listeners.OnFacebookPostCommentSuccessListener;
import com.next.aappublicapp.listeners.OnFacebookPostLikeSuccessListener;
import com.next.aappublicapp.listeners.OnHomeItemLoadSuccessListener;
import com.next.aappublicapp.listeners.OnJanLokPalLoadSuccessListener;
import com.next.aappublicapp.listeners.OnPageBlogLoadSuccessListener;
import com.next.aappublicapp.listeners.OnPageNewsLoadSuccessListener;
import com.next.aappublicapp.listeners.OnPageVideoLoadSuccessListener;
import com.next.aappublicapp.listeners.OnParliamentConstituencyLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.listeners.OnStateLoadSuccessListener;
import com.next.aappublicapp.listeners.OnSuccesfullDeviceRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessFacebookUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessGoogleUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessTwitterUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSwarajLoadSuccessListener;
import com.next.aappublicapp.util.TrackerUtil;
import com.next.aappublicapp.util.VolleyUtil;

public class AapServerServices {

	//private static final String baseUrl = "http://www.donate4india.org/mob/api/";
	private static final String baseReadUrl = "http://mobr2.aamaadmiparty.org/mob/api/";
	private static final String baseWriteUrl = "http://mobu2.aamaadmiparty.org/mob/api/";
	
	//private static final String baseUrl = "http://aappublicserver001snapshot.uhurucloud.com/api/";
	//private static final String baseUrl = "http://ec2-54-214-240-26.us-west-2.compute.amazonaws.com:8080/aap/api/";
	
	private static final String getAllItemsUrl = "get/all/";
	//private static final String addNewsUrl = "news/add";
	private static final String registerDeviceUrl = "device/register/android";
	private static final String registerFacebookUserUrl = "user/register/facebook";
	private static final String registerTwitterUserUrl = "user/register/twitter";
	private static final String registerGoogleUserUrl = "user/register/google";
	private static final String registerUserUrl = "user/register/user";
	private static final String facebookFeedUrl = "facebook/feed";
	private static final String englishCandidateSelectionProcessUrl = "csp/get/eng";
	private static final String hindiCandidateSelectionProcessUrl = "csp/get/hin";
	private static final String donationSummaryUrl = "donation/get";
	private static final String getAllStatesUrl = "state/getall";
	private static final String getDistrictOfStateUrl = "districtofstate/get";
	private static final String getAssemblyConstituencyOfDistrictUrl = "acofdistrict/get";
	private static final String getAssemblyConstituencyOfStateUrl = "acofstate/get";
	private static final String getParliamentConstituencyOfStateUrl = "pcofstate/get";
	private static final String getPageNewsUrl = "news/get/";
	private static final String getPageVideoUrl = "video/get/";
	private static final String getPageBlogUrl = "blog/get/";
	
	private static final String language = "en";
	
	private static AapServerServices instance = new AapServerServices();
	private AapServerServices(){
		
	}
	public static AapServerServices getInstance(){
		return instance;
	}
	private JSONObject convertToJsonObject(Object object) throws JSONException{
		Gson gson = new Gson();
		String data = gson.toJson(object);
		return new JSONObject(data);
	}
	private void onError(OnServiceFailListener onServiceFailListener, String message){
		if(onServiceFailListener != null){
			onServiceFailListener.onServiceFail(message);
		}
	}

	public void registerDevice(final Context context, String deviceId, final OnSuccesfullDeviceRegistrationListener successListener, 
			final OnServiceFailListener onServiceFailListener) {
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseWriteUrl + registerDeviceUrl;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			RegisterUserDevice registerUserDevice = new RegisterUserDevice();
			registerUserDevice.setDeviceType("Android");
			registerUserDevice.setRegId(deviceId);
			JSONObject request = convertToJsonObject(registerUserDevice);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(Request.Method.POST, url, request, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "registerDevice", endTime - startTime);
					//Type type = new TypeToken<ACModel[]>() {
					//}.getType();
					RegisterUserDeviceResponse registerUserDeviceResponse = new Gson().fromJson(response.toString(), RegisterUserDeviceResponse.class);
					Log.i("AAP", "Device Registered Succesfully "+ registerUserDeviceResponse.getStatus());
					if(successListener != null){
						successListener.onSuccesfullDeviceRegistration(registerUserDeviceResponse);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
		
	}
	
	public void loadStatesAsync(final Context context, final OnStateLoadSuccessListener onStateLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getAllStatesUrl;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadStatesAsync", endTime - startTime);
					Type type = new TypeToken<List<StateDto>>() {
					}.getType();
					List<StateDto> stateList = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "stateList "+ stateList);
					if(onStateLoadSuccessListener != null){
						onStateLoadSuccessListener.onSuccesfullStateLoad(stateList);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadDistrictAsync(final Context context, long stateId,final OnDistrictLoadSuccessListener onDistrictLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getDistrictOfStateUrl+"/"+stateId;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadDistrictAsync", endTime - startTime);
					Type type = new TypeToken<List<DistrictDto>>() {
					}.getType();
					List<DistrictDto> districtList = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "districtList "+ districtList);
					if(onDistrictLoadSuccessListener != null){
						onDistrictLoadSuccessListener.onSuccesfullDistrictLoad(districtList);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadAssemblyConstituencyOfDistrictAsync(final Context context, long districtId,final OnAssemblyConstituencyLoadSuccessListener onAssemblyConstituencyLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getAssemblyConstituencyOfDistrictUrl+"/"+districtId;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadAssemblyConstituencyOfDistrictAsync", endTime - startTime);
					Type type = new TypeToken<List<AssemblyConstituencyDto>>() {
					}.getType();
					List<AssemblyConstituencyDto> acList = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "districtList "+ acList);
					if(onAssemblyConstituencyLoadSuccessListener != null){
						onAssemblyConstituencyLoadSuccessListener.onSuccesfullAssemblyConstituencyLoad(acList);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadAssemblyConstituencyOfStateAsync(final Context context, long stateId,final OnAssemblyConstituencyLoadSuccessListener onAssemblyConstituencyLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getAssemblyConstituencyOfStateUrl+"/"+stateId;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadAssemblyConstituencyOfStateAsync", endTime - startTime);
					Type type = new TypeToken<List<AssemblyConstituencyDto>>() {
					}.getType();
					List<AssemblyConstituencyDto> acList = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "districtList "+ acList);
					if(onAssemblyConstituencyLoadSuccessListener != null){
						onAssemblyConstituencyLoadSuccessListener.onSuccesfullAssemblyConstituencyLoad(acList);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadParliamentConstituencyOfStateAsync(final Context context, long stateId,final OnParliamentConstituencyLoadSuccessListener onParliamentConstituencyLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getParliamentConstituencyOfStateUrl+"/"+stateId;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadParliamentConstituencyOfStateAsync", endTime - startTime);
					Type type = new TypeToken<List<ParliamentConstituencyDto>>() {
					}.getType();
					List<ParliamentConstituencyDto> pcList = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "districtList "+ pcList);
					if(onParliamentConstituencyLoadSuccessListener != null){
						onParliamentConstituencyLoadSuccessListener.onSuccesfullParliamentConstituncyLoad(pcList);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void registerFacebookUserAsync(final Context context, RegisterFacebookUserProfile registerFacebookUserProfile, final OnSucessFacebookUserRegistrationListener onSucessFacebookUserRegistrationListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseWriteUrl + registerFacebookUserUrl;
			JSONObject request = convertToJsonObject(registerFacebookUserProfile);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(Request.Method.POST,url, request, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "registerFacebookUserAsync", endTime - startTime);
					FacebookUserRegisteredMessage facebookUserRegisteredMessage = new  Gson().fromJson(response.toString(), FacebookUserRegisteredMessage.class);
					Log.i("AAP", "facebookUserRegisteredMessage "+ facebookUserRegisteredMessage);
					if(onSucessFacebookUserRegistrationListener != null){
						onSucessFacebookUserRegistrationListener.onSuccessfullFacebookRegistration(facebookUserRegisteredMessage);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void registerTwitterUserAsync(final Context context, RegisterTwitterUserProfile registerTwitterUserProfile, final OnSucessTwitterUserRegistrationListener onSucessTwitterUserRegistrationListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseWriteUrl + registerTwitterUserUrl;
			JSONObject request = convertToJsonObject(registerTwitterUserProfile);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(Request.Method.POST,url, request, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "registerTwitterUserAsync", endTime - startTime);
					TwitterUserRegisteredMessage twitterUserRegisteredMessage = new  Gson().fromJson(response.toString(), TwitterUserRegisteredMessage.class);
					Log.i("AAP", "twitterUserRegisteredMessage "+ twitterUserRegisteredMessage);
					if(onSucessTwitterUserRegistrationListener != null){
						onSucessTwitterUserRegistrationListener.onSuccessfullTwitterRegistration(twitterUserRegisteredMessage);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void registerGoogleUserAsync(final Context context, RegisterGoogleUserProfile registerTwitterUserProfile, final OnSucessGoogleUserRegistrationListener onSucessGoogleUserRegistrationListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseWriteUrl + registerGoogleUserUrl;
			JSONObject request = convertToJsonObject(registerTwitterUserProfile);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(Request.Method.POST,url, request, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "registerGoogleUserAsync", endTime - startTime);
					GoogleUserRegisteredMessage googleUserRegisteredMessage = new  Gson().fromJson(response.toString(), GoogleUserRegisteredMessage.class);
					Log.i("AAP", "googleUserRegisteredMessage "+ googleUserRegisteredMessage);
					if(onSucessGoogleUserRegistrationListener != null){
						onSucessGoogleUserRegistrationListener.onSuccessfullGoogleRegistration(googleUserRegisteredMessage);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	
	public void registerUserAsync(final Context context, RegisterUserProfile registerUserProfile, final OnSucessUserRegistrationListener onSucessUserRegistrationListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseWriteUrl + registerUserUrl;
			JSONObject request = convertToJsonObject(registerUserProfile);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(Request.Method.POST,url, request, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "registerUserAsync", endTime - startTime);
					UserRegisteredMessage userRegisteredMessage = new  Gson().fromJson(response.toString(), UserRegisteredMessage.class);
					Log.i("AAP", "userRegisteredMessage "+ userRegisteredMessage);
					if(onSucessUserRegistrationListener != null){
						onSucessUserRegistrationListener.onSuccessfullUserRegistration(userRegisteredMessage);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			Request<JSONObject> requestd = VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest);
			requestd.setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadHomeItemsAsync(final Context context, long livingAcId, long votingAcId, long livingPcId, long votingPcId,
			final OnHomeItemLoadSuccessListener onHomeItemLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getAllItemsUrl +livingAcId+"/"+votingAcId+"/"+livingPcId+"/"+votingPcId+"/"+language;
			Log.i("AAP", "Hitting URL : "+ url);
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadHomeItemsAsync", endTime - startTime);
					Gson gson = new Gson();
					Log.i("AAP", "response : "+ response);
					AllHomeScreenItem allHomeScreenItem = gson.fromJson(response.toString(), AllHomeScreenItem.class);
					try {
						Type newsListType = new TypeToken<List<NewsItem>>() {}.getType();
						Type videoListType = new TypeToken<List<VideoItem>>() {}.getType();
						Type blogListType = new TypeToken<List<BlogItem>>() {}.getType();
						List<NewsItem> newsItems = gson.fromJson(response.getString("newsItems"), newsListType);
						allHomeScreenItem.setNewsItems(newsItems);
						List<VideoItem> videoItems = gson.fromJson(response.getString("videoItems"), videoListType);
						allHomeScreenItem.setVideoItems(videoItems);
						List<BlogItem> blogItems = gson.fromJson(response.getString("blogItems"), blogListType);
						allHomeScreenItem.setBlogItems(blogItems);
					} catch (JsonSyntaxException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					Log.i("AAP", "allHomeScreenItem "+ allHomeScreenItem);
					if(onHomeItemLoadSuccessListener != null){
						onHomeItemLoadSuccessListener.onSuccesfullHomeItemLoad(allHomeScreenItem);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadNewsOfPageAsync(final Context context, int pageNumber,long livingAcId, long votingAcId, long livingPcId, long votingPcId,
			final OnPageNewsLoadSuccessListener onPageNewsLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getPageNewsUrl+"/"+livingAcId+"/"+votingAcId+"/"+livingPcId+"/"+votingPcId+"/"+language+"/"+pageNumber;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadNewsOfPageAsync", endTime - startTime);
					NewsItemList newsItemList = new  Gson().fromJson(response.toString(), NewsItemList.class);
					Log.i("AAP", "newsItemList "+ newsItemList);
					if(onPageNewsLoadSuccessListener != null){
						onPageNewsLoadSuccessListener.onSuccesfullPageNewsLoad(newsItemList.getNewsItems());
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadVideosOfPageAsync(final Context context, int pageNumber,long livingAcId, long votingAcId, long livingPcId, long votingPcId,
			final OnPageVideoLoadSuccessListener onPageVideoLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getPageVideoUrl+"/"+livingAcId+"/"+votingAcId+"/"+livingPcId+"/"+votingPcId+"/"+language+"/"+pageNumber;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadVideosOfPageAsync", endTime - startTime);
					Type type = new TypeToken<List<VideoItem>>() {
					}.getType();
					List<VideoItem> videoItems = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "videoItems "+ videoItems);
					if(onPageVideoLoadSuccessListener != null){
						onPageVideoLoadSuccessListener.onSuccesfullPageVideoLoad(videoItems);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	
	public void loadBlogOfPageAsync(final Context context, int pageNumber,long livingAcId, long votingAcId, long livingPcId, long votingPcId,
			final OnPageBlogLoadSuccessListener onPageBlogLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = baseReadUrl + getPageBlogUrl+"/"+livingAcId+"/"+votingAcId+"/"+livingPcId+"/"+votingPcId+"/"+language+"/"+pageNumber;
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonArrayRequest registerDeviceRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadBlogOfPageAsync", endTime - startTime);
					Type type = new TypeToken<List<BlogItem>>() {
					}.getType();
					List<BlogItem> blogItems = new  Gson().fromJson(response.toString(), type);
					Log.i("AAP", "blogItems "+ blogItems);
					if(onPageBlogLoadSuccessListener != null){
						onPageBlogLoadSuccessListener.onSuccesfullPageBlogLoad(blogItems);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadAapJanLokPalFeedAsync(final Context context, String blogLanguage, final OnJanLokPalLoadSuccessListener onJanLokPalLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = "http://www.blogger.com/feeds/2607599389053497858/posts/default?alt=json&max-results=100";
			if(blogLanguage.equalsIgnoreCase("Hindi")){
				url =  "http://www.blogger.com/feeds/2607599389053497858/posts/default?alt=json&max-results=100";
			}
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadAapJanLokPalFeedAsync", endTime - startTime);
					List<JanLokPalChapter> janLokPalChapters = new ArrayList<JanLokPalChapter>();
					try{
						JSONArray entriesArray = response.getJSONObject("feed").getJSONArray("entry");
						JanLokPalChapter oneJanLokPalChapter;
						JSONObject oneEntry;
						JSONObject oneLinkObject;
						for(int i=entriesArray.length() -1;i>=0;i--){
							oneJanLokPalChapter = new JanLokPalChapter();
							oneEntry = entriesArray.getJSONObject(i);
							oneJanLokPalChapter.setTitle(oneEntry.getJSONObject("title").getString("$t"));
							oneJanLokPalChapter.setContent(oneEntry.getJSONObject("content").getString("$t"));
							Log.i("AAP", "TITLE="+oneJanLokPalChapter.getTitle());
							JSONArray linkArray = oneEntry.getJSONArray("link");
							for(int j=0;j<linkArray.length();j++){
								oneLinkObject = linkArray.getJSONObject(j);
								if(oneLinkObject.getString("rel").equals("alternate")){
									oneJanLokPalChapter.setWebUrl(oneLinkObject.getString("href"));
								}
							}
							janLokPalChapters.add(oneJanLokPalChapter);
						}
					} catch (JsonSyntaxException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					if(onJanLokPalLoadSuccessListener != null){
						onJanLokPalLoadSuccessListener.onSuccesfullJanLokPalLoad(janLokPalChapters);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadSwarajFeedAsync(final Context context, String blogLanguage, final OnSwarajLoadSuccessListener onSwarajLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			String url = "http://www.blogger.com/feeds/8832953156980338490/posts/default?alt=json&max-results=100&m=1";
			if(blogLanguage.equalsIgnoreCase("Hindi")){
				url =  "http://www.blogger.com/feeds/3200132965784891384/posts/default?alt=json&max-results=100&m=1";
			}
			//RegisterDeviceResponse registerDeviceResponse = getSpringData(url, RegisterDeviceResponse.class);
			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadSwarajFeedAsync", endTime - startTime);
					List<SwarajChapter> swarajChapters = new ArrayList<SwarajChapter>();
					try{
						JSONArray entriesArray = response.getJSONObject("feed").getJSONArray("entry");
						SwarajChapter oneSwarajChapter;
						JSONObject oneEntry;
						JSONObject oneLinkObject;
						for(int i=entriesArray.length() -1;i>=0;i--){
							oneSwarajChapter = new SwarajChapter();
							oneEntry = entriesArray.getJSONObject(i);
							oneSwarajChapter.setTitle(oneEntry.getJSONObject("title").getString("$t"));
							oneSwarajChapter.setContent(oneEntry.getJSONObject("content").getString("$t"));
							JSONArray linkArray = oneEntry.getJSONArray("link");
							for(int j=0;j<linkArray.length();j++){
								oneLinkObject = linkArray.getJSONObject(j);
								if(oneLinkObject.getString("rel").equals("alternate")){
									oneSwarajChapter.setWebUrl(oneLinkObject.getString("href"));
								}
							}
							swarajChapters.add(oneSwarajChapter);
						}
					} catch (JsonSyntaxException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					if(onSwarajLoadSuccessListener != null){
						onSwarajLoadSuccessListener.onSuccesfullSwarajLoad(swarajChapters);
					}
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void loadFacebookFeedAsync(final Context context, String url, final OnFacebookFeedLoadSuccessListener onFacebookFeedLoadSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			final long startTime = System.currentTimeMillis();
			if(url == null){
				url = baseReadUrl + facebookFeedUrl;
			}
			Log.i("AAP","FB url = "+url);

			JsonObjectRequest registerDeviceRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "loadFacebookFeedAsync", endTime - startTime);
					try{
						JSONArray entriesArray = response.getJSONArray("data");
						//Log.i("AAP", "entriesArray="+entriesArray);
						Type type = new TypeToken<List<FacebookPost>>() {
						}.getType();
						List<FacebookPost> facebookPosts = new  Gson().fromJson(entriesArray.toString(), type);
						Iterator<FacebookPost> facebookPostIterator = facebookPosts.iterator();
						FacebookPost facebookPost;
						while(facebookPostIterator.hasNext()){
							facebookPost = facebookPostIterator.next();
							if(!"EVERYONE".equals(facebookPost.getPrivacy().getValue())){
								facebookPostIterator.remove();
							}
						}
						FacebookPostList facebookPostList = new FacebookPostList();
						facebookPostList.setFacebookPosts(facebookPosts);
						facebookPostList.setNextLink(response.getJSONObject("paging").getString("next"));
						facebookPostList.setPreviousLink(response.getJSONObject("paging").getString("previous"));
						if(onFacebookFeedLoadSuccessListener != null){
							onFacebookFeedLoadSuccessListener.onSuccesfullPageFacebookFeedLoad(facebookPostList);
						}


					} catch (JsonSyntaxException e) {
						onError(onServiceFailListener, e.getMessage());
						Log.e("AAP", "FB : "+e.getMessage(), e);
					} catch (JSONException e) {
						onError(onServiceFailListener, e.getMessage());
						Log.e("AAP", "FB : "+e.getMessage(), e);
					}
					
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(registerDeviceRequest).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void likeFacebookPostAsync(final Context context, String url, final OnFacebookPostLikeSuccessListener onFacebookPostLikeSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			Log.i("AAP","FB url = "+url);
			final long startTime = System.currentTimeMillis();

			Request<String> request = new StringRequest(url, new Listener<String>() {
				@Override
				public void onResponse(String response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "likeFacebookPostAsync", endTime - startTime);
					try{
						Log.i("AAP", "response="+response);
						
						if(onFacebookPostLikeSuccessListener != null){
							onFacebookPostLikeSuccessListener.onSuccesfullFacebookPostLike();
						}


					} catch (Exception e) {
						onError(onServiceFailListener, e.getMessage());
						Log.e("AAP", "FB : "+e.getMessage(), e);
					} 
					
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(request).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}
	
	public void commentFacebookPostAsync(final Context context, String url, final OnFacebookPostCommentSuccessListener onFacebookPostCommentSuccessListener, final OnServiceFailListener onServiceFailListener){
		try{
			Log.i("AAP","FB url = "+url);
			final long startTime = System.currentTimeMillis();

			Request<String> request = new StringRequest(url, new Listener<String>() {
				@Override
				public void onResponse(String response) {
					long endTime = System.currentTimeMillis();
					TrackerUtil.sendAapTiming(context, "commentFacebookPostAsync", endTime - startTime);
					try{
						Log.i("AAP", "response="+response);
						
						if(onFacebookPostCommentSuccessListener != null){
							onFacebookPostCommentSuccessListener.onSuccesfullFacebookPostComment();
						}
					} catch (Exception e) {
						onError(onServiceFailListener, e.getMessage());
						Log.e("AAP", "FB : "+e.getMessage(), e);
					} 
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w("AAP", "error:" + error);
					onError(onServiceFailListener, error.getMessage());
				}
			});
			VolleyUtil.getInstance().getRequestQueue().add(request).setTag(this);
		}catch(Exception ex){
			onError(onServiceFailListener, ex.getMessage());
		}
	}

}
