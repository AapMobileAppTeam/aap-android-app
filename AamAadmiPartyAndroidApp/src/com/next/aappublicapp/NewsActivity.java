package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.NewsItem;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.TwitterUtil;
import com.next.aappublicapp.util.VolleyUtil;

public class NewsActivity extends TrackableActivity {

	public static final String INTENT_NEWS_ITEM = "INTENT_NEWS_ITEM";
	private NetworkImageView newsImageView;
	private TextView newsTitleTextView;
	private TextView newsAuthorTextView;
	private WebView newsTextView;
	private LinearLayout oneLinersLinearLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		newsImageView = (NetworkImageView) findViewById(R.id.news_image);
		newsTitleTextView = (TextView) findViewById(R.id.news_title);
		newsAuthorTextView = (TextView) findViewById(R.id.news_author);
		newsTextView = (WebView) findViewById(R.id.news_text);
		oneLinersLinearLayout = (LinearLayout) findViewById(R.id.oneliners);
		
		final NewsItem newsItem = (NewsItem)getIntent().getSerializableExtra(INTENT_NEWS_ITEM);
		newsTitleTextView.setText(newsItem.getTitle());
		newsAuthorTextView.setText(newsItem.getAuthor());
		final String mimeType = "text/html";
        final String encoding = "UTF-8";
        newsTextView.loadDataWithBaseURL("", newsItem.getContent(), mimeType, encoding, "");
		//newsTextView.setText(Html.fromHtml(newsItem.getContent()));
		//newsTextView.setMovementMethod(LinkMovementMethod.getInstance());
		if(newsItem.getImageUrl() != null && newsImageView != null){
			newsImageView.setImageUrl(newsItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
		}
		List<String> oneLiners = newsItem.getOneLiners();
		if(oneLiners == null || oneLiners.isEmpty()){
			oneLiners = new ArrayList<String>(1);
			String title = newsItem.getTitle();
			oneLiners.add(title);
			/*
			String webUrl = newsItem.getWebUrl();
			if(title.length() + webUrl.length() + 1 > 140){
				
			}else{
				oneLiners.add(title +" "+ webUrl);
			}
			*/
			
		}
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (final String oneliner : oneLiners) {
			RelativeLayout onelinerLayout = (RelativeLayout) inflater.inflate(R.layout.item_oneliner, oneLinersLinearLayout, false);
			TextView text = (TextView) onelinerLayout.findViewById(R.id.text);
			text.setText(oneliner);
			Button tweetButton = (Button) onelinerLayout.findViewById(R.id.tweet_button);
			Button fbButton = (Button) onelinerLayout.findViewById(R.id.fb_button);
			tweetButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TwitterUtil.onTweetShare(NewsActivity.this,oneliner,newsItem.getWebUrl(), newsItem.getImageUrl(), newsItem.getContent());
				}
			});
			fbButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					FacebookUtil.onFacebookShareClick(NewsActivity.this,newsItem.getOriginalUrl(), newsItem.getImageUrl(), oneliner);
				}
			});
			oneLinersLinearLayout.addView(onelinerLayout);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news, menu);
		return true;
	}
	
	

}
