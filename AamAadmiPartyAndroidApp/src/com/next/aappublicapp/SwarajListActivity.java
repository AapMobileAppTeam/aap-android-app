package com.next.aappublicapp;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;
import com.next.aap.dto.SwarajChapter;
import com.next.aappublicapp.adapters.SwarajListAdapter;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.listeners.OnSwarajLoadSuccessListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.util.LocalPersistence;
import com.next.aappublicapp.util.PreferenceNameUtil;
import com.next.aappublicapp.util.SwarajNavigationDrawerHelper;

public class SwarajListActivity extends TrackableListActivity implements OnSwarajLoadSuccessListener, OnServiceFailListener {

	SwarajListAdapter swarajListAdapter;
	ProgressDialog progressDialog;
	private String language = "English";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jan_lokpal_list);
		progressDialog = ProgressDialog.show(this, "Loading Swaraj eBook", "Please wait while we download Swaraj eBook");
		swarajListAdapter = new SwarajListAdapter(this, new ArrayList<SwarajChapter>());
		setListAdapter(swarajListAdapter);
		loadfromLocalCache();

	}
	private void loadfromLocalCache(){
		new AsyncTask<String, String, List<SwarajChapter>>() {

			@Override
			protected List<SwarajChapter> doInBackground(String... params) {
				Type type = new TypeToken<List<SwarajChapter>>() {
				}.getType();
				List<SwarajChapter> swarajChapters = LocalPersistence.getFromLocalCache(SwarajListActivity.this, PreferenceNameUtil.BOOK_SWARAJ_PREF, language, type);
				if(swarajChapters == null){
					AapServerServices.getInstance().loadSwarajFeedAsync(SwarajListActivity.this, language, SwarajListActivity.this, SwarajListActivity.this);	
				}else{
					return swarajChapters;
				}
				return null;
			}
			
			@Override
			protected void onPostExecute(List<SwarajChapter> swarajChapters) {
				super.onPostExecute(swarajChapters);
				if(swarajChapters != null){
					onSuccesfullSwarajLoad(swarajChapters);
				}
			}
			
		}.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.jan_lokpal_list, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent swarajIntent = new Intent(this, SwarajActivity.class);
		SwarajChapter oneSwarajChapter = swarajListAdapter.getItem(position);
		swarajIntent.putExtra(SwarajActivity.INTENT_SWARAJ_CHAPTER, oneSwarajChapter);
		swarajIntent.putExtra(SwarajActivity.INTENT_SWARAJ_CHAPTER_POSITION, position);
		startActivity(swarajIntent);
		//TrackerUtl.sendAapEvent("CandidateInfo");
	}
	
	@Override
	public void onServiceFail(String message) {
		progressDialog.cancel();
		
	}

	@Override
	public void onSuccesfullSwarajLoad(
			List<SwarajChapter> swarajChapters) {
		for(SwarajChapter oneSwarajChapter : swarajChapters){
			swarajListAdapter.add(oneSwarajChapter);
		}
		//swarajListAdapter.addAll(swarajChapters);
		SwarajNavigationDrawerHelper.getSingletonInstance().setSwarajChapters(swarajChapters);
		LocalPersistence.saveIntoLocalCache(this, PreferenceNameUtil.BOOK_SWARAJ_PREF, language, (Serializable)swarajChapters);
		if(progressDialog != null ){
			progressDialog.cancel();	
		}
		
		
	}
}
