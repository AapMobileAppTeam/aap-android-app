package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import com.facebook.Session;
import com.google.gson.Gson;
import com.next.aap.dto.FacebookPost;
import com.next.aap.dto.FacebookPostList;
import com.next.aappublicapp.adapters.FacebookPostListAdapter;
import com.next.aappublicapp.listeners.OnFacebookFeedLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.util.NotificationUtil;
import com.next.aappublicapp.util.PreferenceNameUtil;
import com.next.aappublicapp.util.TrackerUtil;
import com.next.aappublicapp.util.UserSessionManager;

public class FacebookFeedActivity extends TrackableListActivity implements
		OnScrollListener, OnFacebookFeedLoadSuccessListener,
		OnServiceFailListener {

	FacebookPostList facebookPostList;

	FacebookPostListAdapter facebookPostListAdapter;
	boolean loading = false;
	FacebookPost loadingFacebookPost;
	boolean firstTimeLoading = true;
	
	private String facebookPageId = "290805814352519";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facebook_feed);
		
		NotificationUtil.clearFacebookNotification(this);

		List<FacebookPost> values = new ArrayList<FacebookPost>();
		facebookPostListAdapter = new FacebookPostListAdapter(this, values);
		setListAdapter(facebookPostListAdapter);
		getListView().setOnScrollListener(this);

		loadFacebookPostListInUi(getFacebookResponseFromLocalCache(facebookPageId), false);
		
		String url = null;
		if (UserSessionManager.getInstance().isLoggedOntoFacebook()) {
			TrackerUtil.sendAapEvent(FacebookFeedActivity.this, TrackerUtil.FACEBOOK_FEED_NOT_LOGGED_IN);
			String token = Session.openActiveSessionFromCache(this).getAccessToken();
			//url = "https://graph.facebook.com/"+facebookPageId+"/posts?fields=link,actions,icon,source,type,id,name,status_type,picture,privacy,message,type,status_type,comments.limit(1).summary(true),likes.limit(10).summary(true)&limit=25&date_format=d-M-Y%20h:i%20e&access_token="+token;
			url = "https://graph.facebook.com/"+facebookPageId+"/posts?fields=link,actions,icon,source,type,id,name,status_type,picture,privacy,message,type,status_type,comments.limit(1).summary(true),likes.limit(10).summary(true)&limit=25&date_format=U&access_token="+token;
			loadingFacebookPost = new FacebookPost();
			loadingFacebookPost.setId("-1");//-1 mean Loading view
			loadFacebookPosts(url);
		}else{
			loadingFacebookPost = new FacebookPost();
			loadingFacebookPost.setId("-1");//-1 mean Loading view
			loadFacebookPosts(null);
		}
		

	}
	private void loadFacebookPosts(String url){
		loading = true;
		AapServerServices.getInstance().loadFacebookFeedAsync(this, url, this,this);
		if(firstTimeLoading){
			//facebookPostListAdapter.insert(loadingFacebookPost, 0);
			facebookPostListAdapter.add(loadingFacebookPost);
		}else{
			facebookPostListAdapter.add(loadingFacebookPost);	
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public void onServiceFail(String message) {
		loading = false;
		facebookPostListAdapter.remove(loadingFacebookPost);
	}

	@Override
	public void onSuccesfullPageFacebookFeedLoad(FacebookPostList facebookPostList) {
		if(firstTimeLoading){
			saveFacebookResponseIntoLocalCache(facebookPageId, facebookPostList);
			firstTimeLoading = false;
			if(this.facebookPostList == null){
				//no facebook post list was in local cache
				Log.i("AAP", "no facebook post list was in local cache");
				this.facebookPostList = facebookPostList; 
				loadFacebookPostListInUi(facebookPostList, false);
			}else{
				if(!this.facebookPostList.equals(facebookPostList)){
					Log.i("AAP", "FACEBOOK POSTS HAS CHANGED");
					this.facebookPostList = facebookPostList;
					facebookPostListAdapter.clear();
					loadFacebookPostListInUi(facebookPostList, true);
				}else{
					Log.i("AAP", "No change in facebook posts");
				}
			}
			//Try to merge it in exiting data
		}else{
			loadFacebookPostListInUi(facebookPostList, false);
		}
		//facebookPostListAdapter.addAll(postList);
		loading = false;
		facebookPostListAdapter.remove(loadingFacebookPost);
	}

	private void loadFacebookPostListInUi(FacebookPostList facebookPostList, boolean clear) {
		if(facebookPostList == null){
			return;
		}
		List<FacebookPost> postList = facebookPostList.getFacebookPosts();
		this.facebookPostList = facebookPostList;
		for(FacebookPost oneFacebookPost : postList){
			facebookPostListAdapter.add(oneFacebookPost);
		}
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
	
	        boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
	        
	        if(loadMore && !loading && facebookPostList != null) {
	        	Log.i("AAP", "Loading more Facebook Post items");
	        	loadFacebookPosts(facebookPostList.getNextLink());
	        }
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}
	
	private void saveFacebookResponseIntoLocalCache(String pageId, FacebookPostList facebookPostList){
		try{
			Gson gson = new Gson();
			String response = gson.toJson(facebookPostList);
			SharedPreferences prefs = getSharedPreferences(PreferenceNameUtil.FACEBOO_RESPONSE_PREF, Context.MODE_PRIVATE);
	        SharedPreferences.Editor editor = prefs.edit();
	        editor.putString(pageId, response);
	        editor.commit();
			
		}catch(Exception ex){
			Log.e("AAP", "Unable to save Facebook Response in Local cache", ex);
		}
		//LocalPersistence.witeObjectToFile(context, object, filename);
	}
	
	private FacebookPostList getFacebookResponseFromLocalCache(String pageId){
		try{
			Gson gson = new Gson();
			SharedPreferences prefs = getSharedPreferences(PreferenceNameUtil.FACEBOO_RESPONSE_PREF, Context.MODE_PRIVATE);
	        String response = prefs.getString(pageId, null);
	        if(response == null){
	        	return null;
	        }
	        return gson.fromJson(response, FacebookPostList.class);
			
		}catch(Exception ex){
			Log.e("AAP", "Unable to save Facebook Response in Local cache", ex);
		}
		return null;
	}
}
