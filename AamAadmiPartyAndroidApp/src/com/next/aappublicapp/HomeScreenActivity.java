package com.next.aappublicapp;

import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;
import com.next.aappublicapp.listeners.OnHomeItemLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.server.services.LocalCachService;
import com.next.aappublicapp.util.MainNavigationDrawerHelper;
import com.next.aappublicapp.util.NotificationUtil;
import com.next.aappublicapp.util.UserSessionManager;
import com.next.aappublicapp.util.VolleyUtil;
import com.next.aappublicapp.util.YoutubeUtil;

public class HomeScreenActivity extends TrackableActionBarActivity implements OnHomeItemLoadSuccessListener, OnServiceFailListener {

	private DrawerLayout menuDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView menuDrawerList;
	private CharSequence mTitle;
	private CharSequence mDrawerTitle;
	
	private LinearLayout newsContainer;
	private LinearLayout videosContainer;
	private LinearLayout blogsContainer;
	private ProgressBar progressBar;
	
	private AllHomeScreenItem allHomeScreenItem;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		NotificationUtil.clearAllNotifications(this);
		
		setContentView(R.layout.activity_home_screen);
		setupDrawerMenu(savedInstanceState);
		
		newsContainer = (LinearLayout) findViewById(R.id.news_scroller);
		videosContainer = (LinearLayout) findViewById(R.id.videos_scroller);
		blogsContainer = (LinearLayout) findViewById(R.id.blogs_scroller);
		progressBar = (ProgressBar) findViewById(R.id.update_progress_bar);

		allHomeScreenItem = LocalCachService.getInstance().getAllHomeScreenItem(this);
		loadHomeItemItems(allHomeScreenItem);
		
		progressBar.setVisibility(View.VISIBLE);

		UserSessionManager userSessionManager = UserSessionManager.getInstance();
		long livingAcId = userSessionManager.getLivingAssemblyConstituencyId();
		long votingAcId = userSessionManager.getVotingAssemblyConstituencyId();
		long livingPcId = userSessionManager.getLivingParliamentConstituencyId();
		long votingPcId = userSessionManager.getVotingParliamentConstituencyId();

		AapServerServices.getInstance().loadHomeItemsAsync(this, livingAcId, votingAcId, livingPcId, votingPcId, this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.home_screen, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = menuDrawerLayout.isDrawerOpen(menuDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	private void selectItem(int position) {
		/*
		 * // update the main content by replacing fragments Fragment fragment =
		 * new PlanetFragment(); Bundle args = new Bundle();
		 * args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
		 * fragment.setArguments(args);
		 * 
		 * FragmentManager fragmentManager = getFragmentManager();
		 * fragmentManager.beginTransaction().replace(R.id.content_frame,
		 * fragment).commit();
		 * 
		 * // update selected item and title, then close the drawer
		 * mDrawerList.setItemChecked(position, true);
		 * setTitle(mPlanetTitles[position]);
		 * mDrawerLayout.closeDrawer(mDrawerList);
		 */
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
        case R.id.action_settings:
            // create intent to perform web search for this planet
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, getSupportActionBar().getTitle());
            // catch event that there's no activity to handle intent
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, "Can not do anything", Toast.LENGTH_LONG).show();
            }
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    private void setupDrawerMenu(Bundle savedInstanceState){
    	mTitle = getTitle();
		mDrawerTitle = getTitle();

		menuDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		menuDrawerList = (ListView) findViewById(R.id.left_drawer);

		MainNavigationDrawerHelper.initializeDrawer(this, menuDrawerList, MainNavigationDrawerHelper.OPTION_NONE);
		// set a custom shadow that overlays the main content when the drawer
		// opens
		menuDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		
		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		menuDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				// invalidateOptionsMenu(); // creates call to
				// onPrepareOptionsMenu()
			}
		};
		menuDrawerLayout.setDrawerListener(mDrawerToggle);
		
		if (savedInstanceState == null) {
            selectItem(0);
        }
    }
    
    private void loadHomeItemItems(AllHomeScreenItem allHomeScreenItem){
		newsContainer.removeAllViews();
		videosContainer.removeAllViews();
		blogsContainer.removeAllViews();

		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

		if(allHomeScreenItem.getNewsItems() != null){
			// setup news items
			for (NewsItem newsItem : allHomeScreenItem.getNewsItems()) {
				LinearLayout newsItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, newsContainer, false);
				
				NetworkImageView pic = (NetworkImageView) newsItemContainer.findViewById(R.id.thumb);
				if(newsItem.getImageUrl() != null && !newsItem.getImageUrl().trim().equals("")){
					pic.setDefaultImageResId(R.drawable.ic_launcher);
					pic.setErrorImageResId(R.drawable.ic_launcher);
					pic.setImageUrl(newsItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
				}else{
					pic.setImageResource(R.drawable.ic_launcher);
					pic.setErrorImageResId(R.drawable.ic_launcher);
					//pic.setImageUrl("http://profile.ak.fbcdn.net/hprofile-ak-prn2/1117339_691358626_1825458852_q.jpg", VolleyUtil.getInstance().getImageLoader());
					
					//pic.setImageResource(R.drawable.ic_launcher);	
				}
				TextView text = (TextView) newsItemContainer.findViewById(R.id.news_text);
				text.setText(newsItem.getTitle());
				newsContainer.addView(newsItemContainer);
				final NewsItem oneNewsItem = newsItem;
				newsItemContainer.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent openNewsItem = new Intent(HomeScreenActivity.this, NewsActivity.class);
						openNewsItem.putExtra(NewsActivity.INTENT_NEWS_ITEM, oneNewsItem);
						startActivity(openNewsItem);
					}
				});
			}
			
			LinearLayout moreNewsItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, newsContainer, false);
			moreNewsItemContainer.setBackgroundColor(getResources().getColor(R.color.White));
			
			NetworkImageView moreImage = (NetworkImageView) moreNewsItemContainer.findViewById(R.id.thumb);
			//moreImage.setImageResource(R.drawable.more);
			//moreImage.setImageDrawable(getResources().getDrawable(R.drawable.more));
			moreImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.more));

			TextView text = (TextView) moreNewsItemContainer.findViewById(R.id.news_text);
			text.setText("Click to read more .....");
			//text.setTextColor(getResources().getColor(R.color.Black));
			text.setTextColor(Color.parseColor("#000000"));
			newsContainer.addView(moreNewsItemContainer);
			moreNewsItemContainer.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent openVideoItem = new Intent(HomeScreenActivity.this, NewsListActivity.class);
					startActivity(openVideoItem);
				}
			});
		}
		if(allHomeScreenItem.getVideoItems() !=null){
			for (final VideoItem videoItem : allHomeScreenItem.getVideoItems()) {
				LinearLayout newsItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, videosContainer, false);
				if(videoItem.getImageUrl() != null && !videoItem.getImageUrl().trim().equals("")){
					NetworkImageView pic = (NetworkImageView) newsItemContainer.findViewById(R.id.thumb);
					pic.setDefaultImageResId(R.drawable.ic_launcher);
					pic.setImageUrl(videoItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
				}
				newsItemContainer.findViewById(R.id.ic_video_overlay).setVisibility(View.VISIBLE);
				TextView text = (TextView) newsItemContainer.findViewById(R.id.news_text);
				text.setText(videoItem.getTitle());
				videosContainer.addView(newsItemContainer);
				//final int id = videoItem.id;
				newsItemContainer.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						YoutubeUtil.playYoutubeVideo(HomeScreenActivity.this, videoItem);
						/*
						Intent openVideoItem = new Intent(HomeScreenActivity.this, VideosActivity.class);
						openVideoItem.putExtra(VideosActivity.INTENT_EXTRA_ITEM_ID, id);
						startActivity(openVideoItem);
						*/
					}
				});
			}
			LinearLayout moreNewsItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, videosContainer, false);
			moreNewsItemContainer.setBackgroundColor(getResources().getColor(R.color.White));
			
			NetworkImageView moreImage = (NetworkImageView) moreNewsItemContainer.findViewById(R.id.thumb);
			//moreImage.setImageResource(R.drawable.more);
			//moreImage.setImageDrawable(getResources().getDrawable(R.drawable.more));
			moreImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.more));

			TextView text = (TextView) moreNewsItemContainer.findViewById(R.id.news_text);
			text.setText("Click to watch more .....");
			moreNewsItemContainer.findViewById(R.id.ic_video_overlay).setVisibility(View.VISIBLE);
			//text.setTextColor(getResources().getColor(R.color.Black));
			text.setTextColor(Color.parseColor("#000000"));
			videosContainer.addView(moreNewsItemContainer);
			moreNewsItemContainer.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent openVideoItem = new Intent(HomeScreenActivity.this, VideoListActivity.class);
					startActivity(openVideoItem);
				}
			});
		}
		
		if(allHomeScreenItem.getBlogItems() != null){
			for (final BlogItem blogItem : allHomeScreenItem.getBlogItems()) {
				LinearLayout blogItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, blogsContainer, false);
				if(blogItem.getImageUrl() != null && !blogItem.getImageUrl().trim().equals("")){
					NetworkImageView pic = (NetworkImageView) blogItemContainer.findViewById(R.id.thumb);
					pic.setDefaultImageResId(R.drawable.ic_launcher);
					pic.setImageUrl(blogItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
				}
				TextView titleView = (TextView) blogItemContainer.findViewById(R.id.news_text);
				titleView.setText(blogItem.getTitle());
				blogsContainer.addView(blogItemContainer);
				
				blogItemContainer.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent openBlogItem = new Intent(HomeScreenActivity.this, BlogActivity.class);
						openBlogItem.putExtra(BlogActivity.INTENT_BLOG_ITEM, blogItem);
						startActivity(openBlogItem);
					}
				});
			}
			
			LinearLayout moreNewsItemContainer = (LinearLayout) inflater.inflate(R.layout.home_item_preview, blogsContainer, false);
			moreNewsItemContainer.setBackgroundColor(getResources().getColor(R.color.White));
			
			NetworkImageView moreImage = (NetworkImageView) moreNewsItemContainer.findViewById(R.id.thumb);
			//moreImage.setImageResource(R.drawable.more);
			//moreImage.setImageDrawable(getResources().getDrawable(R.drawable.more));
			moreImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.more));

			TextView text = (TextView) moreNewsItemContainer.findViewById(R.id.news_text);
			text.setText("Click to read more .....");
			moreNewsItemContainer.findViewById(R.id.ic_video_overlay).setVisibility(View.INVISIBLE);
			//text.setTextColor(getResources().getColor(R.color.Black));
			text.setTextColor(Color.parseColor("#000000"));
			blogsContainer.addView(moreNewsItemContainer);
			moreNewsItemContainer.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent openVideoItem = new Intent(HomeScreenActivity.this, BlogListActivity.class);
					startActivity(openVideoItem);
				}
			});
		}
		
		
		/*
		

		
		*/
    }

	@Override
	public void onSuccesfullHomeItemLoad(AllHomeScreenItem allHomeScreenItem) {
		if(!this.allHomeScreenItem.equals(allHomeScreenItem)){
			LocalCachService.getInstance().saveAllHomeScreenItem(this, allHomeScreenItem);
			loadHomeItemItems(allHomeScreenItem);
		}
		progressBar.setVisibility(View.GONE);
	}

	@Override
	public void onServiceFail(String message) {
		progressBar.setVisibility(View.GONE);
		Log.i("AAP", "Loading Error : " + message);
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
