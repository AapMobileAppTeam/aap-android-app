package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aappublicapp.adapters.BlogItemListAdapter;
import com.next.aappublicapp.listeners.OnPageBlogLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.server.services.LocalCachService;
import com.next.aappublicapp.util.TrackerUtil;
import com.next.aappublicapp.util.UserSessionManager;

public class BlogListActivity extends TrackableListActivity implements OnPageBlogLoadSuccessListener, OnServiceFailListener{

	int currentPageNumber = 1;
	BlogItemListAdapter blogItemListAdapter;
	BlogItem moreBlogItem;
	View moreView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blog_list);
		AllHomeScreenItem allHomeScreenItem = LocalCachService.getInstance().getAllHomeScreenItem(this);
		List<BlogItem> allblogItems = new ArrayList<BlogItem>(allHomeScreenItem.getBlogItems());
		blogItemListAdapter = new BlogItemListAdapter(this, allblogItems);
	
		moreBlogItem = new BlogItem();
		moreBlogItem.setId(-1L);
		
		blogItemListAdapter.add(moreBlogItem);
		setListAdapter(blogItemListAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.blog_list, menu);
		return true;
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent openBlogItem = new Intent(this, BlogActivity.class);
		BlogItem oneBlogItem = blogItemListAdapter.getItem(position);
		Log.i("AAP", "ListView="+listView);
		Log.i("AAP", "view="+view);
		if(oneBlogItem.getId() == -1){
			//means clicked on More Item
			TrackerUtil.sendAapEvent(BlogListActivity.this, TrackerUtil.BLOG_LIST_CLICK_MORE);
			moreView = view;
			startLoading(view);
			reloadNextPage();
		}else{
			openBlogItem.putExtra(BlogActivity.INTENT_BLOG_ITEM, oneBlogItem);
			startActivity(openBlogItem);
		}
		//TrackerUtl.sendAapEvent("CandidateInfo");
	}

	@Override
	public void onServiceFail(String message) {
		TrackerUtil.sendAapEvent(BlogListActivity.this, TrackerUtil.BLOG_LIST_SERVICE_FAIL);
		finishLoading(moreView);
	}

	@Override
	public void onSuccesfullPageBlogLoad(List<BlogItem> acList) {
		currentPageNumber++;
		blogItemListAdapter.remove(moreBlogItem);
		for(BlogItem oneBlogItem : acList){
			blogItemListAdapter.add(oneBlogItem);
		}
		//blogItemListAdapter.addAll(acList);
		blogItemListAdapter.add(moreBlogItem);
	}
	
	private void reloadNextPage(){
		UserSessionManager userSessionManager = UserSessionManager.getInstance();
		long livingAcId = userSessionManager.getLivingAssemblyConstituencyId();
		long votingAcId = userSessionManager.getVotingAssemblyConstituencyId();
		long livingPcId = userSessionManager.getLivingParliamentConstituencyId();
		long votingPcId = userSessionManager.getVotingParliamentConstituencyId();

		AapServerServices.getInstance().loadBlogOfPageAsync(this, currentPageNumber + 1,livingAcId, votingAcId, livingPcId, votingPcId, this, this);
	}
	
	private void startLoading(View view){
		ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.load_progress_bar);
		if(progressBar != null){
			progressBar.setVisibility(View.VISIBLE);
		}
		TextView moreTextView = (TextView)view.findViewById(R.id.more_title);
		if(moreTextView != null){
			moreTextView.setVisibility(View.GONE);
		}
	}
	private void finishLoading(View view){
		ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.load_progress_bar);
		if(progressBar != null){
			progressBar.setVisibility(View.GONE);
		}
		TextView moreTextView = (TextView)view.findViewById(R.id.more_title);
		if(moreTextView != null){
			moreTextView.setVisibility(View.GONE);
		}
	}

}
