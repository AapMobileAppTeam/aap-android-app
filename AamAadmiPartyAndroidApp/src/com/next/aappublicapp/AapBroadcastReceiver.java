package com.next.aappublicapp;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.InboxStyle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.next.aap.messages.NewBlogMessage;
import com.next.aap.messages.NewNewsMessage;
import com.next.aap.messages.NewVideoMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aappublicapp.util.TrackerUtil;

public class AapBroadcastReceiver extends BroadcastReceiver {
	static final String TAG = "GCMDemo";
	public static final int NEWS_NOTIFICATION_ID = 1;
	public static final int VIDEO_NOTIFICATION_ID = 2;
	public static final int BLOG_NOTIFICATION_ID = 3;
	public static final int FB_NOTIFICATION_ID = 4;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging
					.getInstance(context);
			String messageType = gcm.getMessageType(intent);
			Log.i("AAP", "messageType=" + messageType);
			Log.i("AAP", "intent.getExtras().toString()="
					+ intent.getExtras().toString());
			Log.i("AAP", "intent.getExtras().toString(msg)="
					+ intent.getExtras().getString(NotificationMessage.MESSAGE));
			Log.i("AAP",
					"intent.getExtras().toString(msg)="
							+ intent.getExtras().getString(
									NotificationMessage.MESSAGE_TYPE));

			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				// sendNotification("Send error: " +
				// intent.getExtras().toString(),"Aam Aadmi Party",messageType,null);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				// sendNotification("Deleted messages on server: " +
				// intent.getExtras().toString(),"Aam Aadmi Party",messageType,null);
			} else {
				processNotfication(context, intent);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		setResultCode(Activity.RESULT_OK);

	}

	private void processNotfication(Context context, Intent intent) {
		// giveVibratation(context);
		String internalMessageType = intent.getExtras().getString(NotificationMessage.MESSAGE_TYPE);
		
		//count event in google analytics
		TrackerUtil.sendAapEvent(context, internalMessageType);
		
		String msg = intent.getExtras().getString(NotificationMessage.MESSAGE);
		if (NotificationMessage.NEW_FACEBOOK_MESSAGE
				.equals(internalMessageType)) {
			onNewFacebookMessage(context, msg, internalMessageType, "DEFAULT_SOUND");
		} else if (NotificationMessage.NEW_NEWS_MESSAGE
				.equals(internalMessageType)) {
			onNewNewsMessage(context, msg, internalMessageType, "DEFAULT_SOUND");
		} else if (NotificationMessage.NEW_VIDEO_MESSAGE
				.equals(internalMessageType)) {
			onNewVideoMessage(context, msg, internalMessageType,
					"DEFAULT_SOUND");
		} else if (NotificationMessage.NEW_BLOG_MESSAGE
				.equals(internalMessageType)) {
			onNewBlogMessage(context, msg, internalMessageType,"DEFAULT_SOUND");
		} else {
			// sendNotification(msg,"Aam Aadmi Party",internalMessageType,null);
			// dont do anything
		}
	}

	private void onNewNewsMessage(Context context, String msg, String msgType,
			String ringtoneKey) {
		Log.i("AAP", "New News message : " + msg);
		Gson gson = new Gson();
		Intent targetIntent = new Intent(context, HomeScreenActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("msg", msg);
		bundle.putString("msgType", msgType);
		targetIntent.putExtra("NotificationMessage", bundle);

		NewNewsMessage newNewsMessage = gson.fromJson(msg,NewNewsMessage.class);
		sendNotification(context, targetIntent, newNewsMessage.getNotificationTitle(),newNewsMessage.getNotificationTitle(), 
				NEWS_NOTIFICATION_ID, R.drawable.ic_launcher);
	}

	private void onNewVideoMessage(Context context, String msg, String msgType,
			String ringtoneKey) {
		Log.i("AAP", "New Video message : " + msg);
		Gson gson = new Gson();
		Intent targetIntent = new Intent(context, HomeScreenActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("msg", msg);
		bundle.putString("msgType", msgType);
		targetIntent.putExtra("NotificationMessage", bundle);

		NewVideoMessage newVideoMessage = gson
				.fromJson(msg, NewVideoMessage.class);
		sendNotification(context, targetIntent, newVideoMessage.getNotificationTitle(),
				newVideoMessage.getNotificationTitle(), VIDEO_NOTIFICATION_ID, R.drawable.ic_launcher);
	}
	private void onNewBlogMessage(Context context, String msg, String msgType,
			String ringtoneKey) {
		Log.i("AAP", "New Blog message : " + msg);
		Intent targetIntent = new Intent(context, HomeScreenActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("msg", msg);
		bundle.putString("msgType", msgType);
		targetIntent.putExtra("NotificationMessage", bundle);

		Gson gson = new Gson();
		NewBlogMessage newBlogMessage = gson.fromJson(msg, NewBlogMessage.class);
		sendNotification(context, targetIntent, newBlogMessage.getNotificationTitle(),
				newBlogMessage.getNotificationTitle(), BLOG_NOTIFICATION_ID, R.drawable.ic_launcher);
	}
	private void onNewFacebookMessage(Context context, String msg, String msgType,
			String ringtoneKey) {
		Log.i("AAP", "New Facebook message : " + msg);
		Intent targetIntent = new Intent(context, FacebookFeedActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("msg", msg);
		bundle.putString("msgType", msgType);
		targetIntent.putExtra("NotificationMessage", bundle);

		Gson gson = new Gson();
		NewBlogMessage newBlogMessage = gson.fromJson(msg, NewBlogMessage.class);
		sendNotification(context, targetIntent, newBlogMessage.getNotificationTitle(),
				newBlogMessage.getNotificationTitle(), FB_NOTIFICATION_ID, R.drawable.ic_launcher);
	}

	/*
	 * private <T extends Activity> void sendForwardNotification(String
	 * msg,String contentTitle,String msgType,String preferenceKey,Class<T>
	 * forwardActivity) { mNotificationManager = (NotificationManager)
	 * ctx.getSystemService(Context.NOTIFICATION_SERVICE);
	 * 
	 * Intent targetIntent = new Intent(ctx, MainActivity.class); Bundle bundle
	 * = new Bundle(); bundle.putSerializable("msg", msg);
	 * bundle.putSerializable("msgType", msgType); bundle.putBoolean("Forward",
	 * true); //Class<FacebookFeedActivity> forwardActivity =
	 * FacebookFeedActivity.class; bundle.putSerializable("ForwardActivity",
	 * forwardActivity); targetIntent.putExtra("NotificationMessage", bundle);
	 * 
	 * PendingIntent contentIntent = PendingIntent.getActivity(ctx,
	 * 0,targetIntent, 0); String notificationMessage = msg; try { JSONObject
	 * jsonObject = new JSONObject(msg); notificationMessage =
	 * jsonObject.getString("notificationTitle");
	 * 
	 * } catch (JSONException e) { e.printStackTrace(); }
	 * NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx)
	 * .setSmallIcon(R.drawable.ic_launcher) .setContentTitle(contentTitle)
	 * .setStyle(new
	 * NotificationCompat.BigTextStyle().bigText(notificationMessage))
	 * .setContentText(notificationMessage);
	 * 
	 * boolean notificationAllowed =
	 * PreferencesUtil.getInstance().getBooleanSetting(ctx, preferenceKey,
	 * false); Log.i("AAP", "notificationAllowed="+notificationAllowed);
	 * if(notificationAllowed ){ vibratate(ctx, preferenceKey+"_vibrate");
	 * Log.i("AAP", "preferenceKey="+preferenceKey); Uri soundUri =
	 * RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	 * if(preferenceKey != null && !preferenceKey.trim().isEmpty()){ String
	 * ringToneKey = preferenceKey +"_ringtone"; Log.i("AAP",
	 * "ringToneKey="+ringToneKey); String ringTone =
	 * PreferencesUtil.getInstance().getStringSetting(ctx, ringToneKey,
	 * "DEFAULT_SOUND"); Log.i("AAP", "ringTone="+ringTone); soundUri =
	 * Uri.parse(ringTone); } mBuilder.setSound(soundUri); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * mBuilder.setContentIntent(contentIntent);
	 * mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build()); }
	 */
	// Put the GCM message into a notification and post it.
	private void sendNotification(Context context, Intent targetIntent, String contentTitle, String notificationMessage
			, int notificationId, int iconId) {
		mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				targetIntent, 0);
		InboxStyle notificationStyle = new NotificationCompat.InboxStyle()
				.addLine(notificationMessage);
		//notificationStyle.addLine("2. " + notificationMessage);
		//notificationStyle.addLine("3. " + notificationMessage);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context)
				.setSmallIcon(iconId)
				.setContentTitle(contentTitle)
				.setStyle(notificationStyle)
				.setContentText(notificationMessage);
		boolean notificationAllowed = true;// PreferencesUtil.getInstance().getBooleanSetting(ctx,
											// preferenceKey, false);
		if (notificationAllowed) {
			//vibratate(context, preferenceKey + "_vibrate");
			vibratate(context, "_vibrate");
			Uri soundUri = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			//if (preferenceKey != null && !preferenceKey.trim().isEmpty()) {
			/*
				String ringToneKey = preferenceKey + "_ringtone";
				String ringTone = "DEFAULT_SOUND";// PreferencesUtil.getInstance().getStringSetting(ctx,
													// ringToneKey,
													// "DEFAULT_SOUND");
				soundUri = Uri.parse(ringTone);
				*/
			//} 
			mBuilder.setSound(soundUri);
		}
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(notificationId, mBuilder.build());

		/*
		 * Uri soundUri =
		 * RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		 * Log.i("AAP", "ringtoneKey="+ringtoneKey); if(ringtoneKey != null &&
		 * !ringtoneKey.trim().isEmpty()){ String ringTone =
		 * PreferencesUtil.getInstance().getStringSetting(ctx, ringtoneKey,
		 * "DEFAULT_SOUND"); Log.i("AAP", "ringTone="+ringTone); soundUri =
		 * Uri.parse(ringTone);
		 */

	}

	private void vibratate(Context context, String key) {
		try {
			Log.i("AAP", "vibrate Key =" + key);
			// boolean vibrate =
			// PreferencesUtil.getInstance().getBooleanSetting(context, key,
			// true);
			boolean vibrate = true;
			Log.i("AAP", "vibrate=" + vibrate);
			if (vibrate) {
				Vibrator v2 = (Vibrator) context
						.getSystemService(Context.VIBRATOR_SERVICE);
				// Vibrate for 500 milliseconds
				v2.vibrate(200);
			}
		} catch (Exception ex) {

		}

	}

}
