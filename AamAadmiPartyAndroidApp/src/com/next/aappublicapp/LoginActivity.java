package com.next.aappublicapp;

import java.io.IOException;

import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.next.aappublicapp.fragment.GoogleAccountDialog;
import com.next.aappublicapp.fragment.GoogleAccountDialog.GoogleAccountDialogListener;
import com.next.aappublicapp.listeners.OnLoginLogoffListener;
import com.next.aappublicapp.util.MainNavigationDrawerHelper;
import com.next.aappublicapp.util.UserSessionManager;
import com.next.aappublicapp.util.VolleyUtil;

public class LoginActivity extends TrackableFragmentActivity implements OnLoginLogoffListener, GoogleAccountDialogListener{

	public static final String INTENT_EXTRA_FROM_START = "INTENT_EXTRA_FROM_START"; 
	private static final String	GOOGLE_PROFILE_FETCH_URL = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
	private static final String	AUTH_TOKEN_TYPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email";
	private static final int	REQUEST_AUTHENTICATE = 1;

	private UiLifecycleHelper uiLifecycleHelper;
	private UserSessionManager userSessionManager;
	private boolean userAskedForFbLogin = false;
	
	private ImageView facebookIconImageView;
	private ImageView twitterIconImageView;
	private ImageView googleIconImageView;
	
	private TextView facebookStatus;
	private TextView twitterStatus;
	private TextView googleStatus;
	
	private View facebookConnect;
	private View twitterConnect;
	private View googleConnect;

	private Button continueButton;
	
	private boolean goToProfileOnContinue;
	
	private boolean firstToken;
	
	private Account selectedGooglEAccount;
	
	private ProgressDialog googleProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i("AAP", "onCreate "+savedInstanceState);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		goToProfileOnContinue = getIntent().getBooleanExtra(INTENT_EXTRA_FROM_START, true);
		
		userSessionManager = UserSessionManager.getInstance();
		uiLifecycleHelper = new UiLifecycleHelper(this, facebookLoginCallback);
		uiLifecycleHelper.onCreate(savedInstanceState);
		
		//get handle to graphical widgets to be used later
		facebookIconImageView = (ImageView) findViewById(R.id.fb_icon);
		twitterIconImageView = (ImageView) findViewById(R.id.twitter_icon);
		googleIconImageView = (ImageView) findViewById(R.id.google_icon);
		
		facebookStatus = (TextView) findViewById(R.id.fb_status);
		twitterStatus = (TextView) findViewById(R.id.twitter_status);
		googleStatus = (TextView) findViewById(R.id.google_status);

		facebookConnect = findViewById(R.id.fb_connect);
		twitterConnect = findViewById(R.id.twitter_connect);
		googleConnect = findViewById(R.id.google_connect);

		continueButton = (Button) findViewById(R.id.continue_button);
		
		//Attack all click listeners
		facebookConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				userAskedForFbLogin = true;
				Log.i("AAP", "Clickedon FB Login");
				if (userSessionManager.isLoggedOntoFacebook()) {
					Log.i("AAP", "used already logged in by Facebook");
					userSessionManager.disconnectFacebook(LoginActivity.this, LoginActivity.this);
					updateLoginScreen();
				} else {
					Log.i("AAP", "used not logged in by Facebook, so will start FB Login flow now");
					userSessionManager.openActiveFacebookSession(LoginActivity.this, true, facebookLoginCallback);
				}
			}
		});
		
		twitterConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (UserSessionManager.getInstance().isLoggedOntoTwitter()) {
					Log.i("AAP", "used already logged in to Twitter");
					userSessionManager.disconnectTwitter(LoginActivity.this, LoginActivity.this);
					updateLoginScreen();
				} else {
					Intent startTwitterFlow = new Intent(LoginActivity.this, TwitterLoginActivity.class);
					startActivity(startTwitterFlow);
				}
			}
		});
		
		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(goToProfileOnContinue){
					Log.i("AAP", "Clicked on Continue");
					if (userSessionManager.isUserLoggedInOnAnyService()) {
						Log.i("AAP", "used already logged in atleast one of service");
						if(userSessionManager.isUserRegistered(LoginActivity.this)){
							Log.i("AAP", "used already registered");
							
						}else{
							Log.i("AAP", "used NOT registered");
							finish();
							Intent userProfileActivity = new Intent(LoginActivity.this, UserProfileActivity.class);
							startActivity(userProfileActivity);

						}
					}else{
						finish();
						Intent homeScreenActivity = new Intent(LoginActivity.this, HomeScreenActivity.class);
						startActivity(homeScreenActivity);

					}
				}else{
					//Just finish to go back to previous activity
					finish();
				}
				
			}
		});
		
		googleConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (userSessionManager.isLoggedOntoGoogle()) {
					// disconnect google
					userSessionManager.disconnectGoogle(LoginActivity.this, LoginActivity.this);
				} else {
					AccountManager am = AccountManager.get(LoginActivity.this);
					final Account[] accounts = am.getAccountsByType("com.google");
					if (accounts.length < 1) {
						Toast.makeText(LoginActivity.this, "Please sign-in to Google account on the phone.", Toast.LENGTH_LONG).show();
						return;
					}

					FragmentManager fm = LoginActivity.this.getSupportFragmentManager();
					GoogleAccountDialog accountSelectDialog = new GoogleAccountDialog();
					accountSelectDialog.show(fm, "account_picker");
				}
			}
		});
		
		updateLoginScreen();
	}
	
	Session.StatusCallback  facebookLoginCallback = new Session.StatusCallback(){

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			if(session.isOpened() && userAskedForFbLogin){
				Log.i("AAP", "Will try FB login now");
				userSessionManager.loginFbUser(LoginActivity.this, session, LoginActivity.this);	
			}else{
				Log.i("AAP", "Will NOT try FB login now");
			}
			updateLoginScreen();
		}
		
	};
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		Log.i("AAP", "onCreateOptionsMenu "+menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.i("AAP", "onResume ");
		uiLifecycleHelper.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i("AAP", "onPause ");
		uiLifecycleHelper.onPause();
	}

	@Override
	public void onStop() {
		Log.i("AAP", "onStop ");
		super.onPause();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("AAP", "onActivityResult ");
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		/*
		case REQUEST_AUTHENTICATE:
			if (resultCode == RESULT_OK) {
				fetchAuthToken(null);
			} else {
				Log.w(TAG, "user cancelled auth");
				Toast.makeText(this, "Sign in cancelled", Toast.LENGTH_LONG).show();
			}
			break;
			*/
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			uiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		Log.i("AAP", "onSaveInstanceState ");
		super.onSaveInstanceState(outState);
		uiLifecycleHelper.onSaveInstanceState(outState);
	}
	
	private void updateLoginScreen() {
		boolean loggedOntoAtleastOneService = false;
		// fb
		facebookIconImageView.setBackgroundResource(R.drawable.ic_fb_disconnected);
		facebookStatus.setText("Not connected");
		if (userSessionManager.isLoggedOntoFacebook()) {
			Log.i("AAP", "user is logged in With Facebook");
			loggedOntoAtleastOneService = true;
			facebookIconImageView.setBackgroundResource(R.drawable.ic_fb_connected);
			facebookStatus.setText(userSessionManager.getFacebookUserEmail());
			facebookConnect.setBackgroundResource(R.drawable.bg_orange_button);
			//facebookConnect.setBackgroundColor(getResources().getColor(R.color.AapOrange));
		} else {
			facebookConnect.setBackgroundResource(R.drawable.bg_grey_button);
			//facebookConnect.setBackgroundColor(getResources().getColor(R.color.AapGreen));
		}

		// twitter
		twitterIconImageView.setBackgroundResource(R.drawable.ic_twitter_disconnected);
		twitterStatus.setText("Not connected");
		if (userSessionManager.isLoggedOntoTwitter()) {
			loggedOntoAtleastOneService = true;
			twitterIconImageView.setBackgroundResource(R.drawable.ic_twitter_connected);
			twitterStatus.setText(userSessionManager.getTwitterHandle());
			twitterConnect.setBackgroundResource(R.drawable.bg_orange_button);
		} else {
			//twitterConnect.setBackgroundColor(getResources().getColor(R.color.AapGreen));
			twitterConnect.setBackgroundResource(R.drawable.bg_grey_button);
			//twitterConnect.setBackgroundColor(getResources().getColor(android.R.color.background_light));
		}

		// google
		googleIconImageView.setBackgroundResource(R.drawable.ic_google_disconnected);
		googleStatus.setText("Not connected");
		if (userSessionManager.isLoggedOntoGoogle()) {
			loggedOntoAtleastOneService = true;
			googleIconImageView.setBackgroundResource(R.drawable.ic_google_connected);
			googleStatus.setText(userSessionManager.getGoogleEmail());
			googleConnect.setBackgroundResource(R.drawable.bg_orange_button);
		} else {
			googleConnect.setBackgroundResource(R.drawable.bg_grey_button);
			//googleConnect.setBackgroundColor(getResources().getColor(R.color.AapGreen));
			//googleConnect.setBackgroundColor(getResources().getColor(android.R.color.background_light));
		}

		if (loggedOntoAtleastOneService)
			continueButton.setText(" Continue ");
		else
			continueButton.setText(" Guest login ");
		
		MainNavigationDrawerHelper.refreshDrawer(this);
	}

	@Override
	public void loginLogoffCompleted() {
		Log.i("AAP", "loginLogoffCompleted ");
		updateLoginScreen();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.i("AAP", "onConfigurationChanged");
	    super.onConfigurationChanged(newConfig);

	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	        Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	        Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
	    }
	    updateLoginScreen();
	    
	}

	@Override
	public void onAccountSelect(Account account) {
		this.selectedGooglEAccount = account;
		fetchGoogleAuthToken(null);
		Toast.makeText(this, "Authenticating using " + account.name, Toast.LENGTH_LONG).show();
	}
	
	private void fetchGoogleAuthToken(String curToken) {
		Bundle options = new Bundle();
		AccountManager am = AccountManager.get(this);
		if (curToken != null) {
			Log.d("AAP", "invalidating first token");
			am.invalidateAuthToken("com.google", curToken);
		}
		am.getAuthToken(selectedGooglEAccount, AUTH_TOKEN_TYPE, options, this, new OnTokenAcquired(), null);
	}
	
	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {
		public void run(AccountManagerFuture<Bundle> result) {
			// Get the result of the operation from the AccountManagerFuture.
			try {
				Bundle bundle = result.getResult();
				Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (launch != null) {
					launch.setFlags(launch.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivityForResult(launch, REQUEST_AUTHENTICATE);
					return;
				}

				String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
				Log.i("AAP", "got token! - " + token);
				if (firstToken) {
					firstToken = false;
					fetchGoogleAuthToken(token);
					return;
				}
				// now fetch the profile
				makeGoogleRequest(token);
			} catch (OperationCanceledException e) {
				Log.w("AAP", "user cancelled auth", e);
				Toast.makeText(LoginActivity.this, "Sign in cancelled", Toast.LENGTH_LONG).show();
			} catch (AuthenticatorException e) {
				Log.w("AAP", "AuthenticatorException", e);
				Toast.makeText(LoginActivity.this, "Problem with Google, please try again.", Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				Log.w("AAP", "IOE", e);
				Toast.makeText(LoginActivity.this, "Please check internet connectivity and try again.", Toast.LENGTH_LONG).show();
			}
		}
		
		private void makeGoogleRequest(final String token) {
			String url = GOOGLE_PROFILE_FETCH_URL + token;
			JsonObjectRequest loginRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject googleUser) {
					googleProgressDialog.dismiss();
					Log.d("AAP", "got response:\n" + googleUser.toString());
					UserSessionManager.getInstance().saveGoogleProfile(googleUser, token);
					updateLoginScreen();
				}
			}, new com.android.volley.Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e("AAP", "error:" + error.toString());
					String message = "Error fetching Google profile.";
					Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
					googleProgressDialog.dismiss();
					updateLoginScreen();
				}
			});
			loginRequest.setShouldCache(false);
			VolleyUtil.getInstance().getRequestQueue().add(loginRequest).setTag(this);
			googleProgressDialog = ProgressDialog.show(LoginActivity.this, "Connecting to Google...", "Please wait while we connect to your Google account");
		}
	}


}
