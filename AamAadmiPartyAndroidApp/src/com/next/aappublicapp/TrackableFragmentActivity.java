package com.next.aappublicapp;

import android.support.v4.app.FragmentActivity;

import com.google.analytics.tracking.android.EasyTracker;

public class TrackableFragmentActivity extends FragmentActivity {

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
