package com.next.aappublicapp;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;
import com.next.aap.dto.JanLokPalChapter;
import com.next.aappublicapp.adapters.JanLokPalListAdapter;
import com.next.aappublicapp.listeners.OnJanLokPalLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.util.LocalPersistence;
import com.next.aappublicapp.util.PreferenceNameUtil;

public class JanLokpalListActivity extends TrackableListActivity implements OnJanLokPalLoadSuccessListener, OnServiceFailListener {

	JanLokPalListAdapter janLokPalListAdapter;
	ProgressDialog progressDialog;
	private String language = "English";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jan_lokpal_list);
		progressDialog = ProgressDialog.show(this, "Loading JanLokPal", "Please wait while we download JanLokpal");
		janLokPalListAdapter = new JanLokPalListAdapter(this, new ArrayList<JanLokPalChapter>());
		setListAdapter(janLokPalListAdapter);
		
		loadfromLocalCache();
	}
	
	private void loadfromLocalCache(){
		new AsyncTask<String, String, List<JanLokPalChapter>>() {

			@Override
			protected List<JanLokPalChapter> doInBackground(String... params) {
				Type type = new TypeToken<List<JanLokPalChapter>>() {
				}.getType();
				List<JanLokPalChapter> janLokPalChapters = LocalPersistence.getFromLocalCache(JanLokpalListActivity.this, PreferenceNameUtil.BOOK_JANLOKPAL_PREF, language, type);
				if(janLokPalChapters == null){
					AapServerServices.getInstance().loadAapJanLokPalFeedAsync(JanLokpalListActivity.this, language, JanLokpalListActivity.this, JanLokpalListActivity.this);	
				}else{
					return janLokPalChapters;
				}
				return null;
			}
			
			@Override
			protected void onPostExecute(List<JanLokPalChapter> swarajChapters) {
				super.onPostExecute(swarajChapters);
				if(swarajChapters != null){
					onSuccesfullJanLokPalLoad(swarajChapters);
				}
			}
			
		}.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.jan_lokpal_list, menu);
		return true;
	}

	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent janLokPalIntent = new Intent(this, JanLokpalActivity.class);
		JanLokPalChapter oneJanLokPalChapter = janLokPalListAdapter.getItem(position);
		Log.i("AAP", "ListView="+listView);
		Log.i("AAP", "view="+view);
		janLokPalIntent.putExtra(JanLokpalActivity.JAN_LOKPAL_CHAPTER, oneJanLokPalChapter);
		startActivity(janLokPalIntent);
		//TrackerUtl.sendAapEvent("CandidateInfo");
	}
	
	@Override
	public void onServiceFail(String message) {
		progressDialog.cancel();
		
	}

	@Override
	public void onSuccesfullJanLokPalLoad(
			List<JanLokPalChapter> janLokPalChapters) {
		for(JanLokPalChapter oneJanLokPalChapter : janLokPalChapters){
			janLokPalListAdapter.add(oneJanLokPalChapter);
		}
		//janLokPalListAdapter.addAll(janLokPalChapters);
		LocalPersistence.saveIntoLocalCache(this, PreferenceNameUtil.BOOK_JANLOKPAL_PREF, language, (Serializable)janLokPalChapters);
		progressDialog.cancel();
		
	}

}
