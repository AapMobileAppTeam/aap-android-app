package com.next.aappublicapp.util;

import android.content.Context;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

public class TrackerUtil {

	public static final String BLOG_TWEET_SHARE_CLICK = "BlogTweetShareClick";
	public static final String BLOG_FACEBOOK_SHARE_CLICK = "BlogFacebookShareClick";
	public static final String BLOG_LIST_CLICK_MORE = "BlogListClickMore";
	public static final String BLOG_LIST_SERVICE_FAIL = "BlogListServiceFail";
	
	public static final String FACEBOOK_FEED_NOT_LOGGED_IN = "FacebookFeedNotLoggedIn";
	
	private static final String FACEBOOK_CATEGORY = "FacebookServer";
	private static final String APP_CATEGORY = "AapServer";
	
	private static final String CATEGORY = "Android";
	
	public static void sendAapServerTiming(Context context,long intervalInMilliseconds,String name,String label){
		EasyTracker.getInstance(context).send(MapBuilder.createTiming(APP_CATEGORY, intervalInMilliseconds, name, label).build());
	}
	public static void sendFacebookServerTiming(Context context,long intervalInMilliseconds,String name,String label){
		EasyTracker.getInstance(context).send(MapBuilder.createTiming(FACEBOOK_CATEGORY, intervalInMilliseconds, name, label).build());
	}

	public static void sendFacebookEvent(Context context,String eventLabel){
		sendFacebookEvent(context, eventLabel, 1L);
	}
	public static void sendFacebookEvent(Context context, String eventLabel, long count){
		EasyTracker.getInstance(context).send(MapBuilder.createEvent(CATEGORY,FACEBOOK_CATEGORY,eventLabel, count).build());
	}
	public static void sendAapEvent(Context context,String eventLabel){
		sendAapEvent(context, eventLabel, 1L);
	}
	public static void sendAapEvent(Context context,String eventLabel, long count){
		EasyTracker.getInstance(context).send(MapBuilder.createEvent(CATEGORY,APP_CATEGORY,eventLabel, count).build());
	}
	public static void sendAapTiming(Context context,String eventLabel, long timeInMilliSeconds){
		EasyTracker.getInstance(context).send(MapBuilder.createTiming(CATEGORY +"-"+ APP_CATEGORY,timeInMilliSeconds,eventLabel, null).build());
	}
	public static void sendFacebookTiming(Context context,String eventLabel, long timeInMilliSeconds){
		EasyTracker.getInstance(context).send(MapBuilder.createTiming(CATEGORY +"-"+ FACEBOOK_CATEGORY,timeInMilliSeconds,eventLabel, null).build());
	}

}
