package com.next.aappublicapp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.User;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterGoogleUserProfile;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.GoogleUserRegisteredMessage;
import com.next.aap.messages.TaskStatus;
import com.next.aap.messages.TwitterUserRegisteredMessage;
import com.next.aappublicapp.R;
import com.next.aappublicapp.listeners.OnLoginLogoffListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.listeners.OnSucessFacebookUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessGoogleUserRegistrationListener;
import com.next.aappublicapp.listeners.OnSucessTwitterUserRegistrationListener;
import com.next.aappublicapp.server.services.AapServerServices;

public class UserSessionManager implements OnSucessFacebookUserRegistrationListener, OnSucessTwitterUserRegistrationListener,
OnSucessGoogleUserRegistrationListener{


	private Context context;
	private static UserSessionManager instance = null;
	private ProgressDialog progressDialog;

	public static UserSessionManager getInstance() {
		if (instance == null) {
			instance = new UserSessionManager();
		}
		return instance;
	}
	/**
	 * This function will be called when application starts
	 * @param context
	 */
	public void initSession(Context context){
		this.context = context;
	}
	
	public boolean isUserLoggedInOnAnyService(){
		return isLoggedOntoFacebook() || isLoggedOntoTwitter() || isLoggedOntoGoogle();
	}
	public boolean isUserRegistered(){
		return false;
        }
	public boolean isLoggedOntoFacebook() {
		Session session = getFBSession();
		return (session != null && getFacebookUserObjectId() != null && session.isOpened());
	}
	public boolean isLoggedOntoTwitter() {
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getBoolean(PreferenceNameUtil.TWITTER_IS_LOGGED_IN, false);
	}
	public boolean isLoggedOntoGoogle() {
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getBoolean(PreferenceNameUtil.GOOGLE_IS_LOGGED_IN, false);
	}
	public void saveGoogleProfile(JSONObject googleUser, String token){
		try{
			Log.i("AAP", "Saving google Profile");
			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
	        SharedPreferences.Editor editor = prefs.edit();
	        editor.putString(PreferenceNameUtil.GOOGLE_EMAIL, getStringFromJsonObject(googleUser,"email", null));
	        editor.putString(PreferenceNameUtil.GOOGLE_USER_FULL_NAME, getStringFromJsonObject(googleUser,"name", null));
	        editor.putString(PreferenceNameUtil.GOOGLE_PROFILE_PIC, getStringFromJsonObject(googleUser,"picture", null));
	        editor.putString(PreferenceNameUtil.GOOGLE_USER_GENDER, getStringFromJsonObject(googleUser,"gender", null));
	        editor.putString(PreferenceNameUtil.GOOGLE_USER_OBJECT_ID, getStringFromJsonObject(googleUser,"id", null));
	        editor.putString(PreferenceNameUtil.GOOGLE_USER_AUTH_TOKEN, token);
	        editor.putString(PreferenceNameUtil.GOOGLE_USER_DOB, getStringFromJsonObject(googleUser,"birthday", null));
	        editor.putBoolean(PreferenceNameUtil.GOOGLE_IS_LOGGED_IN, true);
	        
	        editor.commit();
	        Log.i("AAP", "Saved google Profile Locally andnow will register at server");
	        RegisterGoogleTask registerGoogleTask = new RegisterGoogleTask(googleUser, context, token);
	        registerGoogleTask.execute(new String[1]);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public String getGoogleProfilePic(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.GOOGLE_PROFILE_PIC, null);
	}
	class RegisterGoogleTask extends AsyncTask<String, Void, Integer> implements OnServiceFailListener{
		private ProgressDialog googleRegistrationProgressDialog;
		private Context context;
		private JSONObject googleUser;
		private String token;
		public RegisterGoogleTask(JSONObject googleUser,Context context, String token) {
			this.googleUser = googleUser;
			this.context = context;
			this.token = token;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.i("AAP", "onPreExecute");
			/*
			googleRegistrationProgressDialog = ProgressDialog.show(context,"Saving Google profile...",
					"Please wait while we save your google profile");
					*/
			Log.i("AAP", "showing Progress dialog");
		}

		@Override
		protected Integer doInBackground(String... params) {
			Log.i("AAP", "doInBackground");
			//now register facebook user to server as well
	        RegisterGoogleUserProfile registerGoogleUserProfile = new RegisterGoogleUserProfile();
	        
	        registerGoogleUserProfile.setAccessToken(token);
	        String deviceId = GcmUtil.getOrCreateDeviceRegistrationId(context);
	        if(deviceId == null || deviceId.trim().equals("")){
	        	deviceId = GcmUtil.getGcmDeviceRegistrationId(context);
	        }
	        registerGoogleUserProfile.setDeviceRegId(deviceId);
	        registerGoogleUserProfile.setDeviceType(GcmUtil.getDeviceType());
	        registerGoogleUserProfile.setEmail(getStringFromJsonObject(googleUser,"email", null));
	        registerGoogleUserProfile.setBirthday(getStringFromJsonObject(googleUser,"birthday", null));
	        registerGoogleUserProfile.setGender(getStringFromJsonObject(googleUser,"gender", null));
	        registerGoogleUserProfile.setGoogleUserId(getStringFromJsonObject(googleUser,"id", null));
	        registerGoogleUserProfile.setName(getStringFromJsonObject(googleUser,"name", null));
	        registerGoogleUserProfile.setUserId(getUserId());
	        AapServerServices.getInstance().registerGoogleUserAsync(context, registerGoogleUserProfile, UserSessionManager.this, null);
	        
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (googleRegistrationProgressDialog != null){
				googleRegistrationProgressDialog.cancel();
			}
		}

		@Override
		public void onServiceFail(String message) {
			if (googleRegistrationProgressDialog != null){
				googleRegistrationProgressDialog.cancel();
			}
		}
	}
	private String getStringFromJsonObject(JSONObject jsonObject, String key, String defaultValue){
		try{
			return jsonObject.getString(key);
		}catch(Exception ex){
			
		}
		return defaultValue;
	}
	public void disconnectGoogle(final Context context, final OnLoginLogoffListener loginLogoffListener){
		new AlertDialog.Builder(context)
        .setIcon(R.drawable.ic_launcher)
        .setTitle(R.string.title_activity_login)
        .setMessage(R.string.login_google_logoff_message)
        
        .setPositiveButton(R.string.confirm_yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	try{
        			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
        	        SharedPreferences.Editor editor = prefs.edit();
        	        editor.remove(PreferenceNameUtil.GOOGLE_EMAIL);
        	        editor.remove(PreferenceNameUtil.GOOGLE_USER_FULL_NAME);
        	        editor.remove(PreferenceNameUtil.GOOGLE_PROFILE_PIC);
        	        editor.remove(PreferenceNameUtil.GOOGLE_USER_GENDER);
        	        editor.remove(PreferenceNameUtil.GOOGLE_USER_OBJECT_ID);
        	        editor.remove(PreferenceNameUtil.GOOGLE_USER_AUTH_TOKEN);
        	        editor.remove(PreferenceNameUtil.GOOGLE_USER_DOB);
        	        editor.remove(PreferenceNameUtil.GOOGLE_IS_LOGGED_IN);
        	        
        	        editor.commit();
        		}catch(Exception ex){
        			ex.printStackTrace();
        		}
                if(loginLogoffListener != null){
                	loginLogoffListener.loginLogoffCompleted();
                }
            }

        })
        .setNegativeButton(R.string.confirm_no, null)
        .show();
		
	}


	public Session getFBSession() {
		Session session = Session.getActiveSession();
		if (session == null)
			session = Session.openActiveSessionFromCache(context);
		return session;
	}
	
	public Session openActiveFacebookSession(Activity activity, boolean allowLoginUI, StatusCallback callback) {
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		permissions.add("user_birthday");
	    OpenRequest openRequest = new OpenRequest(activity).setPermissions(permissions).setCallback(callback);
	    Session session = new Builder(activity).build();
	    if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
	        Session.setActiveSession(session);
	        session.openForRead(openRequest);
	        return session;
	    }
	    return null;
	}
	
	public void loginFbUser(final Context context,Session session, final OnLoginLogoffListener onLoginLogoffListener){
		if (progressDialog != null){
			Log.i("AAP", "Progresss Dialog was runnign so cancelled it");
			progressDialog.cancel();
		}

		progressDialog = ProgressDialog.show(context, "Connecting to Facebook...",
				"Please wait while we connect to your Facebook account");

		Request facebookRequest = Request.newMeRequest(session, new Request.GraphUserCallback() {
			public void onCompleted(GraphUser user, Response response) {
				try{
					if (user != null) {
						// if (loginTask != null)
						// loginTask.cancel(true);
						// loginTask = new LoginTask(user.getId(), user.getUsername(), session.getAccessToken());
						// loginTask.execute(new String[] {});
						// sessionProvider.setUserId(user.getId());
						Log.d("AAP", "name: " + user.getName() + ", email=" + user.getProperty("email")+", Birthday="+user.getBirthday());
						Log.d("AAP", "Gender: " + user.getProperty("gender") +", "+user.asMap().get("gender"));
						Log.d("AAP", "Email: " + user.getProperty("email") +", "+user.asMap().get("email"));
						Log.d("AAP", "User: " + user.toString());

						SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
				        SharedPreferences.Editor editor = prefs.edit();
				        if(user.getProperty("email") != null){
				        	editor.putString(PreferenceNameUtil.FACEBOOK_USER_EMAIL, user.getProperty("email").toString());	
				        }
				        String userName = getUserName(user);
				        editor.putString(PreferenceNameUtil.FACEBOOK_USER_NAME, userName);
				        editor.putString(PreferenceNameUtil.FACEBOOK_USER_FULL_NAME, user.getName());
				        editor.putString(PreferenceNameUtil.FACEBOOK_USER_OBJECT_ID, user.getId());
				        editor.putString(PreferenceNameUtil.FACEBOOK_USER_DOB, user.getBirthday());
				        editor.putString(PreferenceNameUtil.FACEBOOK_USER_DOB, user.getBirthday());
				        editor.putString(PreferenceNameUtil.FACEBOOK_PROFILE_PIC, "https://graph.facebook.com/"+user.getId()+"/picture");
				        if(user.getProperty("gender") != null){
					        editor.putString(PreferenceNameUtil.FACEBOOK_USER_DOB, user.getProperty("gender").toString());
				        }
				        editor.commit();
				        if(onLoginLogoffListener != null){
				        	onLoginLogoffListener.loginLogoffCompleted();
				        }
				        //Register at server
				        RegisterFacebookTask registerFacebookTask = new RegisterFacebookTask(user, context);
				        registerFacebookTask.execute(new String[1]);
						return;
					}
					if (response.getError() != null) {
						// Handle errors, will do so later.
						Log.w("AAP", "FB error:" + response.getError().getErrorMessage());
						Toast.makeText(context, "Error connecting to Facebook : "+response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
					}
				}finally{
					if(onLoginLogoffListener != null){
						onLoginLogoffListener.loginLogoffCompleted();	
					}
					hideDialogBox();
				}
			}
		});
		Bundle params = facebookRequest.getParameters();
		params.putString("fields", "email,name");
		facebookRequest.setParameters(params);
		facebookRequest.executeAsync();
		
		// session.closeAndClearTokenInformation();
		
	}
	private String getUserName(GraphUser user){
        String userName = user.getUsername();
        if(userName == null || userName.trim().equals("")){
        	if(user.getProperty("email") != null){
            	userName = user.getProperty("email").toString();
        	}
        }
        return userName;

	}
	class RegisterFacebookTask extends AsyncTask<String, Void, Integer> implements OnServiceFailListener{
		private ProgressDialog facebookRegistrationProgressDialog;
		private Context context;
		private GraphUser user;
		public RegisterFacebookTask(GraphUser user,Context context) {
			this.user = user;
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*
			facebookRegistrationProgressDialog = ProgressDialog.show(context,"Saving Facebook profile...",
					"Please wait while we save your facebook profile");
			*/
		}

		@Override
		protected Integer doInBackground(String... params) {
			//now register facebook user to server as well
			String userName = getUserName(user);
	        RegisterFacebookUserProfile registerFacebookUserProfile = new RegisterFacebookUserProfile();
	        registerFacebookUserProfile.setAccessToken(Session.getActiveSession().getAccessToken());
	        String deviceId = GcmUtil.getOrCreateDeviceRegistrationId(context);
	        if(deviceId == null || deviceId.trim().equals("")){
	        	deviceId = GcmUtil.getGcmDeviceRegistrationId(context);
	        }
	        registerFacebookUserProfile.setDeviceRegId(deviceId);
	        registerFacebookUserProfile.setDeviceType(GcmUtil.getDeviceType());
	        registerFacebookUserProfile.setEmail(user.getProperty("email").toString());
	        registerFacebookUserProfile.setFacebookUserId(user.getId());
	        registerFacebookUserProfile.setUserName(userName);
	        registerFacebookUserProfile.setUserId(getUserId());
	        AapServerServices.getInstance().registerFacebookUserAsync(context, registerFacebookUserProfile, UserSessionManager.this, null);
	        
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (facebookRegistrationProgressDialog != null){
				facebookRegistrationProgressDialog.cancel();
			}
		}

		@Override
		public void onServiceFail(String message) {
			if (facebookRegistrationProgressDialog != null){
				facebookRegistrationProgressDialog.cancel();
			}
		}
	}
	public void saveTwitterProfile(Twitter twitter, User twitterUser){
		try{
			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
	        SharedPreferences.Editor editor = prefs.edit();
	        editor.putString(PreferenceNameUtil.TWITTER_HANDLE, twitterUser.getScreenName());
	        editor.putString(PreferenceNameUtil.TWITTER_PROFILE_PIC, twitterUser.getProfileImageURL());
	        editor.putString(PreferenceNameUtil.TWITTER_USER_FULL_NAME, twitterUser.getName());
	        editor.putString(PreferenceNameUtil.TWITTER_USER_OBJECT_ID, twitterUser.getId()+"");
	        editor.putString(PreferenceNameUtil.TWITTER_TOKEN, twitter.getOAuthAccessToken().getToken());
	        editor.putString(PreferenceNameUtil.TWITTER_TOKEN_SECRET, twitter.getOAuthAccessToken().getTokenSecret());
	        editor.putBoolean(PreferenceNameUtil.TWITTER_IS_LOGGED_IN, true);
	        
	        editor.commit();
		}catch(Exception ex){
			
		}
	}
	public String getTwitterProfilePic(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.TWITTER_PROFILE_PIC, null);
	}
	public String getFacebookUserObjectId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.FACEBOOK_USER_OBJECT_ID, null);
	}
	public String getFacebookProfilePicLarge(){
		//200 width and variable height
		return getFacebookProfilePic("large");
	}
	public String getFacebookProfilePicNormal(){
		//100 width and variable height
		return getFacebookProfilePic("normal");
	}
	public String getFacebookProfilePicSmall(){
		//50 width and variable height
		return getFacebookProfilePic("small");
	}
	public String getFacebookProfilePicSquare(){
		//50X50
		return getFacebookProfilePic("square");
	}
	public String getFacebookProfilePicCustom(String width, String height){
		String picUrl = getFacebookProfileImageUrl();
		if(picUrl != null){
			picUrl = picUrl +"?width="+width+"&height="+height;
		}
		return picUrl;
	}
	private String getFacebookProfilePic(String type){
		String picUrl = getFacebookProfileImageUrl();
		if(picUrl != null){
			picUrl = picUrl+"?type="+type;
		}
		return picUrl;
	}
	private String getFacebookProfileImageUrl(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		String picUrl = prefs.getString(PreferenceNameUtil.FACEBOOK_PROFILE_PIC, null);
		if(picUrl == null){
			String facebookId = prefs.getString(PreferenceNameUtil.FACEBOOK_USER_OBJECT_ID, null);
			if(facebookId != null){
				picUrl = "https://graph.facebook.com/"+facebookId+"/picture";
			}
		}
		return picUrl;
	}
	public String getFacebookUserEmail(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.FACEBOOK_USER_EMAIL, null);
	}
	public String getFacebookUserFullName(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.FACEBOOK_USER_FULL_NAME, null);
	}
	public String getFacebookUserGender(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.FACEBOOK_USER_GENDER, null);
	}

	public String getTwitterHandle(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.TWITTER_HANDLE, null);
	}
	public String getTwitterConsumerKey(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.TWITTER_TOKEN, null);
	}
	public String getTwitterConsumerKeySecret(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.TWITTER_TOKEN_SECRET, null);
	}
	public String getGoogleEmail(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.GOOGLE_EMAIL, null);
	}
	
	public void disconnectFacebook(final Context context, final OnLoginLogoffListener loginLogoffListener){
		new AlertDialog.Builder(context)
        .setIcon(R.drawable.ic_launcher)
        .setTitle(R.string.title_activity_login)
        .setMessage(R.string.login_fb_logoff_message)
        .setPositiveButton(R.string.confirm_yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_EMAIL);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_NAME);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_FULL_NAME);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_OBJECT_ID);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_DOB);
                editor.commit();
                
                if(loginLogoffListener != null){
                	loginLogoffListener.loginLogoffCompleted();
                }
            }

        })
        .setNegativeButton(R.string.confirm_no, null)
        .show();
		
		

	}
	
	public void disconnectTwitter(final Context context, final OnLoginLogoffListener loginLogoffListener){
		new AlertDialog.Builder(context)
        .setIcon(R.drawable.ic_launcher)
        .setTitle(R.string.title_activity_login)
        .setMessage(R.string.login_twitter_logoff_message)
        .setPositiveButton(R.string.confirm_yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	try{
        			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
        	        SharedPreferences.Editor editor = prefs.edit();
        	        editor.remove(PreferenceNameUtil.TWITTER_HANDLE);
        	        editor.remove(PreferenceNameUtil.TWITTER_USER_FULL_NAME);
        	        editor.remove(PreferenceNameUtil.TWITTER_USER_OBJECT_ID);
        	        editor.remove(PreferenceNameUtil.TWITTER_TOKEN);
        	        editor.remove(PreferenceNameUtil.TWITTER_TOKEN_SECRET);
        	        editor.remove(PreferenceNameUtil.TWITTER_IS_LOGGED_IN);
        	        
        	        editor.commit();
        		}catch(Exception ex){
        			
        		}
            	/*
            	SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_EMAIL);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_NAME);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_FULL_NAME);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_OBJECT_ID);
                editor.remove(PreferenceNameUtil.FACEBOOK_USER_DOB);
                editor.commit();
                */
                if(loginLogoffListener != null){
                	loginLogoffListener.loginLogoffCompleted();
                }
            }

        })
        .setNegativeButton(R.string.confirm_no, null)
        .show();
		
		

	}
	
	private void hideDialogBox(){
		if (progressDialog != null){
			Log.i("AAP", "Progresss Dialog was runnign so cancelled it");
			progressDialog.cancel();
		}

	}
	
	public boolean isUserRegistered(Context context){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getBoolean(PreferenceNameUtil.REGISTERED_USER_IS_REGISTERED, false);
	}
	public void setUserRegistered(Context context, Long userId, RegisterUserProfile registerUserProfile){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PreferenceNameUtil.REGISTERED_USER_IS_REGISTERED, true);
        editor.putLong(PreferenceNameUtil.USER_ID, userId);
        editor.putString(PreferenceNameUtil.REGISTERED_USER_FULL_NAME, registerUserProfile.getName());
        editor.putString(PreferenceNameUtil.REGISTERED_USER_EMAIL, registerUserProfile.getEmail());
        editor.putString(PreferenceNameUtil.REGISTERED_USER_GENDER, registerUserProfile.getGender());
        editor.putString(PreferenceNameUtil.REGISTERED_USER_DOB, registerUserProfile.getBirthday());
        editor.putString(PreferenceNameUtil.REGISTERED_USER_MOBILE, registerUserProfile.getMobile());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_LIVING_STATE_ID, registerUserProfile.getLivingStateId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_LIVING_DISTRICT_ID, registerUserProfile.getLivingDistrictId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_LIVING_AC_ID, registerUserProfile.getLivingAcId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_LIVING_PC_ID, registerUserProfile.getLivingPcId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_VOTING_STATE_ID, registerUserProfile.getVotingStateId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_VOTING_DISTRICT_ID, registerUserProfile.getVotingDistrictId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_VOTING_AC_ID, registerUserProfile.getVotingAcId());
        editor.putLong(PreferenceNameUtil.REGISTERED_USER_VOTING_PC_ID, registerUserProfile.getVotingPcId());
        editor.commit();
	}
	public String getRegisteredUserFullName(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.REGISTERED_USER_FULL_NAME, "");
	}
	public String getRegisteredUserEmail(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.REGISTERED_USER_EMAIL, "");
	}
	public String getRegisteredUserGender(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.REGISTERED_USER_GENDER, "");
	}
	public Date getRegisteredUserDateOfiorth(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		long dobInMilli = prefs.getLong(PreferenceNameUtil.REGISTERED_USER_DOB, 0l);
		if(dobInMilli == 0){
			return null;
		}
		return new Date(dobInMilli);
	}
	public String getRegisteredUserMobile(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.REGISTERED_USER_MOBILE, "");
	}
	public String getDateOfBirth(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getString(PreferenceNameUtil.REGISTERED_USER_DOB, "");
	}
	public Long getLivingStateId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_LIVING_STATE_ID, 0);
	}
	public Long getLivingDistrictId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_LIVING_DISTRICT_ID, 0);
	}
	public Long getLivingAssemblyConstituencyId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_LIVING_AC_ID, 0);
	}
	public Long getLivingParliamentConstituencyId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_LIVING_PC_ID, 0);
	}
	
	public Long getVotingStateId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_VOTING_STATE_ID, 0);
	}
	public Long getVotingDistrictId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_VOTING_DISTRICT_ID, 0);
	}
	public Long getVotingAssemblyConstituencyId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_VOTING_AC_ID, 0);
	}
	public Long getVotingParliamentConstituencyId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.REGISTERED_USER_VOTING_PC_ID, 0);
	}
	public Long getUserId(){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		return prefs.getLong(PreferenceNameUtil.USER_ID, 0);
	}
	public void setUserId(Long userId){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
        setUserId(editor, userId);
        editor.commit();
	}
	public void setUserId(SharedPreferences.Editor editor, Long userId){
		Log.i("AAP", "Editor="+editor +", "+userId);
        editor.putLong(PreferenceNameUtil.USER_ID, userId);
	}
	@Override
	public void onSuccessfullFacebookRegistration(
			FacebookUserRegisteredMessage facebookUserRegisteredMessage) {
		try{
			if(TaskStatus.STATUS_COMPLETED.equals(facebookUserRegisteredMessage.getStatus())){
				Log.i("AAP", "Saving User id "+facebookUserRegisteredMessage.getUserId() +" in cache");
				SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				setUserId(facebookUserRegisteredMessage.getUserId());
				editor.putBoolean(PreferenceNameUtil.FACEBOOK_PROFILE_SERVER_REGISTERED, true);
		        editor.commit();
			}
		}catch(Exception ex){
			Log.e("AAP", "failed to register Twitter profile",ex);
		}
		
	}
	@Override
	public void onSuccessfullTwitterRegistration(
			TwitterUserRegisteredMessage twitterUserRegisteredMessage) {
		try{
			if(TaskStatus.STATUS_COMPLETED.equals(twitterUserRegisteredMessage.getStatus())){
				Log.i("AAP", "Saving User id "+twitterUserRegisteredMessage.getUserId() +" in cache");
				SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				setUserId(twitterUserRegisteredMessage.getUserId());
				editor.putBoolean(PreferenceNameUtil.TWITTER_PROFILE_SERVER_REGISTERED, true);
		        editor.commit();
			}
		}catch(Exception ex){
			Log.e("AAP", "failed to register Twitter profile",ex);
		}
		
	}
	@Override
	public void onSuccessfullGoogleRegistration(
			GoogleUserRegisteredMessage googleUserRegisteredMessage) {
		try{
			if(TaskStatus.STATUS_COMPLETED.equals(googleUserRegisteredMessage.getStatus())){
				Log.i("AAP", "Saving User id "+googleUserRegisteredMessage.getUserId() +" in cache");
				SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.USER_LOGIN_PREF, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = prefs.edit();
				setUserId(googleUserRegisteredMessage.getUserId());
				editor.putBoolean(PreferenceNameUtil.GOOGLE_PROFILE_SERVER_REGISTERED, true);
		        editor.commit();
			}
		}catch(Exception ex){
			Log.e("AAP", "failed to register google profile",ex);
		}
		
	}
	
}
