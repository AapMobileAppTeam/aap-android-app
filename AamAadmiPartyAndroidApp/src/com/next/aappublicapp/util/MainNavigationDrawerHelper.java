package com.next.aappublicapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.next.aappublicapp.BlogListActivity;
import com.next.aappublicapp.FacebookFeedActivity;
import com.next.aappublicapp.JanLokpalListActivity;
import com.next.aappublicapp.LoginActivity;
import com.next.aappublicapp.NewsListActivity;
import com.next.aappublicapp.R;
import com.next.aappublicapp.SwarajListActivity;
import com.next.aappublicapp.UserProfileActivity;
import com.next.aappublicapp.VideoListActivity;

public class MainNavigationDrawerHelper {
	private static final String TAG = "NavigationDrawerHelper";

	public static final int OPTION_NONE = -1;
	public static final int OPTION_NEWS = 0;
	public static final int OPTION_VIDEOS = 1;
	public static final int OPTION_BLOGS = 2;
	public static final int OPTION_DONATE = 3;
	public static final int OPTION_FACEBOOK = 4;
	public static final int OPTION_SUPPORT = 5;
	public static final int OPTION_SWARAJ = 6;
	public static final int OPTION_JANLOKPAL = 7;
	public static final int OPTION_ACCOUNTS = 8;
	public static final int OPTION_PROFILE = 9;

	private static String[] optionNames = new String[] { "News", "Videos",
			"Blogs", "Donate", "Facebook", "Support", "Swaraj ebook",
			"JanLokpal ebook", "Accounts", "Profile" };

	public static int[] optionIcons = new int[] { R.drawable.ic_news,
			R.drawable.ic_videos, R.drawable.ic_blogs, R.drawable.ic_donate,
			R.drawable.ic_fb_connected, R.drawable.ic_support, R.drawable.ic_ebook,
			R.drawable.ic_ebook, R.drawable.ic_accounts, R.drawable.ic_accounts };

	private static ListView menuDrawerList;

	public static void refreshDrawer(final Activity context) {
		if (menuDrawerList != null) {
			menuDrawerList.setAdapter(new NDAdapter(context, OPTION_NONE));
		}
	}

	public static void initializeDrawer(final Activity context,
			ListView mDrawerList, int curOption) {
		menuDrawerList = mDrawerList;
		mDrawerList.setDivider(new ColorDrawable(Color.parseColor("#666666")));
		mDrawerList.setDividerHeight(2);
		mDrawerList.setVerticalScrollBarEnabled(false);
		// set the current screen;
		mDrawerList.setTag(curOption);
		// Set the adapter for the list view
		mDrawerList.setAdapter(new NDAdapter(context, curOption));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Toast.makeText(context, "clicked - " + position,
				// Toast.LENGTH_LONG).show();
				int curOption = (Integer) parent.getTag();
				if (position == curOption)
					return;// ignore

				switch (position) {
				case OPTION_NEWS:
					Intent newsListActivity = new Intent(context,
							NewsListActivity.class);
					context.startActivity(newsListActivity);
					break;
				case OPTION_VIDEOS:
					Intent openVideos = new Intent(context,
							VideoListActivity.class);
					context.startActivity(openVideos);
					break;
				case OPTION_BLOGS:
					Intent openBlogs = new Intent(context,
							BlogListActivity.class);
					context.startActivity(openBlogs);
					break;
				case OPTION_FACEBOOK:
					Intent openFacebookIntent = new Intent(context,
							FacebookFeedActivity.class);
					context.startActivity(openFacebookIntent);
					break;
				case OPTION_DONATE:
					DialogUtil.showErrorMessage(context, "Currently you can not donate using mobile. For donation please visit http://donate.aamaadmiparty.org");
					//FacebookUtil.shareAppWithFriend(context);
				break;
				case OPTION_SUPPORT :
					DialogUtil.showErrorMessage(context, "Under Construction");
				break;
				/*
				 * case OPTION_DONATE: String url =
				 * "https://donate.aamaadmiparty.org/"; Intent i = new
				 * Intent(Intent.ACTION_VIEW); i.setData(Uri.parse(url));
				 * context.startActivity(i); break; case OPTION_DELHI: Intent
				 * startDelhi = new Intent(context,
				 * DelhiElectionActivity.class);
				 * startDelhi.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				 * context.startActivity(startDelhi); break; case
				 * OPTION_SUPPORT: // TODO: break;
				 */
				case OPTION_SWARAJ:
					Intent startSwaraj = new Intent(context,
							SwarajListActivity.class);
					context.startActivity(startSwaraj);
					break;
				case OPTION_JANLOKPAL:
					Intent startLokpal = new Intent(context,
							JanLokpalListActivity.class);
					context.startActivity(startLokpal);
					break;
				case OPTION_ACCOUNTS:
					Intent startLogin = new Intent(context, LoginActivity.class);
					startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_START,
							false);
					context.startActivity(startLogin);
					break;
				case OPTION_PROFILE:
					if(UserSessionManager.getInstance().isUserLoggedInOnAnyService()){
						Intent userProfileIntent = new Intent(context,
								UserProfileActivity.class);
						userProfileIntent.putExtra(
								LoginActivity.INTENT_EXTRA_FROM_START, false);
						context.startActivity(userProfileIntent);
						
					}else{
						Intent userProfileIntent = new Intent(context,
								LoginActivity.class);
						userProfileIntent.putExtra(
								LoginActivity.INTENT_EXTRA_FROM_START, true);
						context.startActivity(userProfileIntent);
					}
					break;
				}

			}
		});
	}

	static class NDAdapter extends BaseAdapter {

		LayoutInflater inflater;
		int curOption;

		public NDAdapter(Context context, int curOption) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.curOption = curOption;
		}

		@Override
		public int getCount() {
			return optionNames.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_slider, parent,
						false);
			}
			TextView title = (TextView) convertView
					.findViewById(R.id.news_title);
			ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
			if (position == OPTION_ACCOUNTS) {
				ImageView fbIcon = (ImageView) convertView
						.findViewById(R.id.fbIcon);
				ImageView twitterIcon = (ImageView) convertView
						.findViewById(R.id.twitterIcon);
				ImageView googleIcon = (ImageView) convertView
						.findViewById(R.id.googleIcon);
				fbIcon.setVisibility(View.VISIBLE);
				twitterIcon.setVisibility(View.VISIBLE);
				googleIcon.setVisibility(View.VISIBLE);
				UserSessionManager userSessionManager = UserSessionManager
						.getInstance();
				if (userSessionManager.isLoggedOntoFacebook()) {
					fbIcon.setImageResource(R.drawable.ic_fb_connected);
				}
				if (userSessionManager.isLoggedOntoTwitter()) {
					twitterIcon
							.setImageResource(R.drawable.ic_twitter_connected);
				}
				if (userSessionManager.isLoggedOntoGoogle()) {
					googleIcon.setImageResource(R.drawable.ic_google_connected);
				}
			}
			/*
			 * Log.i("AAP", "position="+position); if(position ==
			 * OPTION_PROFILE){ Log.i("AAP", "OPTION_PROFILE"+position);
			 * NetworkImageView profilePicIcon = (NetworkImageView)
			 * convertView.findViewById(R.id.profile_pic_image);
			 * UserSessionManager userSessionManager =
			 * UserSessionManager.getInstance();
			 * if(userSessionManager.isLoggedOntoFacebook()){ Log.i("AAP",
			 * "User Logged on to Facebook");
			 * profilePicIcon.setVisibility(View.VISIBLE);
			 * profilePicIcon.setImageUrl
			 * (userSessionManager.getFacebookProfilePicSmall(),
			 * VolleyUtil.getInstance().getImageLoader()); }else{
			 * if(userSessionManager.isLoggedOntoTwitter()){ Log.i("AAP",
			 * "User Logged on to Twitter"); String twitterProfilePic =
			 * userSessionManager.getTwitterProfilePic(); if(twitterProfilePic
			 * != null){ profilePicIcon.setVisibility(View.VISIBLE);
			 * profilePicIcon.setImageUrl(twitterProfilePic,
			 * VolleyUtil.getInstance().getImageLoader()); } }else{
			 * if(userSessionManager.isLoggedOntoGoogle()){ String
			 * googleProfilePic = userSessionManager.getGoogleProfilePic();
			 * if(googleProfilePic != null){
			 * profilePicIcon.setVisibility(View.VISIBLE);
			 * profilePicIcon.setImageUrl(googleProfilePic,
			 * VolleyUtil.getInstance().getImageLoader()); } } } }
			 * 
			 * }
			 */

			title.setText(optionNames[position]);
			icon.setImageResource(optionIcons[position]);

			if (position == curOption) {
				convertView.setBackgroundColor(Color.parseColor("#666666"));
			} else {
				convertView
						.setBackgroundResource(android.R.drawable.list_selector_background);
			}
			return convertView;
		}
	}

}
