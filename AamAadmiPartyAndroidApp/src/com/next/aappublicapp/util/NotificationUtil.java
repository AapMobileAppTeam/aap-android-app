package com.next.aappublicapp.util;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;

import com.next.aappublicapp.AapBroadcastReceiver;

public class NotificationUtil {

	public static void clearAllNotifications(Activity activity) {
		NotificationManager mNotificationManager = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancelAll();

	}
	public static void clearFacebookNotification(Activity activity) {
		NotificationManager mNotificationManager = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(AapBroadcastReceiver.FB_NOTIFICATION_ID);
	}

}
