package com.next.aappublicapp.util;

/**
 * 
 * @author ravi
 *
 */
public class PreferenceNameUtil {

	public static final String APP_INTERNAL_SETTINGS = "APP_INTERNAL_SETTINGS";
	public static final String TOTAL_TIMES_APP_STARTED = "TOTAL_TIMES_APP_STARTED";
	public static final String TOTAL_TIMES_APP_STARTED_FOR_LOGIN = "TOTAL_TIMES_APP_STARTED_FOR_LOGIN";
	public static final String TOTAL_TIMES_APP_STARTED_FOR_GCM = "TOTAL_TIMES_APP_STARTED_FOR_GCM";
	public static final String LAST_APP_USAGE_TIME = "LAST_APP_USAGE_TIME";
	public static final String LAST_APP_USAGE_PREV_TIME = "LAST_APP_USAGE_PREV_TIME";

	
	public static final String GCM_REGISTRATION_PREF = "GCM_REGISTRATION_PREF";
	public static final String PROPERTY_REG_ID = "PROPERTY_REG_ID";
	public static final String PROPERTY_REG_ID_REGISTRATION_STATUS = "PROPERTY_REG_ID_REGISTRATION_STATUS";
	public static final String REGISTRATION_STATUS_YES = "YES";

	public static final String USER_LOGIN_PREF = "USER_LOGIN_PREF";
	//ID of user, a number
	public static final String FACEBOOK_USER_OBJECT_ID = "FACEBOOK_USER_OBJECT_ID";
	//String id of user like ping2ravi
	public static final String FACEBOOK_USER_NAME = "FACEBOOK_USER_NAME";
	//Name of User, i.e Ravi Sharma
	public static final String FACEBOOK_USER_FULL_NAME = "FACEBOOK_USER_FULL_NAME";
	//Facebook User email
	public static final String FACEBOOK_USER_EMAIL = "FACEBOOK_USER_EMAIL";
	//Facebook User Date of Birth
	public static final String FACEBOOK_USER_DOB = "FACEBOOK_USER_DOB";
	//Facebook Profile Pic
	public static final String FACEBOOK_PROFILE_PIC = "FACEBOOK_PROFILE_PIC";
	//Facebook User Gender
	public static final String FACEBOOK_USER_GENDER = "FACEBOOK_USER_GENDER";
	//Facebook Profile registered at Server
	public static final String FACEBOOK_PROFILE_SERVER_REGISTERED = "FACEBOOK_PROFILE_SERVER_REGISTERED";
	
	//ID of user, a number
	public static final String TWITTER_USER_OBJECT_ID = "TWITTER_USER_OBJECT_ID";
	//String handle of user like xs22ravi
	public static final String TWITTER_HANDLE = "TWITTER_HANDLE";
	//String handle of user like xs22ravi
	public static final String TWITTER_PROFILE_PIC = "TWITTER_PROFILE_PIC";
	//Name of User, i.e Ravi Sharma
	public static final String TWITTER_USER_FULL_NAME = "TWITTER_USER_FULL_NAME";
	
	public static final String TWITTER_TOKEN = "TWITTER_TOKEN";
	
	public static final String TWITTER_TOKEN_SECRET = "TWITTER_TOKEN_SECRET";
	
	public static final String TWITTER_IS_LOGGED_IN = "TWITTER_IS_LOGGED_IN";

	//Twitter Profile registered at Server
	public static final String TWITTER_PROFILE_SERVER_REGISTERED = "TWITTER_PROFILE_SERVER_REGISTERED";

	
	public static final String REGISTERED_USER_FULL_NAME = "REGISTERED_USER_FULL_NAME";
	
	public static final String REGISTERED_USER_EMAIL = "REGISTERED_USER_EMAIL";
	
	public static final String REGISTERED_USER_GENDER = "REGISTERED_USER_GENDER";
	
	public static final String REGISTERED_USER_DOB = "REGISTERED_USER_DOB";
	
	public static final String REGISTERED_USER_MOBILE = "REGISTERED_USER_MOBILE";
	
	public static final String REGISTERED_USER_LIVING_STATE_ID = "REGISTERED_USER_LIVING_STATE_ID";
	
	public static final String REGISTERED_USER_LIVING_DISTRICT_ID = "REGISTERED_USER_LIVING_DISTRICT_ID";
	
	public static final String REGISTERED_USER_LIVING_AC_ID = "REGISTERED_USER_LIVING_AC_ID";
	
	public static final String REGISTERED_USER_LIVING_PC_ID = "REGISTERED_USER_LIVING_PC_ID";
	
	public static final String REGISTERED_USER_VOTING_STATE_ID = "REGISTERED_USER_VOTING_STATE_ID";
	
	public static final String REGISTERED_USER_VOTING_DISTRICT_ID = "REGISTERED_USER_VOTING_DISTRICT_ID";
	
	public static final String REGISTERED_USER_VOTING_AC_ID = "REGISTERED_USER_VOTING_AC_ID";
	
	public static final String REGISTERED_USER_VOTING_PC_ID = "REGISTERED_USER_VOTING_PC_ID";
	
	public static final String REGISTERED_USER_IS_REGISTERED = "REGISTERED_USER_IS_REGISTERED";
	
	public static final String USER_ID = "USER_ID";

	//App content Preferences
	public static final String APP_CONTENT_PREF = "APP_CONTENT_PREF";
	
	public static final String HOME_NEWS_ITEMS = "HOME_NEWS_ITEMS";
	
	public static final String HOME_VIDEO_ITEMS = "HOME_VIDEO_ITEMS";
	
	public static final String HOME_BLOG_ITEMS = "HOME_BLOG_ITEMS";
	
	public static final String HOME_EVENT_ITEMS = "HOME_EVENT_ITEMS";
	
	
	//ID of user, a number
	public static final String GOOGLE_USER_OBJECT_ID = "GOOGLE_USER_OBJECT_ID";
	//String email of user like abc@gmail.com
	public static final String GOOGLE_EMAIL = "GOOGLE_EMAIL";
	//String profile pic url of user
	public static final String GOOGLE_PROFILE_PIC = "GOOGLE_PROFILE_PIC";
	//Name of User, i.e Ravi Sharma
	public static final String GOOGLE_USER_FULL_NAME = "GOOGLE_USER_FULL_NAME";
	//Name of User, i.e dd-mm-yyyy
	public static final String GOOGLE_USER_DOB = "GOOGLE_USER_DOB";
	//Gender of User, i.e dd-mm-yyyy
	public static final String GOOGLE_USER_GENDER = "GOOGLE_USER_GENDER";
	//Gender of User, i.e dd-mm-yyyy
	public static final String GOOGLE_USER_AUTH_TOKEN = "GOOGLE_USER_AUTH_TOKEN";
	
	public static final String GOOGLE_IS_LOGGED_IN = "GOOGLE_IS_LOGGED_IN";
	//Google Profile registered at Server
	public static final String GOOGLE_PROFILE_SERVER_REGISTERED = "GOOGLE_PROFILE_SERVER_REGISTERED";
	
	
	public static final String FACEBOO_RESPONSE_PREF = "FACEBOO_RESPONSE_PREF";
	
	public static final String BOOK_SWARAJ_PREF = "BOOK_SWARAJ_PREF";
	
	public static final String BOOK_JANLOKPAL_PREF = "BOOK_JANLOKPAL_PREF";
}
