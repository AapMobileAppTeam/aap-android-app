package com.next.aappublicapp.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

/**
 * 
 * Writes/reads an object to/from a private local file
 * 
 * 
 */
public class LocalPersistence {

	/**
	 * 
	 * @param context
	 * @param object
	 * @param filename
	 */
	public static void witeObjectToFile(Context context, Object object,
			String filename) {

		ObjectOutputStream objectOut = null;
		try {

			FileOutputStream fileOut = context.openFileOutput(filename,
					Activity.MODE_PRIVATE);
			objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(object);
			fileOut.getFD().sync();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (objectOut != null) {
				try {
					objectOut.close();
				} catch (IOException e) {
					// do nowt
				}
			}
		}
	}

	/**
	 * 
	 * @param context
	 * @param filename
	 * @return
	 */
	public static Object readObjectFromFile(Context context, String filename) {

		ObjectInputStream objectIn = null;
		Object object = null;
		try {

			FileInputStream fileIn = context.getApplicationContext()
					.openFileInput(filename);
			objectIn = new ObjectInputStream(fileIn);
			object = objectIn.readObject();

		} catch (FileNotFoundException e) {
			// Do nothing
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (objectIn != null) {
				try {
					objectIn.close();
				} catch (IOException e) {
					// do nowt
				}
			}
		}

		return object;
	}

	
	public static void saveIntoLocalCache(Context context, String preferenceName, String key, Serializable object){
		try{
			Gson gson = new Gson();
			String response = gson.toJson(object);
			SharedPreferences prefs = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
	        SharedPreferences.Editor editor = prefs.edit();
	        editor.putString(key, response);
	        editor.commit();
			
		}catch(Exception ex){
			Log.e("AAP", "Unable to save Facebook Response in Local cache", ex);
		}
		//LocalPersistence.witeObjectToFile(context, object, filename);
	}
	
	public static <T> T getFromLocalCache(Context context, String preferenceName,String key, Class<T> cls){
		try{
			Gson gson = new Gson();
			SharedPreferences prefs = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
	        String response = prefs.getString(key, null);
	        if(response == null){
	        	return null;
	        }
	        return gson.fromJson(response, cls);
			
		}catch(Exception ex){
			Log.e("AAP", "Unable to save Facebook Response in Local cache", ex);
		}
		return null;
	}
	
	public static <T> T getFromLocalCache(Context context, String preferenceName,String key, Type cls){
		try{
			Gson gson = new Gson();
			SharedPreferences prefs = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
	        String response = prefs.getString(key, null);
	        if(response == null){
	        	return null;
	        }
	        return gson.fromJson(response, cls);
			
		}catch(Exception ex){
			Log.e("AAP", "Unable to save Facebook Response in Local cache", ex);
		}
		return null;
	}
}
