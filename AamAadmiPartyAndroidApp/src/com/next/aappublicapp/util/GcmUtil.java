package com.next.aappublicapp.util;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.listeners.OnSuccesfullDeviceRegistrationListener;
import com.next.aappublicapp.server.services.AapServerServices;

public class GcmUtil {

	private static String GCM_SENDER_ID = "646798908058";
	
	/**
	 * Get device RegistrationId from Cache
	 * @param prefs
	 * @return
	 */
	public static String getGcmDeviceRegistrationId(Context context){
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.GCM_REGISTRATION_PREF, Context.MODE_PRIVATE);
		return getGcmDeviceRegistrationId(prefs);
	}
	/**
	 * Get device RegistrationId from Cache
	 * @param prefs
	 * @return
	 */
	public static String getGcmDeviceRegistrationId(SharedPreferences prefs){
		return prefs.getString(PreferenceNameUtil.PROPERTY_REG_ID, null);
	}
	public static String getGcmDeviceRegistrationStatus(SharedPreferences prefs){
		return prefs.getString(PreferenceNameUtil.PROPERTY_REG_ID_REGISTRATION_STATUS, null);
	}
	public static String getOrCreateDeviceRegistrationId(Context context){
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		String newRegistrationId = null;
		try {
			newRegistrationId = gcm.register(GCM_SENDER_ID);
		} catch (IOException e) {
			Log.e("AAP", "Unable to get GCM ID",e);
		}
		return newRegistrationId;
	}
	public static String getDeviceType(){
		return "Android";
	}
	public static void ensureDeviceIsRegistered(final Context context){
		new AsyncTask<Object, Boolean, Boolean>() {

			@Override
			protected Boolean doInBackground(Object... params) {
				// Make sure the app is registered with GCM and with the server
				SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.GCM_REGISTRATION_PREF, Context.MODE_PRIVATE);
		        
				String registrationStatus = getGcmDeviceRegistrationStatus(prefs);
				Log.i("AAP", "PROPERTY_REG_ID_REGISTRATION_STATUS="+registrationStatus);
				//Which we have registered to google and our server last time
				String existingRegistrationId = getGcmDeviceRegistrationId(prefs);
				Log.i("AAP", "existingRegistrationId="+existingRegistrationId);
				//Registration Id can change so make sure if it has changed we re register it
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
				String newRegistrationId = null;
				try {
					newRegistrationId = gcm.register(GCM_SENDER_ID);
				} catch (IOException e) {
					Log.e("AAP", "Unable to get GCM ID",e);
				}
				Log.i("AAP", "newRegistrationId="+newRegistrationId);
				
				if(registrationStatus != null && PreferenceNameUtil.REGISTRATION_STATUS_YES.equals(registrationStatus)){
					//Device RegId already registered with google and our server
					Log.i("AAP", "Device already registered with google and our server");
					//Now check if App has been used N times so that we can reregister GCM Id or if GCM Id has been changed
					if(existingRegistrationId != null && newRegistrationId!= null && existingRegistrationId.equals(newRegistrationId)){
						//There is no change in Registration id so just return and dont call server
						Log.i("AAP", "New and exiting device ids are same, wo will not reregister");
						return false;
					}
					if(newRegistrationId == null){
						//registration id returned by google is null, so we can not do anything
						Log.i("AAP", "New device id is null, wo will not reregister");
						return false;
					}
				}
				
				Log.i("AAP", "Registering device id at server");
				registerDeviceOnServer(context, newRegistrationId, existingRegistrationId);
				return true;
			}
		}.execute(null,null);
		
	}
	
	private static void registerDeviceOnServer(final Context context,final String deviceRegId, final String previousRegistrationId){
		try{
			Log.i("Ravi", "Calling DataService to register Device on Server");
			OnSuccesfullDeviceRegistrationListener onSuccesfullDeviceRegistrationListener = new OnSuccesfullDeviceRegistrationListener(){
				@Override
				public void onSuccesfullDeviceRegistration(RegisterUserDeviceResponse registerUserDeviceResponse) {
					deviceRegisteredSuccesfully(context, deviceRegId);
				}
			};
			OnServiceFailListener onServiceFailListener = new OnServiceFailListener(){
				@Override
				public void onServiceFail(String message) {
				}
			};
        	AapServerServices.getInstance().registerDevice(context, deviceRegId, onSuccesfullDeviceRegistrationListener, onServiceFailListener);
        }catch(Exception ex){
        	Log.e("AAP", ex.getMessage(), ex);
        }
	}
	
	public static void deviceRegisteredSuccesfully(Context context,String deviceRegId){
		Log.i("AAP", "deviceRegisteredSuccesfully = "+deviceRegId);
		SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.GCM_REGISTRATION_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PreferenceNameUtil.PROPERTY_REG_ID_REGISTRATION_STATUS, PreferenceNameUtil.REGISTRATION_STATUS_YES);
        editor.putString(PreferenceNameUtil.PROPERTY_REG_ID, deviceRegId);
        editor.commit();
	}

}
