package com.next.aappublicapp.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * 
 * @author ravi
 *
 */
public class VolleyUtil {

	private RequestQueue requestQueue;
	private ImageLoader imageLoader;
	private boolean isInitialized = false;
	private static VolleyUtil volleyUtilInstance = new VolleyUtil();

	public void initilize(Context context){
		requestQueue = Volley.newRequestQueue(context);
		imageLoader = new ImageLoader(requestQueue, new BitmapLruCache());
		isInitialized = true;
	}
	
	public static VolleyUtil getInstance(){
		return volleyUtilInstance;
	}
	
	public RequestQueue getRequestQueue() {
		if(!isInitialized){
			Log.e("AAP", "Volley Util has not been initialized");
			return null;
		}
		return requestQueue;
	}

	public ImageLoader getImageLoader() {
		if(!isInitialized){
			Log.e("AAP", "Volley Util has not been initialized");
			return null;
		}
		return imageLoader;
	}


}
