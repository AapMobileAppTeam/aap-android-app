package com.next.aappublicapp.util;

import android.content.Context;

public class ClientConfig {

	private static final int MAX_APP_USAGE_TO_REREGISTER_FOR_GCM = 50;
	private static final int SPLASH_DURATION = 1000;//Milliseconds
	
	
	public static int getMaxAppUsageToReRegistaerGcmId(Context context){
		return MAX_APP_USAGE_TO_REREGISTER_FOR_GCM;
	}
	public static int getSplashDuration(Context context){
		return SPLASH_DURATION;
	}

}
