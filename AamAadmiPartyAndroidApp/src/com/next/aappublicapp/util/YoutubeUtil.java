package com.next.aappublicapp.util;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.next.aap.dto.VideoItem;

public class YoutubeUtil {

	public static final String DEVELOPER_KEY = "AIzaSyC5zoNmY0IP-pe-hTBpVyeafXObMqdNeH8";
	
	public static final int REQ_START_STANDALONE_PLAYER = 1;
	public static final int REQ_RESOLVE_SERVICE_MISSING = 2;

	public static void playYoutubeVideo(Context context,VideoItem videoItem) {
		Intent lVideoIntent = YouTubeStandalonePlayer.createVideoIntent(
				(Activity) context, DEVELOPER_KEY, videoItem.getYoutubeVideoId(),
				0, true, false);
		if (lVideoIntent != null) {
			if (canResolveIntent(context,lVideoIntent)) {
				((Activity) context).startActivityForResult(lVideoIntent,
						REQ_START_STANDALONE_PLAYER);
			} else {
				// Could not resolve the intent - must need to install or
				// update the YouTube? API service.
				YouTubeInitializationResult.SERVICE_MISSING.getErrorDialog(
						(Activity) context, REQ_RESOLVE_SERVICE_MISSING)
						.show();
			}
		}
	}
	
	public static void playYoutubeVideo(Context context,String videoId) {
		Intent lVideoIntent = YouTubeStandalonePlayer.createVideoIntent(
				(Activity) context, DEVELOPER_KEY, videoId,
				0, true, false);
		if (lVideoIntent != null) {
			if (canResolveIntent(context,lVideoIntent)) {
				((Activity) context).startActivityForResult(lVideoIntent,
						REQ_START_STANDALONE_PLAYER);
			} else {
				// Could not resolve the intent - must need to install or
				// update the YouTube? API service.
				YouTubeInitializationResult.SERVICE_MISSING.getErrorDialog(
						(Activity) context, REQ_RESOLVE_SERVICE_MISSING)
						.show();
			}
		}
	}
	
	public static boolean canResolveIntent(Context context, Intent intent) {
		List<ResolveInfo> resolveInfo = context.getPackageManager()
				.queryIntentActivities(intent, 0);
		return resolveInfo != null && !resolveInfo.isEmpty();
	}

}
