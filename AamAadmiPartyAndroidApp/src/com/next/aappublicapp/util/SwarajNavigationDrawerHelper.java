package com.next.aappublicapp.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.next.aap.dto.SwarajChapter;
import com.next.aappublicapp.R;
import com.next.aappublicapp.SwarajActivity;

public class SwarajNavigationDrawerHelper {
	private static final String TAG = "NavigationDrawerHelper";

	private List<String> optionNames;
	private int selectedOption = -1;

	private ListView menuDrawerList;
	private List<SwarajChapter> swarajChapters;
	private SwarajBookMenuAdapter swarajBookMenuAdapter;

	private static SwarajNavigationDrawerHelper instance = new SwarajNavigationDrawerHelper();
	Activity context;

	private SwarajNavigationDrawerHelper() {
		
	}

	public static SwarajNavigationDrawerHelper getSingletonInstance() {
		return instance;
	}

	public void refreshDrawer(final Activity context) {
		if (menuDrawerList != null) {
			initSwarajBookMenuAdapter(context, selectedOption);
			menuDrawerList.setAdapter(swarajBookMenuAdapter);
		}

	}
	private void initSwarajBookMenuAdapter(final Activity context,int curOption){
		swarajBookMenuAdapter = new SwarajBookMenuAdapter(context, curOption);
	}

	public void setSwarajChapters(List<SwarajChapter> swarajChapters) {
		this.swarajChapters = swarajChapters;
	}

	private void createOptions(List<SwarajChapter> swarajChapters) {
		optionNames = new ArrayList<String>(swarajChapters.size());
		for (SwarajChapter oneSwarajChapter : swarajChapters) {
			optionNames.add(oneSwarajChapter.getTitle());
		}
	}

	public void initializeDrawer(final Activity context, ListView mDrawerList,
			int curOption) {
		initializeDrawer(context, mDrawerList, swarajChapters, curOption);
	}

	public void initializeDrawer(final Activity context, ListView mDrawerList,
			List<SwarajChapter> swarajChapters, int curOption) {
		this.swarajChapters = swarajChapters;
		menuDrawerList = mDrawerList;
		this.context = context;

		createOptions(swarajChapters);
		initSwarajBookMenuAdapter(context, selectedOption);

		mDrawerList.setDivider(new ColorDrawable(Color.parseColor("#666666")));
		mDrawerList.setDividerHeight(2);
		mDrawerList.setVerticalScrollBarEnabled(false);
		// set the current screen;
		mDrawerList.setTag(curOption);
		// Set the adapter for the list view
		mDrawerList.setAdapter(swarajBookMenuAdapter);
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent swarajIntent = new Intent(context, SwarajActivity.class);
				SwarajChapter oneSwarajChapter = SwarajNavigationDrawerHelper.this.swarajChapters.get(position);
				swarajIntent.putExtra(SwarajActivity.INTENT_SWARAJ_CHAPTER, oneSwarajChapter);
				swarajIntent.putExtra(SwarajActivity.INTENT_SWARAJ_CHAPTER_POSITION, position);
				context.finish();
				context.startActivity(swarajIntent);

				// Toast.makeText(context, "clicked - " + position,
				// Toast.LENGTH_LONG).show();
				int curOption = (Integer) parent.getTag();
				if (position == curOption)
					return;// ignore

			}
		});
	}

	class SwarajBookMenuAdapter extends BaseAdapter {

		LayoutInflater inflater;
		int currentOption;

		public SwarajBookMenuAdapter(Context context, int curOption) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.currentOption = curOption;
		}

		@Override
		public int getCount() {
			Log.i("AAP", "Size = " + optionNames.size());
			return optionNames.size();
		}

		@Override
		public Object getItem(int position) {
			return optionNames.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.swaraj_slider, parent,
						false);
			}
			TextView title = (TextView) convertView
					.findViewById(R.id.news_title);
			ImageView icon = (ImageView) convertView.findViewById(R.id.icon);

			Log.i("AAP", "position = " + position);
			Log.i("AAP", "Title = " + optionNames.get(position));

			title.setText(optionNames.get(position));
			//title.setTextColor(context.getResources().getColor(R.color.AapOrange));
			 icon.setImageResource(R.drawable.ic_ebook);

			if (position == currentOption) {
				convertView
				.setBackgroundResource(R.drawable.bg_subhead);
				//convertView.setBackgroundColor(Color.parseColor("#666666"));
			} else {
				convertView
						.setBackgroundResource(android.R.drawable.list_selector_background);
			}
			return convertView;
		}

		public int getCurrentOption() {
			return currentOption;
		}

		public void setCurrentOption(int currentOption) {
			this.currentOption = currentOption;
		}
	}

	public int getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(int selectedOption) {
		this.selectedOption = selectedOption;
		swarajBookMenuAdapter.setCurrentOption(selectedOption);
	}

}
