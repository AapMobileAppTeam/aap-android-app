package com.next.aappublicapp.util;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.next.aappublicapp.TwitterLoginActivity;

public class TwitterUtil {

	private static Twitter twitter = null;
	public static void onTweetShare(final Activity context, final String webUrl, final String imageUrl, final String tweetText, final String fullContent) {
		UserSessionManager userSessionManager = UserSessionManager.getInstance();
		
		if (!NetworkUtil.isNetworkConnected(context)) {
			Toast.makeText(context, "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (userSessionManager.isLoggedOntoTwitter()) {
			Log.d("AAP", "posting to twitter::");
			if(twitter == null){
				twitter = new TwitterFactory().getInstance();
				twitter.setOAuthConsumer(TwitterLoginActivity.TWITTER_CONSUMER_KEY, TwitterLoginActivity.TWITTER_CONSUMER_SECRET);
				AccessToken accessToken = new AccessToken(userSessionManager.getTwitterConsumerKey(),userSessionManager.getTwitterConsumerKeySecret());
				twitter.setOAuthAccessToken(accessToken);
			}
			PostTweetTask postTweetTask = new PostTweetTask(context);
			postTweetTask.execute(new String[] { tweetText });
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Tweet").setMessage("Would you like to connect your Twitter account so you can tweet directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(context, TwitterLoginActivity.class);
							context.startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@SuppressLint("InlinedApi")
						public void onClick(DialogInterface dialog, int id) {
							Intent genericShareIntent = new Intent(Intent.ACTION_SEND, Uri.parse(webUrl));
							genericShareIntent.putExtra(Intent.EXTRA_SUBJECT, tweetText);
							genericShareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(fullContent +" "+ webUrl));
							genericShareIntent.putExtra(Intent.EXTRA_HTML_TEXT, fullContent +" "+ webUrl);
							genericShareIntent.setType("text/plain");
							context.startActivity(Intent.createChooser(genericShareIntent, "Select app"));
						}
					});
			builder.create().show();
		}
	}
	
	static class PostTweetTask extends AsyncTask<String, Void, Integer> {
		ProgressDialog tweetProgressDialog;
		Activity context;
		public PostTweetTask(Activity context){
			this.context = context;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			tweetProgressDialog = ProgressDialog.show(context, "Tweeting...", "Please wait while we tweet");
		}

		@Override
		protected Integer doInBackground(String... params) {
			try {
				StatusUpdate statusUpdate = new StatusUpdate(params[0]);
				twitter.updateStatus(statusUpdate);
			} catch (TwitterException e) {
				Log.w("AAP", "error tweeting", e);
				e.printStackTrace();
				Log.w("AAP", e.getMessage());
				return 1;
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			tweetProgressDialog.cancel();
			if (result == 0) {
				Toast.makeText(context, "Tweeted Succefully", Toast.LENGTH_LONG).show();
			}
			if (result == 1) {
				Toast.makeText(context, "Tweeting failed!", Toast.LENGTH_LONG).show();
			}
		}
	}

}
