package com.next.aappublicapp.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.next.aappublicapp.LoginActivity;
import com.next.aappublicapp.R;
import com.next.aappublicapp.listeners.OnFacebookPostLikeSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;

public class FacebookUtil {

	private static boolean pendingPublishReauthorization;
	private static ProgressDialog progressDialog;

	public static void onFacebookShareClick(final Activity context, String webUrl,String imageUrl, final String text) {
		onFacebookShareClick(context, webUrl, imageUrl, text, text);
	}
	public static void onFacebookShareClick(final Activity context, String webUrl,String imageUrl, final String text, final String description) {
		if (!NetworkUtil.isNetworkConnected(context)) {
			Toast.makeText(context, "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (UserSessionManager.getInstance().isLoggedOntoFacebook()) {
			Session session = Session.openActiveSessionFromCache(context);
			
			Log.d("AAP", "posting to facebook "+session.isPublishPermission("publish_actions")+","+session.getPermissions().contains("publish_actions"));
			if(!session.isOpened()){
				Log.d("AAP", "Session was closed so reopening it");
				//session.openActiveSessionFromCache(this);
			}
			if (session != null) {
				// Check for publish permissions
				List<String> permissions = session.getPermissions();
				if (!session.isPublishPermission("publish_actions")) {
					pendingPublishReauthorization = true;
					List<String> newPermissions = new ArrayList<String>();
					newPermissions.add("publish_actions");
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, newPermissions);
					session.requestNewPublishPermissions(newPermissionsRequest);
					Toast.makeText(context, "Please grant permission to post and try again.", Toast.LENGTH_LONG).show();
					Log.d("AAP", "Lets see if you have given permisison "+ session.isPublishPermission("publish_actions"));
					return;
				}
				Log.d("AAP", "we may have permissions, now post to fb");
				if (session.isPublishPermission("publish_actions")){
					Log.d("AAP", "we GOT permissions, now post to fb");
					Bundle postParams = new Bundle();
					postParams.putString("message", text);
					postParams.putString("caption", text);
					postParams.putString("name", "Aam Aadmi Party Android App");
					postParams.putString("description", description);
					postParams.putString("link", webUrl);
					if(imageUrl != null && !imageUrl.trim().equals("")){
						postParams.putString("picture", imageUrl);
					}
					Request.Callback callback = new Request.Callback() {
						public void onCompleted(Response response) {
							if (progressDialog != null)
								progressDialog.cancel();
							FacebookRequestError error = response.getError();
							if (error != null) {
								Toast.makeText(context, "Error posting to Facebook", Toast.LENGTH_SHORT).show();
								Log.i("AAP", "Error posting to Facebook:" + error.getErrorMessage());
								return;
							} else {
								Toast.makeText(context, "Posted to Facebook!", Toast.LENGTH_LONG).show();
							}
						}
					};

					Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
					if (progressDialog != null)
						progressDialog.cancel();
					progressDialog = ProgressDialog.show(context, "Posting to Facebook...", "Please wait while we post your message");
					RequestAsyncTask fbTask = new RequestAsyncTask(request);
					fbTask.execute();
				}else{
					Log.d("AAP", "we DID NOT permissions, now post to fb");
				}
					
				
			}
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Post to Facebook").setMessage("Would you like to connect your Facebook account so you can post directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(context, LoginActivity.class);
							startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_START, false);
							context.startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							Intent genericShareIntent = new Intent(Intent.ACTION_SEND);
							genericShareIntent.putExtra(Intent.EXTRA_TEXT, text);
							genericShareIntent.setType("text/plain");
							context.startActivity(Intent.createChooser(genericShareIntent, "Select app"));
						}
					});
			builder.create().show();
		}
		Log.d("AAP", "All finished");
	}
	
	public static void onFacebookCommentClick(final Activity context, final String facebookObjectUrl) {
		if (!NetworkUtil.isNetworkConnected(context)) {
			Toast.makeText(context, "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (UserSessionManager.getInstance().isLoggedOntoFacebook()) {
			Session session = Session.openActiveSessionFromCache(context);
			
			Log.d("AAP", "posting to facebook "+session.isPublishPermission("publish_actions")+","+session.getPermissions().contains("publish_actions"));
			if(!session.isOpened()){
				Log.d("AAP", "Session was closed so reopening it");
				//session.openActiveSessionFromCache(this);
			}
			if (session != null) {
				// Check for publish permissions
				List<String> permissions = session.getPermissions();
				if (!session.isPublishPermission("publish_actions")) {
					pendingPublishReauthorization = true;
					List<String> newPermissions = new ArrayList<String>();
					newPermissions.add("publish_actions");
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, newPermissions);
					session.requestNewPublishPermissions(newPermissionsRequest);
					Toast.makeText(context, "Please grant permission to post and try again.", Toast.LENGTH_LONG).show();
					Log.d("AAP", "Lets see if you have given permisison "+ session.isPublishPermission("publish_actions"));
					return;
				}
				Log.d("AAP", "we may have permissions, now post to fb");
				if (session.isPublishPermission("publish_actions")){
					Log.d("AAP", "we GOT permissions, now post to fb");
					LayoutInflater li = LayoutInflater.from(context);
					View promptsView = li.inflate(R.layout.comment_prompt, null);

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

					// set prompts.xml to alertdialog builder
					alertDialogBuilder.setView(promptsView);

					final EditText userInput = (EditText) promptsView
							.findViewById(R.id.facebook_comment_text);

					// set dialog message
					alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton("Post",
						  new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog,int id) {
							// get user input and set it to result
							// edit text
						    	String commentUrl = facebookObjectUrl +"&message="+userInput.getText().toString();
						    	String urlWithToken = commentUrl +"&access_token="+UserSessionManager.getInstance().getFBSession().getAccessToken();
						    	AapServerServices.getInstance().commentFacebookPostAsync(context, urlWithToken, null, null);
						    }
						  })
						.setNegativeButton("Cancel",
						  new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						    }
						  });

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}else{
					Log.d("AAP", "we DID NOT permissions, now post to fb");
				}
					
				
			}
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Post to Facebook").setMessage("Would you like to connect your Facebook account so you can post directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(context, LoginActivity.class);
							startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_START, false);
							context.startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			builder.create().show();
		}
		Log.d("AAP", "All finished");
	}
	
	public static void onFacebookLikeClick(final Activity context, final String facebookObjectUrl, 
			final OnFacebookPostLikeSuccessListener onFacebookPostLikeSuccessListener, final OnServiceFailListener onServiceFailListener) {
		if (!NetworkUtil.isNetworkConnected(context)) {
			Toast.makeText(context, "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (UserSessionManager.getInstance().isLoggedOntoFacebook()) {
			Session session = Session.openActiveSessionFromCache(context);
			
			Log.d("AAP", "posting to facebook "+session.isPublishPermission("publish_actions")+","+session.getPermissions().contains("publish_actions"));
			if(!session.isOpened()){
				Log.d("AAP", "Session was closed so reopening it");
				//session.openActiveSessionFromCache(this);
			}
			if (session != null) {
				// Check for publish permissions
				List<String> permissions = session.getPermissions();
				if (!session.isPublishPermission("publish_actions")) {
					pendingPublishReauthorization = true;
					List<String> newPermissions = new ArrayList<String>();
					newPermissions.add("publish_actions");
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(context, newPermissions);
					session.requestNewPublishPermissions(newPermissionsRequest);
					Toast.makeText(context, "Please grant permission to post and try again.", Toast.LENGTH_LONG).show();
					Log.d("AAP", "Lets see if you have given permisison "+ session.isPublishPermission("publish_actions"));
					return;
				}
				Log.d("AAP", "we may have permissions, now post to fb");
				if (session.isPublishPermission("publish_actions")){
					Log.d("AAP", "we GOT permissions, now post to fb");
			    	String urlWithToken = facebookObjectUrl +"&access_token="+UserSessionManager.getInstance().getFBSession().getAccessToken();
					final ProgressDialog progressDialog = ProgressDialog.show(context, "Liking Facebook Post", "Please wait while we like post on facebook");

			    	AapServerServices.getInstance().likeFacebookPostAsync(context, urlWithToken, new OnFacebookPostLikeSuccessListener() {
						
						@Override
						public void onSuccesfullFacebookPostLike() {
							if(progressDialog != null){
								progressDialog.cancel();
							}
							onFacebookPostLikeSuccessListener.onSuccesfullFacebookPostLike();
						}
					}, new OnServiceFailListener() {
						
						@Override
						public void onServiceFail(String message) {
							if(progressDialog != null){
								progressDialog.cancel();
							}
							onServiceFailListener.onServiceFail(message);
						}
					});
				}else{
					Log.d("AAP", "we DID NOT permissions, now post to fb");
				}
					
				
			}
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Post to Facebook").setMessage("Would you like to connect your Facebook account so you can post directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(context, LoginActivity.class);
							startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_START, false);
							context.startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			builder.create().show();
		}
		Log.d("AAP", "All finished");
	}

	public static void shareAppWithFriend(final Activity context) {
	    Bundle params = new Bundle();
	    params.putString("message", "Learn how to make your Android apps social");

	    if (FacebookDialog
				.canPresentShareDialog(context.getApplicationContext(),
						FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
			Log.i("AAP", "Yes Facebook Dialog can be presented");
	    }
	    WebDialog requestsDialog = (
	        new WebDialog.RequestsDialogBuilder(context,
	            Session.getActiveSession(),
	            params))
	            .setOnCompleteListener(new WebDialog.OnCompleteListener() {

	                @Override
	                public void onComplete(Bundle values,
	                    FacebookException error) {
	                    if (error != null) {
	                        if (error instanceof FacebookOperationCanceledException) {
	                            Toast.makeText(context.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(context.getApplicationContext(), 
	                                "Network Error", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    } else {
	                        final String requestId = values.getString("request");
	                        if (requestId != null) {
	                            Toast.makeText(context.getApplicationContext(), 
	                                "Request sent",  
	                                Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(context.getApplicationContext(), 
	                                "Request cancelled", 
	                                Toast.LENGTH_SHORT).show();
	                        }
	                    }   
	                }

	            })
	            .build();
	    requestsDialog.show();
	}

}
