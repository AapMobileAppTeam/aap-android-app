package com.next.aappublicapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.next.aap.dto.FacebookPost;
import com.next.aappublicapp.listeners.OnFacebookPostLikeSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.VolleyUtil;

public class FullFacebookPostActivity extends TrackableActivity implements
		OnFacebookPostLikeSuccessListener, OnServiceFailListener {

	public static final String INTENT_FACEBOOK_POST_ITEM = "INTENT_FACEBOOK_POST_ITEM";
	private NetworkImageView facebookImageView;
	private WebView facebookTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_full_facebook_post);

		facebookImageView = (NetworkImageView) findViewById(R.id.facebook_post_image_preview);
		facebookTextView = (WebView) findViewById(R.id.facebook_text);

		final FacebookPost facebookPost = (FacebookPost) getIntent()
				.getSerializableExtra(INTENT_FACEBOOK_POST_ITEM);
		final String mimeType = "text/html";
		final String encoding = "UTF-8";
		facebookTextView.loadDataWithBaseURL("", facebookPost.getMessage(),
				mimeType, encoding, "");

		// blogTextView.setText(Html.fromHtml(blogItem.getContent()));
		// blogTextView.setMovementMethod(LinkMovementMethod.getInstance());
		if (facebookPost.getPicture() != null) {
			String imageUrl = facebookPost.getPicture();
			imageUrl = imageUrl.replace("_s", "_n");
			facebookImageView.setImageUrl(imageUrl, VolleyUtil.getInstance()
					.getImageLoader());
		}

		TextView facebookLikeTextViewButton = (TextView) findViewById(R.id.facebook_like_textview_button);
		TextView facebookCommentTextViewButton = (TextView) findViewById(R.id.facebook_comment_textview_button);
		TextView facebookShareTextViewButton = (TextView) findViewById(R.id.facebook_share_textview_button);
		TextView facebookStatusLikeCommentTitle = (TextView) findViewById(R.id.facebook_status_like_comment_title);

		String likeString = "";
		if (facebookPost.getLikes() != null
				&& facebookPost.getLikes().getSummary() != null) {
			likeString = facebookPost.getLikes().getSummary()
					.getTotal_count()
					+ " Likes ";
		}
		String commentString = "";
		if (facebookPost.getComments() != null
				&& facebookPost.getComments().getSummary() != null) {

			commentString = facebookPost.getComments().getSummary()
					.getTotal_count()
					+ " Comments ";
		}
		facebookStatusLikeCommentTitle.setText(likeString + commentString);
		facebookLikeTextViewButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						likePost(facebookPost.getId());
					}
				});

		facebookCommentTextViewButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						commentPost(facebookPost.getId());
					}
				});

		facebookShareTextViewButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UiLifecycleHelper uiHelper = new UiLifecycleHelper(
								FullFacebookPostActivity.this, null);
						if (FacebookDialog.canPresentShareDialog(
								FullFacebookPostActivity.this
										.getApplicationContext(),
								FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
							FacebookDialog.ShareDialogBuilder builder = new FacebookDialog.ShareDialogBuilder(
									FullFacebookPostActivity.this)
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									// .setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage());
							if(facebookPost.getPicture() != null && !facebookPost.getPicture().trim().equals("")){
								builder.setPicture(facebookPost.getPicture());
							}
							FacebookDialog shareDialog = builder.build(); 
							uiHelper.trackPendingDialogCall(shareDialog
									.present());

						} else {
							WebDialog shareDialog = new WebDialog.FeedDialogBuilder(
									FullFacebookPostActivity.this, Session
											.getActiveSession())
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									.setSource(facebookPost.getPicture())
									// .setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage())
									// .setPicture(facebookPost.getPicture())
									.build();
							shareDialog.show();
						}

					}
				});

		facebookImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent fullImageIntent = new Intent(
						FullFacebookPostActivity.this, FullImageActivity.class);
				String imageUrl = facebookPost.getPicture();
				imageUrl = imageUrl.replace("_s", "_n");
				fullImageIntent.putExtra(
						FullImageActivity.INTENT_FULL_IMAGE_URL_ITEM, imageUrl);
				fullImageIntent.putExtra(
						FullImageActivity.INTENT_FULL_IMAGE_TITLE_ITEM,
						facebookPost.getMessage());
				startActivity(fullImageIntent);
			}
		});

	}

	private void likePost(String postId) {
		String likeUrlForPost = "https://graph.facebook.com/" + postId
				+ "/likes?method=POST";
		FacebookUtil.onFacebookLikeClick(this, likeUrlForPost, this, this);
	}

	private void commentPost(String postId) {
		String commentUrlForPost = "https://graph.facebook.com/" + postId
				+ "/comments?method=POST";
		FacebookUtil.onFacebookCommentClick(this, commentUrlForPost);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.full_facebook_post, menu);
		return true;
	}

	@Override
	public void onServiceFail(String message) {
		Toast.makeText(this, "Error : " + message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onSuccesfullFacebookPostLike() {
		Toast.makeText(this, "Facebook Post like succesfully",
				Toast.LENGTH_SHORT).show();
	}

}
