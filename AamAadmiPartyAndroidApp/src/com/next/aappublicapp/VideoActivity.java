package com.next.aappublicapp;

import android.os.Bundle;
import android.view.Menu;

public class VideoActivity extends TrackableActivity {

	public static final String INTENT_VIDEO_ITEM = "INTENT_VIDEO_ITEM";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video, menu);
		return true;
	}

}
