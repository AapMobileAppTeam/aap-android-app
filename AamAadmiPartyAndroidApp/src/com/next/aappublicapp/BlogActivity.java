package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.BlogItem;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.TrackerUtil;
import com.next.aappublicapp.util.TwitterUtil;
import com.next.aappublicapp.util.VolleyUtil;

public class BlogActivity extends TrackableActivity {

	public static final String INTENT_BLOG_ITEM = "INTENT_BLOG_ITEM";
	private NetworkImageView blogImageView;
	private TextView blogTitleTextView;
	private TextView blogAuthorTextView;
	private WebView blogTextView;
	private LinearLayout oneLinersLinearLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blog);
		
		blogImageView = (NetworkImageView) findViewById(R.id.blog_image);
		blogTitleTextView = (TextView) findViewById(R.id.blog_title);
		blogAuthorTextView = (TextView) findViewById(R.id.blog_author);
		blogTextView = (WebView) findViewById(R.id.blog_text);
		oneLinersLinearLayout = (LinearLayout) findViewById(R.id.oneliners);
		
		final BlogItem blogItem = (BlogItem)getIntent().getSerializableExtra(INTENT_BLOG_ITEM);
		blogTitleTextView.setText(blogItem.getTitle());
		blogAuthorTextView.setText(blogItem.getAuthor());
		final String mimeType = "text/html";
        final String encoding = "UTF-8";
        blogTextView.loadDataWithBaseURL("", blogItem.getContent(), mimeType, encoding, "");

		//blogTextView.setText(Html.fromHtml(blogItem.getContent()));
		//blogTextView.setMovementMethod(LinkMovementMethod.getInstance());
		if(blogItem.getImageUrl() != null){
			blogImageView.setImageUrl(blogItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
		}else{
			blogImageView.setVisibility(View.GONE);
		}

		List<String> oneLiners = blogItem.getOneLiners();
		if(oneLiners == null || oneLiners.isEmpty()){
			oneLiners = new ArrayList<String>(1);
			String title = blogItem.getTitle();
			oneLiners.add(title);
		}
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (final String oneliner : oneLiners) {
			RelativeLayout onelinerLayout = (RelativeLayout) inflater.inflate(R.layout.item_oneliner, oneLinersLinearLayout, false);
			TextView text = (TextView) onelinerLayout.findViewById(R.id.text);
			text.setText(oneliner);
			Button tweetButton = (Button) onelinerLayout.findViewById(R.id.tweet_button);
			Button fbButton = (Button) onelinerLayout.findViewById(R.id.fb_button);
			tweetButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TrackerUtil.sendAapEvent(BlogActivity.this, TrackerUtil.BLOG_TWEET_SHARE_CLICK);
					TwitterUtil.onTweetShare(BlogActivity.this,oneliner,blogItem.getWebUrl(), blogItem.getImageUrl(), blogItem.getContent());
				}
			});
			fbButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TrackerUtil.sendAapEvent(BlogActivity.this, TrackerUtil.BLOG_FACEBOOK_SHARE_CLICK);
					FacebookUtil.onFacebookShareClick(BlogActivity.this,blogItem.getOriginalUrl(), blogItem.getImageUrl(), oneliner);
				}
			});
			oneLinersLinearLayout.addView(onelinerLayout);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	

}
