package com.next.aappublicapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.next.aap.dto.SwarajChapter;
import com.next.aappublicapp.R;

public class SwarajListAdapter extends ArrayAdapter<SwarajChapter> {

	public SwarajListAdapter(Context context, List<SwarajChapter> values) {
		super(context, R.layout.activity_news_list, values);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		SwarajChapter swarajChapter = (SwarajChapter) getItem(position);
		View rowView;
		rowView = inflater.inflate(R.layout.swaraj_row_list, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.chapter_title);
		textView.setText("Chapter " + (position + 1) + " : "
				+ swarajChapter.getTitle());

		textView.setTextColor(getContext().getResources().getColor(R.color.DarkGray));
		/*
		if (position % 2 == 0) {
			rowView.setBackgroundColor(getContext().getResources().getColor(
					R.color.AapGreen));
		} else {
			rowView.setBackgroundColor(getContext().getResources().getColor(
					R.color.AapOrange));
		}
		*/

		return rowView;
	}

}
