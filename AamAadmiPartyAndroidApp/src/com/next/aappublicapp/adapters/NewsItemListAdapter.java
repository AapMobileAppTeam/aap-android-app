package com.next.aappublicapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.NewsItem;
import com.next.aappublicapp.R;
import com.next.aappublicapp.util.VolleyUtil;


public class NewsItemListAdapter extends ArrayAdapter<NewsItem> {

	public NewsItemListAdapter(Context context, List<NewsItem> values) {
		super(context, R.layout.activity_news_list, values);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		NewsItem newsItem = (NewsItem) getItem(position);
		View rowView;
		if(newsItem.getId() == -1){
			rowView = inflater.inflate(R.layout.more_item_row_list, parent, false);
		}else{
			rowView = inflater.inflate(R.layout.news_item_row_list, parent, false);
			NetworkImageView imagePreview = (NetworkImageView)rowView.findViewById(R.id.news_image_preview);
			ImageView imageLocalPreview = (ImageView)rowView.findViewById(R.id.news_image_local_preview);
			TextView textView = (TextView) rowView.findViewById(R.id.news_title);
			textView.setText(newsItem.getTitle());
			if(newsItem.getImageUrl() != null && !newsItem.getImageUrl().trim().equals("")){
				imagePreview.setVisibility(View.VISIBLE);
				imageLocalPreview.setVisibility(View.GONE);
				imagePreview.setImageUrl(newsItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
			}else{
				imagePreview.setVisibility(View.GONE);
				imageLocalPreview.setVisibility(View.VISIBLE);
				imageLocalPreview.setImageResource(R.drawable.ic_launcher);
			}
			/*
			if(position % 2 == 0){
				rowView.setBackgroundColor(getContext().getResources().getColor(R.color.AapGreen));
			}else{
				rowView.setBackgroundColor(getContext().getResources().getColor(R.color.AapOrange));
			}
			*/
		}
		
		
		return rowView;
	}


}
