package com.next.aappublicapp.adapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aappublicapp.R;

public class ParliamentConstituncySpinnerAdapter extends ArrayAdapter<ParliamentConstituencyDto> {
	public ParliamentConstituncySpinnerAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public ParliamentConstituncySpinnerAdapter(Context context, int resource,
			int textViewResourceId) {
		super(context, resource, textViewResourceId);
	}

	public ParliamentConstituncySpinnerAdapter(Context context, int resource,
			int textViewResourceId, List<ParliamentConstituencyDto> objects) {
		super(context, resource, textViewResourceId, objects);
	}

	public ParliamentConstituncySpinnerAdapter(Context context, int textViewResourceId,
			List<ParliamentConstituencyDto> objects) {
		super(context, textViewResourceId, objects);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.i("Ravi", "getView");
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.spinner_view_layout, parent, false);
		TextView textView = (TextView) row.findViewById(R.id.textView);
		textView.setText(getItem(position).getName());
		return row;
	}

	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		Log.i("Ravi", "getDropDownView");
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.spinner_dropdown_view_layout, parent, false);
		TextView textView = (TextView) row.findViewById(R.id.textView);
		textView.setText(getItem(position).getName());
		return row;
	}

}
