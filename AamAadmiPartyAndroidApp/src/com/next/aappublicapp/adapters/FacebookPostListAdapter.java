package com.next.aappublicapp.adapters;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.next.aap.dto.FacebookPost;
import com.next.aappublicapp.FullFacebookPostActivity;
import com.next.aappublicapp.FullImageActivity;
import com.next.aappublicapp.R;
import com.next.aappublicapp.listeners.OnFacebookPostLikeSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.util.DialogUtil;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.VolleyUtil;
import com.next.aappublicapp.util.YoutubeUtil;

public class FacebookPostListAdapter extends ArrayAdapter<FacebookPost> implements OnFacebookPostLikeSuccessListener, OnServiceFailListener{

	private final int PHOTO_POST_TYPE = 1;
	private final int VIDEO_POST_TYPE = 2;
	private final int LINK_POST_TYPE = 3;
	private final int STATUS_POST_TYPE = 4;
	
	ProgressDialog progressDialog;
	
	public FacebookPostListAdapter(Context context, List<FacebookPost> values) {
		super(context, R.layout.activity_news_list, values);
	}
	private int getFacebookStoryType(String type){
		if("photo".equalsIgnoreCase(type)){
			return PHOTO_POST_TYPE;
		}
		if("status".equalsIgnoreCase(type)){
			return STATUS_POST_TYPE;
		}
		if("link".equalsIgnoreCase(type)){
			return LINK_POST_TYPE;
		}
		if("video".equalsIgnoreCase(type)){
			return VIDEO_POST_TYPE;
		}
		
		return PHOTO_POST_TYPE;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final FacebookPost facebookPost = (FacebookPost) getItem(position);
		View rowView;
		if (facebookPost.getId().equals("-1")) {
			LayoutInflater inflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater
					.inflate(R.layout.loading_row_list, parent, false);
		} else {
			int facebookStoryType = getFacebookStoryType(facebookPost.getType());
			switch(facebookStoryType){
			case PHOTO_POST_TYPE :
				rowView = viewPhotoPost(parent, facebookPost);
				break;
			case VIDEO_POST_TYPE :
				rowView = viewVideoPost(parent, facebookPost);
				break;
			case LINK_POST_TYPE :
				rowView = viewPhotoPost(parent, facebookPost);
				break;
			case STATUS_POST_TYPE :
				rowView = viewPhotoPost(parent, facebookPost);
				break;
			default :
				rowView = viewPhotoPost(parent, facebookPost);
			}
			
			/*
			 * if(position % 2 == 0){
			 * rowView.setBackgroundColor(getContext().getResources
			 * ().getColor(R.color.AapGreen)); }else{
			 * rowView.setBackgroundColor(
			 * getContext().getResources().getColor(R.color.AapOrange)); }
			 */
		}

		return rowView;
	}
	
	private View viewPhotoPost(ViewGroup parent, final FacebookPost facebookPost){
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.facebook_photo_row_list, parent,
				false);
		NetworkImageView facebookAccountIconPreview = (NetworkImageView) rowView
				.findViewById(R.id.facebook_account_icon_preview);
		NetworkImageView facebookPostImagePreview = (NetworkImageView) rowView
				.findViewById(R.id.facebook_post_image_preview);
		TextView facebookAccountTitleTextView = (TextView) rowView
				.findViewById(R.id.facebook_account_title);
		TextView minuteAgoTitle = (TextView) rowView
				.findViewById(R.id.minutes_ago_title);

		TextView facebookStatusTextView = (TextView) rowView
				.findViewById(R.id.facebook_status_title);
		TextView facebookStatusLikeCommentTitle = (TextView) rowView
				.findViewById(R.id.facebook_status_like_comment_title);
		String likeString = "";
		if (facebookPost.getLikes() != null
				&& facebookPost.getLikes().getSummary() != null) {
			likeString = facebookPost.getLikes().getSummary()
					.getTotal_count()
					+ " Likes ";
		}
		String commentString = "";
		if (facebookPost.getComments() != null
				&& facebookPost.getComments().getSummary() != null) {

			commentString = facebookPost.getComments().getSummary()
					.getTotal_count()
					+ " Comments ";
		}
		facebookStatusLikeCommentTitle.setText(likeString + commentString);
		if (facebookPost.getPicture() != null
				&& !facebookPost.getPicture().trim().equals("")) {
			facebookPostImagePreview.setVisibility(View.VISIBLE);
			String imageUrl = facebookPost.getPicture();
			imageUrl = imageUrl.replace("_s", "_n");
			facebookPostImagePreview.setImageUrl(imageUrl ,
					VolleyUtil.getInstance().getImageLoader());
		} else {
			facebookPostImagePreview.setVisibility(View.GONE);
		}
		String iconUrl = "https://graph.facebook.com/290805814352519/picture";
		facebookAccountIconPreview.setVisibility(View.VISIBLE);
		facebookAccountIconPreview.setImageUrl(iconUrl, VolleyUtil
				.getInstance().getImageLoader());
		facebookAccountTitleTextView.setText("Aam Aadmi Party");
		
		String postCreationTime = facebookPost.getCreated_time();
		Log.e("AAP", "postCreationTime = "+ postCreationTime);
		if(postCreationTime != null){
			long postCreationTimeLong = 0;
			try{
				postCreationTimeLong = Long.parseLong(postCreationTime) * 1000;//convert Unix time to milli seconds
				String result = DateUtils.getRelativeTimeSpanString(postCreationTimeLong, System.currentTimeMillis(), 0).toString();
				Log.e("AAP", "result = "+ result);
				minuteAgoTitle.setText(result);
			}catch(Exception ex){
				Log.e("AAP", "Unable to convert date "+ facebookPost.getCreated_time() +" to Long");
			}
		}
		facebookStatusTextView.setText(facebookPost.getMessage());

		TextView facebookLikeTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_like_textview_button);
		TextView facebookCommentTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_comment_textview_button);
		TextView facebookShareTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_share_textview_button);

		facebookLikeTextViewButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						likePost(facebookPost.getId());
					}
				});

		facebookCommentTextViewButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						commentPost(facebookPost.getId());
					}
				});

		facebookShareTextViewButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UiLifecycleHelper uiHelper = new UiLifecycleHelper((Activity)getContext(), null);
						if (FacebookDialog
								.canPresentShareDialog(getContext().getApplicationContext(),
										FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
							FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity)getContext())
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									//.setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage())
									.setPicture(facebookPost.getPicture())
									.build();
							uiHelper.trackPendingDialogCall(shareDialog
									.present());

						}else{
							WebDialog shareDialog = new WebDialog.FeedDialogBuilder((Activity)getContext(), Session.getActiveSession())
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									.setSource(facebookPost.getPicture())
									//.setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage())
									//.setPicture(facebookPost.getPicture())
									.build();
							shareDialog.show();
						}

					}
				});
		
		facebookPostImagePreview.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent fullImageIntent = new Intent(getContext(), FullImageActivity.class);
						String imageUrl = facebookPost.getPicture() ;
						imageUrl = imageUrl.replace("_s", "_n");
						fullImageIntent.putExtra(FullImageActivity.INTENT_FULL_IMAGE_URL_ITEM, imageUrl);
						fullImageIntent.putExtra(FullImageActivity.INTENT_FULL_IMAGE_TITLE_ITEM, facebookPost.getMessage());
						getContext().startActivity(fullImageIntent);
					}
				});
		
		facebookStatusTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent fullImageIntent = new Intent(getContext(), FullFacebookPostActivity.class);
				fullImageIntent.putExtra(FullFacebookPostActivity.INTENT_FACEBOOK_POST_ITEM, facebookPost);
				getContext().startActivity(fullImageIntent);
			}
		});
		
		return rowView;
	}
	
	private void likePost(String postId){
		String likeUrlForPost = "https://graph.facebook.com/"+ postId+ "/likes?method=POST";
		FacebookUtil.onFacebookLikeClick((Activity) getContext(), likeUrlForPost, this, this);
	}
	
	private void commentPost(String postId){
		String commentUrlForPost = "https://graph.facebook.com/"+ postId + "/comments?method=POST";
		FacebookUtil.onFacebookCommentClick((Activity) getContext(), commentUrlForPost);
	}

	private View viewVideoPost(ViewGroup parent, final FacebookPost facebookPost){
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.facebook_video_row_list, parent,
				false);
		NetworkImageView facebookAccountIconPreview = (NetworkImageView) rowView
				.findViewById(R.id.facebook_account_icon_preview);
		NetworkImageView facebookPostImagePreview = (NetworkImageView) rowView
				.findViewById(R.id.facebook_post_image_preview);
		ImageView facebookPostVideoPlay = (ImageView) rowView
				.findViewById(R.id.facebook_video_play);
		ImageView facebookPostVideoYoutube = (ImageView) rowView
				.findViewById(R.id.facebook_video_youtube);
		
		TextView facebookAccountTitleTextView = (TextView) rowView
				.findViewById(R.id.facebook_account_title);
		TextView minuteAgoTitle = (TextView) rowView
				.findViewById(R.id.minutes_ago_title);

		TextView facebookStatusTextView = (TextView) rowView
				.findViewById(R.id.facebook_status_title);
		TextView facebookStatusLikeCommentTitle = (TextView) rowView
				.findViewById(R.id.facebook_status_like_comment_title);
		String likeString = "";
		if (facebookPost.getLikes() != null
				&& facebookPost.getLikes().getSummary() != null) {
			likeString = facebookPost.getLikes().getSummary()
					.getTotal_count()
					+ " Likes ";
		}
		String commentString = "";
		if (facebookPost.getComments() != null
				&& facebookPost.getComments().getSummary() != null) {
			commentString = facebookPost.getComments().getSummary()
					.getTotal_count()
					+ " Comments ";
		}
		facebookStatusLikeCommentTitle.setText(likeString + commentString);
		if (facebookPost.getPicture() != null
				&& !facebookPost.getPicture().trim().equals("")) {
			facebookPostImagePreview.setVisibility(View.VISIBLE);
			facebookPostImagePreview.setImageUrl(facebookPost.getPicture(),
					VolleyUtil.getInstance().getImageLoader());
		} else {
			facebookPostImagePreview.setVisibility(View.GONE);
		}
		Log.i("AAP", "facebookPost.getLink()="+facebookPost.getLink());
		if(facebookPost.getLink() != null){
			if(facebookPost.getLink().indexOf("youtube") > 0){
				facebookPostVideoYoutube.setVisibility(View.VISIBLE);
				facebookPostVideoYoutube.setImageResource(R.drawable.youtube);
			}else if(facebookPost.getLink().indexOf("facebook") > 0){
				facebookPostVideoYoutube.setVisibility(View.VISIBLE);
				facebookPostVideoYoutube.setImageResource(R.drawable.facebook_video);
			}else{
				facebookPostVideoYoutube.setVisibility(View.GONE);
			}
		}else{
			facebookPostVideoYoutube.setVisibility(View.GONE);
		}
		String iconUrl = "https://graph.facebook.com/290805814352519/picture";
		facebookAccountIconPreview.setVisibility(View.VISIBLE);
		facebookAccountIconPreview.setImageUrl(iconUrl, VolleyUtil
				.getInstance().getImageLoader());
		facebookAccountTitleTextView.setText("Aam Aadmi Party");
		minuteAgoTitle.setText("10 Minutes");
		facebookStatusTextView.setText(facebookPost.getMessage());

		TextView facebookLikeTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_like_textview_button);
		TextView facebookCommentTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_comment_textview_button);
		TextView facebookShareTextViewButton = (TextView) rowView
				.findViewById(R.id.facebook_share_textview_button);

		facebookLikeTextViewButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						likePost(facebookPost.getId());
					}
				});

		facebookCommentTextViewButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						commentPost(facebookPost.getId());
					}
				});

		facebookShareTextViewButton
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UiLifecycleHelper uiHelper = new UiLifecycleHelper((Activity)getContext(), null);
						if (FacebookDialog
								.canPresentShareDialog(getContext().getApplicationContext(),
										FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
							FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder((Activity)getContext())
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									//.setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage())
									.setPicture(facebookPost.getPicture())
									.build();
							uiHelper.trackPendingDialogCall(shareDialog
									.present());

						}else{
							WebDialog shareDialog = new WebDialog.FeedDialogBuilder((Activity)getContext(), Session.getActiveSession())
									.setLink(facebookPost.getLink())
									.setName("Aam Aadmi Party")
									.setSource(facebookPost.getPicture())
									//.setApplicationName("AAP Android App")
									.setDescription(facebookPost.getMessage())
									//.setPicture(facebookPost.getPicture())
									.build();
							shareDialog.show();
						}

					}
				});
		View.OnClickListener imageClickListener = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Play youtube video is youtube
				String videoUrl = facebookPost.getLink(); 
				if(videoUrl.indexOf("youtube") > 0){
					String videoId = null;
					try{
						String[] withOuturl = videoUrl.split("\\?");
						String[] allParams = withOuturl[1].split("&");
						for(String oneParam:allParams){
							String[] params = oneParam.split("=");	
							if(params[0].equalsIgnoreCase("v")){
								videoId = params[1];
							}
						}
						if(videoId != null){
							YoutubeUtil.playYoutubeVideo(getContext(), videoId);
						}
					}catch(Exception ex){
						Log.e("AAP", "Error to run the video "+ facebookPost.getLink(), ex);
					}
					
				}else{
					DialogUtil.showErrorMessage(getContext(), "Currently can not play facebook videos, can play youtube videos only");
				}
				
			}
		};
		facebookPostImagePreview.setOnClickListener(imageClickListener);
		facebookPostVideoPlay.setOnClickListener(imageClickListener);
		return rowView;
	}
	
	@Override
	public void onSuccesfullFacebookPostLike() {
		Toast.makeText(getContext(), "Post liked succesfully", Toast.LENGTH_LONG).show();
	}
	@Override
	public void onServiceFail(String message) {
		Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
	}
}
