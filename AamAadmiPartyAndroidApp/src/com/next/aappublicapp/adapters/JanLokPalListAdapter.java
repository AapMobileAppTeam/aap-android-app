package com.next.aappublicapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.next.aap.dto.JanLokPalChapter;
import com.next.aappublicapp.R;

public class JanLokPalListAdapter extends ArrayAdapter<JanLokPalChapter> {

	public JanLokPalListAdapter(Context context, List<JanLokPalChapter> values) {
		super(context, R.layout.activity_news_list, values);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		JanLokPalChapter janLokPalChapter = (JanLokPalChapter) getItem(position);
		View rowView;
		rowView = inflater.inflate(R.layout.jan_lokpal_row_list, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.chapter_title);
		textView.setText("Chapter " + (position + 1) + " : "
				+ janLokPalChapter.getTitle());

		/*
		if (position % 2 == 0) {
			rowView.setBackgroundColor(getContext().getResources().getColor(
					R.color.AapGreen));
		} else {
			rowView.setBackgroundColor(getContext().getResources().getColor(
					R.color.AapOrange));
		}
		*/

		return rowView;
	}

}
