package com.next.aappublicapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.next.aap.dto.BlogItem;
import com.next.aappublicapp.R;
import com.next.aappublicapp.util.VolleyUtil;


public class BlogItemListAdapter extends ArrayAdapter<BlogItem> {

	public BlogItemListAdapter(Context context, List<BlogItem> values) {
		super(context, R.layout.activity_blog_list, values);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		BlogItem blogItem = (BlogItem) getItem(position);
		View rowView;
		if(blogItem.getId() == -1){
			rowView = inflater.inflate(R.layout.more_item_row_list, parent, false);
		}else{
			rowView = inflater.inflate(R.layout.blog_item_row_list, parent, false);
			NetworkImageView imagePreview = (NetworkImageView)rowView.findViewById(R.id.blog_image_preview);
			ImageView imageLocalPreview = (ImageView)rowView.findViewById(R.id.blog_image_local_preview);
			TextView textView = (TextView) rowView.findViewById(R.id.blog_title);
			textView.setText(blogItem.getTitle());
			if(blogItem.getImageUrl() != null && !blogItem.getImageUrl().trim().equals("")){
				imagePreview.setVisibility(View.VISIBLE);
				imageLocalPreview.setVisibility(View.GONE);
				imagePreview.setImageUrl(blogItem.getImageUrl(), VolleyUtil.getInstance().getImageLoader());
			}else{
				imagePreview.setVisibility(View.GONE);
				imageLocalPreview.setVisibility(View.VISIBLE);
				imageLocalPreview.setImageResource(R.drawable.ic_launcher);
			}
			/*
			if(position % 2 == 0){
				rowView.setBackgroundColor(getContext().getResources().getColor(R.color.AapGreen));
			}else{
				rowView.setBackgroundColor(getContext().getResources().getColor(R.color.AapOrange));
			}
			*/
		}
		
		
		return rowView;
	}


}
