package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.next.aap.dto.JanLokPalChapter;
import com.next.aappublicapp.util.FacebookUtil;
import com.next.aappublicapp.util.TwitterUtil;

public class JanLokpalActivity extends TrackableActivity {

	public static final String JAN_LOKPAL_CHAPTER = "JAN_LOKPAL_CHAPTER"; 
	TextView janlokpalChapterTitle;
	WebView janlokpalChapterContentWebView;
	private LinearLayout oneLinersLinearLayout;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jan_lokpal);
		
		final JanLokPalChapter janLokPalChapter = (JanLokPalChapter)getIntent().getExtras().getSerializable(JAN_LOKPAL_CHAPTER);
		janlokpalChapterTitle = (TextView)findViewById(R.id.janlokpal_chapter_title);
		janlokpalChapterContentWebView = (WebView)findViewById(R.id.janlokpal_chapter_content);
		oneLinersLinearLayout = (LinearLayout) findViewById(R.id.oneliners);
		
		
		janlokpalChapterTitle.setText(janLokPalChapter.getTitle());
		
		final String mimeType = "text/html";
        final String encoding = "UTF-8";
        janlokpalChapterContentWebView.loadDataWithBaseURL("", janLokPalChapter.getContent(), mimeType, encoding, "");
        
        
        List<String> oneLiners =  new ArrayList<String>(1);
		String title = janLokPalChapter.getTitle();
		oneLiners.add("#swaraj "+ title);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (final String oneliner : oneLiners) {
			RelativeLayout onelinerLayout = (RelativeLayout) inflater.inflate(R.layout.item_oneliner, oneLinersLinearLayout, false);
			TextView text = (TextView) onelinerLayout.findViewById(R.id.text);
			text.setText(oneliner);
			Button tweetButton = (Button) onelinerLayout.findViewById(R.id.tweet_button);
			Button fbButton = (Button) onelinerLayout.findViewById(R.id.fb_button);
			tweetButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					TwitterUtil.onTweetShare(JanLokpalActivity.this,janLokPalChapter.getWebUrl(), "", oneliner, janLokPalChapter.getContent());
				}
			});
			fbButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					//https://lh3.googleusercontent.com/-x3seksAy3lE/UpqdgDvQGJI/AAAAAAAANs4/NPOMxdI0Zpo/s320/swaraj2.JPG
					FacebookUtil.onFacebookShareClick(JanLokpalActivity.this,janLokPalChapter.getWebUrl(), "https://lh6.googleusercontent.com/-jHIQMtZPgag/UpqgaH7PrsI/AAAAAAAANtI/fDBUHxtcq1A/s800/Janlokpal.jpg", oneliner);
				}
			});
			oneLinersLinearLayout.addView(onelinerLayout);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.jan_lokpal, menu);
		return true;
	}

}
