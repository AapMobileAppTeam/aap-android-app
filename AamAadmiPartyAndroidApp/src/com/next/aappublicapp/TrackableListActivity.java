package com.next.aappublicapp;

import android.app.ListActivity;

import com.google.analytics.tracking.android.EasyTracker;

public class TrackableListActivity extends ListActivity {

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
