package com.next.aappublicapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

import com.next.aappublicapp.util.AppUsageUtil;
import com.next.aappublicapp.util.ClientConfig;
import com.next.aappublicapp.util.GcmUtil;
import com.next.aappublicapp.util.UserSessionManager;

public class SplashActivity extends TrackableActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		//Splash Screen Activity task
		
		//1.0 First make sure device is registered on server
		GcmUtil.ensureDeviceIsRegistered(this);
		//2.0 call App Usages to mark various usages of app
		AppUsageUtil.appUsed(this);
		
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				UserSessionManager userSessionManager = UserSessionManager.getInstance();
				finish();
				if (userSessionManager.isUserLoggedInOnAnyService()) {
					if (userSessionManager.isUserRegistered(SplashActivity.this)) {
						Intent homeActivityIntent = new Intent(SplashActivity.this, HomeScreenActivity.class);
						startActivity(homeActivityIntent);
					} else {
						Intent registerActivityIntent = new Intent(SplashActivity.this, UserProfileActivity.class);
						startActivity(registerActivityIntent);
					}
				} else {
					Intent loginActivityIntent = new Intent(SplashActivity.this, LoginActivity.class);
					startActivity(loginActivityIntent);
				}
			}
		}, ClientConfig.getSplashDuration(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}
	

}
