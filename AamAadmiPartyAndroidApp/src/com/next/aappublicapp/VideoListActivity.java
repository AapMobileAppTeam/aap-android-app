package com.next.aappublicapp;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.VideoItem;
import com.next.aappublicapp.adapters.VideoItemListAdapter;
import com.next.aappublicapp.listeners.OnPageVideoLoadSuccessListener;
import com.next.aappublicapp.listeners.OnServiceFailListener;
import com.next.aappublicapp.server.services.AapServerServices;
import com.next.aappublicapp.server.services.LocalCachService;
import com.next.aappublicapp.util.UserSessionManager;
import com.next.aappublicapp.util.YoutubeUtil;

public class VideoListActivity extends TrackableListActivity implements OnPageVideoLoadSuccessListener, OnServiceFailListener{

	int currentPageNumber = 1;
	VideoItemListAdapter videoItemListAdapter;
	VideoItem moreVideoItem;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_list);
		AllHomeScreenItem allHomeScreenItem = LocalCachService.getInstance().getAllHomeScreenItem(this);
		List<VideoItem> allVideoItems = new ArrayList<VideoItem>(allHomeScreenItem.getVideoItems());
		videoItemListAdapter = new VideoItemListAdapter(this, allVideoItems);
	
		moreVideoItem = new VideoItem();
		moreVideoItem.setId(-1L);
		
		videoItemListAdapter.add(moreVideoItem);
		setListAdapter(videoItemListAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video_list, menu);
		return true;
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent openVideoItem = new Intent(this, VideoActivity.class);
		VideoItem oneVideoItem = videoItemListAdapter.getItem(position);
		Log.i("AAP", "ListView="+listView);
		Log.i("AAP", "view="+view);
		if(oneVideoItem.getId() == -1){
			//means clicked on More Item
			startLoading(view);
			reloadNextPage();
		}else{
			/*
			openVideoItem.putExtra(VideoActivity.INTENT_VIDEO_ITEM, oneVideoItem);
			startActivity(openVideoItem);
			*/
			YoutubeUtil.playYoutubeVideo(this, oneVideoItem);
		}
		//TrackerUtl.sendAapEvent("CandidateInfo");
	}

	@Override
	public void onServiceFail(String message) {
		
	}

	@Override
	public void onSuccesfullPageVideoLoad(List<VideoItem> acList) {
		currentPageNumber++;
		videoItemListAdapter.remove(moreVideoItem);
		for(VideoItem oneVideoItem : acList){
			videoItemListAdapter.add(oneVideoItem);
		}
		//videoItemListAdapter.addAll(acList);
		videoItemListAdapter.add(moreVideoItem);
		
	}
	
	private void reloadNextPage(){
		UserSessionManager userSessionManager = UserSessionManager.getInstance();
		long livingAcId = userSessionManager.getLivingAssemblyConstituencyId();
		long votingAcId = userSessionManager.getVotingAssemblyConstituencyId();
		long livingPcId = userSessionManager.getLivingParliamentConstituencyId();
		long votingPcId = userSessionManager.getVotingParliamentConstituencyId();

		AapServerServices.getInstance().loadVideosOfPageAsync(this, currentPageNumber + 1, livingAcId, votingAcId, livingPcId, votingPcId, this, this);
	}
	
	private void startLoading(View view){
		ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.load_progress_bar);
		if(progressBar != null){
			progressBar.setVisibility(View.VISIBLE);
		}
		TextView moreTextView = (TextView)view.findViewById(R.id.more_title);
		if(moreTextView != null){
			moreTextView.setVisibility(View.GONE);
		}
	}

}
