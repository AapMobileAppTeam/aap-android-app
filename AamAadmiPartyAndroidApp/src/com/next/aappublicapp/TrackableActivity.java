package com.next.aappublicapp;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;

public class TrackableActivity extends Activity {

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
