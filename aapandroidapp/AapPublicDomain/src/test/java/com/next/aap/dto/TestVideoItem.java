package com.next.aap.dto;

import java.util.Date;

import com.google.gson.Gson;

public class TestVideoItem {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		createVideoItem(1L, new Date(), "En-AM0xf95E", "An Important announcement of AAP (Arvind Kejriwal)", "Streamed live on 5 Oct 2013\nAn Important announcement of AAP (Arvind Kejriwal)", gson);
		createVideoItem(2L, new Date(), "DXWa5-BySAM", "Arvind Kejriwal and Manish Sisodia Jansabha at ShashiGarden, MayurVihar", "Streamed live on 10 Oct 2013\n\nArvind Kejriwal and Manish Sisodia Jansabha at ShashiGarden, MayurVihar", gson);
		createVideoItem(3L, new Date(), "Aeq2pkoj0MI", "AAP Mahila Samiti ka Gathan 12.10.2013", "Streamed live on 12 Oct 2013\nAAP Mahila Samiti ka Gathan 12.10.2013", gson);
	}
	
	
	private static void createVideoItem(Long id,Date date,String videoId,String title,String description,Gson gson){
		VideoItem videoItem = new VideoItem();
		videoItem.setPublishDate(date);
		videoItem.setDescription(description);
		videoItem.setId(id);
		videoItem.setTitle(title);
		videoItem.setWebUrl("http://www.youtube.com/watch?v="+videoId);
		videoItem.setYoutubeVideoId(videoId);
		videoItem.setImageUrl("http://i1.ytimg.com/vi/"+videoId+"/mqdefault.jpg");
		System.out.println(gson.toJson(videoItem));
		
	}

}
