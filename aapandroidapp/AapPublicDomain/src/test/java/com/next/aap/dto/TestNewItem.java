package com.next.aap.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class TestNewItem {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		createNewsItem01();
		createNewsItem02();
		createNewsItem03();
		createNewsItem04();
	}
	private static void createNewsItem01(){
		NewsItem newsItem = new NewsItem();
		newsItem.setAuthor("Yogender Yadav");
		newsItem.setContent("(हिंदी अनुवाद के लिए नीचें जाएँ) \n 28 September 2013 <br> Dr. M. M. Pallam Raju " +
				"\n\nMinister of Human Resource Development" +
				"\n\nGovernment of India" +
				"<br><br>New Delhi" +
				"\n\nDear Dr. Raju," +
				"<br><br>I must thank you for your ministry's Order retiring me from my responsibility as UGC Member \"with immediate effect\" (F. No. 7-1/2013-UiA, dated 18 September 2013).  My first thought on receiving this order was to recall Kabir: \"भला हुआ मोरी गगरी फूटी, मैं पनियां भरन से छूटी\"  (I'm glad my pitcher broke, relieving me of the drudgery of collecting water). I believe I am the first Member in the history of the UGC to have been retired by the government. This badge of honour assures me that I must have done something right." +
						"\n\n<p>They say arbitrary power is always nervous. So is your Order, as it labours for 11 pages to discover grounds that simply do not exist, cleverly buries in legalese the substantive issues that I had raised in my response to your Show Cause Notice, and focuses on technicalities.  I would not waste your or my time in reiterating how this Order is illegal. I believe you have been told that the Order would be hard to defend in a court of law and that I could obtain a stay. That is perhaps why you have rushed to file a caveat in the Delhi High Court (dated 24 September 2013, filed by Shri Sandeep Jain, Under Secretary in your Ministry) in anticipation of my petition challenging this Order. I wish your Ministry had exercised the same legal acumen to deal with the cases of corruption and fraud within the UGC where several Members like me had to push the UGC and the Ministry to take action against the guilty. Your anxiety also explains the alacrity with which your government has appointed my successor. I wish your Ministry had shown an iota of this efficiency in matters that affect thousands of students, such as framing rules to regulate fees for the Private Universities and instituting scholarships for undergraduate and graduate students.</p>" +
						"<br><br><p>The Order is also full of ironies; I am sure the lawyer who drafted it for you did not intend these. I could not help smiling when I read that “[a]ny iota of political influence in the UGC’s decision making may vitiate the sacrosanct academic exercise of UGC’s decision making”. I remembered the proposal for IUC at Kakinada being set up to please you and the earlier episode involving the then Finance Minister Mr Pranab Mukherjee – in both these cases, I was the one who stood against political interference in the UGC’s decision making.  The idea that appointment to the UGC must be “rigidly protected from political or personal lobbying” brought another smile, for I had read several news reports about the recent appointment of the Chairman, UGC.</p>" +
						"\n\nThe supreme irony, however, lay in the repeated reference to ‘conflict of interest’ in this Order. Your government had most brazenly institutionalized conflict of interest by appointing owners/managers of Deemed/Private universities and other institutions as Members of the UGC. In fact I was the one who raised this point in the very first meeting of the Commission that I attended, leading to the setting up of a Committee under my chairmanship.  The report of that Committee resulted in the UGC becoming one of the few public institutions in India to have framed a policy on Conflict of Interests (which incidentally does not bar one from the membership of political parties). Perhaps I should not mind your government now teaching me lessons on ‘conflict of interest’.Perhaps we can expect your Ministry to now act on other cases of conflict of interest that lie all over the higher education sector." +
						"<br><br>Above all, I found your Order amusing due to its obsession with the grammar of power. It repeatedly refers to this “post” and its “prestige” and “the power/influence, that any member of the UGC can wield” and assumes that anyone in their right mind would fight to retain and reclaim this position of power. Those who sit in Shastri Bhavan can easily forget that the world of ideas is not governed by a grammar of power, that these ‘powerful’ positions do not hold any value for many academics." +
						"\n\nAs I wrote in my last letter, I did not seek this 'post', your government handed over this responsibility to me. As soon as the formation of Aam Aadami Party was announced on 2nd October 2012, I checked with your predecessor if my continuation in the UGC violated any rules, norms or precedent; he requested me to continue. Normally, I would have welcomed the prospect of being relieved of this responsibility but not if it were done to silence an inconvenient voice. I felt that I had a responsibility, to stay put in the Commission, if only to alert the academic community about some of the measures that your government was rushing through the UGC. Having done this, I would like to turn to other things." +
						"<br><br>I know your government has made all preparations for fighting a legal battle. Many of my friends and well-wishers have urged me to approach the court, if only to ensure that this case does not become a bad precedent that robs autonomous institutions of their remaining autonomy. My friend, and a leading lawyer of the country, Shri Prashant Bhushan tells me that this Order is against the letter and spirit of law, is inconsistent with past practices and is not likely to stand judicial scrutiny. He offered to take up this matter in public interest, but I had to decline his kind offer." +
						"<br><br>I suspect that you government would like this moral and political debate that is being carried out in the public domain to be reduced to a courtroom battle over someone’s reinstatement, a dispute over rules and their interpretation, a debate defined by the grammar of power. I would like this to remain above all, a debate about institutional norms and public good. I would like this to be a political debate in the highest sense of the term." +
						"\n\nSo, I’m afraid I am going to disappoint you. I am happy to be relieved and am truly relieved. Therefore, I do not plan to seek any more relief from any court of law. I would no doubt miss my wonderful colleagues in the Commission, but I have no desire to reclaim this responsibility. Ever since your Ministry issued me the Show Cause Notice, hundreds of students, teachers, researchers and public intellectuals, including some of the finest minds of this country, have protested against this move. A friend sent me “Sacking Mubaarak” greetings! More than one Member of the Commission spoke to me to express a wish to resign in protest." +
						"<br><br>This kind of moral and political support matters much more to me than any legal order. It tells me that when one voice of dissent is suppressed, many more and more powerful voices come up from nowhere. It assures me that the larger questions that I raised within the UGC will continue to be raised in my absence." +
						"<br><br>Yours sincerely," +
						"\nYogendra Yadav," +
						"\nCentre for the Study of Developing Societies,29 Rajpur Road, Delhi 110054 India" +
						"\nOffice Phone: 23981012 (telefax Lokniti, CSDS), 23942199 (PBX, CSDS)" +
						"\n\n||" +
						"\n\n\n28 सितंबर 2013" +
						"\nडा.एम एम पल्लम राजू" +
						"\nमानव संसाधन विकास मंत्री" +
						"\nभारत सरकार" +
						"\nनई दिल्ली" +
						"\n\nप्रिय डाक्टर राजू," +
						"\n\nबहुत शुक्रिया आपका- मंत्रालय के उस आदेश के लिए जिसमें मुझे यूजीसी की सदस्यता के पदभार से \"तत्काल प्रभा\" से मुक्त कर दिया गया है (एफ.नं. 7-1/2013-यूआईए, तारीख 18 सितंबर 2013).इस आदेश के मिलते ही मेरे मन में सबसे पहले कबीर की यह पंक्ति गूंजी-: \"भला हुआ मोरी गगरी फूटी, मैं पनियां भरन से छूटी\"।  मुझे यकीन है कि यूजीसी के इतिहास में मैं पहला व्यक्ति हूं जिसे सरकार ने पदभार से मुक्त किया है। मन कह रहा है कि यूजीसी में रहते हुए मैंने जरुर कोई अच्छा काम किया जो मुझे यह तमगा मिला है। " +
								"\n\nकहते हैं कि मदमत्त सत्ता के अंतकरण में एक घबराहट होती है। यह घबराहट आपके आदेश में भी है। एक तो यह आदेश  11 पन्नों की दौड़ लगाता है, गोया इस लंबी दौड़ के बाद वह बहाना मिल जाएगा जिसकी ओट में मुझे पदभार-मुक्त करना जायज लगे। दूसरे, आपके भेजे कारण-बताओ नोटिस के जवाब में मैंने जो जरुरी मसले उठाये थे उन्हें यह आदेश 11 पन्नों की इस लंबी दौड़ में कानूनी पेचीदगियों के जाल में ढांप कर सिर्फ तकनीकी नुक्तों में उलझाता है।यह आदेश क्योंकर गैरकानूनी है, इसे बताने में मैं ना आपका समय बर्बाद करना चाहता हूं ना ही अपना। " +
								"\nआपको यह सलाह मिली ही होगी कि इस आदेश को अदालत में जायज ठहराना मुश्किल होगा और यह भी कि इस पर मुझे स्टे मिल सकता है। शायद यही वजह रही जो इस आशंका में कि कहीं मैं आदेश को चुनौती देते हुए अदालत में अर्जी ना डाल दूं आपने आनन-फानन में दिल्ली हाईकोर्ट में एक कैविएट फाइल की(आपके मंत्रालय के अवर सचिव श्री संदीप जैन द्वारा 24 सितंबर 2013 को दायर)। काश! आपके मंत्रालय ने कानून के इस्तेमाल का ऐसा ही जौहर यूजीसी में व्याप्त भ्रष्टाचार के उन मामलों में भी दिखाया होता जिसमें मुझ सरीखे इसके कई सदस्यों को दोषी कर्मचारी और अधिकारियों के विरुद्ध कार्रवाई के लिए बार-बार कहना पडा। आपकी सरकार ने बतौर सदस्य मेरा उत्तराधिकारी चुनने में जिस तत्परता का परिचय दिया है, वह भी आपकी चिन्ता और बेचैनी का ही एक नमूना है।काश! आपके मंत्रालय ने निजी विश्वविद्यालयों की फीस के नियमन के विधान रचने और ग्रेजुएट या फिर अंडरग्रेजुएट स्तर के छात्रों को स्कॉलरशिप देने की फौरी जरुरत के मामले में यही तत्परता दिखायी होती। " +
								"\nआपका यह आदेश ऊपर से नीचे तक विडंबनाओं से भरा है। आपके कहने पर जिस वकील ने इसे आपके लिए तैयार किया है, उसे इस बात का गुमान भी ना होगा।बेसाख्ता हंसी आई जब मैंने आदेश को यह कहते पाया कि “ यूजीसी के निर्णयन में राजनीतिक प्रभाव की छाया-मात्र पड़ने से यूजीसी के निर्णयन की विद्वत पावनता दूषित हो सकती है”. मुझे याद आया काकीनाड़ा में अंतर विश्वविद्यालयी केंद्र बनाने का प्रस्ताव जो आपकी खुशामद में लाया गया था और इसके पहले का वह वाकया जिसका वास्ता तत्कालीन वित्तमंत्री प्रणव मुखर्जी से था। इन दोनों मामलों में अकेला मैं ही था जो यूजीसी के निर्णयन में किसी राजनीतिक हस्तक्षेप के खिलाफ उठ खड़ा हुआ । दूसरी बार हंसी फूटी आदेश के उस वाक्य पर जहां कहा गया है कि यूजीसी में होने वाली नियुक्ति को “ राजनीतिक या निजी किस्म की लॉबिंग” से पूरी सख्ती से बचाया जाना चाहिए। हंसी आई इसलिए कि यूजीसी के अध्यक्ष की हालिया नियुक्ति के बारे में कई समाचार मेरी नजर से भी गुजरे हैं। " +
								"\nबहरहाल, इस आदेश की सबसे बड़ी विडंबना तो यह है कि इसमें  “हितो के टकराव” की बात एक टेक की तरह दोहरायी गई है।आपकी सरकार ने यूजीसी में डीम्ड/निजी विश्वविद्यालयों के प्रबंधको/ मालिकों की बतौर सदस्य नियुक्ति करके खुलेआम हितों के टकराव को संस्थागत रुप दिया था। हकीकत तो यह है कि यूजीसी में अपनी भागीदारी की पहली ही बैठक में मैंने इस मुद्दे को उठाया। नतीजतन, एक समिति बनी और उसकी सदारत की जिम्मा मुझे सौंपा गया।इस समिति की रिपोर्ट का ही नतीजा है कि आज यूजीसी “हितों के टकराव” पर नीति-निर्माण करने वाली  देश की चुनिन्दा सार्वजनिक संस्थाओं में एक है(और हां, यहां याद दिलाता चलूं कि इसमें यूजीसी के मेंबर को राजनीतिक सदस्यता से मना नहीं किया गया है)। खैर ! अब आपकी सरकार मुझे ही हितों के टकराव पर कोई सबक सिखा रही है तो शायद मुझे इस बात का रंजो-मलाल नहीं करना चाहिए। शायद अब हम यह उम्मीद पाल सकते हैं कि सरकार हितों के टकराव के बाकी मामलों में भी कार्रवाई करेगी जिनसे उच्च-शिक्षा का पूरा क्षेत्र पटा पडा है।" +
								"\nखैर, आपका यह आदेश सत्ता के व्याकरण से खास लगाव रखने के कारण मुझे खास दिलचस्प जान पडा। पढ़ने वाले के कानों में आदेश की पदावली बारंबार इस “पद” , उससे जुड़ी “ प्रतिष्ठा ” और उस “रसूख/ प्रभाव ” का उच्चार करती है जिसका इस्तेमाल “यूजीसी का कोई सदस्य कर सकता” है। आदेश की पदावली यह मानकर चलती है कि कोई भी व्यक्ति ताकत के इस पद को अपने पास बचाये रखने के लिए लड़ाई पर उतर आयेगा। शास्त्रीभवन में बैठने वाले लोग कितनी आसानी से भूल जाते हैं कि विचारों की दुनिया सत्ता के व्याकरण से बंधकर नहीं चलती और सरस्वती के ऐसे साधक बहुत हैं जिनकी नजर में ताकत के इन तथाकथित पदों की अहमियत राई बराबर नहीं। " +
								"\nमैंने पिछली चिट्ठी में भी लिखा था कि यह पद मैंने मांगा नहीं था, बल्कि आपकी सरकार ने यह जिम्मेवारी मुझे सौंप दी थी। 2 अक्तूबर 2012 के दिन जैसे ही आम आदमी पार्टी के गठन की घोषणा हुई, मैंने आपके मंत्रालय के तत्कालीन मुखिया से पूछा कि यूजीसी में बतौर सदस्य मेरी भागीदारी कहीं इसके किसी नियम, चलन या परंपरा का उल्लंघन तो नहीं? तब जवाब निवेदन के स्वर में मिला था कि आप यूजीसी में अपना काम जारी रखें! माथे पर लाद दिया गया यह बोझ किसी ने उतारने की पेशकश की होती तो मैं अमूमन उसका स्वागत ही करता। मेरी आवाज को असहज मान उसे मौन करने के ख्याल से यह किया गया तो क्योंकर उसका स्वागत करुं? मुझे लगा कि मेरी एक जिम्मेदारी बनती है। यूजीसी में रहना जरुरी लगा ताकि कम से कम विद्वत-समाज को उन कदमों से आगाह कर सकूं जो आपकी सरकार यूजीसी की ओट में उठा रही है। यह हो गया, अब कदम बाकी चीजों की और बढाऊंगा। " +
								"\nमुझे पता है कि आपकी सरकार ने कानूनी लड़ाई लड़ने की पूरी तैयारी कर रखी है। कई दोस्त और शुभचिन्तकों ने मुझे अदालत का दरवाजा खटखटाने की सलाह दी ताकि आगे के लिए मेरी पदमुक्ति का मामला संस्थाओं की बची-खुची स्वायत्तता को बेखौफ हड़पने की एक गलत नजीर ना बन जाये। देश के अग्रणी वकीलों में एक मेरे मित्र श्री प्रशांतभूषण ने मुझसे कहा कि आपके मंत्रालय का यह आदेश कानून की कहनी-रहनी के खिलाफ तथा रवायत से बेमेल ठहरता है और अदालत में इस आदेश का बचा रहना मुश्किल होगा। उन्होंने जनहित में यह मामला खुद उठाने की पेशकश की लेकिन मैंने ही इस पेशकश को विनम्रतापूर्वक ना कह दिया।" +
								"\nमुझे संदेह है कि आपकी सरकार की मंशा जनता की पंचायत में चलने वाली नैतिकता और राजनीति की इस बहस का कद बौना करके उसे एक ऐसी अदालती लड़ाई में बदलने की है कि पूरा मामला किसी व्यक्ति के पुनर्नियुक्ति का जान पड़े। पूरा मसला नियमों और उनकी व्याख्या की आपसी टकराहट का जान पड़े यानि सबकुछ एक ऐसी बहस में बदल जाय जिसे सत्ता के व्याकरण से परिभाषित होना है।लेकिन मैं चाहता हूं पूरा मामला संस्थागत मान-मूल्यों और सार्वजनिक हित पर जारी बहस का रुप ले। मेरे लिए यही राजनीति है। " +
								"\nअफसोस कि मैं आपको निराश करने जा रहा हूं। पद के भार से छूटकर खुश हूं और यह सचमुच मेरे लिए राहत की बात है। यह राहत मिल गई है तो आगे किसी और राहत के लिए मैं कानून की अदालत का दरवाजा नहीं खटखटाने जा रहा। बेशक यूजीसी के अपने बेहतरीन सहकर्मियों से बिछड़ने का मुझे अफसोस होगा लेकिन यूजीसी में मिले पद पर पुनर्नियुक्ति की मेरे मन में तनिक भी इच्छा नहीं है।जब से आपके मंत्रालय ने मुझे कारण-बताओ नोटिस जारी किया है तबसे सैकड़ों छात्र, शिक्षक, शोधकर्ता और बुद्धिजीवियों ने ( इनमें देश के कुछ सर्वश्रेष्ठ विद्वान शामिल हैं) सरकार के इस कदम का विरोध किया है।एक मित्र ने तो मुझे “पद-मुक्ति मुबारक” लिखते हुए बधाई-संदेश भेजा! यूजीसी के एक से ज्यादा सदस्यों ने कहा कि उनका मन विरोध-स्वरुप पद से इस्तीफा देने का हो रहा है।" +
								"\nइस तरह का नैतिक समर्थन मेरे लिए कहीं ज्यादा मानीखेज है बनिस्बत किसी कानूनी आदेश के। इस नैतिक समर्थन ने मेरे भीतर इस विश्वास को और पुख्ता किया है कि विरोध में उठी एक आवाज को दबाने की कोशिश की जाती है तो विरोध की एक से ज्यादा बुलंद आवाजें चहुंओर से उठती हैं। इस नैतिक समर्थन ने मुझे भरोसा दिलाया है कि यूजीसी में रहते मैंने जिन संगीन सवालों को उठाया उन्हें मेरी गैर-मौजूदगी में भी उठाया जाता रहेगा। " +
								"\n\nआपका विश्वासी," +
								"\n\nयोगेन्द्र यादव," +
								"\nविकासशील समाज अध्ययन केंद्र(सीएसडीएस),29 राजपुर रोड, दिल्ली 110054 भारत" +
								"\nOffice Phone: 23981012 (telefax Lokniti, CSDS), 23942199 (PBX, CSDS)");
		newsItem.setDate("28-Sep-2013");
		newsItem.setId(1L);
		newsItem.setImageUrl("http://www.aamaadmiparty.org/sites/default/files/styles/670_x_270/public/yogenji%20letter.jpg");
		newsItem.setSource("aamaadmiparty.org");
		newsItem.setTitle("An Open Letter to the HRD Minister from Yogendra Yadav");
		newsItem.setWebUrl("http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		List<String> oneLiners = new ArrayList<String>();
		oneLiners.add("An Open Letter to the HRD Minister from Yogendra Yadav http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		oneLiners.add("Yogender yadav's reply to HRD misntery http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		newsItem.setOneLiners(oneLiners);

		Gson gson = new Gson();
		System.out.println(gson.toJson(newsItem));
		
	}
	
	private static void createNewsItem02(){
		NewsItem newsItem = new NewsItem();
		newsItem.setAuthor("");
		newsItem.setContent("Aam Aadmi Party constituted a cell for Sikh Community today to focus on the issues faced by the community and work for their welfare in Delhi. At an event organised at Constitution Club, which was attended by around 500 members, a five member body was elected to represent them." +
				"<br><br><p>It was decided that the Sikh cell of Aam Aadmi Party would be headed by Parminder Singh Veerji (President), Jaswant Singh Arora (Convenor), Pritpal Singh Saluja (Senior Vice President), Davinder Singh Sahni (Vice President) and Balwinder Singh Renu (General Secretary)</p>." +
				"<br><br><p>Speaking at the event, Arvind Kejriwal said that the Sikh community has played a very important role in the development of the country. Unfortunately remembering 1984 riots, he said, that the community has been till now treated as vote banks with none of the members who lost their family in the riots getting justice. Kumar Vishwas said right from the time of Independence, when Bhagat Singh sacrificed his life, till today the community has been pivotal in putting India on the road to success. However, corruption within the government has led to delayed justice of 1984 riot victims. Addressing the event, Sanjay Singh mentioned that with the constitution of the cell the party wishes to focus on the problems of the community.</p>" +
				"<br><br><p>Parminder Singh ‘Veerji’ said that the entire Sikh community of Delhi is now with Aam Aadmi Party in its fight against corruption. The cell will now work on all possible ways to reach out to the community members so that AAP emerges victorious in the forth coming Assembly elections.</p>");
		newsItem.setDate("28-Sep-2013");
		newsItem.setId(2L);
		newsItem.setImageUrl("https://lh6.googleusercontent.com/-2iMjOOT33Sw/UbFj-L7EnCI/AAAAAAAALt4/e6_XNje-rj0/s640/Police%2520guarding%2520people%2527s%2520representatives%2520from%2520people..jpg");
		newsItem.setSource("aamaadmiparty.org");
		newsItem.setTitle("AAM AADMI PARTY CONSTITUTES CELL FOR SIKH COMMUNITY");
		newsItem.setWebUrl("http://www.aamaadmiparty.org/news/aam-aadmi-party-constitutes-cell-for-sikh-community");
		List<String> oneLiners = new ArrayList<String>();
		oneLiners.add("An Open Letter to the HRD Minister from Yogendra Yadav http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		oneLiners.add("Yogender yadav's reply to HRD misntery http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		newsItem.setOneLiners(oneLiners);

		Gson gson = new Gson();
		System.out.println(gson.toJson(newsItem));
		
	}
	
	private static void createNewsItem03(){
		NewsItem newsItem = new NewsItem();
		newsItem.setAuthor("Manish Sisodiya");
		newsItem.setContent("(Scroll down for English Translation)" +
				"\n\n23 साल पहले मेडिकल एडमिशन में हुए फर्जीवाड़े के मामले में कांग्रेस सांसद रशीद मसूद को दिल्ली की तीस हजारी अदालत ने चार साल कैद की सजा सुनाई है। ‘चार-सौ-बीसी’ और ‘फर्जीवाड़े’ के दम पर योग्य छात्रों के भविष्य के साथ खिलवाड़ करके अपने रिश्तेदारों और अयोग्य उम्मीदवारों को एडमिशन दिलवाने करने वाले रशीद मसूद पिछले 23 साल से देश की संसद में बैठकर हमारे लिए कानून भी बनाते रहे। वह संसद सदस्य सहित कई महत्वपूर्ण पदों पर रहे। और तो और 2007 में समाजवादी पार्टी ने उन्हें उप-राष्ट्रपति जैसे गरिमापूर्ण पद का उम्मीदवार तक बना दिया था। अगर वह चुनाव जीत गए होते तो राज्यसभा का संचालन भी करते क्योंकि उप-राष्ट्रपति ही राज्यसभा का सभापति होता है। यही नहीं संभव है राष्ट्रपति पद के लिए भी उनकी दावेदारी बन सकती थी क्योंकि हमारे देश में कई उप-राष्ट्रपति बाद में राष्ट्रपति की कुर्सी पर पहुंचे हैं। खैर, इस दुर्भाग्य से तो हम बच गए लेकिन वास्तव में जनता के साथ न्याय हुआ है, ये बहस का विषय है।" +
				"<br><br>सच तो यह है कि न्याय तो तब माना जाता जब इस दौरान उप-राष्ट्रपति तो बहुत दूर की बात है उन्हें ग्राम प्रधान का चुनाव लड़ने की अनुमति न दी जाती। खुले आम भ्रष्टाचार करने वाला नेता राजनीति में इतनी अहमियत क्यों पाता रहा,  इसका जवाब राजनीतिक दलों को देना चाह‌िए। धीमी न्यायिक प्रक्रिया के चलते भले ही फैसला आने में देरी हुई लेकिन उनके ऊपर लगे आरोप गंभीर थे और व‌िभिन्न पार्टियां अगर अपने तुच्छ राजनीतिक स्वार्थों को तवज्जो न देती तो न जाने कब उनका राजनीतिक अवसान हो चुका होता। पिछले 23 साल के दौरान वह संसद में रहे। नीति-निर्धारण में उनकी अहम भूमिका रही, यह हमारे लोकतंत्र के लिए शर्मनाक नहीं, तो और क्या है। तकरीबन 40 साल के अपने राजनीतिक करियर में उन्हें कई बार दल बदले। इस तरह वह कांग्रेस, जनता पार्टी, लोकदल और समाजवादी पार्टी में सक्रिय रहे।" +
				"<br><br><p>एक दिन पहले ही बिहार के पूर्व मुख्यमंत्री लालू प्रसाद यादव भी चारा घोटाले के मामले में दोषी ठहराये गए। इस घोटाले में 1996 में प्राथमिकी दर्ज की गई थी जिसके 17 साल बाद यह फैसला आया। अफसोसजनक बात यह है कि लालू, जयप्रकाश नारायण के भ्रष्टाचार विरोधी आंदोलन की उपज हैं। इन सब हालातों के बीच एक अहम सवाल यह भी खड़ा हुआ है कि रशीद मसूद और लालू यादव जैसे सांसद जिस संसद में बैठे हों वह भला भ्रष्टाचार के खिलाफ अन्ना हजारे का सख्त लोकपाल कानून कैसे पास कर सकती है. सवाल यह भी है कि अगर आज लालू प्रसाद यादव की राजनीतिक हैसियत बड़ी होती, मसलन उनके 10-15 सांसद होते तो क्या मामले को दबाने के लिए केंद्र सरकार पर उनका दबाव काम नहीं करता? वैसे भी सीबीआई के पूर्व महानिदेशक जोगिंदर सिंह ने माना ही है कि पूर्व प्रधानमंत्री इंद्रकुमार गुजराल ने उनसे चारा घोटाले के मामले को धीमा रखने के लिए कहा था।</p>" +
				"<br><br><p>इन दो फैसलों को अभूतपूर्व कहकर हमें चुप हो जाने की जरूरत नहीं है। इन दोनों मामलों में देखें तो लालू और रशीद को कोई राजनीतिक नुकसान नहीं हुआ। हालात का फायदा उठाकर दोनों ने जमकर सत्ता की मलाई काटी है। भ्रष्टाचार से कमाई गई संपत्ति अब भी इनके पास है और अभी इन्हें अंतिम रूप से सजा मिलने में हाईकोर्ट-सुप्रीम कोर्ट में 10-15 साल और लग सकते हैं. मामला सामने आने के बाद से ही अगर ये लोग जेल जाते, इनकी संपत्ति जब्त होती तो वह असली सजा होती।</p>" +
				"<br><br><br><p>Twenty three years ago the Tis Hazari court in Delhi sentenced Congress MP Rashid Masood to four years imprisonment in the case of the Medical Admissions Scam.  Masood was charged under Section 420 of IPC for playing with the future of meritorious students by fraudulently providing admissions to his own relatives and unqualified individuals. Ironically, despite his conviction, he has held several important positions including Member of Parliament, making laws for us these past 23 years.  On top of this, in 2007 Samajwadi Party presented him as a candidate for the dignified post of Vice-President of India. Had he won he would have been chairman of the Rajya Sabha, as it is the Vice-President of India who is in charge of the Rajya Sabha. Not only that, he would also have been a possible candidate for the post of President of India; as in our country many Vice-Presidents have been elevated to the chair of the President. Well, we have at least been spared that misfortune but has the public really received justice? This is the matter for discussion.</p>" +
				"<br><br><p>The truth is that real justice would have been served when the accused is prohibited from contesting elections for any post, not even for Gram Pradhan let alone the Vice-Presidency. The political parties must answer why someone openly involved in corruption held such important posts. The slow judicial process may well have delayed the verdict, but the accusations were serious and if various parties had not given priority to their petty political gains, there is no telling when the demise of Masood’s political career would have come. He stayed in the Parliament for the last 23 years playing an important role in the law-making process. If this is not a shame on our democracy, then what is? He has changed his political affiliations several times over the last forty years being part of the Congress, Janta Party, Lokdal and Samajwadi Party alternatively.</p>" +
				"<br><br><p>Yesterday, the Ex-Chief Minister of Bihar, Lalu Prasad Yadav was also convicted for his involvement in the ‘Fodder Scam’. In this case, the original FIR was registered in 1996 and the verdict came after seventeen years. It is unfortunate that Lalu Prasad Yadav is a product of Jayprakash Narayan’s movement against corruption.  Under such conditions the most important question that arises is: how will a Parliament with members like Rashid Masood and Lalu Prasad Yadav pass Anna Hazare’s strong anti-corruption laws? Another important question, if in the present situation Lalu Yadav had a higher political status, for e.g. had 10-15 MPs with him, would his pressure to choke the case against him have worked on the government? As it is the Ex-CBI Director, Mr. Yoginder Singh, had earlier admitted that the then Prime Minster, Mr. Indra Kumar Gujral had asked him to ‘go slow’ on the ‘Fodder Scam’ case</p>." +
				"<br><br><p>We shouldn’t take these cases quietly, considering them a matter of the past. If we look at it carefully, in these two cases Lalu and Rashid did not suffer any political loss. Taking advantage of their positions, they have squeezed every last bit they could out of their powerful positions. They are still in possession of the money earned through corruption. Moreover, it may take 10-15 more years to get a final verdict on these cases from the High Courts or the Supreme Court. The real punishment would have come if they were jailed and their properties confiscated as soon as the scams came to light.</p>" +
				"<br><br>- Manish Sisodia");
		newsItem.setDate("28-Sep-2013");
		newsItem.setId(3L);
		newsItem.setImageUrl("https://lh6.googleusercontent.com/-atij2hzmr9o/UdHBD8G1GeI/AAAAAAAAME4/lgvC8uIMchM/s720/bg-wallpaper-01.jpg");
		newsItem.setSource("aamaadmiparty.org");
		newsItem.setTitle("Why does our Country have to suffer at the hands of those like Masood and Laloo");
		newsItem.setWebUrl("http://www.aamaadmiparty.org/news/why-our-country-has-to-suffer-at-the-hands-of-those-like-masood-and-laloo");
		List<String> oneLiners = new ArrayList<String>();
		oneLiners.add("Why does our Country have to suffer at the hands of those like Masood and Laloo http://www.aamaadmiparty.org/news/why-our-country-has-to-suffer-at-the-hands-of-those-like-masood-and-laloo");
		oneLiners.add("1 Yogender yadav's reply to HRD misntery http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		oneLiners.add("2 Yogender yadav's reply to HRD misntery http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		newsItem.setOneLiners(oneLiners);

		Gson gson = new Gson();
		System.out.println(gson.toJson(newsItem));
		
	}
	
	private static void createNewsItem04(){
		NewsItem newsItem = new NewsItem();
		newsItem.setAuthor("");
		newsItem.setContent("On October 2 each year, the nation remembers Gandhiji, the most revered leader of our Independence movement. But sadly, nearly seven decades after achieving our freedom, we find that the quality of our democracy needs a lot of improvement. Political parties have subverted the inspirational meanings of words like 'liberty', 'freedom' and 'independence' to pursue their own private, crooked goals. " +
				"<br><br><p>As all of us are well aware, the Supreme Court recently barred criminals from contesting elections, and also ruled that convicted MLAs and MPs would lose their seats. The government immediately tried to overturn this through an Ordinance. The other parties, which are supposed to provide opposition to such dubious steps, also kept quiet because such moves benefit them too. The ruling party as well as the opposition is now resorting to public posturing. This  match fixing by the entire political class is so obvious to We The People that even the Supreme Court in its welcome decision in the NOTA verdict felt an urgent need to force the entire political class of India to accept the will of the people to usher in Systemic Change.  </p>" +
				"<br><p>It’s a sad de-facto admission that the entire political class has failed the people of this great nation by incessantly trying to introduce retrograde steps that harm the hard earned democracy in India. </p>" +
				"<br><br><p>Who will resist such developments? The Election Commission? Certainly not. That body is already weakened and made a hand-maiden of the same shady politics. All the things that should support a strong democracy - easy voter registration, reliable voter rolls, high percentages of voting, limiting the role of money in elections ... there are many such things that the EC should have ensured decades ago, but have still not been done. In such a scenario, we cannot rely on the EC either to defend our democracy from the shenanigans of different parties.</p>" +
				"<br><br><p>In all major political parties, honest people are progressively being sidelined and a situation is being so created that only the corrupt-criminal flourish, obtain tickets and win elections. And laws and procedures are being twisted in every institution to ensure things remain this way.</p>" +
				"<br><br><p>Friends, the water is now above our nose. It is high time that the well meaning people of this country, who are the overwhelming majority, wherever our ideological loyalties may lie, come together and demand that our country should remain loyal to the original goals of the freedom movement.We The People Specifically Demand that :</p>" +
				"<br><br>1. The laws of the country must not, under any circumstance, allow convicted persons to remain elected representatives." +
				"<br>2. The Election Commission must be fully empowered to and held accountable to ensure that money power in elections is reduced progressively and eliminated." +
				"<br>3. The Election Commission must be held accountable to ensure easy registration of voters, including in all colleges and through post offices, and that the voter rolls should be accurate, and continuously updated, with penalties applicable for errors and deficiencies in registration." +
				"<br>4. RTI on all Political Parties be made compulsory." +
				"<br>5. Transparency in declaring funds is made mandatory on all political parties." +
				"<br>6. Polarization of this uniquely diverse nation by campaigning for votes in the name of religion, caste, community is banned." +
				"<br><br><p>Let us turn the clock back to the original, true meaning of democracy.</p>" +
				"<br><br>Come let us walk for a free India that Gandhiji would be proud of, and rebuild the nation according to principles and goals with integrity." +
				"<br><br>Aam Aadmi Party, Loksatta, Civil society, various Social service organizations, intellectuals and activists have organized this WALK FOR DEMOCRACY." +
				"<br><br>The Program: " +
				"<br>Wed 2nd Oct 2013, Gandhi Jayanti" +
				"<br>Starts: at 9.30 am UB City Square, Kasturba Road." +
				"<br>Route: Kasturba Road, Mallya Hospital, Raja Rammohunroy Square," +
				"<br>NR Circle, Mysore Bank, KG Circle, Kapali Road." +
				"<br>Concludes at: 12.30 pm at Gandhi Statue, near AnandRao Circle, Gandhinagar." +
				"<br>TOGETHER, LET US SHOW THAT THE FORCES FOR PROGRESS ARE JOINING HANDS to take on the corrupt nexus of the major parties who have let down our democracy all these years." +
				"<br><br>+++++++++++++++++++++++++++++++++++++" +
				"<br><br>- ಚುನಾವಣಾ ಅಕ್ರಮಗಳನ್ನು ವಿರೋಧಿಸಿ" +
				"<br>- ಸಮಗ್ರ ಚುನಾವಣಾ ಸುಧಾರಣೆಗಳನ್ನು ಆಗ್ರಹಿಸಿ" +
				"<br>ಗಾಂಧಿ ಜಯಂತಿಯಂದು" +
				"<br>“ನೈಜ ಪ್ರಜಾಪ್ರಭುತ್ವಕ್ಕಾಗಿ ಪಾದಯಾತ್ರೆ”" +
				"<br>ಮಾನ್ಯರೆ," +
				"<br>ನಿಮಗೆಲ್ಲರಿಗೂ ತಿಳಿದಿರುವಂತೆ ಇತ್ತೀಚೆಗೆ ಸುಪ್ರೀಂ ಕೋರ್ಟ್ ತನ್ನ ಮಹತ್ವದ ತೀರ್ಪಿನಲ್ಲಿ ಕಳಂಕಿತರು ಚುನಾವಣೆಯಲ್ಲಿ ಸ್ಪರ್ಧಿಸುದು ಕಾನೂನಿಗೆ ವಿರುದ್ಧ ಮತ್ತು ಅವರು ಕಳಂಕಿತರು ಎಂದು ತೀರ್ಪು ಬಂದ ಕೂಡಲೇ ಅವರ ಚುನಾಯಿತ ಸ್ಥಾನ ರದ್ದಾಗುತ್ತದೆ ಎಂದು ಹೇಳಿತ್ತು. ಆದರೆ ಕೇಂದ್ರ ಸರ್ಕಾರ ತರಾತುರಿಯಲ್ಲಿ ಈ ತೀರ್ಪನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸುವ ಸಲುವಾಗಿ ಸುಗ್ರಿವಾಜ್ಞೆಯನ್ನು ತಂದಿದೆ. ಇದು ಪ್ರಜಾಪ್ರಭುತ್ವ ವಿರೋಧಿ ಮತ್ತು ಹಲವಾರು ವರ್ಷಗಳಿಂದ ನಮ್ಮೆಲ್ಲರ ಹೋರಾಟಕ್ಕೆ ಹಿನ್ನೆಡೆಯಾಗಲಿದೆ. ಜೊತೆಗೆ ಎಲ್ಲರಿಗೂ ತಿಳಿದಿರುವಂತೆ ಚುನಾವಣೆ ನಡೆಯುವ ರೀತಿ, ಆ ಸದರ್ಭದಲ್ಲಿ ನಡೆಯುವ ಅಕ್ರಮಗಳು, ಅವುಗಳನ್ನು ಪರಿಣಾಮಕಾರಿಯಾಗಿ ತಡೆಯಲಾಗದ ಚುನಾವಣಾ ಆಯೋಗದ ಅಸಹಾಯಕತೆ ಇತ್ಯಾದಿಗಳು ನಮ್ಮ ಸಮಾಜದಲ್ಲಿ ಜಾತಿವಾದಿಗಳು, ಭ್ರಷ್ಟರು, ಅನರ್ಹರು ಮತ್ತು ಯಾವುದೇ ರೀತಿಯ ಜನಪರ ಕಾಳಜಿ ಇಲ್ಲದ ಸ್ವಕೇಂದ್ರಿತ ಸ್ವಾರ್ಥಿ ರಾಜಕಾರಣಿಗಳು ಮಾತ್ರ ಗೆದ್ದು ಬಂದು ಜನಪ್ರತಿನಿಧಿಗಳಾಗುವ ದುರದೃಷ್ಟಕರ ಪರಿಸ್ಥಿತಿಯನ್ನು ಸೃಷ್ಟಿಸಿದೆ." +
				"<br>ಯಾವುದೇ ಪ್ರಮುಖ ರಾಜಕೀಯ ಪಕ್ಷಗಳನ್ನು ತೆಗೆದುಕೊಂಡರೂ, ಅರ್ಹರು, ಪ್ರಾಮಾಣಿಕರು ಮತ್ತು ಪ್ರಜಾಪ್ರಭುತ್ವವಾದಿಗಳಿಗೆ ಜಾತಿ ಮತ್ತು ಹಣದ ಆಧಾರದ ಮೇಲೆ ಟಿಕೆಟ್ ನಿರಾಕರಿಸಿ ಅವರು ಚುನಾವಣೆಗಳಲ್ಲಿ ಸ್ಪರ್ಧಿಸಿ ಗೆಲ್ಲಲಾಗದಂತಹ ವ್ಯವಸ್ಥೆ ಈ ಪಕ್ಷಗಳಲ್ಲಿದೆ. ಆ ಪಕ್ಷಗಳಲ್ಲಿನ ಅಷ್ಟಿಷ್ಟು ಯೋಗ್ಯತೆ ಉಳ್ಳವರು ಮತ್ತು ಜನಪರವಾಗಿರುವವರು ಸಹ ಯಾವುದಾದರು ಒಂದು ರೀತಿಯಲ್ಲಿ ಕಾನೂನು ಉಲ್ಲಂಘಿಸಿ ಮತ್ತು ಚುನಾವಣಾ ಅಕ್ರಮಗಳನ್ನು ಎಸಗಿ ಅನೈತಿಕವಾಗಿಯೇ ಗೆದ್ದು ಬರುತ್ತಿದ್ದಾರೆ." +
				"<br>ಈ ಸಂದರ್ಭದಲ್ಲಿ ಪ್ರಮುಖ ಪಕ್ಷಗಳಲ್ಲದ ಆದರೆ ಸಿದ್ದಾಂತ, ಪ್ರಾಮಾಣಿಕತೆ, ಬದ್ಧತೆ, ಜಾತ್ಯಾತೀತ, ಜನಪರ ನಿಲುವಿನ ಆಧಾರದ ಮೇಲೆ ರಾಜಕೀಯ ವ್ಯವಸ್ಥೆಯನ್ನು ಕಟ್ಟಲು ಪ್ರಯತ್ನಿಸುತ್ತಿರುವ ಇತರ ಪಕ್ಷಗಳು ಮತ್ತು ವ್ಯಕ್ತಿಗಳು ಎಂತಹುದೇ ಸಣ್ಣ ಚುನಾವಣೆಯಲ್ಲೂ ನ್ಯಾಯುತವಾಗಿ ಸ್ಪರ್ಧಿಸಿ ಗೆದ್ದು ಬರಲಾಗದ ಪರಿಸ್ಥಿತಿ ನಮ್ಮದಾಗಿದೆ. ಇದು ಹೀಗೆಯೆ ಮುಂದುವೆರದೆರೆ ಮತ್ತಷ್ಟು ಭೀಕರವಾದ ದಿನಗಳನ್ನು, ನ್ಯಾಯ ಮತ್ತು ಕಾನೂನಿಗೆ ಬೆಲೆ ಇಲ್ಲದ ಅನೈತಿಕ ಪರಿಸರವನ್ನು ನಾವು ಕಾಣಲಿದ್ದೇವೆ." +
				"<br>ಈ ನಿಟ್ಟಿನಲ್ಲಿ ಹಾಲಿ ಚುನಾವಣಾ ಪದ್ದತಿಯಲ್ಲಿನ ದೋಷಗಳು, ಅಕ್ರಮಗಳು, ಅರ್ಹರ ಅನುಪಸ್ಥಿತಿಯ ಕಾರಣವಾಗಿ ಹೆಚ್ಚುತ್ತಿರುವ ಸಮಸ್ಯೆಗಳು, ಪ್ರತಿಗಾಮಿತನ, ಭ್ರಷ್ಟಾಚಾರ, ಅಸಮಾನತೆ, ಈ ಕಾರಣಗಳಿಂದಾಗಿ ಚುನಾವಣಾ ವ್ಯವಸ್ಥೆಯಲ್ಲಿ ಆಗಬೇಕಾದ ಸುಧಾರಣೆಗಳ ಅಗತ್ಯತೆಯನ್ನು ಪ್ರತಿಪಾದಿಸಿ, ಚುನಾವಣಾ ಆಯೋಗ ಸ್ವತಂತ್ರವಾಗಿ ಮತ್ತು ನಿಷ್ಪಕ್ಷಪಾತವಾಗಿ ಚುನಾವಣೆಗಳನ್ನು ನಡೆಸಲು ಮತ್ತು ಚುನಾವಣಾ ಅಕ್ರಮಗಳ ವಿರುದ್ದ ದಿಟ್ಟ ಕ್ರಮ ಕೈಗೊಳ್ಳಲು ಆಗ್ರಹಿಸಿ, ಅರ್ಹರು ಮತ್ತು ಪ್ರಾಮಾಣಿಕರು ರಾಜಕೀಯ ಹೋರಾಟಕ್ಕೆ ಮುಂದಾಗಲು ವಿನಂತಿಸಿ, ಇದೇ ಗಾಂಧಿ ಜಯಂತಿಯಂದು ಬೆಂಗಳೂರಿನಲ್ಲಿ “ನೈಜ ಪ್ರಜಾಪ್ರಭುತ್ವಕ್ಕಾಗಿ ಪಾದಯಾತ್ರೆ” ಹಮ್ಮಿಕೊಳ್ಳಲಾಗಿದೆ." +
				"<br>ಈ ಪಾದಯಾತ್ರೆಯಲ್ಲಿ ಮೌಲ್ಯಾಧಾರಿತ ರಾಜಕಾರಣದಲ್ಲಿ ವಿಶ್ವಾಸವಿಟ್ಟಿರುವ ಮತ್ತು ಅದನ್ನು ಪಾಲಿಸುತ್ತಾ ಬರುತ್ತಿರುವ ರಾಜ್ಯದ ಹಲವು ರಾಜಕೀಯ ಪಕ್ಷಗಳು, ಸಾಮಾಜಿಕ ಹೋರಾಟಗಾರರು, ಚಿಂತಕರು, ಜನಪರ ಸಂಘ ಸಂಸ್ಥೆಗಳು ಪಾಲ್ಗೊಳ್ಳುತ್ತಿದಾರೆ." +
				"<br>ಈ ಪ್ರಯತ್ನಕ್ಕೆ ತಮ್ಮ ಬೆಂಬಲದ ನಿರೀಕ್ಷೆಯಲ್ಲಿದ್ದೇವೆ," +
				"<br><br><br>ಆಯೋಜಕರು:" +
				"<br>ಆಮ್ ಆದ್ಮಿ ಪಕ್ಷ,   ಲೋಕಸತ್ತಾ ಪಕ್ಷ, ಇತರೆ ಸಂಘಸಂಸ್ಥೆಗಳು..");
		newsItem.setDate("28-Sep-2013");
		newsItem.setId(4L);
		newsItem.setImageUrl("http://www.aamaadmiparty.org/sites/default/files/styles/670_x_270/public/walk%20for%20democracy_0.jpg");
		newsItem.setSource("aamaadmiparty.org");
		newsItem.setTitle("WALK FOR DEMOCRACY");
		newsItem.setWebUrl("http://www.aamaadmiparty.org/news/walk-for-democracy");
		List<String> oneLiners = new ArrayList<String>();
		oneLiners.add("An Open Letter to the HRD Minister from Yogendra Yadav http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		oneLiners.add("Yogender yadav's reply to HRD misntery http://www.aamaadmiparty.org/news/an-open-letter-to-the-hrd-minister-from-yogendra-yadav");
		newsItem.setOneLiners(oneLiners);

		Gson gson = new Gson();
		System.out.println(gson.toJson(newsItem));
		
	}

	
}
