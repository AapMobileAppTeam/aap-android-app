package com.next.aap.dto;

import com.google.gson.Gson;

public class TestCandidateSelectionProcessDto {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CandidateSelectionProcess csp = new CandidateSelectionProcess();
		csp.setMessage("दिल्ली विधानसभा चुनाव में राजनैतिक क्रान्ति के लिए \n\n" +
				"उम्मीदवार के आवेदन एवं चयन की प्रक्रिया" +
				"\n\n1. किसी भी विधानसभा के 100 मतदाता किसी प्रत्याशी का नाम प्रस्तावित कर सकते हैं, अथवा कोई प्रत्याशी स्वयं भी 100 मतदाताओं के अनुमोदन के साथ अपना नाम प्रस्तावित कर सकता है. कोई एक मतदाता किसी एक ही उम्मीदवार के नाम का प्रस्ताव दे सकता है. " +
				"\n\n2. आवेदन फ़ार्म आम आदमी पार्टी कि वेबसाईट www.aamaadmiparty.org से डाउनलोड किया जा सकता है अथवा हमारे किसी भी पार्टी ऑफिस से प्राप्त किया जा सकता है. " +
				"<br><br>3. पूर्ण रूप से भरा हुआ आवेदन पत्र, 15 अप्रैल 2013 के बाद किसी भी दिन, सुबह 10 से शाम 5 बजे के बीच, (रविवार को छोड़) पार्टी के मुख्य कार्यालय में दिलीप पांडे, सचिव, चुनाव समिति, ए-119, कौशांबी, गाज़ियाबाद के पास जमा करवाएं. प्रत्याशी चयन प्रक्रिया के बारे में कोई भी पत्र व्यवहार इसी आफिस से होगा. " +
				"\n\n4. पार्टी की ओर से इस प्रक्रिया के लिए एक स्क्रीनिंग कमेटी बनाई गई है– जिसमें निम्न सदस्य होंगे–" +
				"\n\n1. Manish Sisodia" +
				"\n2. Sanjay Singh" +
				"\n3. Vinod Kumar Binny" +
				"\n4. Rajan Prakash" +
				"\n5. Dilip K Pandey" +
				"\n6. Nisha Singh" +
				"\n7. Ashish Talwar" +
				"\n8. Kishore Asthana" +
				"\n9. BS Dhanola" +
				"\n10. Rahul Mehra" +
				"\n11. Abhay Singh" +
				"\n12. Vinay Mittal" +
				"\n\n5. यह स्क्रीनिंग कमेटी, ५ मई के बाद से, प्रत्येक विधान सभा के लिए प्राप्त आवेदनों में से अधिकतम ५ आवेदकों की, एक शार्ट-लिस्ट घोषित करेगी. स्क्रीनिंग कमेटी, शार्ट-लिस्ट तैयार करने के लिए, अपने अलग अलग स्रोतों यथा- कार्यकर्ता साथी, पत्रकारों एवं गणमान्य संपर्कों से जानकारी लेगी और सभी आवेदकों से मुलाकात करेगी. शार्टलिस्ट के बारे में कमेटी का निर्णय अंतिम होगा और इस बारे में कोई शिकायत स्वीकार्य नहीं होगी. शार्ट-लिस्ट की घोषणा के बाद उस विधानसभा के लिए आवेदन स्वीकार नहीं किया जाएगा. " +
				"\n\n6. शार्टलिस्ट को वेबसाईट, कार्यकर्ताओं और विभिन्न माध्यमों के ज़रिये, जनता के बीच प्रचारित किया जाएगा और उन पर कार्यकर्ताओं तथा अन्य आम नागरिकों से राय/सूचना मांगी जायेगी. लोग किसी भी प्रत्याशी के बारे में अपनी अच्छी-बुरी राय प्रमाण या तथ्यों सहित, वेबसाईट के माध्यम से अथवा सीधे पार्टी मुख्यालय में दे सकेंगे. इसके लिए कुछ दिन का समय रहेगा." +
				"<br><br>7. स्क्रीनिंग कमेटी प्रत्येक सूचना/राय को संज्ञान में लेगी और किसी प्रत्याशी के बारे में प्रमाण सहित नकारात्मक सूचना मिलने पर उसका नाम शार्टलिस्ट से हटाकर संशोधित शार्टलिस्ट घोषित करेगी. " +
				"\n\n8. आम आदमी पार्टी नई-नई बनी पार्टी है. इसमें बहुत से ऐसे साथी हैं जो आन्दोलन में लम्बे समय से वालंटियर करते आ रहे हैं. लेकिन अभी पार्टी के औपचारिक सक्रिय सदस्य नहीं बने हैं. ऐसे साथियों को इस चुनाव के लिए 'विशेष सक्रिय कार्येकर्ता' घोषित किया जायेगा और शार्टलिस्ट में शामिल उम्मीदवारों के बारे में उनकी राय ली जाएगी." +
				"<br><br>इसके बाद विधानसभा स्तर पर एक बैठक बुलाकर 'विशेष सक्रिय कार्येकर्ताओं' से प्रेफरेंशियल वोटिंग कराई जाएगी. यह प्रक्रिया गुप्त मतदान द्वारा होगी और चूँकि अभी हर विधानसभा में 'विशेष सक्रिय कार्यकर्ताओं' की संख्या बेहद सीमित है, इसलिए इसके नतीजे घोषित न करते हुए इन्हें पोलिटिकल अफेयर कमेटी के सामने रखा जायेगा. " +
				"\n\n9. पोलिटिकल अफेयर कमेटी शार्टलिस्ट में शामिल सभी प्रत्याशियों से व्यक्तिगत मुलाकात कर संवाद करेगी. " +
				"<br><br>10.  कार्यकर्ता वोटिंग से उम्मीदवार की लोकप्रियता निकलकर आएगी. साथ ही व्यक्तिगत संवाद से यह समझने की कोशिश की जाएगी कि क्या वो व्यक्ति साहसी है, दिल्ली में व्यवस्था परिवर्तन का विज़न रखता है, स्वराज की धारणा की कितनी समझ है, उसके अलग-अलग मुद्दों पर क्या विचार हैं, उसमें सभी धर्म, जाति के प्रति सम्माsन है या नहीं, इत्यादि. उस व्यक्ति की लोकप्रियता, विभिन्न मुद्दों पर उनके विचार और आम जनता की राय को ध्यान में रखते हुए पोलिटिकल अफैयर कमेटी निर्णय लेगी कि शार्टलिस्ट में शामिल व्यक्तियों में से उम्मीदवार कौन होगा? ");
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(csp));
		//System.out.println(manifestoDto.getContent());
		
		
		
		String cspEnglish = "As the next step in our political revolution, the below process would be followed for selection of candidates for the Delhi Assembly Elections of 2013." +
				"\n\n1. 100 voters of any assembly constituency can come together and propose the name of a person they wish to be the candidate for their constinuency. Also, a prospective candidate himself, with the approval of the 100 voters from his consituency, may propose his name. A particular voter may propose the name of a single candidate only." +
				"<br><br>2. Application forms can be downloaded online or can be obtained from any of our party offices." +
				"\n\n3. After filling in all details asked for in the application form, one can deposit it at the party head quarters with Mr. Dilip Pandey, Secretary, Selection Committee (Address: A -119, Kaushambi, Ghaziabad). This can be done on any day after the 15th of April 2013, between 10am and 5pm (except for Sundays). Any correspondence regarding the candidate selection process will happend from this same office." +
				"\n\n4. A screening committee has been created by the party to work on this process. Members of this screening committee are -" +
				"\n\n1. Manish Sisodia" +
				"\n2. Sanjay Singh" +
				"\n3. Vinod Kumar Binny" +
				"\n4. Rajan Prakash" +
				"\n5. Dilip K Pandey" +
				"\n6. Nisha Singh" +
				"\n7. Ashish Talwar" +
				"\n8. Kishore Asthana" +
				"\n9. BS Dhanola" +
				"\n10. Rahul Mehra" +
				"\n11. Abhay Singh" +
				"\n12. Vinay Mittal" +
				"\n\n5. After the 5th of May, the screening committee will start the process of putting together a short list of a maximum of 5 candidates from eacy constituency, from among all the applications received. For the purpose of creating such a short list, the screening committee will have one-on-one interactions with all the applicants and will also take inputs from various sources such as - active volunteers, journalists, reputated citizens and more. The decision of the screening committee will be final in shortlisting candidates. Ater releasing a shortlist, candidate applications will not be received for that particular constituency." +
				"\n\n6. The above shortlist will be promoted in the public domain by various means such as - releasing it online, campaign by party workers and other media, and opinion on the candidates will be sought from active party volunteers and the public at large. Everybody will be allowed to share their views, good or bad, about any candidate, and would also be invited to submit proof of any wrong-doings by the shortlisted candidates. Such submissions can be made online or sent directly to the party office." +
				"\n\n7. The screening committee will take congnisance of every information/opinion that is shared, and on receiving evidence-backed negative information about a candidate, strike out the name of such candidates and release an updated shortlist." +
				"\n\n8. Aam Aadmi Party is a recently formed political outfit. There are many people who have now participated and contributed for a long time as volunteers in this movement, but have not been formally enrolled as party members. All such volunteers will be indentified/declared as 'active volunteers' and their opinion will be sought on the shortlist of candidates." +
				"\n\n<p>Post this, such 'active volunteers' would gather together at constituency level meetings and would rank the candidates by means of preferential voting. This process would be carried out by a secret ballot, the results of which would be made available to the political affairs committee. At the moment, the number of active volunteers at each constituency is not significantly large. For this reason, we do not plan to release the outcome of the preferential ranking vote and rather only submit it to the political affairs committee as an input.</p.>" +
				"\n\n9. Next, the political affairs committee will have one-on-one interaction with each of the shortlisted candidates." +
				"\n\n10. Preferential voting by active volunteers will bring forward the popularity of candidates. While personal interaction with the screening and political committee will be used to determine if the candidate is - courageous, has a vision for bringing in systemic change in Delhi, understands all aspects and nuances of direct democracy (swaraj), has learned opinions on broad range of issues, has respect for every caste and religion etc. Considering the popularity of all candidates, their thoughts on various issues and opinion held by the public at large about them - the political affairs committee will take the decision on who would be the candidate from a particular constituency.";
		CandidateSelectionProcess cspEn = new CandidateSelectionProcess();
		cspEn.setMessage(cspEnglish);
		System.out.println(gson.toJson(cspEn));
;	}

}
