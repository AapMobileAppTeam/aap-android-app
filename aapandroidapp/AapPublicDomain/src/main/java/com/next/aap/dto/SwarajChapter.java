package com.next.aap.dto;

import java.io.Serializable;

public class SwarajChapter implements Serializable {

	private static final long serialVersionUID = 1L;
	private String title;
	private String content;
	private String webUrl;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getWebUrl() {
		return webUrl;
	}
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
}
