package com.next.aap.dto;

import java.io.Serializable;
import java.util.List;

public class AllHomeScreenItem implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<BlogItem> blogItems;
	private List<NewsItem> newsItems;
	private List<VideoItem> videoItems;
	private List<EventItem> eventItems;
	
	public List<BlogItem> getBlogItems() {
		return blogItems;
	}
	public void setBlogItems(List<BlogItem> blogItems) {
		this.blogItems = blogItems;
	}
	public List<NewsItem> getNewsItems() {
		return newsItems;
	}
	public void setNewsItems(List<NewsItem> newsItems) {
		this.newsItems = newsItems;
	}
	public List<VideoItem> getVideoItems() {
		return videoItems;
	}
	public void setVideoItems(List<VideoItem> videoItems) {
		this.videoItems = videoItems;
	}
	public List<EventItem> getEventItems() {
		return eventItems;
	}
	public void setEventItems(List<EventItem> eventItems) {
		this.eventItems = eventItems;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((blogItems == null) ? 0 : blogItems.hashCode());
		result = prime * result
				+ ((eventItems == null) ? 0 : eventItems.hashCode());
		result = prime * result
				+ ((newsItems == null) ? 0 : newsItems.hashCode());
		result = prime * result
				+ ((videoItems == null) ? 0 : videoItems.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AllHomeScreenItem other = (AllHomeScreenItem) obj;
		if (blogItems == null) {
			if (other.blogItems != null)
				return false;
		} else if (!blogItems.equals(other.blogItems))
			return false;
		if (eventItems == null) {
			if (other.eventItems != null)
				return false;
		} else if (!eventItems.equals(other.eventItems))
			return false;
		if (newsItems == null) {
			if (other.newsItems != null)
				return false;
		} else if (!newsItems.equals(other.newsItems))
			return false;
		if (videoItems == null) {
			if (other.videoItems != null)
				return false;
		} else if (!videoItems.equals(other.videoItems))
			return false;
		return true;
	}
	

}
