package com.next.aap.messages;

public class TaskStatus {

	public static final String STATUS_RUNNING_IN_BACKGROUND = "RunningInBackGround";
	public static final String STATUS_COMPLETED = "Completed";
	public static final String STATUS_FAILED = "Failed";
	public static final String STATUS_ALREADY_REGISTERED = "AlreadyRegistered";

}
