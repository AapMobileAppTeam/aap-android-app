package com.next.aap.messages;

import java.io.Serializable;

public class FacebookUserRegisteredMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String deviceRegId;
	private Long userId;
	private String status;

	public String getDeviceRegId() {
		return deviceRegId;
	}

	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
