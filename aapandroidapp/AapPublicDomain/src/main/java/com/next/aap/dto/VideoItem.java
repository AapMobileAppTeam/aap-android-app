package com.next.aap.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class VideoItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String title;//title of the video
	private String imageUrl;//image preview of the video
	private String webUrl;//youtube page url
	private Date publishDate;//Publish date of this item
	private List<String> oneLiners;//all one liners attached to this video which can be tweeted
	private String description;//description of video
	private String youtubeVideoId;
	private boolean global;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getWebUrl() {
		return webUrl;
	}
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public List<String> getOneLiners() {
		return oneLiners;
	}
	public void setOneLiners(List<String> oneLiners) {
		this.oneLiners = oneLiners;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getYoutubeVideoId() {
		return youtubeVideoId;
	}
	public void setYoutubeVideoId(String youtubeVideoId) {
		this.youtubeVideoId = youtubeVideoId;
	}
	public boolean isGlobal() {
		return global;
	}
	public void setGlobal(boolean global) {
		this.global = global;
	}
	
}
