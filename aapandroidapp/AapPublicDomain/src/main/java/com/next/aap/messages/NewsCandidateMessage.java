package com.next.aap.messages;

import java.io.Serializable;

public class NewsCandidateMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int totalNewCandidates;

	public int getTotalNewCandidates() {
		return totalNewCandidates;
	}

	public void setTotalNewCandidates(int totalNewCandidates) {
		this.totalNewCandidates = totalNewCandidates;
	}

	
}
