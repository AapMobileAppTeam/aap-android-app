package com.next.aap.dto;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class FacebookPost implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String message;
	private String picture;
	private String link;
	private String source;
	private String name;
	private String icon;
	private String type;
	private String status_type;
	private List<Action> actions;
	private Privacy privacy;
	private LikeCommentSummary likes;
	private LikeCommentSummary comments;
	private Long created_time;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacebookPost other = (FacebookPost) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public static void main(String[] args){
		FacebookPost facebookPost = new FacebookPost();
		LikeCommentSummary likes = facebookPost.new LikeCommentSummary();
		Summary summary = facebookPost.new Summary();
		summary.setTotal_count(10);
		likes.setSummary(summary);
		facebookPost.setLikes(likes);

		
		LikeCommentSummary comments = facebookPost.new LikeCommentSummary();
		Summary summaryComment = facebookPost.new Summary();
		summaryComment.setTotal_count(20);
		comments.setSummary(summaryComment);
		facebookPost.setComments(comments);

		Gson gson = new Gson();
		System.out.println(gson.toJson(facebookPost));
		
		FacebookPost facebookPost2 = gson.fromJson(gson.toJson(facebookPost), FacebookPost.class);
		System.out.println(facebookPost2.getLikes());
		System.out.println(facebookPost2.getComments());
		
		
		String data = "[{likes: { data: [{id: \"100001019174107\",name: \"Kuber Sharma\"}],paging: {cursors: " +
				"{after: \"MTAwMDAxMDE5MTc0MTA3\",before: \"MTAwMDAxMDE5MTc0MTA3\"}," +
				"next: \"https://graph.facebook.com/290805814352519_440462989386800/likes?date_format=d-M-Y+h%3Ai+e&summary=1&limit=1&access_token=CAACjs4PJeR8BAA0fU8CQsqja53juuqKv8HgTQvoKqlQeLp6iP9Xds32HyReJ85raVZBl2rJCZAoy4HZBgmLUGZCLCG7A2jjZAyz1GsAXX88ZAJXo8W2OL3M3OxdiEvF9WAuBlZATGZBkO8a0TaENjEgcZBBZCZAZCVi6LGF5dEdUsJDGuCqqZBx1uZCurPCaJTiWeNVC6CaFILZCjwRotkzTHcQ936Q7JW36kDnQGHKfdOZA7U1VTAZDZD&after=MTAwMDAxMDE5MTc0MTA3\"}," +
				"summary: {total_count: 2707}}," +
				"comments: {data: [{id: \"440462976053468_1588859\",from: {name: \"Nitin Pratap Singh\",id: \"624173061\"}," +
				"message: \"Vibhu Nayan :)\",message_tags: [{id: \"100000889587279\",name: \"Vibhu Nayan\",type: \"user\",offset: 0," +
				"length: 11}],can_remove: true,created_time: \"05-Dec-2013 04:15 UTC\",like_count: 2,user_likes: false}]," +
				"paging: {cursors: {after: \"MTU3\",before: \"MTU3\"}," +
				"next: \"https://graph.facebook.com/290805814352519_440462989386800/comments?date_format=d-M-Y+h%3Ai+e&summary=1&limit=1&access_token=CAACjs4PJeR8BAA0fU8CQsqja53juuqKv8HgTQvoKqlQeLp6iP9Xds32HyReJ85raVZBl2rJCZAoy4HZBgmLUGZCLCG7A2jjZAyz1GsAXX88ZAJXo8W2OL3M3OxdiEvF9WAuBlZATGZBkO8a0TaENjEgcZBBZCZAZCVi6LGF5dEdUsJDGuCqqZBx1uZCurPCaJTiWeNVC6CaFILZCjwRotkzTHcQ936Q7JW36kDnQGHKfdOZA7U1VTAZDZD&after=MTU3\"}," +
				"summary: {order: \"ranked\",total_count: 157}}}]";
		
		Type type = new TypeToken<List<FacebookPost>>() {
		}.getType();
		List<FacebookPost> facebookPosts = new  Gson().fromJson(data, type);

		System.out.println(facebookPosts.get(0).getLikes());
		System.out.println(facebookPosts.get(0).getComments());

	}

	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getPicture() {
		return picture;
	}


	public void setPicture(String picture) {
		this.picture = picture;
	}


	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getStatus_type() {
		return status_type;
	}


	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}


	public List<Action> getActions() {
		return actions;
	}


	public void setActions(List<Action> actions) {
		this.actions = actions;
	}


	public Privacy getPrivacy() {
		return privacy;
	}


	public void setPrivacy(Privacy privacy) {
		this.privacy = privacy;
	}


	public LikeCommentSummary getLikes() {
		return likes;
	}


	public void setLikes(LikeCommentSummary likes) {
		this.likes = likes;
	}


	public LikeCommentSummary getComments() {
		return comments;
	}


	public void setComments(LikeCommentSummary comments) {
		this.comments = comments;
	}


	public Long getCreated_time() {
		return created_time;
	}


	public void setCreated_time(Long created_time) {
		this.created_time = created_time;
	}


	public class Action implements Serializable{
		private static final long serialVersionUID = 1L;
		private String name;
		private String link;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		@Override
		public String toString() {
			return "Action [name=" + name + ", link=" + link + "]";
		}
		
	}
	
	public class Privacy implements Serializable{
		private static final long serialVersionUID = 1L;
		private String description;
		private String value;
		private String friends;
		private String networks;
		private String allow;
		private String deny;
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getFriends() {
			return friends;
		}
		public void setFriends(String friends) {
			this.friends = friends;
		}
		public String getNetworks() {
			return networks;
		}
		public void setNetworks(String networks) {
			this.networks = networks;
		}
		public String getAllow() {
			return allow;
		}
		public void setAllow(String allow) {
			this.allow = allow;
		}
		public String getDeny() {
			return deny;
		}
		public void setDeny(String deny) {
			this.deny = deny;
		}
		@Override
		public String toString() {
			return "Privacy [description=" + description + ", value=" + value
					+ ", friends=" + friends + ", networks=" + networks
					+ ", allow=" + allow + ", deny=" + deny + "]";
		}
		
	}
	
	public class Summary implements Serializable{
		private static final long serialVersionUID = 1L;
		private int total_count;
		private String order;

		public int getTotal_count() {
			return total_count;
		}

		public void setTotal_count(int total_count) {
			this.total_count = total_count;
		}

		public String getOrder() {
			return order;
		}

		public void setOrder(String order) {
			this.order = order;
		}

		@Override
		public String toString() {
			return "Summary [total_count=" + total_count + ", order=" + order
					+ "]";
		}
		
	}
	public class LikeCommentSummary implements Serializable{
		private static final long serialVersionUID = 1L;
		private Summary summary;
		private List<Data> data;
		private Object paging;

		public Summary getSummary() {
			return summary;
		}

		public void setSummary(Summary summary) {
			this.summary = summary;
		}

		public List<Data> getData() {
			return data;
		}

		public void setData(List<Data> data) {
			this.data = data;
		}

		public Object getPaging() {
			return paging;
		}

		public void setPaging(Object paging) {
			this.paging = paging;
		}

		@Override
		public String toString() {
			return "LikeCommentSummary [summary=" + summary + ", data=" + data
					+ ", paging=" + paging + "]";
		}
		
	}
	
	public class Data implements Serializable{
		private static final long serialVersionUID = 1L;
		private String id;
		private String name;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		@Override
		public String toString() {
			return "Data [id=" + id + ", name=" + name + "]";
		}
		
	}
}
