package com.next.aap.dto;

import java.io.Serializable;

public class RegisterTwitterUserProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String twitterUserId;
	private String handle;
	private String accessToken;
	private String accessTokenSecret;
	private String deviceRegId;
	private String deviceType;
	private Long userId;
	
	public String getTwitterUserId() {
		return twitterUserId;
	}
	public void setTwitterUserId(String twitterUserId) {
		this.twitterUserId = twitterUserId;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}
	public void setAccessTokenSecret(String accessTokenSecret) {
		this.accessTokenSecret = accessTokenSecret;
	}
	public String getDeviceRegId() {
		return deviceRegId;
	}
	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	

}
