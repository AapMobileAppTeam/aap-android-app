package com.next.aap.dto;

public class DistrictWeb extends DistrictDto {

	private static final long serialVersionUID = 1L;
	private Long stateId;

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
}
