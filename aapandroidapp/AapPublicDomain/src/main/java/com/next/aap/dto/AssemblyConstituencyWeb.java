package com.next.aap.dto;


public class AssemblyConstituencyWeb extends AssemblyConstituencyDto{

	private static final long serialVersionUID = 1L;
	private Long districtId;

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

}
