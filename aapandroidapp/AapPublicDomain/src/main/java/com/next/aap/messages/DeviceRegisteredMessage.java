package com.next.aap.messages;

import java.io.Serializable;

public class DeviceRegisteredMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String deviceRegId;

	public String getDeviceRegId() {
		return deviceRegId;
	}

	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
}
