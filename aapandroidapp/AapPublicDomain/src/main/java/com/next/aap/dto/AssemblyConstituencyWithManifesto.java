package com.next.aap.dto;


public class AssemblyConstituencyWithManifesto extends AssemblyConstituencyDto{

	private static final long serialVersionUID = 1L;
	private boolean available;
	
	public AssemblyConstituencyWithManifesto(AssemblyConstituencyDto assemblyConstituencyDto){
		super(assemblyConstituencyDto);
	}
	public AssemblyConstituencyWithManifesto(){
		
	}
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	
	
}
