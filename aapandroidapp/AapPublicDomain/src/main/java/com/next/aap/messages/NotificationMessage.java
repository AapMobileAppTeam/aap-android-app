package com.next.aap.messages;

import java.io.Serializable;

public class NotificationMessage<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String MESSAGE = "msg";
	public static final String MESSAGE_TYPE = "msgType";
	public static final String DEVICE_REGISTERED_MESSAGE = "deviceRegistered";
	public static final String FACEBOOK_USER_REGISTERED_MESSAGE = "facebookUserRegistered";
	public static final String NEW_FACEBOOK_MESSAGE = "newFacebookMessage";
	public static final String NEW_NEWS_MESSAGE = "newNewsMessage";
	public static final String NEW_VIDEO_MESSAGE = "newVideoMessage";
	public static final String NEW_BLOG_MESSAGE = "newBlogMessage";
	public static final String NEW_CANDIDATE_MESSAGE = "newCandidateMessage";
	public static final String NEW_VERSION_MESSAGE = "newVersionMessage";

	private String notificationTitle;
	private String notificationType;
	private int totalItem;
	private T source;
	
	public String getNotificationTitle() {
		return notificationTitle;
	}
	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public int getTotalItem() {
		return totalItem;
	}
	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}
	public T getSource() {
		return source;
	}
	public void setSource(T source) {
		this.source = source;
	}

}
