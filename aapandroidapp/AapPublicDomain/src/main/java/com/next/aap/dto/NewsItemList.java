package com.next.aap.dto;

import java.util.List;

public class NewsItemList {

	private List<NewsItem> newsItems;
	private long pageNumber;
	private long pageSize;
	private long totalPages;
	
	public List<NewsItem> getNewsItems() {
		return newsItems;
	}
	public void setNewsItems(List<NewsItem> newsItems) {
		this.newsItems = newsItems;
	}
	public long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}
	public long getPageSize() {
		return pageSize;
	}
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

}
