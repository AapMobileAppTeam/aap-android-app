package com.next.aap.messages;

import java.io.Serializable;

public class NewBlogMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String notificationTitle;
	private String notificationDescription;
	private int totalItem;
	
	public String getNotificationTitle() {
		return notificationTitle;
	}
	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}
	public String getNotificationDescription() {
		return notificationDescription;
	}
	public void setNotificationDescription(String notificationDescription) {
		this.notificationDescription = notificationDescription;
	}
	public int getTotalItem() {
		return totalItem;
	}
	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}
}
