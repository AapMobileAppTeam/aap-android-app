package com.next.aap.dto;

import java.io.Serializable;

public class CandidateSelectionProcess implements Serializable {

	private static final long serialVersionUID = 1L;

	private String message;
	private String imageUrl;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
