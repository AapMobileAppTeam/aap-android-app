package com.next.aap.messages;

import java.io.Serializable;

public class NewsVersionMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int appVersion;

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}
	
	
}
