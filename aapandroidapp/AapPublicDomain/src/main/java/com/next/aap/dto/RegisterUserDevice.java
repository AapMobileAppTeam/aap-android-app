package com.next.aap.dto;

import java.io.Serializable;


public class RegisterUserDevice implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String oldRegId;
	private String regId;
	private String deviceType;
	public String getOldRegId() {
		return oldRegId;
	}
	public void setOldRegId(String oldRegId) {
		this.oldRegId = oldRegId;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

}
