package com.next.aap.dto;


public class UserDeviceDto {

	private Long id;
	private Long stateLivingId;
	private Long districtLivingId;
	private Long assemblyConstituencyLivingId;
	private Long stateVotingId;
	private Long districtVotingId;
	private Long assemblyConstituencyVotingId;
	private String regId;
	private String deviceType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getStateLivingId() {
		return stateLivingId;
	}
	public void setStateLivingId(Long stateLivingId) {
		this.stateLivingId = stateLivingId;
	}
	public Long getDistrictLivingId() {
		return districtLivingId;
	}
	public void setDistrictLivingId(Long districtLivingId) {
		this.districtLivingId = districtLivingId;
	}
	public Long getAssemblyConstituencyLivingId() {
		return assemblyConstituencyLivingId;
	}
	public void setAssemblyConstituencyLivingId(Long assemblyConstituencyLivingId) {
		this.assemblyConstituencyLivingId = assemblyConstituencyLivingId;
	}
	public Long getStateVotingId() {
		return stateVotingId;
	}
	public void setStateVotingId(Long stateVotingId) {
		this.stateVotingId = stateVotingId;
	}
	public Long getDistrictVotingId() {
		return districtVotingId;
	}
	public void setDistrictVotingId(Long districtVotingId) {
		this.districtVotingId = districtVotingId;
	}
	public Long getAssemblyConstituencyVotingId() {
		return assemblyConstituencyVotingId;
	}
	public void setAssemblyConstituencyVotingId(Long assemblyConstituencyVotingId) {
		this.assemblyConstituencyVotingId = assemblyConstituencyVotingId;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

}
