package com.next.aap.dto;

import java.io.Serializable;

public class RegisterUserProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private String email;
	private String name;
	private String gender;
	private String birthday;
	private String location;
	private String deviceRegId;
	private String deviceType;
	private String mobile;
	private String votingState;
	private String livingState;
	private Long votingStateId;
	private Long votingDistrictId;
	private Long votingAcId;
	private Long votingPcId;
	private Long livingStateId;
	private Long livingDistrictId;
	private Long livingAcId;
	private Long livingPcId;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDeviceRegId() {
		return deviceRegId;
	}
	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVotingState() {
		return votingState;
	}
	public void setVotingState(String votingState) {
		this.votingState = votingState;
	}
	public String getLivingState() {
		return livingState;
	}
	public void setLivingState(String livingState) {
		this.livingState = livingState;
	}
	public Long getVotingStateId() {
		return votingStateId;
	}
	public void setVotingStateId(Long votingStateId) {
		this.votingStateId = votingStateId;
	}
	public Long getVotingDistrictId() {
		return votingDistrictId;
	}
	public void setVotingDistrictId(Long votingDistrictId) {
		this.votingDistrictId = votingDistrictId;
	}
	public Long getVotingAcId() {
		return votingAcId;
	}
	public void setVotingAcId(Long votingAcId) {
		this.votingAcId = votingAcId;
	}
	public Long getLivingStateId() {
		return livingStateId;
	}
	public void setLivingStateId(Long livingStateId) {
		this.livingStateId = livingStateId;
	}
	public Long getLivingDistrictId() {
		return livingDistrictId;
	}
	public void setLivingDistrictId(Long livingDistrictId) {
		this.livingDistrictId = livingDistrictId;
	}
	public Long getLivingAcId() {
		return livingAcId;
	}
	public void setLivingAcId(Long livingAcId) {
		this.livingAcId = livingAcId;
	}
	public Long getVotingPcId() {
		return votingPcId;
	}
	public void setVotingPcId(Long votingPcId) {
		this.votingPcId = votingPcId;
	}
	public Long getLivingPcId() {
		return livingPcId;
	}
	public void setLivingPcId(Long livingPcId) {
		this.livingPcId = livingPcId;
	}

}
