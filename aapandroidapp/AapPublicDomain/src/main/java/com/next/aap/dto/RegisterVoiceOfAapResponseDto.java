package com.next.aap.dto;

import java.io.Serializable;

public class RegisterVoiceOfAapResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String userName;
	private String status;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
