package com.next.aap.dto;

import java.util.List;

public class VideoItemList {

	private List<VideoItem> videoItems;
	private long pageNumber;
	private long pageSize;
	private long totalPages;
	
	public List<VideoItem> getVideoItems() {
		return videoItems;
	}
	public void setVideoItems(List<VideoItem> videoItems) {
		this.videoItems = videoItems;
	}
	public long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}
	public long getPageSize() {
		return pageSize;
	}
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

}
