package com.next.aap.messages;

import java.io.Serializable;

public class TwitterUserRegisteredMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String deviceRegId;
	private Long userId;
	private String status;
	private String twitterUserId;

	public String getDeviceRegId() {
		return deviceRegId;
	}

	public void setDeviceRegId(String deviceRegId) {
		this.deviceRegId = deviceRegId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTwitterUserId() {
		return twitterUserId;
	}

	public void setTwitterUserId(String twitterUserId) {
		this.twitterUserId = twitterUserId;
	}
}
