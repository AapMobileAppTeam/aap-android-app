package com.next.aap.dto;

import java.util.List;

public class FacebookPostList {


	private List<FacebookPost> facebookPosts;
	private String previousLink;
	private String nextLink;
	private int pageNumber;
	
	public List<FacebookPost> getFacebookPosts() {
		return facebookPosts;
	}
	public void setFacebookPosts(List<FacebookPost> facebookPosts) {
		this.facebookPosts = facebookPosts;
	}
	public String getPreviousLink() {
		return previousLink;
	}
	public void setPreviousLink(String previousLink) {
		this.previousLink = previousLink;
	}
	public String getNextLink() {
		return nextLink;
	}
	public void setNextLink(String nextLink) {
		this.nextLink = nextLink;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((facebookPosts == null) ? 0 : facebookPosts.hashCode());
		result = prime * result
				+ ((nextLink == null) ? 0 : nextLink.hashCode());
		result = prime * result + pageNumber;
		result = prime * result
				+ ((previousLink == null) ? 0 : previousLink.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacebookPostList other = (FacebookPostList) obj;
		if (facebookPosts == null) {
			if (other.facebookPosts != null)
				return false;
		} else if (!facebookPosts.equals(other.facebookPosts))
			return false;
		if (nextLink == null) {
			if (other.nextLink != null)
				return false;
		} else if (!nextLink.equals(other.nextLink))
			return false;
		if (pageNumber != other.pageNumber)
			return false;
		if (previousLink == null) {
			if (other.previousLink != null)
				return false;
		} else if (!previousLink.equals(other.previousLink))
			return false;
		return true;
	}
	
}
