package com.next.aap.dto;


public class AssemblyConstituencyWithCandidate extends AssemblyConstituencyDto{

	private static final long serialVersionUID = 1L;
	private CandidateDto candidate;
	
	public AssemblyConstituencyWithCandidate(AssemblyConstituencyDto assemblyConstituencyDto){
		super(assemblyConstituencyDto);
	}
	public AssemblyConstituencyWithCandidate(AssemblyConstituencyDto assemblyConstituencyDto, CandidateDto candidate){
		super(assemblyConstituencyDto);
		this.candidate = candidate;
	}
	public AssemblyConstituencyWithCandidate(){
		
	}
	public CandidateDto getCandidate() {
		return candidate;
	}
	public void setCandidate(CandidateDto candidate) {
		this.candidate = candidate;
	}
	
}
