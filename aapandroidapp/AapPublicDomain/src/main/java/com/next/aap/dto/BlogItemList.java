package com.next.aap.dto;

import java.util.List;

public class BlogItemList {

	private List<BlogItem> blogItems;
	private long pageNumber;
	private long pageSize;
	private long totalPages;
	
	public List<BlogItem> getBlogItems() {
		return blogItems;
	}
	public void setBlogItems(List<BlogItem> blogItems) {
		this.blogItems = blogItems;
	}
	public long getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}
	public long getPageSize() {
		return pageSize;
	}
	public void setPageSize(long pageSize) {
		this.pageSize = pageSize;
	}
	public long getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

}
