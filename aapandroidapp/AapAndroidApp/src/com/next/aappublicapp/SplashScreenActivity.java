package com.next.aappublicapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;

import com.next.aappublicapp.util.Config;

public class SplashScreenActivity extends ActionBarActivity {

	UserSessionProvider	sessionProvider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sessionProvider = UserSessionProvider.getInstance(this);
		setContentView(R.layout.activity_splashscreen);
		getSupportActionBar().hide();
	}

	@Override
	protected void onStart() {
		super.onStart();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (sessionProvider.isFBConnected() || sessionProvider.isGoogleConnected() || sessionProvider.isTwitterConnected()) {
					finish();
					if (sessionProvider.isUserRegistered()) {
						Intent homeActivityIntent = new Intent(SplashScreenActivity.this, HomeActivity.class);
						startActivity(homeActivityIntent);
					} else {
						Intent registerActivityIntent;
						registerActivityIntent = new Intent(SplashScreenActivity.this, RegistrationActivity.class);
						startActivity(registerActivityIntent);
					}
				} else {
					Intent loginActivityIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
					startActivity(loginActivityIntent);
				}
			}
		}, Config.SPLASH_DURATION);
	}

	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}

}
