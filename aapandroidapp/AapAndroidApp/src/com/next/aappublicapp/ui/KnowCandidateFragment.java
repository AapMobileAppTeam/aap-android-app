package com.next.aappublicapp.ui;

import java.lang.reflect.Type;

import org.json.JSONArray;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.next.aappublicapp.AAPApplication;
import com.next.aappublicapp.R;
import com.next.aappublicapp.util.ServerConfig;

public class KnowCandidateFragment extends Fragment {
	private static final String	TAG						= "KnowCandidateFragment";

	TextView					constituencySelector;
	TextView					name;
	private ViewGroup			candidateDetails;
	ACModel[]					constituencies;
	String[]					constituencyNames;

	int							selectedConstituency	= -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_knowcandidate, container, false);
		constituencySelector = (TextView) rootView.findViewById(R.id.acSelector);
		name = (TextView) rootView.findViewById(R.id.name);
		candidateDetails = (ViewGroup) rootView.findViewById(R.id.candidate_details);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		constituencySelector.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (constituencies == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,
						constituencyNames);
				new AlertDialog.Builder(getActivity()).setTitle("Select Constituency")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								selectedConstituency = which;
								constituencySelector.setText(constituencies[selectedConstituency].name);
								showCandidateDetails(constituencies[selectedConstituency].candidate);
								dialog.dismiss();
							}
						}).create().show();
			}
		});
		// fetch ac details
		String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_STATES_URL;
		if (ServerConfig.USE_TEMP_URL)
			url = "http://www.donate4india.org/mob/api/acofstate/candidatelist/12";
		candidateDetails.setVisibility(View.GONE);
		JsonArrayRequest getAllConstituenciesRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Type type = new TypeToken<ACModel[]>() {
				}.getType();
				constituencies = new Gson().fromJson(response.toString(), type);
				constituencyNames = new String[constituencies.length];
				for (int i = 0; i < constituencies.length; i++)
					constituencyNames[i] = constituencies[i].name;
				constituencySelector.setText("Select constituency");
				candidateDetails.setVisibility(View.VISIBLE);
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.w(TAG, "error:" + error);
				Toast.makeText(getActivity(), "Error fetching constituencies", Toast.LENGTH_LONG).show();
				constituencySelector.setText("N/A");
			}
		});
		constituencySelector.setText("Loading constituencies...");
		AAPApplication.get().getRequestQueue().add(getAllConstituenciesRequest).setTag(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		AAPApplication.get().getRequestQueue().cancelAll(this);
	}

	private void showCandidateDetails(CandidateModel candidate) {
		if (candidate != null) {
			candidateDetails.setVisibility(View.VISIBLE);
			name.setText(candidate.name);
		} else {
			candidateDetails.setVisibility(View.GONE);
		}
	}

	class ACModel {
		public String			id;
		public String			name;
		public CandidateModel	candidate;
	}

	class CandidateModel {
		public int		id;
		public String	name;
		public String	profile;
		public String	education;
		public String	wealth;
		public String	sourceOfIncome;
		public String	legalCases;
		public String	address;
		public String	contactNumber1;
		public String	profilePic;
		public String	objectives;
		public String	twitterHandle;
		public String	facebookId;
	}

}
