package com.next.aappublicapp.ui;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.FadeInNetworkImageView;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.next.aappublicapp.AAPApplication;
import com.next.aappublicapp.LoginActivity;
import com.next.aappublicapp.R;
import com.next.aappublicapp.UserSessionProvider;
import com.next.aappublicapp.content.ContentManager;
import com.next.aappublicapp.content.models.HomeItemsResponse;
import com.next.aappublicapp.content.models.NewsItemModel;
import com.next.aappublicapp.util.Config;

public class NewsItemFragment extends Fragment {
	private static final String	TAG			= "NewsItemFragment";
	private static final String	EXTRA_ID	= "EXTRA_ID";

	private ContentManager		contentManager;
	private UserSessionProvider	sessionProvider;

	FadeInNetworkImageView		pic;
	LinearLayout				oneliners;
	private TextView			title, author, text;
	ProgressDialog				pd			= null;
	TweetTask					twitterTask	= null;

	public static NewsItemFragment newInstance(int id) {
		Bundle b = new Bundle();
		b.putInt(EXTRA_ID, id);
		NewsItemFragment f = new NewsItemFragment();
		f.setArguments(b);
		return f;
	}

	public NewsItemFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_newsitem, container, false);
		pic = (FadeInNetworkImageView) rootView.findViewById(R.id.pic);
		title = (TextView) rootView.findViewById(R.id.title);
		author = (TextView) rootView.findViewById(R.id.author);
		text = (TextView) rootView.findViewById(R.id.text);
		oneliners = (LinearLayout) rootView.findViewById(R.id.oneliners);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		contentManager = ContentManager.getInstance(getActivity());
		sessionProvider = UserSessionProvider.getInstance(getActivity());
		HomeItemsResponse allItems = contentManager.getLatestResponse();
		int id = getArguments().getInt(EXTRA_ID);
		NewsItemModel itemToDisplay = null;
		for (int i = 0; i < allItems.newsItems.length; i++) {
			if (allItems.newsItems[i].id == (id)) {
				itemToDisplay = allItems.newsItems[i];
			}
		}
		if (itemToDisplay != null) {
			pic.setDefaultImageResId(R.drawable.ic_launcher);
			if (itemToDisplay.imageUrl != null && itemToDisplay.imageUrl.length() > 0)
				pic.setImageUrl(itemToDisplay.imageUrl, AAPApplication.get().getImageLoader());
			else
				pic.setVisibility(View.GONE);
			title.setText(itemToDisplay.title);
			text.setText(Html.fromHtml(itemToDisplay.content));
			author.setText(itemToDisplay.author + "    " + itemToDisplay.date);

			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			oneliners.removeAllViews();
			if (itemToDisplay.oneLiners != null) {
				for (final String oneliner : itemToDisplay.oneLiners) {
					final String url = itemToDisplay.webUrl;
					RelativeLayout onelinerLayout = (RelativeLayout) inflater.inflate(R.layout.item_oneliner, oneliners, false);
					TextView text = (TextView) onelinerLayout.findViewById(R.id.text);
					text.setText(oneliner);
					Button tweetButton = (Button) onelinerLayout.findViewById(R.id.tweet_button);
					Button fbButton = (Button) onelinerLayout.findViewById(R.id.fb_button);
					tweetButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							onTweetClick(oneliner);
						}
					});
					fbButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							onFbClick(oneliner, url);
						}
					});
					oneliners.addView(onelinerLayout);
				}
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (pd != null)
			pd.cancel();
		if (twitterTask != null)
			twitterTask.cancel(true);
	}

	/* FB/Twitter stuff */

	private static final List<String>	PERMISSIONS						= Arrays.asList("publish_actions");
	private static final String			PENDING_PUBLISH_KEY				= "pendingPublishReauthorization";
	private boolean						pendingPublishReauthorization	= false;

	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	RequestAsyncTask	fbTask		= null;
	private Twitter		mTwitter	= null;

	private void onTweetClick(final String tweetText) {
		if (!isNetworkConnected()) {
			Toast.makeText(getActivity(), "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (sessionProvider.isTwitterConnected()) {
			if (Config.LOG_DEBUG)
				Log.d(TAG, "posting to twitter::");
			mTwitter = new TwitterFactory().getInstance();
			mTwitter.setOAuthConsumer(Config.TWITTER_CONSUMER_KEY, Config.TWITTER_CONSUMER_SECRET);
			Log.d(TAG, sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_TOKEN));
			Log.d(TAG, sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_SECRET));
			AccessToken at = new AccessToken(sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_TOKEN),
					sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_SECRET));
			mTwitter.setOAuthAccessToken(at);
			twitterTask = new TweetTask();
			twitterTask.execute(new String[] { tweetText });
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Tweet").setMessage("Would you like to connect your Twitter account so you can tweet directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(getActivity(), LoginActivity.class);
							startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_SETTINGS, true);
							getActivity().startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							Intent genericShareIntent = new Intent(Intent.ACTION_SEND);
							genericShareIntent.putExtra(Intent.EXTRA_TEXT, tweetText);
							genericShareIntent.setType("text/plain");
							getActivity().startActivity(Intent.createChooser(genericShareIntent, "Select app"));
						}
					});
			builder.create().show();
		}
	}

	private void onFbClick(final String text, final String url) {
		if (!isNetworkConnected()) {
			Toast.makeText(getActivity(), "Please connect to internet.", Toast.LENGTH_LONG).show();
			return;
		}
		if (sessionProvider.isFBConnected()) {
			if (Config.LOG_DEBUG)
				Log.d(TAG, "posting to fb::");
			Session session = Session.getActiveSession();
			if (session != null) {
				// Check for publish permissions
				List<String> permissions = session.getPermissions();
				if (!isSubsetOf(PERMISSIONS, permissions)) {
					pendingPublishReauthorization = true;
					Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS);
					session.requestNewPublishPermissions(newPermissionsRequest);
					Toast.makeText(getActivity(), "Please grant permission to post and try again.", Toast.LENGTH_LONG).show();
					return;
				}
				Log.d(TAG, "we have permissions, now post to fb");
				Bundle postParams = new Bundle();
				postParams.putString("message", text);
				postParams.putString("name", "Aam Aadmi Party");
				// postParams.putString("description", "blah blah blah");
				postParams.putString("link", url);
				// postParams.putString("picture", "http://blah/postlogo.jpg");
				Request.Callback callback = new Request.Callback() {
					public void onCompleted(Response response) {
						if (pd != null)
							pd.cancel();
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(getActivity(), "Error posting to Facebook", Toast.LENGTH_SHORT).show();
							if (Config.LOG_DEBUG)
								Log.d(TAG, "Error posting to Facebook:" + error.getErrorMessage());
							return;
						} else {
							Toast.makeText(getActivity(), "Posted to Facebook!", Toast.LENGTH_LONG).show();
						}
					}
				};

				Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
				if (pd != null)
					pd.cancel();
				pd = ProgressDialog.show(getActivity(), "Posting to Facebook...", "Please wait while we post your message");
				fbTask = new RequestAsyncTask(request);
				fbTask.execute();
			}
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Post to Facebook").setMessage("Would you like to connect your Facebook account so you can post directly?")
					.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
							Intent startLogin = new Intent(getActivity(), LoginActivity.class);
							startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_SETTINGS, true);
							getActivity().startActivity(startLogin);
							return;
						}
					}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							Intent genericShareIntent = new Intent(Intent.ACTION_SEND);
							genericShareIntent.putExtra(Intent.EXTRA_TEXT, text);
							genericShareIntent.setType("text/plain");
							getActivity().startActivity(Intent.createChooser(genericShareIntent, "Select app"));
						}
					});
			builder.create().show();
		}
	}

	private boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		} else {
			return true;
		}
	}

	class TweetTask extends AsyncTask<String, Void, Integer> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null)
				pd.cancel();
			pd = ProgressDialog.show(getActivity(), "Tweeting...", "Please wait while we tweet your one liner");
		}

		@Override
		protected Integer doInBackground(String... params) {
			try {
				mTwitter.updateStatus(params[0]);
			} catch (TwitterException e) {
				Log.w(TAG, "error tweeting", e);
				e.printStackTrace();
				Log.w(TAG, e.getMessage());
				return 1;
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.cancel();
			if (result != 0) {
				Toast.makeText(getActivity(), "Tweeting failed!", Toast.LENGTH_LONG).show();
			}
		}
	}
}
