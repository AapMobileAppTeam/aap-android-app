package com.next.aappublicapp.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.next.aappublicapp.BlogsActivity;
import com.next.aappublicapp.BookReaderActivity;
import com.next.aappublicapp.DelhiElectionActivity;
import com.next.aappublicapp.LoginActivity;
import com.next.aappublicapp.NewsActivity;
import com.next.aappublicapp.R;
import com.next.aappublicapp.VideosActivity;

public class NavigationDrawerHelper {
	private static final String	TAG					= "NavigationDrawerHelper";

	public static final int		OPTION_NONE			= -1;
	public static final int		OPTION_NEWS			= 0;
	public static final int		OPTION_VIDEOS		= 1;
	public static final int		OPTION_BLOGS		= 2;
	public static final int		OPTION_DONATE		= 3;
	public static final int		OPTION_DELHI		= 4;
	public static final int		OPTION_SUPPORT		= 5;
	public static final int		OPTION_SWARAJ		= 6;
	public static final int		OPTION_JANLOKPAL	= 7;
	public static final int		OPTION_ACCOUNTS		= 8;

	private static String[]		optionNames			= new String[] { "News", "Videos", "Blogs", "Donate", "Delhi elections", "Support",
			"Swaraj ebook", "JanLokpal ebook", "Accounts" };

	public static int[]			optionIcons			= new int[] { R.drawable.ic_news, R.drawable.ic_videos, R.drawable.ic_blogs,
			R.drawable.ic_donate, R.drawable.ic_election, R.drawable.ic_support, R.drawable.ic_ebook, R.drawable.ic_ebook,
			R.drawable.ic_accounts					};

	public static void initializeDrawer(final Context context, ListView mDrawerList, int curOption) {
		mDrawerList.setBackgroundColor(Color.parseColor("#333333"));
		mDrawerList.setDivider(new ColorDrawable(Color.parseColor("#666666")));
		mDrawerList.setDividerHeight(2);
		mDrawerList.setVerticalScrollBarEnabled(false);
		// set the current screen;
		mDrawerList.setTag(curOption);
		// Set the adapter for the list view
		mDrawerList.setAdapter(new NDAdapter(context, curOption));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(context, "clicked - " + position, Toast.LENGTH_LONG).show();
				int curOption = (Integer) parent.getTag();
				if (position == curOption)
					return;// ignore
				switch (position) {
				case OPTION_NEWS:
					Intent openNews = new Intent(context, NewsActivity.class);
					context.startActivity(openNews);
					break;
				case OPTION_VIDEOS:
					Intent openVideos = new Intent(context, VideosActivity.class);
					context.startActivity(openVideos);
					break;
				case OPTION_BLOGS:
					Intent openBlogs = new Intent(context, BlogsActivity.class);
					context.startActivity(openBlogs);
					break;
				case OPTION_DONATE:
					String url = "https://donate.aamaadmiparty.org/";
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					context.startActivity(i);
					break;
				case OPTION_DELHI:
					Intent startDelhi = new Intent(context, DelhiElectionActivity.class);
					startDelhi.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					context.startActivity(startDelhi);
					break;
				case OPTION_SUPPORT:
					// TODO:
					break;
				case OPTION_SWARAJ:
					Intent startSwaraj = new Intent(context, BookReaderActivity.class);
					startSwaraj.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					startSwaraj.putExtra(BookReaderActivity.INTENT_EXTRA_BOOK_SWARAJ, true);
					context.startActivity(startSwaraj);
					break;
				case OPTION_JANLOKPAL:
					Intent startLokpal = new Intent(context, BookReaderActivity.class);
					startLokpal.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					startLokpal.putExtra(BookReaderActivity.INTENT_EXTRA_BOOK_LOKPAL, true);
					context.startActivity(startLokpal);
					break;
				case OPTION_ACCOUNTS:
					Intent startLogin = new Intent(context, LoginActivity.class);
					startLogin.putExtra(LoginActivity.INTENT_EXTRA_FROM_SETTINGS, true);
					context.startActivity(startLogin);
					break;
				}
			}
		});
	}

	static class NDAdapter extends BaseAdapter {

		LayoutInflater	inflater;
		int				curOption;

		public NDAdapter(Context context, int curOption) {
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.curOption = curOption;
		}

		@Override
		public int getCount() {
			return optionNames.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_slider, parent, false);
			}
			TextView title = (TextView) convertView.findViewById(R.id.title);
			ImageView icon = (ImageView) convertView.findViewById(R.id.icon);

			title.setText(optionNames[position]);
			icon.setImageResource(optionIcons[position]);

			if (position == curOption) {
				convertView.setBackgroundColor(Color.parseColor("#666666"));
			} else {
				convertView.setBackgroundResource(android.R.drawable.list_selector_background);
			}
			return convertView;
		}
	}

}
