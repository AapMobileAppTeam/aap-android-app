package com.next.aappublicapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.next.aappublicapp.R;
import com.next.aappublicapp.content.ContentManager;
import com.next.aappublicapp.content.models.HomeItemsResponse;
import com.next.aappublicapp.content.models.VideoItemModel;

public class VideoItemFragment extends Fragment {
	private static final String	EXTRA_ID	= "EXTRA_ID";

	private ContentManager		contentManager;

	FrameLayout					videoHolder;
	private TextView			title;

	public static VideoItemFragment newInstance(int id) {
		Bundle b = new Bundle();
		b.putInt(EXTRA_ID, id);
		VideoItemFragment f = new VideoItemFragment();
		f.setArguments(b);
		return f;
	}

	public VideoItemFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_videoitem, container, false);
		videoHolder = (FrameLayout) rootView.findViewById(R.id.video_holder);
		title = (TextView) rootView.findViewById(R.id.title);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		contentManager = ContentManager.getInstance(getActivity());
		HomeItemsResponse allItems = contentManager.getLatestResponse();
		int id = getArguments().getInt(EXTRA_ID);
		VideoItemModel itemToDisplay = null;
		for (int i = 0; i < allItems.newsItems.length; i++) {
			if (allItems.videoItems[i].id == (id)) {
				itemToDisplay = allItems.videoItems[i];
			}
		}
		if (itemToDisplay != null) {
			title.setText(itemToDisplay.title);

			final String youtubeVideoId = itemToDisplay.youtubeVideoId;
			FragmentManager fragmentManager = getChildFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

			YouTubePlayerSupportFragment fragment = new YouTubePlayerSupportFragment();
			fragmentTransaction.add(R.id.video_holder, fragment);
			fragmentTransaction.commit();

			fragment.initialize("AIzaSyDdBNMRvLTD5Ktk3R62xDXrzYiv3Gmaf4s", new OnInitializedListener() {
				@Override
				public void onInitializationSuccess(Provider arg0, YouTubePlayer arg1, boolean arg2) {
					if (!arg2) {
						arg1.loadVideo(youtubeVideoId);
					}
				}

				@Override
				public void onInitializationFailure(Provider arg0, YouTubeInitializationResult arg1) {
				}
			});
		}
	}

}
