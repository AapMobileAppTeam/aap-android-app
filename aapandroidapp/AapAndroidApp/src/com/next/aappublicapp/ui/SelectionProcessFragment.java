package com.next.aappublicapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.next.aappublicapp.R;
import com.next.aappublicapp.util.Config;

public class SelectionProcessFragment extends Fragment {

	private static final String	TAG	= "SelectionProcessFragment";

	WebView						webView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_selectionprocess, container, false);
		webView = (WebView) rootView.findViewById(R.id.webView);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		webView.setWebViewClient(new MyWebViewClient());
		webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		WebSettings s = webView.getSettings();
		s.setBuiltInZoomControls(true);
		// s.setDisplayZoomControls(false);
		s.setLoadWithOverviewMode(false);
		s.setSavePassword(false);
		s.setSaveFormData(false);
		s.setJavaScriptEnabled(true);

		webView.setInitialScale(110);
		s.setGeolocationEnabled(false);
		s.setDomStorageEnabled(false);

		webView.loadUrl("file:///android_asset/books/swaraj_eng.htm");
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (Config.LOG_DEBUG)
				Log.d(TAG, " in shouldOverrideUrlLoading");

			return true; // never let the webview load the url
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			super.onLoadResource(view, url);
			// if (Config.DEBUG)
			// Log.d(TAG, "loading resource-" + url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			Log.d(TAG, "onPageFinished - " + url);
		}

		@Override
		public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
			super.doUpdateVisitedHistory(view, url, isReload);
			Log.d(TAG, "doUpdateVisitedHistory - " + url);
		}
	}
}
