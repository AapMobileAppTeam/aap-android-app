package com.next.aappublicapp.ui;

import java.lang.reflect.Type;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.next.aappublicapp.AAPApplication;
import com.next.aappublicapp.R;
import com.next.aappublicapp.util.ServerConfig;

public class ManifestoFragment extends Fragment {
	private static final String	TAG						= "ManifestoFragment";

	TextView					constituencySelector;
	TextView					name, content;
	ACModel[]					constituencies			= new ACModel[0];

	int							selectedConstituency	= -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_manifesto, container, false);
		constituencySelector = (TextView) rootView.findViewById(R.id.acSelector);
		name = (TextView) rootView.findViewById(R.id.name);
		content = (TextView) rootView.findViewById(R.id.content);
		return rootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		constituencySelector.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (constituencies == null)
					return;
				new AlertDialog.Builder(getActivity()).setTitle("Select Constituency")
						.setAdapter(getAdapterHelper(), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								selectedConstituency = which;
								constituencySelector.setText(constituencies[selectedConstituency].name);
								name.setText(constituencies[selectedConstituency].name);
								loadManifesto(constituencies[selectedConstituency].id);
								dialog.dismiss();
							}
						}).create().show();
			}
		});
		// fetch ac details
		String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_STATES_URL;
		if (ServerConfig.USE_TEMP_URL)
			url = "http://www.donate4india.org/mob/api/acofstate/manifestolist/12";
		JsonArrayRequest getAllConstituenciesRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Type type = new TypeToken<ACModel[]>() {
				}.getType();
				constituencies = new Gson().fromJson(response.toString(), type);
				// constituencyNames = new String[constituencies.length];
				// for (int i = 0; i < constituencies.length; i++)
				// constituencyNames[i] = constituencies[i].name;
				constituencySelector.setText("Select constituency");
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.w(TAG, "error:" + error);
				Toast.makeText(getActivity(), "Error fetching constituencies", Toast.LENGTH_LONG).show();
				constituencySelector.setText("N/A");
			}
		});
		constituencySelector.setText("Loading constituencies...");
		AAPApplication.get().getRequestQueue().add(getAllConstituenciesRequest).setTag(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		AAPApplication.get().getRequestQueue().cancelAll(this);
	}

	private void loadManifesto(int acId) {
		String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_STATES_URL;
		if (ServerConfig.USE_TEMP_URL)
			url = "http://www.donate4india.org/mob/api/ac/manifesto/";
		url = url + acId;
		JsonObjectRequest getManifestoRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Type type = new TypeToken<ManifestoModel>() {
				}.getType();
				ManifestoModel manifesto = new Gson().fromJson(response.toString(), type);
				content.setText(Html.fromHtml(manifesto.content));
				constituencySelector.setText("Select constituency");
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.w(TAG, "error:" + error);
				Toast.makeText(getActivity(), "Error fetching manifesto", Toast.LENGTH_LONG).show();
				constituencySelector.setText("N/A");
			}
		});
		AAPApplication.get().getRequestQueue().add(getManifestoRequest).setTag(this);
	}

	private ListAdapter getAdapterHelper() {
		return new ArrayAdapter<ACModel>(getActivity(), R.layout.item_constituency, constituencies) {
			public View getView(int position, View convertView, ViewGroup parent) {
				final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				if (convertView == null) {
					convertView = inflater.inflate(R.layout.item_constituency, null);
				}
				ACModel acModel = getItem(position);
				TextView name = (TextView) convertView.findViewById(R.id.name);
				TextView availability = (TextView) convertView.findViewById(R.id.availability);

				name.setText(acModel.name);
				if (acModel.available) {
					availability.setVisibility(View.GONE);
				} else {
					availability.setVisibility(View.VISIBLE);
					availability.setText("Unavailable");
				}
				return convertView;
			}
		};
	}

	class ACModel {
		public int		id;
		public String	name;
		public boolean	available;
	}

	class ManifestoModel {
		public int		id;
		public int		assemblyConstituencyId;
		public String	content;
	}
}
