package com.next.aappublicapp;

import java.util.HashMap;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.Session;
import com.next.aappublicapp.util.Config;

public class UserSessionProvider {
	private static final String			TAG							= "UserSessionProvider";
	private static final String			PREFS_FILE					= "PREFS";
	private static final String			PREFS_KEY_DEVICE_ID			= "PREFS_KEY_DEVICE_ID";
	private static final String			PREFS_KEY_REGISTERED		= "PREFS_KEY_REGISTERED";
	private static final String			PREFS_KEY_GCM_ID			= "PREFS_KEY_GCM_ID";
	private static final String			PREFS_KEY_UID				= "PREFS_KEY_UID";
	private static final String			PREFS_KEY_USERNAME			= "PREFS_KEY_USERNAME";
	private static final String			PREFS_KEY_EMAIL				= "PREFS_KEY_EMAIL";
	private static final String			PREFS_KEY_PROFILEPIC		= "PREFS_KEY_PROFILEPIC";
	// Facebook
	public static final String			PREFS_KEY_FACEBOOK_UID		= "PREFS_KEY_FACEBOOK_UID";
	public static final String			PREFS_KEY_FACEBOOK_NAME		= "PREFS_KEY_FACEBOOK_NAME";
	// Twitter
	public static final String			PREFS_KEY_TWITTER_ID		= "PREFS_KEY_TWITTER_ID";
	public static final String			PREFS_KEY_TWITTER_NAME		= "PREFS_KEY_TWITTER_NAME";
	public static final String			PREFS_KEY_TWITTER_TOKEN		= "PREFS_KEY_TWITTER_TOKEN";
	public static final String			PREFS_KEY_TWITTER_SECRET	= "PREFS_KEY_TWITTER_SECRET";
	// LinkedIn
	public static final String			PREFS_KEY_GOOGLE_ID			= "PREFS_KEY_GOOGLE_ID";
	public static final String			PREFS_KEY_GOOGLE_NAME		= "PREFS_KEY_GOOGLE_NAME";
	public static final String			PREFS_KEY_GOOGLE_TOKEN		= "PREFS_KEY_GOOGLE_TOKEN";
	public static final String			PREFS_KEY_GOOGLE_SECRET		= "PREFS_KEY_GOOGLE_SECRET";

	private static UserSessionProvider	instance					= null;

	public static UserSessionProvider getInstance(Context context) {
		if (instance == null) {
			instance = new UserSessionProvider(context);
		}
		return instance;
	}

	private Context					context;
	private String					gcmId				= null;
	private String					deviceId			= null;
	private Boolean					isUserRegistered	= null;
	private String					uid					= null;
	private String					name				= null;
	private String					email				= null;
	private String					profilePicUrl		= null;

	private HashMap<String, String>	cache				= new HashMap<String, String>();

	private UserSessionProvider(Context context) {
		this.context = context;
	}

	public String getDeviceId() {
		if (deviceId == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			deviceId = prefs.getString(PREFS_KEY_DEVICE_ID, null);
			if (deviceId == null) {
				deviceId = UUID.randomUUID().toString();
				prefs.edit().putString(PREFS_KEY_DEVICE_ID, deviceId).commit();
				if (Config.LOG_DEBUG)
					Log.d(TAG, "saved deviceId:" + deviceId);
			}
		}
		return deviceId;
	}

	public boolean isUserRegistered() {
		if (isUserRegistered == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			isUserRegistered = prefs.getBoolean(PREFS_KEY_REGISTERED, false);
		}
		return isUserRegistered;
	}

	public void setUserRegistered(boolean registered) {
		isUserRegistered = registered;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putBoolean(PREFS_KEY_REGISTERED, registered).commit();
	}

	public String getGcmId() {
		if (gcmId == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			gcmId = prefs.getString(PREFS_KEY_GCM_ID, null);
		}
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(PREFS_KEY_GCM_ID, gcmId).commit();
	}

	public String getUserId() {
		if (this.uid == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			this.uid = prefs.getString(PREFS_KEY_UID, null);
		}
		return this.uid;
	}

	public void setUserId(String uid) {
		this.uid = uid;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(PREFS_KEY_UID, uid).commit();
	}

	public String getEmail() {
		if (this.email == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			this.email = prefs.getString(PREFS_KEY_EMAIL, null);
		}
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(PREFS_KEY_EMAIL, email).commit();
	}

	public String getUserName() {
		if (this.name == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			this.name = prefs.getString(PREFS_KEY_USERNAME, null);
		}
		return this.name;
	}

	public void setUserName(String name) {
		this.name = name;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(PREFS_KEY_USERNAME, name).commit();
	}

	public String getProfilePicUrl() {
		if (this.profilePicUrl == null) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			this.profilePicUrl = prefs.getString(PREFS_KEY_PROFILEPIC, null);
		}
		return this.profilePicUrl;
	}

	public void setProfilePicUrl(String url) {
		this.profilePicUrl = url;
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(PREFS_KEY_PROFILEPIC, url).commit();
	}

	public String getParam(String key) {
		if (!cache.containsKey(key)) {
			final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
			cache.put(key, prefs.getString(key, null));
		}
		return cache.get(key);
	}

	public void putParam(String key, String value) {
		cache.put(key, value);
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().putString(key, value).commit();
	}

	public Session getFBSession() {
		Session session = Session.getActiveSession();
		if (session == null)
			session = Session.openActiveSessionFromCache(context);
		return session;
	}

	public boolean isFBConnected() {
		Session session = getFBSession();
		return (session != null && getParam(PREFS_KEY_FACEBOOK_UID) != null && session.isOpened());
	}

	public boolean isTwitterConnected() {
		return (getParam(PREFS_KEY_TWITTER_TOKEN) != null && getParam(PREFS_KEY_TWITTER_SECRET) != null);
	}

	public boolean isGoogleConnected() {
		return (getParam(PREFS_KEY_GOOGLE_TOKEN) != null);// &&
															// getParam(PREFS_KEY_GOOGLE_SECRET)
															// != null);
	}

	public void logout() {
		// clear fb session
		Session fbSession = getFBSession();
		if (fbSession != null)
			fbSession.closeAndClearTokenInformation();

		// clear our cache
		cache.clear();
		deviceId = null;
		uid = null;
		name = null;
		profilePicUrl = null;

		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().remove(PREFS_KEY_UID).remove(PREFS_KEY_USERNAME).remove(PREFS_KEY_PROFILEPIC).remove(PREFS_KEY_TWITTER_ID)
				.remove(PREFS_KEY_TWITTER_TOKEN).remove(PREFS_KEY_TWITTER_NAME).remove(PREFS_KEY_TWITTER_SECRET)
				.remove(PREFS_KEY_FACEBOOK_UID).remove(PREFS_KEY_FACEBOOK_NAME).remove(PREFS_KEY_GOOGLE_TOKEN)
				.remove(PREFS_KEY_GOOGLE_NAME).remove(PREFS_KEY_GOOGLE_SECRET).commit();
	}

	public void disconnectFB() {
		// clear fb session
		Session fbSession = getFBSession();
		if (fbSession != null)
			fbSession.closeAndClearTokenInformation();

		cache.clear();
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().remove(PREFS_KEY_FACEBOOK_UID).remove(PREFS_KEY_FACEBOOK_NAME).commit();
	}

	public void disconnectTwitter() {
		cache.clear();
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().remove(PREFS_KEY_TWITTER_ID).remove(PREFS_KEY_TWITTER_TOKEN).remove(PREFS_KEY_TWITTER_NAME)
				.remove(PREFS_KEY_TWITTER_SECRET).commit();
	}

	public void disconnectGoogle() {
		cache.clear();
		final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
		prefs.edit().remove(PREFS_KEY_GOOGLE_ID).remove(PREFS_KEY_GOOGLE_TOKEN).remove(PREFS_KEY_GOOGLE_NAME)
				.remove(PREFS_KEY_GOOGLE_SECRET).commit();
	}
}