package com.next.aappublicapp.util;

public final class ServerConfig {
	public static final boolean	USE_TEMP_URL				= true;

	public static final String	SERVER_ENDPOINT				= "http://www.donate4india.org/mob/api";
	public static final String	GET_STATES_URL				= "/state/getall";
	public static final String	GET_DISTRICTS_URL			= "/districtofstate/get/";
	public static final String	GET_AC_URL					= "/acofdistrict/get/";

	public static final String	GET_HOME					= "/en/get/home";

	public static final String	POST_FB_REGISTER_URL		= "/user/register/facebook";
	public static final String	POST_TWITTER_REGISTER_URL	= "/user/register/twitter";
}
