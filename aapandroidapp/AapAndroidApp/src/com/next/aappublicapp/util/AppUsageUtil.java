package com.next.aappublicapp.util;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;



/**
 * 
 * @author Ravi
 *
 */
public class AppUsageUtil {

	/**
	 * check if APP has been used N(total) times since last reset for login functionality
	 * @param context
	 * @param total
	 * @return
	 */
	public boolean isAppUsedNTimeForLogin(Context context,int total) {
		return isAppUsedNTime(context, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_LOGIN, total);
	}
	/**
	 * reset total app use counter for login functionality.
	 * from app logic we will call isAppUsedNTimeForLogin and if that returns true, we will do what need to be done 
	 * and at the end call this function to reset counter
	 * @param context
	 * @return
	 */
	public static void resetAppUsedCounterForLogin(Context context){
		resetCounter(context, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_LOGIN);
	}
	/**
	 * check if APP has been used N(total) times since last reset for gcm functionality
	 * @param context
	 * @param total
	 * @return
	 */
	public boolean isAppUsedNTimeForGcm(Context context,int total) {
		return isAppUsedNTime(context, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_GCM, total);
	}
	/**
	 * reset total app use counter for gcm functionality.
	 * from app logic we will call isAppUsedNTimeForGcm and if that returns true, we will do what need to be done 
	 * and at the end call this function to reset counter
	 * @param context
	 * @return
	 */
	public static void resetAppUsedCounterForGcm(Context context){
		resetCounter(context, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_GCM);
	}
	private boolean isAppUsedNTime(Context context,String preferenceName, int total) {
		try {
			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.APP_INTERNAL_SETTINGS, Context.MODE_PRIVATE);
			int totalTimeAppWasUsed = prefs.getInt(preferenceName, 0);
			if(totalTimeAppWasUsed > total){
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
	/**
	 * This function will be called whenever app is started, from splash screen and it will increase various counters
	 * @param context
	 */
	public static void appUsed(Context context) {
		try {
			//increase all app use counters
			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.APP_INTERNAL_SETTINGS, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			increaseCounter(prefs, editor, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_LOGIN);
			increaseCounter(prefs, editor, PreferenceNameUtil.TOTAL_TIMES_APP_STARTED_FOR_GCM);
			updateLastUseTime(prefs, editor);
			editor.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	private static void increaseCounter(SharedPreferences prefs,SharedPreferences.Editor editor,String prefernceName){
		int counter = prefs.getInt(prefernceName, 0);
		editor.putInt(prefernceName, counter+1);
	}
	private static void resetCounter(Context context,String prefernceName){
		try {
			SharedPreferences prefs = context.getSharedPreferences(PreferenceNameUtil.APP_INTERNAL_SETTINGS, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putInt(prefernceName, 0);
			editor.commit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	private static void updateLastUseTime(SharedPreferences prefs,SharedPreferences.Editor editor){
		Calendar cal = Calendar.getInstance();
		Long lastUsedTime = prefs.getLong(PreferenceNameUtil.LAST_APP_USAGE_TIME,0L);
		//Update previous used time
		if(lastUsedTime > 0){
			editor.putLong(PreferenceNameUtil.LAST_APP_USAGE_PREV_TIME, lastUsedTime);
		}else{
			editor.putLong(PreferenceNameUtil.LAST_APP_USAGE_PREV_TIME, cal.getTimeInMillis());
		}
		//Update current time to be used as previous time for future run
		editor.putLong(PreferenceNameUtil.LAST_APP_USAGE_TIME, cal.getTimeInMillis());
	}
}
