package com.next.aappublicapp.util;

public class PreferenceNameUtil {

	public static final String APP_INTERNAL_SETTINGS = "APP_INTERNAL_SETTINGS";
	public static final String TOTAL_TIMES_APP_STARTED = "TOTAL_TIMES_APP_STARTED";
	public static final String TOTAL_TIMES_APP_STARTED_FOR_LOGIN = "TOTAL_TIMES_APP_STARTED_FOR_LOGIN";
	public static final String TOTAL_TIMES_APP_STARTED_FOR_GCM = "TOTAL_TIMES_APP_STARTED_FOR_GCM";
	public static final String LAST_APP_USAGE_TIME = "LAST_APP_USAGE_TIME";
	public static final String LAST_APP_USAGE_PREV_TIME = "LAST_APP_USAGE_PREV_TIME";

}
