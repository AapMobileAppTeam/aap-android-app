package com.next.aappublicapp;

import java.util.ArrayList;

import org.json.JSONException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.RequestToken;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.android.Util;
import com.next.aappublicapp.util.Config;
import com.next.aappublicapp.util.LocalPersistence;

public class TwitterLoginActivity extends Activity {
	private static final String	TAG							= "TwitterLoginActivity";

	static final String			TWITTER_REQUEST_TOKEN_FILE	= "requestToken.txt";
	UserSessionProvider			sessionProvider;
	Uri							tokenUri					= null;

	ProgressDialog				pd							= null;
	LoginTask					loginTask					= null;

	// twitter
	private Twitter				mTwitter;
	private RequestToken		twitterRequestToken;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#9e000000")));

		sessionProvider = UserSessionProvider.getInstance(this);

		mTwitter = new TwitterFactory().getInstance();
		mTwitter.setOAuthConsumer(Config.TWITTER_CONSUMER_KEY, Config.TWITTER_CONSUMER_SECRET);
	}

	@Override
	protected void onStart() {
		super.onStart();
		tokenUri = getIntent().getData();
		if (tokenUri != null && tokenUri.toString().startsWith(Config.TWITTER_CALLBACK_URL)) {
			String oauthVerifier = tokenUri.getQueryParameter("oauth_verifier");
			twitterRequestToken = (RequestToken) LocalPersistence.readObjectFromFile(this, TWITTER_REQUEST_TOKEN_FILE);
			new TwitterConnectTask().execute(new String[] { oauthVerifier });
		} else {
			new TwitterRequestTokenTask().execute(new String[] {});
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.cancel();
	}

	class TwitterRequestTokenTask extends AsyncTask<String, Void, Integer> {
		RequestToken	requestToken;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = ProgressDialog.show(TwitterLoginActivity.this, "Starting Twitter login", "Please wait while we login to Twitter");
		}

		@Override
		protected Integer doInBackground(String... params) {
			try {
				requestToken = mTwitter.getOAuthRequestToken(Config.TWITTER_CALLBACK_URL);
				if (Config.LOG_DEBUG)
					Log.d(TAG, "requestToken" + requestToken.getToken());
				return 0;
			} catch (TwitterException e) {
				Log.e(TAG, "Twitter connect failed!", e);
				return 1;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (pd != null)
				pd.cancel();
			if (result == 0) {
				twitterRequestToken = requestToken;
				LocalPersistence.witeObjectToFile(TwitterLoginActivity.this, twitterRequestToken, TWITTER_REQUEST_TOKEN_FILE);
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterRequestToken.getAuthenticationURL()));
				i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(i);
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!", Toast.LENGTH_LONG).show();
				finish();
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (pd != null)
				pd.cancel();
		}
	}

	class TwitterConnectTask extends AsyncTask<String, Void, Integer> {
		User	twitterUser	= null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null)
				pd.cancel();
			pd = ProgressDialog.show(TwitterLoginActivity.this, "Fetching Twitter profile...",
					"Please wait while we fetch your Twitter profile");
		}

		@Override
		protected Integer doInBackground(String... params) {
			String oauthVerifier = params[0];
			twitter4j.auth.AccessToken at;
			try {
				at = mTwitter.getOAuthAccessToken(twitterRequestToken, oauthVerifier);
				mTwitter.setOAuthAccessToken(at);
				twitterUser = mTwitter.showUser(mTwitter.getId());
				return 0;
			} catch (TwitterException e) {
				Log.e(TAG, "twitter connect failed", e);
				return 1;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.cancel();
			if (result == 0) {
				try {
					if (loginTask != null)
						loginTask.cancel(true);
					loginTask = new LoginTask("" + mTwitter.getId(), twitterUser.getScreenName(), twitterUser.getName(), mTwitter
							.getOAuthAccessToken().getToken(), mTwitter.getOAuthAccessToken().getTokenSecret());
					loginTask.execute(new String[] {});
				} catch (TwitterException e) {
					Log.e(TAG, "error connecting twitter", e);
					Toast.makeText(TwitterLoginActivity.this, "Twitter connect failed", Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!", Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}

	class LoginTask extends AsyncTask<String, Void, Integer> {
		ArrayList<String>	tokenTypes, tokens;
		String				userId, name, profilePicUrl;
		String				twitId, twitName, twitToken, twitSecret;

		public LoginTask(String twitId, String twitName, String name, String twitToken, String twitSecret) {
			this.twitId = twitId;
			this.twitName = twitName;
			this.twitToken = twitToken;
			this.twitSecret = twitSecret;
			this.name = name;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (pd != null)
				pd.cancel();
			pd = ProgressDialog.show(TwitterLoginActivity.this, "Updating connection...", "Please wait while we update your connections");

			tokenTypes = new ArrayList<String>();
			tokens = new ArrayList<String>();
			// Util.fillTokens(sessionProvider, tokenTypes, tokens);

			tokenTypes.add("twitter-id");
			tokens.add(twitId);
			tokenTypes.add("twitter-token");
			tokens.add(twitToken);
			tokenTypes.add("twitter-token-secret");
			tokens.add(twitSecret);
		}

		@Override
		protected Integer doInBackground(String... params) {
			// ApiHandler apiHandler = ApiHandler.getInstance(getApplicationContext());
			//
			// TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
			// ApiResponse response = apiHandler.doLogin(sessionProvider.getUserId(), tokenTypes, tokens,
			// tm.getDeviceId());
			// if (response.responseCode == ApiResponse.RESPONSE_OK) {
			// try {
			// userId = response.responseData.getString("user-id");
			// name = response.responseData.getString("name");
			// profilePicUrl = response.responseData.getString("image");
			// } catch (JSONException e) {
			// Log.e(TAG, "error fetching userid", e);
			// return 1;
			// }
			// }
			// return response.responseCode;
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			pd.cancel();
			if (result == 0) {
				// sessionProvider.setUserId(userId);
				sessionProvider.setUserName(name);
				// sessionProvider.setProfilePicUrl(profilePicUrl);

				sessionProvider.putParam(UserSessionProvider.PREFS_KEY_TWITTER_ID, twitId);
				sessionProvider.putParam(UserSessionProvider.PREFS_KEY_TWITTER_NAME, twitName);
				sessionProvider.putParam(UserSessionProvider.PREFS_KEY_TWITTER_TOKEN, twitToken);
				sessionProvider.putParam(UserSessionProvider.PREFS_KEY_TWITTER_SECRET, twitSecret);
				Toast.makeText(TwitterLoginActivity.this, "Connection successful!", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(TwitterLoginActivity.this, "Connection failed!", Toast.LENGTH_LONG).show();
			}
			finish();
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (pd != null)
				pd.cancel();
			finish();
		}
	}
}
