package com.next.aappublicapp.content.models;

public class NewsItemModel {
	public int	id;
	public String	title;
	public String	content;
	public String	imageUrl;
	public String	webUrl;
	public String	source;
	public String	author;
	public String	date;
	public String[]	oneLiners;
}
