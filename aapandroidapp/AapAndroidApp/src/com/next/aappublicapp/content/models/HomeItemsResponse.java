package com.next.aappublicapp.content.models;

public class HomeItemsResponse {
	public BlogItemModel[]	blogItems;
	public NewsItemModel[]	newsItems;
	public VideoItemModel[]	videoItems;
}
