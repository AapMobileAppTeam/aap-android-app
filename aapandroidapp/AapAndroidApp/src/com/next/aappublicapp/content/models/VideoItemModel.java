package com.next.aappublicapp.content.models;

public class VideoItemModel {
	public int	id;
	public String	title;
	public String	imageUrl;
	public String	webUrl;
	public String	date;
	public String[]	oneLiners;
	public String	description;
	public String	youtubeVideoId;
}
