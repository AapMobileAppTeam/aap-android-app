package com.next.aappublicapp.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.next.aappublicapp.content.models.HomeItemsResponse;

public class ContentManager {
	private static final String					TAG						= "ContentManager";
	private static final String					PREF_CONTENT_CACHE		= "CONTENT_CACHE";
	private static final String					PREF_LATEST_FILE_NAME	= "PREF_LATEST_FILE_NAME";
	private static final String					PREF_LAST_UPDATE_TIME	= "PREF_LAST_UPDATE_TIME";

	private static ContentManager				instance				= null;

	private Context								context;
	private SharedPreferences					prefs;
	private boolean								isDownloading			= false;
	private long								lastFetchTime			= 0;
	private HomeItemsResponse					latestHomeItems			= null;

	private ArrayList<ContentManagerListener>	listeners;

	public static ContentManager getInstance(Context context) {
		if (instance == null) {
			instance = new ContentManager(context);
		}
		return instance;
	}

	public interface ContentManagerListener {
		public void onNewContent();

		public void onDownloading();
	}

	private ContentManager(Context context) {
		prefs = context.getSharedPreferences(PREF_CONTENT_CACHE, 0);
		listeners = new ArrayList<ContentManagerListener>();
	}

	public void addListener(ContentManagerListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);
	}

	public void removeListener(ContentManagerListener listener) {
		listeners.remove(listener);
	}

	public void setDownloading(boolean isDownloading) {
		this.isDownloading = isDownloading;
		for (ContentManagerListener listener : listeners)
			listener.onDownloading();
	}

	public boolean getDownloading() {
		return isDownloading;
	}

	public HomeItemsResponse getLatestResponse() {
		if (latestHomeItems == null) {
			// load last file
			if (prefs.contains(PREF_LATEST_FILE_NAME)) {
				File cacheFile = new File(prefs.getString(PREF_LATEST_FILE_NAME, null));
				String cacheFileString = readFile(cacheFile);
				if (cacheFileString != null) {
					lastFetchTime = prefs.getLong(PREF_LAST_UPDATE_TIME, 0);
					latestHomeItems = new Gson().fromJson(cacheFileString, HomeItemsResponse.class);
				}
			}
		}
		return latestHomeItems;
	}

	public long getLastUpdateTime() {
		if (lastFetchTime <= 0) {
			lastFetchTime = prefs.getLong(PREF_LAST_UPDATE_TIME, 0);
		}
		return lastFetchTime;
	}

	public void setHomeItems(File cacheFile, HomeItemsResponse homeItems) {
		this.latestHomeItems = homeItems;
		this.lastFetchTime = System.currentTimeMillis();
		prefs.edit().putLong(PREF_LAST_UPDATE_TIME, lastFetchTime).putString(PREF_LATEST_FILE_NAME, cacheFile.getAbsolutePath()).commit();
		for (ContentManagerListener listener : listeners) {
			listener.onNewContent();
		}
		// TODO: delete old files
	}

	private String readFile(File fileToRead) {
		try {
			FileInputStream fin = new FileInputStream(fileToRead);
			return IOUtils.toString(fin, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
