package com.next.aappublicapp.content;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.google.gson.Gson;
import com.next.aappublicapp.content.models.HomeItemsResponse;
import com.next.aappublicapp.util.ServerConfig;

public class ContentDownloaderService extends WakefulIntentService {
	private static final String	TAG	= "ContentDownloaderService";

	Handler						handler;

	public ContentDownloaderService() {
		super("ContentDownloaderService");
		handler = new Handler();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ContentManager.getInstance(ContentDownloaderService.this).setDownloading(false);
	}

	@Override
	protected void doWakefulWork(Intent arg0) {
		Log.d(TAG, "begin content download");
		handler.post(new Runnable() {
			@Override
			public void run() {
				ContentManager.getInstance(ContentDownloaderService.this).setDownloading(true);
			}
		});
		disableConnectionReuseIfNecessary();

		InputStream is = null;
		String urlString = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_HOME;
		if (ServerConfig.USE_TEMP_URL)
			urlString = "https://dl.dropboxusercontent.com/u/5474079/aap_requests/home.json";
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			int responseCode = conn.getResponseCode();
			is = conn.getInputStream();
			String responseString = readIt(is);
			Log.d(TAG, "The response is: " + responseCode);
			Log.d(TAG, "The response is: " + responseString);
			if (responseCode == 200) {
				final HomeItemsResponse homeItems = new Gson().fromJson(responseString, HomeItemsResponse.class);
				// save the response string
				final File cacheFile = new File(getFilesDir(), "" + System.currentTimeMillis() + ".json");
				saveToFile(cacheFile, responseString);
				handler.post(new Runnable() {
					@Override
					public void run() {
						ContentManager.getInstance(ContentDownloaderService.this).setHomeItems(cacheFile, homeItems);
					}
				});
			} else {
				// TODO: reschedule
			}
		} catch (Exception e) {
			Log.e(TAG, "error fetching content", e);
			// TODO: retry
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// TODO: handle errors fetching content
		handler.post(new Runnable() {
			@Override
			public void run() {
				ContentManager.getInstance(ContentDownloaderService.this).setDownloading(false);
			}
		});
	}

	private boolean saveToFile(File responseFile, String responseString) {
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(responseFile);
			outputStream.write(responseString.getBytes());
			outputStream.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private String readIt(InputStream stream) throws IOException, UnsupportedEncodingException {
		return IOUtils.toString(stream, "UTF-8");
	}

	private void disableConnectionReuseIfNecessary() {
		// Work around pre-Froyo bugs in HTTP connection reuse.
		if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
			System.setProperty("http.keepAlive", "false");

		}
	}

}
