package com.next.aappublicapp.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class OnAlarmReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("OnAlarmReceiver", "onReceive");
		WakefulIntentService.sendWakefulWork(context, ContentDownloaderService.class);
	}
}