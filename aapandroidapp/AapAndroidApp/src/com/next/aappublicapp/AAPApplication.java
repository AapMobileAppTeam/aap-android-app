package com.next.aappublicapp;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.next.aappublicapp.util.BitmapLruCache;

public class AAPApplication extends Application {
	private static final String		TAG	= "DiviApplication";

	private static AAPApplication	instance;

	private RequestQueue			requestQueue;
	private ImageLoader				imageLoader;

	public static AAPApplication get() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		requestQueue = Volley.newRequestQueue(this);
		imageLoader = new ImageLoader(requestQueue, new BitmapLruCache());
	}

	public RequestQueue getRequestQueue() {
		return requestQueue;
	}

	public ImageLoader getImageLoader() {
		return imageLoader;
	}

}
