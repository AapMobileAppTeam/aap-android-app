package com.next.aappublicapp;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.FadeInNetworkImageView;
import com.next.aappublicapp.content.ContentManager;
import com.next.aappublicapp.content.ContentManager.ContentManagerListener;
import com.next.aappublicapp.content.models.BlogItemModel;
import com.next.aappublicapp.content.models.HomeItemsResponse;
import com.next.aappublicapp.content.models.NewsItemModel;
import com.next.aappublicapp.content.models.VideoItemModel;
import com.next.aappublicapp.ui.NavigationDrawerHelper;

public class HomeActivity extends ActionBarActivity implements ContentManagerListener {
	private static final String		TAG	= "HomeActivity";

	private ContentManager			contentManager;
	private DrawerLayout			mDrawerLayout;
	private ListView				mDrawerList;
	private ActionBarDrawerToggle	mDrawerToggle;
	private LinearLayout			newsContainer, videosContainer, blogsContainer;
	private TextView				updatedTV;
	private ProgressBar				pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		contentManager = ContentManager.getInstance(this);
		setContentView(R.layout.activity_home2);
		newsContainer = (LinearLayout) findViewById(R.id.news_scroller);
		videosContainer = (LinearLayout) findViewById(R.id.videos_scroller);
		blogsContainer = (LinearLayout) findViewById(R.id.blogs_scroller);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		updatedTV = (TextView) findViewById(R.id.updatedText);
		pd = (ProgressBar) findViewById(R.id.updateProgressBar);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		NavigationDrawerHelper.initializeDrawer(this, mDrawerList, NavigationDrawerHelper.OPTION_NONE);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, 0, 0) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				// invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
				// invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};
		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onStart() {
		super.onStart();
		contentManager.addListener(this);
		setupUI();
		setupProgressBar();
	}

	@Override
	protected void onStop() {
		super.onStop();
		contentManager.removeListener(this);
	}

	private void setupUI() {
		HomeItemsResponse homeItems = contentManager.getLatestResponse();
		newsContainer.removeAllViews();
		videosContainer.removeAllViews();
		blogsContainer.removeAllViews();
		if (homeItems == null) {
			// TODO: show no content text, ensure updating
		} else {
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

			// setup news items
			for (NewsItemModel newsItem : homeItems.newsItems) {
				LinearLayout newsItemContainer = (LinearLayout) inflater.inflate(R.layout.item_home_thumb, newsContainer, false);
				FadeInNetworkImageView pic = (FadeInNetworkImageView) newsItemContainer.findViewById(R.id.thumb);
				pic.setDefaultImageResId(R.drawable.ic_launcher);
				pic.setImageUrl(newsItem.imageUrl, AAPApplication.get().getImageLoader());
				TextView text = (TextView) newsItemContainer.findViewById(R.id.text);
				text.setText(newsItem.title);
				newsContainer.addView(newsItemContainer);
				final int id = newsItem.id;
				newsItemContainer.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent openNewsItem = new Intent(HomeActivity.this, NewsActivity.class);
						openNewsItem.putExtra(NewsActivity.INTENT_EXTRA_ITEM_ID, id);
						startActivity(openNewsItem);
					}
				});
			}

			for (VideoItemModel videoItem : homeItems.videoItems) {
				LinearLayout newsItemContainer = (LinearLayout) inflater.inflate(R.layout.item_home_thumb, videosContainer, false);
				FadeInNetworkImageView pic = (FadeInNetworkImageView) newsItemContainer.findViewById(R.id.thumb);
				pic.setDefaultImageResId(R.drawable.ic_launcher);
				pic.setImageUrl(videoItem.imageUrl, AAPApplication.get().getImageLoader());
				newsItemContainer.findViewById(R.id.ic_video_overlay).setVisibility(View.VISIBLE);
				TextView text = (TextView) newsItemContainer.findViewById(R.id.text);
				text.setText(videoItem.title);
				videosContainer.addView(newsItemContainer);
				final int id = videoItem.id;
				newsItemContainer.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent openVideoItem = new Intent(HomeActivity.this, VideosActivity.class);
						openVideoItem.putExtra(VideosActivity.INTENT_EXTRA_ITEM_ID, id);
						startActivity(openVideoItem);
					}
				});
			}

			for (BlogItemModel blogItem : homeItems.blogItems) {
				LinearLayout newsItemContainer = (LinearLayout) inflater.inflate(R.layout.item_home_thumb, blogsContainer, false);
				FadeInNetworkImageView pic = (FadeInNetworkImageView) newsItemContainer.findViewById(R.id.thumb);
				pic.setDefaultImageResId(R.drawable.ic_launcher);
				pic.setImageUrl(blogItem.imageUrl, AAPApplication.get().getImageLoader());
				TextView text = (TextView) newsItemContainer.findViewById(R.id.text);
				text.setText(blogItem.title);
				blogsContainer.addView(newsItemContainer);
			}
		}
	}

	private void setupProgressBar() {
		if (contentManager.getDownloading()) {
			updatedTV.setVisibility(View.GONE);
			pd.setVisibility(View.VISIBLE);
		} else {
			updatedTV.setVisibility(View.VISIBLE);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			updatedTV.setText("Updated at " + sdf.format(new Date(contentManager.getLastUpdateTime())));
			pd.setVisibility(View.GONE);
		}
	}

	@Override
	public void onNewContent() {
		setupUI();
		setupProgressBar();
	}

	@Override
	public void onDownloading() {
		setupProgressBar();
	}

}
