package com.next.aappublicapp;

import java.io.IOException;

import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
import com.next.aappublicapp.TwitterLoginActivity.LoginTask;
import com.next.aappublicapp.content.OnBootReceiver;
import com.next.aappublicapp.ui.GoogleAccountDialog;
import com.next.aappublicapp.ui.GoogleAccountDialog.GoogleAccountDialogListener;
import com.next.aappublicapp.util.Config;

public class LoginActivity extends ActionBarActivity implements GoogleAccountDialogListener {
	private static final String	TAG							= "LoginActivity";

	private static final String	GOOGLE_PROFILE_FETCH_URL	= "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";
	private static final String	AUTH_TOKEN_TYPE				= "oauth2:https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email";
	private static final int	REQUEST_AUTHENTICATE		= 1;

	public static final String	INTENT_EXTRA_FROM_SETTINGS	= "INTENT_EXTRA_FROM_SETTINGS";

	static final int			NETWORK_FB					= 0;
	static final int			NETWORK_TWITTER				= 1;
	static final int			NETWORK_GOOGLE				= 2;

	TextView					title, text;
	View						fbConnect, twitterConnect, googleConnect;
	ImageView					fbIcon, twitterIcon, googleIcon;
	TextView					fbStatus, twitterStatus, googleStatus;
	Button						nextButton;
	Typeface					robotoThin, robotoNormal, robotoBold;

	ProgressDialog				pd							= null;
	UserSessionProvider			sessionProvider;
	LoginTask					loginTask					= null;

	// google
	Account						selectedAccount				= null;
	boolean						firstToken					= true;
	// fb
	private UiLifecycleHelper	uiHelper;
	Request						fbRequest					= null;
	Session.StatusCallback		fbCallback					= new Session.StatusCallback() {
																// callback when session changes state
																@Override
																public void call(Session session, SessionState state, Exception exception) {
																	if (session.isOpened() && isShowing) {
																		if (pd != null)
																			pd.cancel();
																		// only first time login
																		if (!sessionProvider.isFBConnected())
																			makeMeRequest(session);
																		return;
																	}
																}
															};

	boolean						isShowing					= false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// start content fetch
		sendBroadcast(new Intent(this, OnBootReceiver.class));

		robotoThin = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
		robotoNormal = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		robotoBold = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
		uiHelper = new UiLifecycleHelper(this, fbCallback);
		uiHelper.onCreate(savedInstanceState);
		sessionProvider = UserSessionProvider.getInstance(this);
		setContentView(R.layout.activity_login);

		// map & setup views
		title = (TextView) findViewById(R.id.title);
		nextButton = (Button) findViewById(R.id.next_button);
		fbConnect = findViewById(R.id.fb_connect);
		twitterConnect = findViewById(R.id.twitter_connect);
		googleConnect = findViewById(R.id.google_connect);
		fbIcon = (ImageView) findViewById(R.id.fb_icon);
		twitterIcon = (ImageView) findViewById(R.id.twitter_icon);
		googleIcon = (ImageView) findViewById(R.id.google_icon);
		fbStatus = (TextView) findViewById(R.id.fb_status);
		twitterStatus = (TextView) findViewById(R.id.twitter_status);
		googleStatus = (TextView) findViewById(R.id.google_status);
		fbStatus.setTypeface(robotoNormal);
		twitterStatus.setTypeface(robotoNormal);
		googleStatus.setTypeface(robotoNormal);
		nextButton.setTypeface(robotoNormal);
		((TextView) findViewById(R.id.fb_label)).setTypeface(robotoNormal);
		((TextView) findViewById(R.id.twitter_label)).setTypeface(robotoNormal);
		((TextView) findViewById(R.id.google_label)).setTypeface(robotoNormal);

		setTitle("Login");

		// click listeners
		nextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				Intent registerActivityIntent;
				if (getIntent().getBooleanExtra(INTENT_EXTRA_FROM_SETTINGS, false)) {
					finish();
					return;
				} else {
					if (sessionProvider.isFBConnected() || sessionProvider.isGoogleConnected() || sessionProvider.isTwitterConnected())
						registerActivityIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
					else
						// skip
						registerActivityIntent = new Intent(LoginActivity.this, HomeActivity.class);
				}
				startActivity(registerActivityIntent);
			}
		});
		fbConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (sessionProvider.isFBConnected()) {
					disconnectSocial(NETWORK_FB);
				} else {
					if (pd != null)
						pd.cancel();
					pd = ProgressDialog.show(LoginActivity.this, "Connecting to FB...",
							"Please wait while we connect to your Facebook account");
					Session.openActiveSession(LoginActivity.this, true, fbCallback);
				}
			}
		});

		twitterConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (sessionProvider.isTwitterConnected()) {
					// disconnect twitter
					disconnectSocial(NETWORK_TWITTER);
				} else {
					Intent startTwitterFlow = new Intent(LoginActivity.this, TwitterLoginActivity.class);
					startActivity(startTwitterFlow);
				}
			}
		});

		googleConnect.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (sessionProvider.isGoogleConnected()) {
					// disconnect google
					disconnectSocial(NETWORK_GOOGLE);
				} else {
					AccountManager am = AccountManager.get(LoginActivity.this);
					final Account[] accounts = am.getAccountsByType("com.google");
					if (accounts.length < 1) {
						Toast.makeText(LoginActivity.this, "Please sign-in to Google account on the phone.", Toast.LENGTH_LONG).show();
						return;
					}

					FragmentManager fm = getSupportFragmentManager();
					GoogleAccountDialog accountSelectDialog = new GoogleAccountDialog();
					accountSelectDialog.show(fm, "account_picker");
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		isShowing = true;

		updateConnectPanel();

	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
		isShowing = false;
	}

	@Override
	protected void onStop() {
		super.onPause();
		if (pd != null)
			pd.cancel();
		// if (disconnectTask != null)
		// disconnectTask.cancel(true);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_AUTHENTICATE:
			if (resultCode == RESULT_OK) {
				fetchAuthToken(null);
			} else {
				Log.w(TAG, "user cancelled auth");
				Toast.makeText(this, "Sign in cancelled", Toast.LENGTH_LONG).show();
			}
			break;
		case Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE:
			uiHelper.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	private void updateConnectPanel() {
		boolean socialConnected = false;
		// fb
		fbIcon.setBackgroundResource(R.drawable.ic_fb_disconnected);
		fbStatus.setText("Not connected");
		if (sessionProvider.isFBConnected()) {
			socialConnected = true;
			fbIcon.setBackgroundResource(R.drawable.ic_fb_connected);
			fbStatus.setText(sessionProvider.getParam(UserSessionProvider.PREFS_KEY_FACEBOOK_NAME));
			fbConnect.setBackgroundResource(R.drawable.bg_login_s);
		} else {
			fbConnect.setBackgroundResource(0);
		}

		// twitter
		twitterIcon.setBackgroundResource(R.drawable.ic_twitter_disconnected);
		twitterStatus.setText("Not connected");
		if (sessionProvider.isTwitterConnected()) {
			socialConnected = true;
			twitterIcon.setBackgroundResource(R.drawable.ic_twitter_connected);
			twitterStatus.setText(sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_NAME));
			twitterConnect.setBackgroundResource(R.drawable.bg_login_s);
		} else {
			twitterConnect.setBackgroundResource(0);
		}

		// google
		googleIcon.setBackgroundResource(R.drawable.ic_google_disconnected);
		googleStatus.setText("Not connected");
		if (sessionProvider.isGoogleConnected()) {
			socialConnected = true;
			googleIcon.setBackgroundResource(R.drawable.ic_google_connected);
			googleStatus.setText(sessionProvider.getParam(UserSessionProvider.PREFS_KEY_GOOGLE_NAME));
			googleConnect.setBackgroundResource(R.drawable.bg_login_s);
		} else {
			googleConnect.setBackgroundResource(0);
		}

		if (socialConnected)
			nextButton.setText(" Continue ");
		else
			nextButton.setText(" Guest login ");
	}

	// Google stuff
	public void onAccountSelect(Account account) {
		this.selectedAccount = account;
		fetchAuthToken(null);
		Toast.makeText(this, "Authenticating using " + account.name, Toast.LENGTH_LONG).show();
	}

	private void fetchAuthToken(String curToken) {
		Bundle options = new Bundle();
		AccountManager am = AccountManager.get(this);
		if (curToken != null) {
			Log.d(TAG, "invalidating first token");
			am.invalidateAuthToken("com.google", curToken);
		}
		am.getAuthToken(selectedAccount, AUTH_TOKEN_TYPE, options, this, new OnTokenAcquired(), null);
	}

	private class OnTokenAcquired implements AccountManagerCallback<Bundle> {
		public void run(AccountManagerFuture<Bundle> result) {
			// Get the result of the operation from the AccountManagerFuture.
			try {
				Bundle bundle = result.getResult();
				Intent launch = (Intent) bundle.get(AccountManager.KEY_INTENT);
				if (launch != null) {
					launch.setFlags(launch.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivityForResult(launch, REQUEST_AUTHENTICATE);
					return;
				}

				String token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
				Log.i(TAG, "got token! - " + token);
				if (firstToken) {
					firstToken = false;
					fetchAuthToken(token);
					return;
				}
				// now fetch the profile
				makeGoogleRequest(token);
			} catch (OperationCanceledException e) {
				Log.w(TAG, "user cancelled auth", e);
				Toast.makeText(LoginActivity.this, "Sign in cancelled", Toast.LENGTH_LONG).show();
			} catch (AuthenticatorException e) {
				Log.w(TAG, "AuthenticatorException", e);
				Toast.makeText(LoginActivity.this, "Problem with Google, please try again.", Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				Log.w(TAG, "IOE", e);
				Toast.makeText(LoginActivity.this, "Please check internet connectivity and try again.", Toast.LENGTH_LONG).show();
			}
		}
	}

	private void makeGoogleRequest(final String token) {
		String url = GOOGLE_PROFILE_FETCH_URL + token;
		JsonObjectRequest loginRequest = new JsonObjectRequest(url, null, new Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				pd.dismiss();
				Log.d(TAG, "got response:\n" + response.toString());
				// parse response
				GoogleProfileFetchResponse resp = new Gson().fromJson(response.toString(), GoogleProfileFetchResponse.class);
				if (resp != null) {
					sessionProvider.putParam(UserSessionProvider.PREFS_KEY_GOOGLE_ID, resp.id);
					sessionProvider.putParam(UserSessionProvider.PREFS_KEY_GOOGLE_NAME, resp.name);
					sessionProvider.putParam(UserSessionProvider.PREFS_KEY_GOOGLE_TOKEN, token);
					sessionProvider.setUserName(resp.name);
					sessionProvider.setEmail(resp.email);
					updateConnectPanel();
				} else {
					Toast.makeText(LoginActivity.this, "Error fetching Google profile", Toast.LENGTH_LONG).show();
				}
			}
		}, new com.android.volley.Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pd.dismiss();
				Log.e(TAG, "error:" + error.toString());
				String message = "Error fetching Google profile.";
				Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
			}
		});
		loginRequest.setShouldCache(false);
		AAPApplication.get().getRequestQueue().add(loginRequest).setTag(this);
		pd = ProgressDialog.show(this, "Connecting to Google...", "Please wait while we connect to your Google account");
	}

	static class GoogleProfileFetchResponse {
		public String	id;
		public String	name;
		public String	email;
	}

	// FB stuff
	// Fetch FB profile data
	private void makeMeRequest(final Session session) {
		// Make an API call to get user data and define a
		// new callback to handle the response.
		fbRequest = Request.newMeRequest(session, new Request.GraphUserCallback() {
			public void onCompleted(GraphUser user, Response response) {
				if (pd != null)
					pd.cancel();
				if (!isShowing)// dont bother
					return;
				// If the response is successful
				if (session == Session.getActiveSession()) {
					if (user != null) {
						// if (loginTask != null)
						// loginTask.cancel(true);
						// loginTask = new LoginTask(user.getId(), user.getUsername(), session.getAccessToken());
						// loginTask.execute(new String[] {});
						// sessionProvider.setUserId(user.getId());
						Log.d(TAG, "name: " + user.getName() + ", email" + user.getProperty("email"));
						sessionProvider.setUserName(user.getName());
						// sessionProvider.setProfilePicUrl(profilePicUrl);
						sessionProvider.setEmail("" + user.getProperty("email"));
						sessionProvider.putParam(UserSessionProvider.PREFS_KEY_FACEBOOK_UID, user.getId());
						sessionProvider.putParam(UserSessionProvider.PREFS_KEY_FACEBOOK_NAME, user.getName());

						updateConnectPanel();
						return;
					}
				}
				if (response.getError() != null) {
					// Handle errors, will do so later.
					Log.w(TAG, "FB error:" + response.getError().getErrorMessage());
				}
				// session.closeAndClearTokenInformation();
				Toast.makeText(LoginActivity.this, "Error occured connecting to Facebook", Toast.LENGTH_LONG).show();
			}
		});
		Bundle params = fbRequest.getParameters();
		params.putString("fields", "email,name");
		fbRequest.setParameters(params);
		if (pd != null)
			pd.cancel();
		pd = ProgressDialog.show(LoginActivity.this, "Connecting to FB...", "Please wait while we connect to your Facebook account");
		fbRequest.executeAsync();
	}

	private void disconnectSocial(final int network) {
		String networkName = "";
		switch (network) {
		case NETWORK_FB:
			networkName = "Facebook";
			break;
		case NETWORK_TWITTER:
			networkName = "Twitter";
			break;
		case NETWORK_GOOGLE:
			networkName = "Google";
			break;
		}
		String isFinalWarn = "";
		if (isLastNetwork())
			isFinalWarn = " This is the last connected social network. ";
		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
		builder.setTitle("Disconnect " + networkName).setMessage("Are you sure you want to disconnect " + networkName + "?" + isFinalWarn)
				.setPositiveButton("Disconnect", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
						switch (network) {
						case NETWORK_FB:
							sessionProvider.disconnectFB();
							break;
						case NETWORK_TWITTER:
							sessionProvider.disconnectTwitter();
							break;
						case NETWORK_GOOGLE:
							sessionProvider.disconnectGoogle();
							break;
						}
						updateConnectPanel();
					}
				}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}

	private boolean isLastNetwork() {
		int count = 0;
		if (sessionProvider.isFBConnected())
			count++;
		if (sessionProvider.isTwitterConnected())
			count++;
		if (sessionProvider.isGoogleConnected())
			count++;

		return count == 1;
	}
}
