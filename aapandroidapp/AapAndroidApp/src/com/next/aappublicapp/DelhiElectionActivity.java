package com.next.aappublicapp;

import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.next.aappublicapp.ui.KnowCandidateFragment;
import com.next.aappublicapp.ui.ManifestoFragment;
import com.next.aappublicapp.ui.SelectionProcessFragment;

public class DelhiElectionActivity extends ActionBarActivity {
	private static final String		TAG	= "DelhiElectionActivity";

	private DrawerLayout			mDrawerLayout;
	private ListView				mDrawerList;
	private ActionBarDrawerToggle	mDrawerToggle;
	FrameLayout						container;
	Button							selectionProcess, candidateInfo, manifestos, support;

	Fragment						selectionProcessFragment;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_delhielection);
		container = (FrameLayout) findViewById(R.id.content_frame);
		selectionProcess = (Button) findViewById(R.id.btn_selection);
		candidateInfo = (Button) findViewById(R.id.btn_candidate);
		manifestos = (Button) findViewById(R.id.btn_manifesto);
		support = (Button) findViewById(R.id.btn_support);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onStart() {
		super.onStart();

		selectionProcess.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment newFragment = new SelectionProcessFragment();
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				ft.add(R.id.content_frame, newFragment).addToBackStack(null).commit();
			}
		});

		candidateInfo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment newFragment = new KnowCandidateFragment();
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				ft.add(R.id.content_frame, newFragment).addToBackStack(null).commit();
			}
		});

		manifestos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment newFragment = new ManifestoFragment();
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				ft.add(R.id.content_frame, newFragment).addToBackStack(null).commit();
			}
		});
	}
}
