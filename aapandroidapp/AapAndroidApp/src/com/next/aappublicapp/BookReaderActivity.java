package com.next.aappublicapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.Toast;

import com.next.aappublicapp.ui.NavigationDrawerHelper;
import com.next.aappublicapp.util.Config;

public class BookReaderActivity extends ActionBarActivity {
	private static final String		TAG							= "BookReaderActivity";
	public static final String		INTENT_EXTRA_BOOK_SWARAJ	= "INTENT_EXTRA_BOOK_SWARAJ";
	public static final String		INTENT_EXTRA_BOOK_LOKPAL	= "INTENT_EXTRA_BOOK_LOKPAL";

	private DrawerLayout			mDrawerLayout;
	private ListView				mDrawerList;
	private ActionBarDrawerToggle	mDrawerToggle;
	private WebView					webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookreader);
		webView = (WebView) findViewById(R.id.webView);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, 0, 0) {
		//
		// /** Called when a drawer has settled in a completely closed state. */
		// public void onDrawerClosed(View view) {
		// // getActionBar().setTitle(mTitle);
		// // invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
		// }
		//
		// /** Called when a drawer has settled in a completely open state. */
		// public void onDrawerOpened(View drawerView) {
		// // getActionBar().setTitle(mDrawerTitle);
		// // invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
		// }
		// };
		// Set the drawer toggle as the DrawerListener
		// mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	protected void onStart() {
		super.onStart();
		webView.setWebViewClient(new MyWebViewClient());
		webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		WebSettings s = webView.getSettings();
		s.setBuiltInZoomControls(true);
		// s.setDisplayZoomControls(false);
		s.setLoadWithOverviewMode(false);
		s.setSavePassword(false);
		s.setSaveFormData(false);
		s.setJavaScriptEnabled(true);

		webView.setInitialScale(110);
		s.setGeolocationEnabled(false);
		s.setDomStorageEnabled(false);

		// load the book
		loadBook();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		// load the book
		loadBook();
	}

	private void loadBook() {
		if (getIntent().getBooleanExtra(INTENT_EXTRA_BOOK_SWARAJ, false)) {
			webView.loadUrl("file:///android_asset/books/swaraj_eng.htm");
			setTitle("Swaraj Ebook");
			NavigationDrawerHelper.initializeDrawer(this, mDrawerList, NavigationDrawerHelper.OPTION_SWARAJ);
		} else {
			webView.loadUrl("file:///android_asset/books/janlokpal.htm");
			setTitle("Janlokpal Ebook");
			NavigationDrawerHelper.initializeDrawer(this, mDrawerList, NavigationDrawerHelper.OPTION_JANLOKPAL);
		}
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (Config.LOG_DEBUG)
				Log.d(TAG, " in shouldOverrideUrlLoading");

			return true; // never let the webview load the url
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Toast.makeText(BookReaderActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onLoadResource(WebView view, String url) {
			super.onLoadResource(view, url);
			// if (Config.DEBUG)
			// Log.d(TAG, "loading resource-" + url);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			Log.d(TAG, "onPageFinished - " + url);
		}

		@Override
		public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
			super.doUpdateVisitedHistory(view, url, isReload);
			Log.d(TAG, "doUpdateVisitedHistory - " + url);
		}
	}
}
