package com.next.aappublicapp;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.next.aappublicapp.util.Config;
import com.next.aappublicapp.util.ServerConfig;

public class RegistrationActivity extends ActionBarActivity {
	private static final String	TAG									= "RegistrationActivity";
	private final static int	PLAY_SERVICES_RESOLUTION_REQUEST	= 9000;

	private EditText			name, email, phone, dob;
	private Button				registerButton;
	private RadioGroup			genderEdit;
	private TextView			livingStatesTV, livingDistrictsTV, livingACsTV;
	private TextView			voterStatesTV, voterDistrictsTV, voterACsTV;

	private ProgressDialog		pd									= null;
	private UserSessionProvider	sessionProvider;

	private StateModel[]		states								= null;
	private String[]			stateNames							= null;
	private DistrictModel[]		districts							= null;
	private String[]			districtNames						= null;
	private ACModel[]			acs									= null;
	private String[]			acNames								= null;
	int							livingSelectedState					= -1;
	int							livingSelectedDistrict				= -1;
	int							livingSelectedAC					= -1;

	// voter location
	private DistrictModel[]		voterDistricts						= null;
	private String[]			voterDistrictNames					= null;
	private ACModel[]			voterAcs							= null;
	private String[]			voterAcNames						= null;
	int							voterSelectedState					= -1;
	int							voterSelectedDistrict				= -1;
	int							voterSelectedAC						= -1;

	// push notification
	GoogleCloudMessaging		gcm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sessionProvider = UserSessionProvider.getInstance(this);
		setContentView(R.layout.activity_registration);
		name = (EditText) findViewById(R.id.nameEdit);
		email = (EditText) findViewById(R.id.emailEdit);
		genderEdit = (RadioGroup) findViewById(R.id.genderEdit);
		phone = (EditText) findViewById(R.id.phoneEdit);
		dob = (EditText) findViewById(R.id.birthdayEdit);
		dob.setFocusable(false);
		dob.setFocusableInTouchMode(false);
		livingStatesTV = (TextView) findViewById(R.id.livingStateEdit);
		livingDistrictsTV = (TextView) findViewById(R.id.livingDistrictEdit);
		livingACsTV = (TextView) findViewById(R.id.livingConstituencyEdit);
		voterStatesTV = (TextView) findViewById(R.id.voterStateEdit);
		voterDistrictsTV = (TextView) findViewById(R.id.voterDistrictEdit);
		voterACsTV = (TextView) findViewById(R.id.voterConstituencyEdit);

		registerButton = (Button) findViewById(R.id.registerButton);
		registerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// get GCM stuff
				String gcmId = sessionProvider.getGcmId();
				if (gcmId == null) {
					if (!checkPlayServices()) {
						Log.w(TAG, "no GCM!");
						doRegister(null);
					} else {
						getGcmId();
					}
				} else {
					doRegister(gcmId);
				}
			}
		});

		// living location
		livingStatesTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (states == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, stateNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select State")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								livingSelectedState = which;
								livingStatesTV.setText(states[livingSelectedState].name);
								// begin loading districts
								districts = null;
								livingSelectedDistrict = -1;
								acs = null;
								livingSelectedAC = -1;
								livingACsTV.setText("");
								if (states[livingSelectedState].districtDataAvailable) {
									populateDistrictData(livingDistrictsTV, true);
								} else {
									livingDistrictsTV.setText("N/A");
								}
								dialog.dismiss();
							}
						}).create().show();
			}
		});

		livingDistrictsTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (districts == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, districtNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select District")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								livingSelectedDistrict = which;
								livingDistrictsTV.setText(districts[livingSelectedDistrict].name);
								// begin loading ACs
								acs = null;
								livingSelectedAC = -1;
								if (districts[livingSelectedDistrict].acDataAvailable) {
									populateACData(livingACsTV, true);
								} else {
									livingACsTV.setText("N/A");
								}
								dialog.dismiss();
							}
						}).create().show();
			}
		});

		livingACsTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (acs == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, acNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select Constituency")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								livingSelectedAC = which;
								livingACsTV.setText(acs[livingSelectedAC].name);
								dialog.dismiss();
							}
						}).create().show();
			}
		});

		// voter location
		voterStatesTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (states == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, stateNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select State")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								voterSelectedState = which;
								voterStatesTV.setText(states[voterSelectedState].name);
								// begin loading districts
								voterDistricts = null;
								voterSelectedDistrict = -1;
								voterAcs = null;
								voterSelectedAC = -1;
								voterACsTV.setText("");
								if (states[voterSelectedState].districtDataAvailable) {
									populateDistrictData(voterDistrictsTV, false);
								} else {
									voterDistrictsTV.setText("N/A");
								}
								dialog.dismiss();
							}
						}).create().show();
			}
		});

		voterDistrictsTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (voterDistricts == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, voterDistrictNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select District")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								voterSelectedDistrict = which;
								voterDistrictsTV.setText(voterDistricts[voterSelectedDistrict].name);
								// begin loading ACs
								voterAcs = null;
								voterSelectedAC = -1;
								if (voterDistricts[voterSelectedDistrict].acDataAvailable) {
									populateACData(voterACsTV, false);
								} else {
									voterACsTV.setText("N/A");
								}
								dialog.dismiss();
							}
						}).create().show();
			}
		});

		voterACsTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (voterAcs == null)
					return;
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistrationActivity.this,
						android.R.layout.simple_spinner_dropdown_item, voterAcNames);
				new AlertDialog.Builder(RegistrationActivity.this).setTitle("Select Constituency")
						.setAdapter(adapter, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								voterSelectedAC = which;
								voterACsTV.setText(voterAcs[voterSelectedAC].name);
								dialog.dismiss();
							}
						}).create().show();
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		// pre-fill name and email
		if (sessionProvider.isFBConnected()) {
			name.setText(sessionProvider.getUserName());
			// email.setText(sessionProvider.getEmail());
		} else if (sessionProvider.isGoogleConnected()) {
			name.setText(sessionProvider.getUserName());
			email.setText(sessionProvider.getEmail());
		} else if (sessionProvider.isTwitterConnected()) {
			// if twitter we only have name
			name.setText(sessionProvider.getUserName());
		}
		// fetch states
		if (states == null) {
			String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_STATES_URL;
			if (ServerConfig.USE_TEMP_URL)
				url = "https://dl.dropboxusercontent.com/u/5474079/aap_requests/states.txt";
			JsonArrayRequest getAllStatesRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
				@Override
				public void onResponse(JSONArray response) {
					Type type = new TypeToken<StateModel[]>() {
					}.getType();
					states = new Gson().fromJson(response.toString(), type);
					stateNames = new String[states.length];
					for (int i = 0; i < states.length; i++)
						stateNames[i] = states[i].name;
					livingStatesTV.setText("Select state");
					voterStatesTV.setText("Select state");
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w(TAG, "error:" + error);
					Toast.makeText(RegistrationActivity.this, "Error fetching states", Toast.LENGTH_LONG).show();
					livingStatesTV.setText("N/A");
					voterStatesTV.setText("N/A");
				}
			});
			livingStatesTV.setText("Loading states...");
			voterStatesTV.setText("Loading states...");
			AAPApplication.get().getRequestQueue().add(getAllStatesRequest).setTag(this);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (pd != null)
			pd.cancel();
		AAPApplication.get().getRequestQueue().cancelAll(this);
	}

	private void doRegister(String gcmId) {
		try {
			int livingStateId, livingDistrictId, livingAcId;
			int voterStateId, voterDistrictId, voterAcId;
			livingStateId = livingDistrictId = livingAcId = -1;
			voterStateId = voterDistrictId = voterAcId = -1;
			if (livingSelectedState >= 0 && states.length > livingSelectedState)
				livingStateId = states[livingSelectedState].id;
			if (voterSelectedState >= 0 && states.length > voterSelectedState)
				voterStateId = states[voterSelectedState].id;
			if (livingSelectedDistrict >= 0 && districts.length > livingSelectedDistrict)
				livingDistrictId = districts[livingSelectedDistrict].id;
			if (livingSelectedAC >= 0 && acs.length > livingSelectedAC)
				livingAcId = acs[livingSelectedAC].id;
			if (voterSelectedDistrict >= 0 && voterDistricts.length > voterSelectedDistrict)
				voterDistrictId = voterDistricts[voterSelectedDistrict].id;
			if (voterSelectedAC >= 0 && voterAcs.length > voterSelectedAC)
				voterAcId = voterAcs[voterSelectedAC].id;

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("name", name.getText().toString());
			jsonRequest.put("email", email.getText().toString());
			RadioButton rb = (RadioButton) findViewById(genderEdit.getCheckedRadioButtonId());
			if (rb != null)
				jsonRequest.put("gender", rb.getText().toString());
			jsonRequest.put("birthday", dob.getText().toString());
			jsonRequest.put("mobile", phone.getText().toString());
			jsonRequest.put("livingStateId", livingStateId);
			jsonRequest.put("livingDistrictId", livingDistrictId);
			jsonRequest.put("livingAcId", livingAcId);
			jsonRequest.put("votingStateId", voterStateId);
			jsonRequest.put("votingDistrictId", voterDistrictId);
			jsonRequest.put("votingAcId", voterAcId);
			jsonRequest.put("deviceRegId", gcmId);

			String url;
			if (sessionProvider.isFBConnected()) {
				url = ServerConfig.SERVER_ENDPOINT + ServerConfig.POST_FB_REGISTER_URL;
				jsonRequest.put("userName", sessionProvider.getParam(UserSessionProvider.PREFS_KEY_FACEBOOK_UID));
			} else if (sessionProvider.isTwitterConnected()) {
				url = ServerConfig.SERVER_ENDPOINT + ServerConfig.POST_TWITTER_REGISTER_URL;
				jsonRequest.put("handle", sessionProvider.getParam(UserSessionProvider.PREFS_KEY_TWITTER_ID));
			} else {
				url = ServerConfig.SERVER_ENDPOINT + ServerConfig.POST_TWITTER_REGISTER_URL;
			}
			if (ServerConfig.USE_TEMP_URL)
				url = "http://postcatcher.in/catchers/5272ffe150cf3302000006f4";
			JsonObjectRequest registerRequest = new JsonObjectRequest(Method.POST, url, jsonRequest, new Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					Log.d(TAG, "got response:\n" + response.toString());
					// validate response
					pd.cancel();
					Toast.makeText(RegistrationActivity.this, "Registered!", Toast.LENGTH_LONG).show();
					sessionProvider.setUserRegistered(true);
					finish();
					Intent homeActivityIntent = new Intent(RegistrationActivity.this, HomeActivity.class);
					startActivity(homeActivityIntent);
				}
			}, new ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					Log.w(TAG, "error:" + error);
					pd.cancel();
					Toast.makeText(RegistrationActivity.this, "Error registering...", Toast.LENGTH_LONG).show();
					finish();
					Intent homeActivityIntent = new Intent(RegistrationActivity.this, HomeActivity.class);
					startActivity(homeActivityIntent);
				}
			});
			pd = ProgressDialog.show(RegistrationActivity.this, "Please wait", "Saving your details");
			registerRequest.setShouldCache(false);
			AAPApplication.get().getRequestQueue().add(registerRequest).setTag(this);
		} catch (Exception e) {
			Log.e(TAG, "error processing register", e);
			Toast.makeText(RegistrationActivity.this, "Error registering...", Toast.LENGTH_LONG).show();
			finish();
			Intent homeActivityIntent = new Intent(RegistrationActivity.this, HomeActivity.class);
			startActivity(homeActivityIntent);
		}
	}

	private void getGcmId() {
		new AsyncTask<Void, Void, String>() {
			String	regId	= null;

			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(RegistrationActivity.this);
					}
					regId = gcm.register(Config.GCM_SENDER_ID);
					msg = "Device registered, registration ID=" + regId;
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Log.d(TAG, "GCM:" + regId);
				sessionProvider.setGcmId(regId);
				doRegister(regId);
			}
		}.execute(null, null, null);
	}

	// living location UI stuff

	private void populateDistrictData(final TextView spinnerView, final boolean isLivingData) {
		// fetch districts
		String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_DISTRICTS_URL + states[livingSelectedState].id;
		if (ServerConfig.USE_TEMP_URL)
			url = "https://dl.dropboxusercontent.com/u/5474079/aap_requests/districts.txt";
		JsonArrayRequest getDistrictsRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Type type = new TypeToken<DistrictModel[]>() {
				}.getType();
				if (isLivingData) {
					districts = new Gson().fromJson(response.toString(), type);
					districtNames = new String[districts.length];
					for (int i = 0; i < districts.length; i++)
						districtNames[i] = districts[i].name;
				} else {
					voterDistricts = new Gson().fromJson(response.toString(), type);
					voterDistrictNames = new String[voterDistricts.length];
					for (int i = 0; i < voterDistricts.length; i++)
						voterDistrictNames[i] = voterDistricts[i].name;
				}
				spinnerView.setText("Select district");
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.w(TAG, "error:" + error);
				Toast.makeText(RegistrationActivity.this, "Error fetching districts", Toast.LENGTH_LONG).show();
				spinnerView.setText("N/A");
			}
		});
		spinnerView.setText("Loading districts...");
		AAPApplication.get().getRequestQueue().add(getDistrictsRequest).setTag(this);
	}

	private void populateACData(final TextView spinnerView, final boolean isLivingData) {
		// fetch districts
		String url = ServerConfig.SERVER_ENDPOINT + ServerConfig.GET_AC_URL + districts[livingSelectedDistrict].id;
		if (ServerConfig.USE_TEMP_URL)
			url = "https://dl.dropboxusercontent.com/u/5474079/aap_requests/acs.txt";
		JsonArrayRequest getACRequest = new JsonArrayRequest(url, new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				Type type = new TypeToken<ACModel[]>() {
				}.getType();

				if (isLivingData) {
					acs = new Gson().fromJson(response.toString(), type);
					acNames = new String[acs.length];
					for (int i = 0; i < acs.length; i++)
						acNames[i] = acs[i].name;
				} else {
					voterAcs = new Gson().fromJson(response.toString(), type);
					voterAcNames = new String[voterAcs.length];
					for (int i = 0; i < voterAcs.length; i++)
						voterAcNames[i] = voterAcs[i].name;
				}
				spinnerView.setText("Select Constituency");
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.w(TAG, "error:" + error);
				Toast.makeText(RegistrationActivity.this, "Error fetching assembly constituencies", Toast.LENGTH_LONG).show();
				spinnerView.setText("N/A");
			}
		});
		spinnerView.setText("Loading Constituencies...");
		AAPApplication.get().getRequestQueue().add(getACRequest).setTag(this);
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	// date picker & gender

	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	public void onRadioButtonClicked(View v) {
		// switch (v.getId()) {
		// case R.id.gender_male:
		//
		// default:
		// break;
		// }
	}

	public void onDateSet(int year, int month, int day) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, day);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		dob.setText(sdf.format(c.getTime()));
	}

	public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onStop() {
			super.onStop();
			dismiss();
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			((RegistrationActivity) getActivity()).onDateSet(year, month, day);
		}
	}

	class StateModel {
		public int		id;
		public String	name;
		public boolean	districtDataAvailable;
	}

	class DistrictModel {
		public int		id;
		public String	name;
		public boolean	acDataAvailable;
	}

	class ACModel {
		public int		id;
		public String	name;
	}
}
