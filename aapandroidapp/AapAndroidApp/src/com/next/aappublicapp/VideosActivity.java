package com.next.aappublicapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.next.aappublicapp.content.ContentManager;
import com.next.aappublicapp.content.models.HomeItemsResponse;
import com.next.aappublicapp.ui.NavigationDrawerHelper;
import com.next.aappublicapp.ui.VideoItemFragment;

public class VideosActivity extends ActionBarActivity {
	private static final String	TAG						= "NewsActivity";

	public static final String	INTENT_EXTRA_ITEM_ID	= "INTENT_EXTRA_ITEM_ID";

	private ContentManager		contentManager;
	private ListView			mDrawerList;

	private ViewPager			viewPager;
	HomeItemsResponse			allItems;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		contentManager = ContentManager.getInstance(this);
		setContentView(R.layout.activity_news);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		viewPager = (ViewPager) findViewById(R.id.view_pager);
		NavigationDrawerHelper.initializeDrawer(this, mDrawerList, NavigationDrawerHelper.OPTION_NONE);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}

	@Override
	protected void onStart() {
		super.onStart();
		int id = getIntent().getIntExtra(INTENT_EXTRA_ITEM_ID, -1);
		allItems = contentManager.getLatestResponse();
		if (allItems == null) {
			Toast.makeText(this, "Error fetching content", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		for (int i = 0; i < allItems.videoItems.length; i++) {
			if (allItems.videoItems[i].id == id) {
				viewPager.setAdapter(new VideosAdapter(getSupportFragmentManager()));
				return;
			}
		}
		// havent found the content user looking for?
		Toast.makeText(this, "Oops, couldn't find the item...", Toast.LENGTH_LONG).show();
		finish();
		return;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	class VideosAdapter extends FragmentStatePagerAdapter {
		public VideosAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return VideoItemFragment.newInstance(allItems.videoItems[position].id);
		}

		@Override
		public int getCount() {
			return allItems.videoItems.length;
		}

	}
}